# Configuraciones de la aplicación

Esta sección está dedicada a la lista de configuraciones que influyen en el comportamiento de las aplicaciones WinCE y multiplataforma.


## LOGIN

### **displayStores** ###

Tipo: array (default: []) | **1.27.1-rc.0** (``task-type-definitions``)

Si está configurado, el tipo de tarea solo está disponible para las tiendas seleccionadas.



!!! note "Ejemplo"
    ```json
    {
      "displayStores": [
        "1",
        "2"
      ]
    }
    ```


### **allow-change-password**

Tipo: boolean (default: false) | **1.13.0-rc.1** (``application-setting``)

Define si es posible cambiar la contraseña en la aplicación.

!!! note "Ejemplo"
    ```json
    {
        "allow-change-password": true
    }
    ```

![image alt <>](assets/change_pass.png){:height="300px" width="300px"}

### **allow-download-working-task**

Tipo: ``boolean`` (default: ``{"active": false}) | **1.37.0-rc.0**

Configuración en el nivel de la aplicación Instore.

Define si está permitido descargar tareas en el estado W.

!!! note "Ejemplo"
    ```json
    {
        "allow-download-working-task": {"active": false}
    }
    ```

### **Currency**

Tipo: ``string`` | (``application-setting``)

Define la moneda por cliente.

!!! note "Ejemplo"
    ```json
    {
        "currency": "€"
    }
    ```

### **default-language**

Tipo: ``string`` | (``application-setting``)

Define el idioma predeterminado por cliente

!!! note "Ejemplo"
    ```json
    {
        "default-language": "pt-PT"
    }
    ```

### **default-role**

Tipo: ``string`` | (``application-setting``)

Establece el nivel de acceso predeterminado al crear un usuario

!!! note "Ejemplo"
    ```json
    {
        "default-role": "DEFAULT"
    }
    ```

### **get-tasks-options**

Tipo: ``object`` | **1.4.0-rc.0** (``application-setting``)

Define posibles opciones a la hora de obtener tareas (todos los campos son opcionales)

???+ example "Valores posibles:"
    * ``status``: ``string []`` - Define todos los estados de las tareas a obtener. Valores posibles:
        * A
        * AW
        * W
        * P 
    * ``fromScheduledStart``: ``string`` - Solo devuelve tareas con scheduleStart igual o mayor que la fecha definida - formato ISO estándar.
    * ``toScheduledStart``: ``string`` - Solo devuelve tareas con scheduleStart igual o menor que la fecha definida - formato ISO estándar.
    * ``offset``: ``number`` - Define el número de tareas que se ignorarán (ejemplo: de un conjunto de 20 tareas, si el desplazamiento es 5, solo se devolverán las tareas 6 a 20).
    * ``limit``: ``number`` - Número total de tareas que se devolverán.

    **Ejemplo**
    ```json
    {
        "get-tasks-options": {
          "status": ["A", "AW", "W"],
          "limit": 20,
          "offset": 5,
          "toScheduledStart": "5",
          "fromScheduledStart": "0"
        }
    }
    ```


### **show-damage-approval**

Tipo: ``object`` (default: "active": ``false``) | **1.37.0-rc.0**

Configuración en el nivel de la aplicación Instore.

Define si la aplicación presenta el formulario de Aprobación de daños en el menú.

???+ example "Valores posibles::"
    * ``active``: ``string`` - Define si la aplicación coincide con el formulario de Aprobación de daños en el menú. (por defecto: ``false``)
    * ``limit``: ``number`` - Define el valor de paginación que la aplicación coloca en la llamada de servicio GET. (por defecto: ``null``)

    **Ejemplo**
    ```json
    {
        "showDamageApproval": {
          "active": true,
          "limit": 20
        }
    }
    ```


## FLUJO DE CREACIÓN DE TAREAS

### **adhocTasks**  

!!! warning "Obsoleto"
    Utilice  [adhocTasksV2](#adhoctasksv2) en su lugar

   

Tipo: string (default: `never`) | **1.1.0-rc.9** (``task-type-definitions``)

Define si está permitido crear tareas ad-hoc del tipo de tarea correspondiente.

???+ example "Valores posibles::"

    * `always` - le permite crear tareas ad-hoc para el tipo de tarea sin restricciones.

    * `parent` - le permite crear tareas ad-hoc para el tipo de tarea si la tarea no tiene un padre.

    * `never` - no permite crear tareas ad-hoc para el tipo de tarea.

### **adhocTasksV2**

Tipo: `object` | **1.15.0-rc.0** (``task-type-definitions``)

Define si está permitido crear tareas adHoc, a qué nivel y de qué forma. Esta definición es un objeto que consta de un campo de nivel (define en qué nivel se puede crear la tarea) y un tipo de matriz (define cómo se puede crear la tarea).

???+ example "Ajustes"
    **Campos disponibles:**

    * ``level`` - "never"

    * ``types`` - ["WITHOUTLIST"]

    **Ejemplo**
    
    ```json

    {
      "level": "parent",
      "types": [
        "HS",
        "WITHOUTLIST",
        "WITHLIST"
      ]
    }
    ```
    
    **Valores disponíveis para o campo level:** 

    * `always` - le permite crear tareas adHoc para el tipo de tarea sin restricciones

    * `parent` - le permite crear tareas adHoc para el tipo de tarea si la tarea no tiene ascendencia


    * `never` - no permite crear tareas adHoc para el tipo de tarea


    **"Valores posibles para la matriz de tipos:**

    * `HS` - creación de tareas adHoc basadas en una estructura jerárquica

    * `WITHLIST` - creación de tareas adHoc basadas en una lista

    * `WITHOUTLIST` - creación de tareas adHoc sin lista

    * `CHECKLIST` - creación de tareas adHoc basadas en una lista de verificación
    
    

![image alt <>](assets/adhocTasksV2.png){:height="300px" width="300px"}

### **checkConnectionOnStart**

Tipo: `boolean` (default: `false`) | **1.0.0-rc.2** (``task-type-definitions``)

Comprueba la conexión al entrar en una tarea. Si está activo para un tipo específico de tarea, al ingresar una tarea sin red, se muestra una alerta indicando que no hay red.

!!! note "Ejemplo"
    ```json
    {
        "checkConnectionOnStart": true
    }
    ```


### **destinationSearchInput**

Tipo: `array` (default: []) | **1.27.0-rc.0** (``task-type-definitions``)

Define qué opciones de destino están disponibles al crear la tarea y qué entradas son posibles para cada destino.

???+ example "Valores posibles::"
    * ``store``: Opción que define el destino "Tienda"
    * ``warehouse``: Opción que define el destino "Entreposto"
    * ``supplier``: Opción que define el destino "Proveedor"
    
    **Ejemplo:**
    ```json
    "destinationSearchInput": [
			{
				"code": "store",
				"input": "text"
			}
		],
    ```

![image alt <>](assets/destinationSearchInput.png){:height="300px" width="300px"}

### **inventory-barcode-rules**

Tipo: ``object`` | **1.32.0-rc.0** (``application-setting``)

Permite gestionar el formato de los parámetros enviados en el chip de inventario.

???+ example "Campos posibles::"
    * `zoneIdRightDigits` - define la cantidad de dígitos que la aplicación debe eliminar del código de barras y colocar en la llamada de servicio de tareas. Los dígitos deben contarse de derecha a izquierda y no recortar los ceros de la izquierda.

!!! note "Ejemplo"
    ```json
        {
          "inventory-barcode-rules": 
          {
            "zoneIdRightDigits": "3"
          }
        }
    ```

### **preventReplenishingWhen**

Tipo: `object` | **1.3.0-rc.3** (``task-type-definitions``)

Impedir la creación de tareas (separación) bajo condiciones.

"obligatorio": stringcon la condición obligatoria para permitir la creación de la tarea

???+ example "Valores posibles::"
    * `HIERARCHY_CATEGOR` - requiere la existencia de una estructura jerárquica

!!! note "Ejemplo"
    ```json
    {
      "preventReplenishingWhen": 
      {
        "mandatory": "hierarchy_category"
      }
    }
    ```



### **adHocTasksDefaultName**

!!! warning "Obsoleto" 
    Utilice  [adHocTasksDefaultNameV2](#adhoctasksdefaultnamev2) en su lugar

Define si el nombre de la tarea se creará o no de forma predeterminada en la creación de tareas adHoc. Si el valor de esta configuración es falso, el usuario tiene la posibilidad de definir el nombre de la tarea.


### **adHocTasksDefaultNameV2**


Tipo: `boolean` (default: ``true``) | **1.27.0-rc.0** (``task-type-definitions``)


Define si el nombre de la tarea se creará o no de forma predeterminada en la creación de tareas adHoc. Si el valor del campo activo es falso, el usuario tiene la posibilidad de definir el nombre de la tarea por tipo de creación.

!!! note "Ejemplo"
    ```json
    {
      "adHocTasksDefaultNameV2": {
        "active": false,
        "hsExpression": "%hsName% (%hsId%) | %user%",
        "withListExpression": "%withListName% | %user%",
        "withoutListExpression": "%datetime% | %date% | %user%",
        "checklistExpression": "%checklistName% | %user%"
      }
    }
    ```
![image alt <>](assets/default_name.png){:height="300px" width="300px"}

### **handleMode**

Tipo: ``string`` | **1.29.0-rc.0** (``task-type-definitions``)


Comprueba cómo se gestiona la tarea.

???+ example "Valores posibles::"
    * ``normal``:  Define el tratamiento del producto del tipo de tarea REPLENISHPREP (predeterminado)
    * ``styled``: Define el comportamiento de manejo de la tarea de moda (comportamiento existente para el tipo de tarea REPLENISHPREP)
    * ``simpler``: (nuevo campo) Define la versión simplificada en el manejo del tipo de tarea Recibiendo para el cliente Marisa.

!!! note "Ejemplo"
    ```json
    {
      "handleMode": "styled"
    }
    ```
![image alt <>](assets/handleMode.png){:height="300px" width="300px"}

### **show-labelprint-on-summary**

Tipo: ``object`` | **1.14.0-rc.0** (``application-setting``)

Define si el tipo de tarea LABELPREINT está visible en el menú de la aplicación o en el resumen de la tarea. Si es verdadero y las tareas están disponibles, el tipo de tarea LABELPRINT debe estar disponible en el resumen de la tarea. Si es falso, debe estar disponible en el menú "Más".

!!! note "Ejemplo"
    ```json
    {
      "active": false
    }
    ```
![image alt <>](assets/show-labelprint-on-summury.png){:height="300px" width="300px"}

### **taskDestination**

Tipo: ``object`` (default: { ``"active": false`` } ) | **1.29.0-rc.0** (``task-type-definitions``)

Permite al usuario definir el destino de la tarea.

!!! note "Ejemplo"
    ```json
    {
      "active": true,
      "code": "standard"
    }
    ```

![image alt <>](assets/taskLocationPrint.png){:height="300px" width="300px"}


### **taskLocation**

!!! warning "Obsoleto"
    Utilice [taskLocationV2](#tasklocationv2) en su lugar

Tipo: ``object`` (default: {} ) | **1.20.0-rc.0** (``task-type-definitions``)

Le permite definir y mostrar la ubicación de la tarea.

???+ example "Valores posibles::"
    * ``active`` (boolean) - activa la función para definir la ubicación
    * ``display`` (boolean) - si la ubicación está definida para la tarea, permite su consulta
    * ``options`` (array) - las ubicaciones que puede elegir para la tarea

    **Ejemplo**

    ```json
    {
      "taskLocation": {
        "active": true,
        "display": true,
        "options": [
          "store",
          "warehouse"
        ]
      }
    }
    ```


### **taskLocationV2**

Tipo: ``object`` (default: {} ) | **1.33.0-rc.0** (``task-type-definitions``)

Le permite definir y mostrar la ubicación de la tarea.

???+ example "Valores posibles::"
    * ``active`` (boolean) - activa la función para definir la ubicación
    * ``display`` (boolean) - si la ubicación está definida para la tarea, permite su consulta
    * ``options`` (array) - las posibles ubicaciones a elegir para la tarea. Es posible definir el Id y el nombre de cada ubicación. 

    **Ejemplo**
    ```json
    {
      "taskLocationV2": {
	      "active": true,
	      "display": true,
	      "options": [
		        {
		            "id": "store",
			          "description": "Loja"
		        },
		        {
			          "id": "warehouse",
			          "description": "Armazem"
		        }
		      ]
	    }
    }
    ```

![image alt <>](assets/taskLocationV2.png){:height="300px" width="300px"}


### **tasks-query-status**

Tipo: ``string[]`` | (``application-setting``)

Define el estado de las tareas disponibles para su visualización en la aplicación.

!!! note "Ejemplo"
    ```json
    [
        "A",
        "AW",
        "W",
        "P",
        "F
    ]
    ```


### **taskSearchInput**

Tipo: ``array`` | **1.25.0-rc.1** (``task-type-definitions``)

Permite definir qué entradas son posibles en la búsqueda avanzada de tareas.

???+ example "Valores posibles::"
    * "document_id": ``text`` - Habilita la búsqueda por ID de documento.
    * "document_type": ``text`` - Habilita la búsqueda por tipo de documento.
    * "document_barcode": ``text`` - Habilita la búsqueda de código de barras del documento.
    * "from_scheduled_start": ``date`` - Entrada de datos de.
    * "to_scheduled_start": ``date`` - Entrada desde la fecha hasta.
    * "vehicle_id": ``text`` - Habilita la búsqueda por identificación del vehículo de transporte.
    * "driver_id": ``text`` - Habilita la búsqueda por ID de operador.
    * "status": ``array`` - Parametriza los posibles estados para la investigación.

    **Ejemplo**
    ```json
    "taskSearchInput": [
          {
            "code": "document_id",
            "type": "text"
          },
          {
            "code": "document_type",
            "type": "text"
          },
          {
            "code": "document_barcode",
            "type": "text"
          },
          {
            "code": "from_scheduled_start",
            "type": "date"
          },
          {
            "code": "to_scheduled_start",
            "type": "date"
          },
          {
            "code": "vehicle_id",
            "type": "text"
          },
          {
            "code": "driver_id",
            "type": "text"
          }
        ],
    ```

![image alt <>](assets/taskSearchInput.png){:height="300px" width="300px"}

### **visible**

Tipo: ``boolean`` (default: ``false``) | **1.0.0-rc.3** (``task-type-definitions``)

Si el tipo de tarea es visible para el usuario en la APP (en el caso de un error al obtener tareas del servidor o cuando el número de tareas a realizar es 0).

!!! note "Ejemplo"
    ```json
    {
        "visible": true
    }
    ```

![image alt <>](assets/visible.png){:height="300px" width="300px"}

### **zones**
Tipo: ``Array`` [""] (``task-type-definitions``)


Define si se realiza la solicitud de creación de tareas con zonas.

!!! note "Ejemplo"
    ```json
    {
        "zones":[
            "store",
            "warehouse"
        ]
    }
    ```


## FLUJO DE PROCESAMIENTO DE ARTÍCULOS ##

### **alertColors**

Tipo: ``array`` (default: []) | **1.6.0-rc.0** (``task-type-definitions``)

Matriz de objetos con la definición de colores para una alerta que pertenece al rango especificado.

???+ example "Estructura:"
    * "range": number[``2``] - rango de peso incluido (matriz con dos posiciones)
    * "color": number[3] - códigos de color RGB que se aplicarán al rango (matriz con tres posiciones)

    **Ejemplo**
    ```json
       "alertColors": [
      {
        "range": [0, 10],
        "color": [255, 165, 0]
      },
      {
        "range": [11, 20],
        "color": [255, 187, 149]
      }
    ]
    ```

![image alt <>](assets/alertColors.png){:height="300px" width="300px"}

### **alertIcon**

Tipo: ``boolean`` (default: ``false``) | **1.27.0-rc.0** (``task-type-definitions``)

Establece si se muestra el icono en presencia de alertas en artículos / recuadros.

!!! note "Ejemplo"
    ```json
    {
        "alertIcon": true
    }
    ```

<!--|          |                          |
| -------------- | ----------------------------------- |
| ![image alt <>](assets/alertIcon.png){:height="400px" width="400px"}       | ![image alt <>](assets/alertIcon_2.png){:height="400px" width="400px"}  |-->

![alt](assets/alertIcon.png){:height="375px" width="375px"} ![alt](assets/alertIcon_2.png){:height="375px" width="375px"}

<!--| Method      | Description                          |
| :----------- | :------------------------------------ |
| `GET`       | :material-check:     Fetch resource  |
| `PUT`       | :material-check-all: Update resource |
| `DELETE`    | :material-close:     Delete resource |-->

### **allowCreateZonesAdhoc**

Tipo: ``boolean`` (default: ``false``) | **1.22.0-rc.0** (``task-type-definitions``)

Define si es posible crear zonas ad hoc en tareas de inventario. Si el valor es 'falso', no es posible crear zonas ad hoc. Si el valor es 'verdadero', permite la creación de zonas ad hoc a nivel de 'niños'. Se aplica exclusivamente a las tareas de Inventario.

!!! note "Ejemplo"
    ```json
    {
        "allowCreateZonesAdhoc": true
    }
    ```

![image alt <>](assets/allowCreateZonesAdhoc.png){:height="300px" width="300px"}

### **allowContainers** ###

!!! warning "Obsoleto"
    Utilice  [allowContainersV2](#allowcontainersv2) en su lugar

Tipo: ``string`` (default: ``never``) | **1.0.0-rc.3** (``task-type-definitions``)

Define si está permitido crear volúmenes ad-hoc para el tipo de tarea.

<!--![Placeholder](assets/taskLocationPrint.png){: align=left width=300 }

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

```json
{
    "zones":[
        "store",
        "warehouse"
    ]
}
```
-->

### **allowContainersV2** ###

Tipo: ``object``(default: { "level": ``"never"`` } ) | **1.19.0-rc.0** (``task-type-definitions``)

Define si está permitido crear volúmenes ad-hoc para el tipo de tarea.

* ``"withoutLabel"``: indica si es posible crear volúmenes ad hoc sin etiquetas, en la raíz de la tarea (primer nivel). Valor por defecto: ``false``
* ``"level"`` - identifica el nivel en el que es posible crear volúmenes.

???+ example "Valores posibles::"
    * ``always`` - le permite crear volúmenes ad-hoc para el tipo de tarea en cualquier nivel
    * ``first-level`` - le permite crear volúmenes ad-hoc solo en la raíz de la tarea (primer nivel)
    * ``never`` - no permite crear volúmenes ah-hoc para el tipo de tarea (valor predeterminado)
    
    **Ejemplo**
    ```json
    {
      "allowContainersV2": {
        "level": "first",
        "withoutLabel": true
      }
    }
    ```




<!--![Foto](assets/allowContainersV2.png){: align=left width=300 }
docs\assets\allowContainersV2.png
-->

![alt](assets/alertIcon_3.png){:height="375px" width="375px"} ![alt](assets/allowContainersV2.png){:height="375px" width="375px"}

<!--![image alt <>](assets/allowContainersV2.png){:height="300px" width="300px"}-->


Si la configuración se definió con el campo ``"withoutLabel"``: true, la aplicación hace que el símbolo esté disponible en la esquina superior derecha de la pantalla ![foto](assets/box_allowContainersV2.png){:height="20px" width="20px"}.

A través de esta entrada, la aplicación permite al usuario crear un contenedor con ID generada aleatoriamente.

Solo se permite la creación de un contenedor ad hoc para el nivel definido por el campo ``"level"``.


### **allowDateOverLimit** ###

Tipo: ``boolean`` (default: ``true``) | **1.24.0-rc.0** (``task-type-definitions``)

Permite insertar una fecha de caducidad dentro del número de días parametrizados en la tarea.

!!! note "Ejemplo"
    ```json
    {
      "allowDateOverLimit": false
    }
    ```
![image alt <>](assets/allowDateOverLimit.png){:height="300px" width="300px"}

### **allowDepreciateOverSoh** ###

Tipo: ``boolean`` (default: ``true``) | **1.20.0-rc.0** (``task-type-definitions``)

Evita que un artículo se deprecie si el monto a depreciar es mayor que SOH. Si no es posible obtener el valor de SOH, la aplicación le permite continuar con el proceso de depreciación incluso si el valor de la configuración es ``false``.
Si la configuración está parametrizada con el valor ``true``, a aplicación permite la depreciación del artículo incluso si la cantidad ingresada es mayor que la cantidad de SOH. En este caso, proporciona una alerta al usuario pero permite proceder con la depreciación. Si el valor es,  ``false`` proporciona una alerta al usuario que evita que continúe la depreciación.

!!! note "Ejemplo"
    ```json
    {
      "allowDepreciateOverSoh": true
    }
    ```
![image alt <>](assets/allowDepreciateOverSoh.png){:height="300px" width="300px"}

### **allowExpectedQuantityDefault** ###

Tipo: ``boolean`` (default: ``false``) | **1.18.1-rc.0**

Cuando la cantidad del artículo aún no ha sido alterada, permite colocar la cantidad esperada en el artículo por defecto.

!!! note "Ejemplo"
    ```json
    {
      "allowExpectedQuantityDefault": false
    }
    ```
![image alt <>](assets/allowExpectedQuantityDefault.png){:height="300px" width="300px"}

<!--:fontawesome-regular-check-circle:{: .medium }-->

### **allowOverQuantity** ###

Tipo: ``boolean`` (default: ``true``) | **1.1.0-rc.2** (``task-type-definitions``)

Define si un artículo puede tener un valor de unidades procesadas mayor que el valor de unidades predichas.

!!! note "Ejemplo"
    ```json
    {
      "allowOverQuantity": false
    }
    ```


### **allowPickingSkus** ###

!!! warning "Obsoleto"
    Utilice [searchItem](#searchitem) en su lugar


Tipo: ``boolean`` (default: ``true``) 

Esta parametrización permite, por funcionalidad, limitar el tipo de entrada cuando se quiere procesar un artículo.

### **askLabelPrice** ###

!!! warning "Obsoleto"
    Utilice [getPricesOnPicking](#getpricesonpicking) en su lugar

Tipo: ``boolean`` (default: ``false``) | **1.13.0-rc.0** 

Realice el pedido de entrada manual del precio cuando la etiqueta no tenga precio incluido.

### **allowUndo** ###

Tipo: : ``boolean`` (default: ``true``) | **1.19.0-rc.0** (``task-type-definitions``)

Le permite deshacer el último cambio realizado en un elemento / caja.


!!! note "Ejemplo"
    ```json
    {
      "allowUndo": true
    }
    ```


![image alt <>](assets/allowUndo.png){:height="300px" width="300px"}

### **allowShelfQuantity** 

Tipo: ``boolean`` (default: ``false``) | **1.17.0-rc.2** (``task-type-definitions``)

Le permite habilitar la entrada "Cantidad de estante" en las tareas de STOCKOUTAUDIT para los clientes que no tienen un retorno sobre el valor del servicio de PS.


!!! note "Ejemplo"
    ```json
    {
      "allowShelfQuantity": true
    }
    ```

![image alt <>](assets/allowShelfQuantity.png){:height="300px" width="300px"}


### **askOnEqualPrice**

Tipo: ``boolean`` (default: ``false``) (``task-type-definitions``)


Esta parametrización permite rentabilizar la impresión de etiquetas si el precio de la etiqueta es igual al nuevo precio a imprimir.


!!! note "Ejemplo"
    ```json
    {
      "askOnEqualPrice": true
    }
    ```

### **askSecondEAN**

Tipo: ``boolean`` (default: false) | **1.24.0-rc.0** (``task-type-definitions``)

Solicita la inserción de un segundo EAN para artículos integrados con atributo ``pair`` con valor  ``true``.

!!! note "Ejemplo"
    ```json
    {
      "askSecondEAN": true
    }
    ```

![image alt <>](assets/askSecondEAN.png){:height="300px" width="300px"}


### **checkReaderInput**

Tipo: ``object`` (default: {}) | **1.14.0-rc.2** (``task-type-definitions``)

Define una expresión regular que se aplicará a los pollitos, ya sea a través del lector o manualmente.


???+ example "Estructura de Ajuste"

    * ``pattern``: string - el patrón que se aplicará al código de barras / ean / sku picado

    * `manual`: boolean - define si el patrón se aplicará al astillado manual (defalt-``false``)

    * `reader`: boolean - define si el patrón se aplicará a los cheques a través del escáner (defalt-``false``)

    **Ejemplo**

    ```json
    "checkReaderInput":
    {
      "pattern": "^\d{1,13}$",
      "manual": true,
      "reader": false
    }
    ```


### **difficulty**

Tipo: ``string`` (default: ``"Q"``) | **1.20.0-rc.2** (``task-type-definitions``)

Define la dificultad de agregar artículos / volúmenes a una tarea.


???+ example "Valores Possíveis:"
    * ``N`` - "None": agregue el artículo / volumen a la tarea sin hacer preguntas.
    * ``DP`` - "Double picking": el usuario tiene que picar el artículo dos veces para insertarlo en la tarea.
    * ``Q`` - "Question": Pregunte al usuario si desea agregar ad-hoc.
    * ``W`` - "Warning": muestra una alerta al usuario y agrega el elemento / volumen.

    **Ejemplo**
    ```json
    "difficulty": "N"
    ```


### **filterResources**

Tipo: ``boolean`` (default: ``false``) | **1.1.0-rc.1** (``task-type-definitions``)

Define si se permite filtrar la lista de artículos dentro de la tarea.


???+ example "Valores Possíveis:"
    * ``UP`` - Define el filtro "Incrementos de precio"
    * ``DOWN`` - Define el filtro "Descensos de precios".

    **Ejemplo**
    ```json
    "filterResources": true,
    ```
![image alt <>](assets/filterResources.png){:height="300px" width="300px"}

### **getExpirationParameters**

Tipo: ``boolean`` (default: ``false``) - **1.3.0-rc.1** (``task-type-definitions``)

Al agregar un artículo ad-hoc, solicita la información sobre los parámetros de vigencia (número de días de depreciación, retiro y% de descuento máximo).



!!! note "Ejemplo"
    ```json
    {
      "getExpirationParameters": true
    }
    ```


### **getItemAdhocInfo**

Tipo: ``boolean`` (default: ``true``) | **1.0.0-rc.3** (``task-type-definitions``)

!!! warning "Obsoleto"
    Utilice [getItemAdhocInfoV3](#getitemadhocinfov3) en su lugar

Al agregar un elemento ad-hoc, realiza una solicitud al servidor para complementar su información (se requiere conexión).

### **getItemAdhocInfoTimeout**

Tipo: ``number`` (default: ``5000``) | **1.0.0-rc.3** (``task-type-definitions``)

!!! warning "Obsoleto"
    Utilice [getItemAdhocInfoV3](#getitemadhocinfov3) en su lugar

(solo se aplica si  "getItemAdhocInfo" = ``true``)

Establece el número máximo de milisegundos para esperar la resolución del pedido. En el caso de un tiempo de espera, el flujo continúa asumiendo que la definición getItemAdhocInfo tiene el valor ``false``.


### **getItemAdHocInfoV2**

Tipo: ``object`` (default: { "active": ``true`` }) | **1.24.0-rc.0** (``task-type-definitions``)

!!! warning "Obsoleto"
    Utilice [getItemAdhocInfoV3](#getitemadhocinfov3) en su lugar

Al agregar un elemento ad-hoc, realiza una solicitud al servidor para complementar su información. También permite definir si la existencia del artículo en el rango de la tienda de destino es válida, así como un timeout para el pedido.



### **getItemAdHocInfoV3**

Tipo: ``object`` (default: { "active": ``true`` }) | **1.27.1-rc.0** (``task-type-definitions``)

Al agregar un elemento ad-hoc, realiza una solicitud al servidor para complementar su información. También permite definir parámetros de envío en el pedido (validación del artículo en el rango de la tienda de destino y otras opciones), así como un timeout para el pedido. También le permite definir en caso de error en la respuesta del servicio si la aplicación inserta el elemento en la tarea.


???+ example "Valores Possíveis:"
    * ``"active"``: ``boolean`` -  Define si la función está activa (default: ``true``).
    * ``"timeout"``: ``number`` - Define el número máximo de milisegundos a esperar hasta que se resuelva la orden. En el caso de un tiempo de espera, el flujo continúa asumiendo que la funcionalidad no está activa (default: ``5000``).
    * ``"queryParameters"``: ``array``- parámetros a pasar en el pedido  ("destination" (a loja de destino) e "extra").
    * ``createResourceOnError``: ``boolean`` - Define si agregar un elemento a la tarea en caso de un error en la respuesta del servicio.

    **Ejemplo**
    ```json
    "getItemAdHocInfoV3": {
			"active": true,
      "createResourceOnError": true,
			"queryParameters": [
				{
					"code": "destination",
					"value": true
				},
				{
					"code": "extra",
					"value": "replenish"
				}
			],
			"timeout": 3000
		},
    ```


### **getPricesOnPicking**

Tipo: ``object`` (default: ``{}``) | **1.17.0-rc.0** (``task-type-definitions``)

Posibilidad de obtener precios del artículo en chip manual o mediante lector. Además, permite al usuario ingresar manualmente el precio (s) cuando la etiqueta de precio no devuelve el precio (s).


???+ example "Valores Possíveis:"
    * ``"active"``: ``boolean`` - Define si la función está activa (default: ``false``).
    * ``"askLabelPrice"``: ``boolean`` - si la función está activada y cuando la etiqueta no tiene precio (s) que el usuario puede introducir manualmente precio (s)  (default: ``false``)

    **Ejemplo**
    ```json
    "getPricesOnPicking": {
			"active": true,
			"askLabelPrice": true
		},
    ```
![image alt <>](assets/getPricesOnPicking.png){:height="300px" width="300px"}

### **insertWeightManually**

Tipo: ``boolean`` (default: ``false``) | **1.8.0-rc.0** (``task-type-definitions``)

Al leer / pinchar una etiqueta de precio variable (comenzando con 26 o 27), le pide al usuario que ingrese manualmente el peso, bloqueando la posibilidad de cambiar manualmente este peso al solicitar reemplazo (solo con un nuevo pinchazo).


!!! note "Ejemplo"
    ```json
    {
      "insertWeightManually": true
    }
    ```


### **labelChooseByServer**

Tipo: ``boolean`` (default: ``false``) | **1.8.0-rc.0** (``task-type-definitions``)

!!! warning "Obsoleto"
    Utilice [labelChooseByServerV2](#labelchoosebyserverv2) en su lugar

Define si se permite elegir el tipo de etiqueta a imprimir o si esta configuración la realiza automáticamente el servidor.


### **labelChooseByServerV2**

Tipo:  ``object`` (default: ``{}``) | **1.16.1-rc.0** (``task-type-definitions``)

Si se permite elegir el tipo de etiqueta que se va a imprimir o si esta configuración la realiza automáticamente el servidor. Además, le permite definir una etiqueta predeterminada, así como la opción "comprobar configuración" al imprimir.


???+ example "Valores Possíveis:"
    * ``"active"``: ``boolean`` - determina si el servidor para definir el tipo de etiqueta a imprimir (valor por defeito: ``false``)
    * ``"defaultLabelCode"``: ``string`` - Define si el tipo de etiqueta por defecto cuando el usuario no elige un tipo de etiqueta para imprimir
    * ``"checkSettingOnPrint"``: ``boolean`` - Define si se enviará el parámetro "comprobar-ajuste" en la impresión

    **Ejemplo**
    ```json
    "labelChooseByServerV2": {
			"active": true,
			"checkSettingOnPrint": false,
			"defaultLabelCode": "30"
		},
    ```
![image alt <>](assets/labelChooseByServerV2.png){:height="300px" width="300px"}

### **manageContainers**

Tipo: ``boolean`` (default: ``false``) | **1.1.0-rc.1** (``task-type-definitions``)

!!! warning "Obsoleto"
    Utilice [manageContainersV2](#managecontainersv2) en su lugar

Si está permitido administrar volúmenes para el tipo de tarea (agregar y / o cambiar volúmenes).


### **manageContainersV2**

Tipo: ``object`` (default: { "active": ``false`` }) | **1.34.0-rc.0** (``task-type-definitions``)

Define si está permitido administrar volúmenes para el tipo de tarea (agregar y / o cambiar volúmenes).


???+ example "Valores Possíveis:"
    * ``"active"``: ``boolean`` - define si está permitido administrar volúmenes (valor por defeito: ``false``).
    * ``"name"``: ``boolean`` - define si se permite definir el nombre de un volumen (valor por defeito: ``false``).
    * ``"default_container"``: ``boolean`` - ddefine si se crea automáticamente un volumen al iniciar una tarea vacía (valor por defeito: ``false``).
    * ``"print": ``string`` - Define si es posible imprimir la etiqueta de identificación del contenedor y define el tipo de impresión:
        * ``mobile``: ``string`` - Define la impresión móvil.
        * ``laser``: ``string`` - Define la impresión a través de una impresora fija.
    * ``action``: ``string`` - Define qué tipo de acción se realizará sobre el artículo:
        * ``moved``: ``string`` - Artículo movido entre niveles de tareas.
        * ``delete``: ``string`` - Artículo eliminado de la tarea.

    **Ejemplo**
    ```json
    "manageContainersV2": {
			"active": true,
			"name": true,
			"default_container": true,
            "print": "mobile",
            "action": "moved"
		},
    ```
![image alt <>](assets/manageContainersV2.png){:height="300px" width="300px"}

### **maxEanLength**

Tipo: ``number`` | **1.2.0-rc.1** (``task-type-definitions``)

!!! warning "Obsoleto"
    Utilice [checkReaderInput](#checkreaderinput) en su lugar

Esta parametrización se utiliza solo en el tipo de tarea en la que se pretende limitar el tamaño de la lectura de EAN. Si el EAN es mayor que el máximo permitido, se mostrará un mensaje de error.


!!! note "Ejemplo"
    ```json
    {
      "maxEanLength": 13
    }
    ```

### **onAddResourceError**

Tipo: ``boolean`` (default: ``false``) | **1.0.0-rc.3** (``task-type-definitions``)

!!! warning "Obsoleto"


(solo aplicable si getItemAdhocInfo = ``true``)

Aplica acciones predefinidas en caso de error al agregar un elemento ad hoc.


!!! note "Ejemplo"
    ```json
    {
      "onAddResourceError": true
    }
    ```


### **onGetItemAdhocInfo**

Tipo: ``boolean`` (default: ``false``) | **1.0.0-rc.3** (``task-type-definitions``)

!!! warning "Obsoleto"
    Utilice [getItemAdhocInfoV3](#getitemadhocinfov3) en su lugar

Aplica acciones predefinidas después de obtener información sobre el elemento agregado.


### **onlyChangeQuantityByPicking**

Tipo: ``boolean`` (default: ``false``) | **1.0.0-rc.3** (``task-type-definitions``)

Si es valor ``true``, solo le permite cambiar las cantidades de un artículo mediante astillado.

!!! note "Ejemplo"
    ```json
    {
      "onlyChangeQuantityByPicking": true
    }
    ```
![image alt <>](assets/onlyChangeQuantityByPicking.png){:height="300px" width="300px"}

### **processOnPicking**

Tipo: ``boolean`` (default: ``false``) | **1.1.0-rc.10** (``task-type-definitions``)

Procese la cantidad total de un artículo en el astillado. Si el artículo no tiene una cantidad esperada, se procesará una unidad. Para que se aplique esta definición, es necesario que el artículo esté al mismo nivel que el astillado.


!!! note "Ejemplo"
    ```json
    {
      "processOnPicking": false
    }
    ```


### **product-overview**

Tipo: ``object []`` | **v0.8.0** (``application-setting``)

Define la plantilla para la página de entrada de la hoja del artículo.


??? note "Ejemplo"
    ```json
    [
    {
    "attributes": {
    "condensed": false
    },
    "code": "item_details",
    "template": "map",
    "value": [
    {
    "code": "mrs_name",
    "value": {
    "$ref": "$.product.name"
    }
    },
    {
    "code": "mrs_image_default",
    "description": {
    "$ref": "$.product.name"
    },
    "value": {
    "$ref": "$.product.image"
    }
    },
    {
    "code": "mrs_sku",
    "description": "SKU",
    "value": {
    "$ref": "$.product.sku"
    }
    },
    {
    "code": "mrs_ean",
    "description": "EAN",
    "value": {
    "$ref": "$.product.ean"
    }
    },
    {
    "code": "mrs_stock",
    "description": "SOH",
    "value": {
    "$ref": "$.mrs_stock.value"
    }
    },
    {
    "code": "mrs_status",
    "description": "Status",
    "value": {
    "$ref": "$.product.status"
    }
    }
    ]
    },
    {
    "code": "item_price",
    "description": "${price}",
    "template": "grid",
    "value": [
    {
    "code": "row",
    "value": [
    {
    "code": "col",
    "value": [
    {
    "code": "row",
    "value": [
    {
    "code": "mrs_price",
    "description": "MRS",
    "value": {
    "$ref": "$.price.mrs"
    }
    }
    ]
    }
    ]
    }
    ]
    }
    ]
    },
    {
    "code": "mrs_attributes",
    "description": "Attributes",
    "template": "list",
    "value": {
    "$map": {
    "code": "mrs_attribute",
    "description": {
    "$ref": "@.attribute"
    },
    "value": {
    "$map": {
    "code": "mrs_sibling_attribute",
    "description": {
    "$ref": "@.attribute"
    },
    "value": {
    "$ref": "@.skus"
    }
    },
    "$ref": "@.siblings"
    }
    },
    "$ref": "$.mrs_attributes"
    }
    },
    {
    "code": "item_related_pages",
    "template": "nav_list",
    "value": [
    {
    "code": "stock",
    "description": "${stock}",
    "value": "product-stock"
    },
    {
    "code": "price",
    "description": "${price} & ${promotions}",
    "value": "product-price"
    },
    {
    "code": "features",
    "description": "${features}",
    "must": [
    {
    "$ref": "$.product.fashion"
    },
    {
    "$ref": "$.product.productTipo"
    }
    ],
    "value": {
    "$ref": "$.mrs_product_features"
    }
    },
    {
    "code": "hierarchy",
    "description": "${hierarchy}",
    "value": "product-category-hierarchy"
    },
    {
    "code": "eans",
    "description": "${eans}",
    "value": "product-eans"
    }
    ]
    }
    ]
    ```
![image alt <>](assets/product-overview.png){:height="300px" width="300px"}

### **product-price**

Tipo: ``object []`` | **v0.8.0** (``application-setting``)

Define la plantilla para la página de precios y promociones del artículo (hoja de artículo).


??? note "Ejemplo"
    ```json
    [
    {
    "attributes": {
    "condensed": true
    },
    "code": "item_details",
    "template": "map",
    "value": [
    {
    "code": "mrs_name",
    "value": {
    "$ref": "$.product.name"
    }
    },
    {
    "code": "mrs_image_default",
    "description": {
    "$ref": "$.product.name"
    },
    "value": {
    "$ref": "$.product.image"
    }
    },
    {
    "code": "mrs_sku",
    "description": "SKU",
    "value": {
    "$ref": "$.product.sku"
    }
    },
    {
    "code": "mrs_ean",
    "description": "EAN",
    "value": {
    "$ref": "$.product.ean"
    }
    },
    {
    "code": "mrs_stock",
    "description": "SOH",
    "value": {
    "$ref": "$.mrs_stock.value"
    }
    },
    {
    "code": "mrs_status",
    "description": "Status",
    "value": {
    "$ref": "$.product.status"
    }
    }
    ]
    },
    {
    "code": "item_price",
    "description": "${price}",
    "template": "grid",
    "value": [
    {
    "code": "row",
    "value": [
    {
    "code": "col",
    "value": [
    {
    "code": "row",
    "value": [
    {
    "code": "mrs_price",
    "description": "MRS",
    "value": {
    "$ref": "$.mrs_price.value"
    }
    }
    ]
    },
    {
    "code": "row",
    "value": [
    {
    "code": "mrs_erp_price",
    "description": "ERP",
    "value": {
    "$ref": "$.erp_price.value"
    }
    }
    ]
    }
    ]
    }
    ]
    }
    ]
    },
    {
    "code": "item_promotions",
    "description": "${promotions}",
    "template": "grid",
    "value": {
    "$map": {
    "code": "row",
    "description": {
    "$oneOf": [
    {
    "$ref": "@.group/name"
    },
    {
    "$ref": "@.description"
    }
    ]
    },
    "value": [
    {
    "code": "col",
    "value": [
    {
    "code": "row",
    "value": [
    {
    "code": "mrs_promotion_start",
    "description": "Inicio",
    "value": {
    "$ref": "@.startDate",
    "$transform": "moment|timezone>$.mrs_store.timezone.value|format>DD-MM-YYYY"
    }
    }
    ]
    },
    {
    "code": "row",
    "value": [
    {
    "code": "mrs_promotion_end",
    "description": "Fim",
    "value": {
    "$ref": "@.endDate",
    "$transform": "moment|timezone>$.mrs_store.timezone.value|format>DD-MM-YYYY"
    }
    }
    ]
    }
    ]
    },
    {
    "code": "col",
    "value": [
    {
    "code": "row",
    "value": [
    {
    "code": "mrs_promotion_info",
    "description": "Preço",
    "value": {
    "$ref": "@.price"
    }
    }
    ]
    }
    ]
    }
    ]
    },
    "$ref": "$.promotions"
    }
    }
    ]
    ```
![image alt <>](assets/product-overview.png){:height="300px" width="300px"}

### **product-stock**

Tipo: ``object[]`` | **v0.8.0** (``application-setting``)

Define la plantilla para la página de existencias del artículo (hoja de artículo).


??? note "Ejemplo"
    ```json
    [
    {
    "attributes": {
    "condensed": true
    },
    "code": "item_details",
    "template": "map",
    "value": [
    {
    "code": "mrs_name",
    "value": {
    "$ref": "$.product.name"
    }
    },
    {
    "code": "mrs_image_default",
    "description": {
    "$ref": "$.product.name"
    },
    "value": {
    "$ref": "$.product.image"
    }
    },
    {
    "code": "mrs_sku",
    "description": "SKU",
    "value": {
    "$ref": "$.product.sku"
    }
    },
    {
    "code": "mrs_ean",
    "description": "EAN",
    "value": {
    "$ref": "$.product.ean"
    }
    },
    {
    "code": "mrs_stock",
    "description": "SOH",
    "value": {
    "$ref": "$.mrs_stock.value"
    }
    },
    {
    "code": "mrs_status",
    "description": "Status",
    "value": {
    "$ref": "$.product.status"
    }
    }
    ]
    },
    {
    "code": "stock",
    "description": "${stock}",
    "template": "list",
    "value": [
    {
    "code": "stock_on_hand",
    "description": "${stock-on-hand}",
    "value": {
    "$ref": "$.erp_stock.stock_on_hand"
    }
    },
    {
    "code": "stock_unavailable",
    "description": "${stock-unavailable}",
    "value": {
    "$ref": "$.erp_stock.stock_unavailable"
    }
    },
    {
    "code": "stock_on_transit",
    "description": "${stock-on-transit}",
    "value": {
    "$ref": "$.erp_stock.stock_on_transit"
    }
    },
    {
    "code": "stock_on_order",
    "description": "${stock-on-order}",
    "value": {
    "$ref": "$.erp_stock.stock_on_order"
    }
    }
    ]
    },
    {
    "code": "item_related_pages",
    "template": "nav_list",
    "value": [
    {
    "code": "stock",
    "description": "${stock-other-stores}",
    "value": "product-stock-others"
    }
    ]
    }
    ]
    ```
![image alt <>](assets/product-stock.png){:height="300px" width="300px"}

### **product-eans**

Tipo: ``object[]`` | **v0.8.0** (``application-setting``)

Define la plantilla para la página de eans (primaria y no primaria) del artículo (hoja de artículo).


??? note "Ejemplo"
    ```json
    [
    {
    "code": "product_eans",
    "description": "EANs",
    "template": "list",
    "value": {
    "$map": {
    "code": {
    "$ref": "mrs_ean"
    },
    "description": {
    "$ref": "@.ean"
    },
    "value": {
    "$ref": "@.primary",
    "$transform": "strictEquals>Y|then>${primary-ean}>"
    }
    },
    "$ref": "$.mrs_eans"
    }
    }
    ]
    ```
![image alt <>](assets/product-overview.png){:height="300px" width="300px"}

### **product-features-f**

Tipo: ``object[]`` | **v0.8.0** (``application-setting``)

Define la plantilla para las características de moda del artículo (hoja de artículo).


??? note "Ejemplo"
    ```json
    [
    {
    "attributes": {
    "condensed": true
    },
    "code": "product-features-f",
    "description": "${features}",
    "template": "list",
    "value": [
    {
    "code": "season",
    "description": "${season}",
    "value": {
    "$ref": "$.mrs_features_fashion.season"
    }
    },
    {
    "code": "phase",
    "description": "${phase}",
    "value": {
    "$ref": "$.mrs_features_fashion.phase"
    }
    },
    {
    "code": "theme",
    "description": "${theme}",
    "value": {
    "$ref": "$.mrs_features_fashion.theme"
    }
    }
    ]
    }
    ]
    ```
![image alt <>](assets/product-overview.png){:height="300px" width="300px"}

### **product.category-hierarchy**

Tipo: ``object[]`` | **v0.8.0** (``application-setting``)

Define la plantilla para la categoría jerárquica del artículo (hoja de artículo).


??? note "Ejemplo"
    ```json
    [
    {
    "code": "product_category_hierarchy",
    "description": "${hierarchy}",
    "template": "list",
    "value": {
    "$map": {
    "code": {
    "$ref": "@.code"
    },
    "description": {
    "$ref": "@.name"
    },
    "value": {
    "$ref": "@.code"
    }
    },
    "$ref": "$.mrs_category_hierarchy"
    }
    }
    ]
    ```
![image alt <>](assets/product.category-hierarchy.png){:height="300px" width="300px"}

### **products-stock-others**

Tipo: ``object[]`` | **v0.8.0** (``application-setting``)

Define la plantilla para el stock en otras tiendas (hoja de artículo).


??? note "Ejemplo"
    ```json
    [
    {
    "attributes": {
    "condensed": true
    },
    "code": "item_details",
    "template": "map",
    "value": [
    {
    "code": "mrs_name",
    "value": {
    "$ref": "$.product.name"
    }
    },
    {
    "code": "mrs_image_default",
    "description": {
    "$ref": "$.product.name"
    },
    "value": {
    "$ref": "$.product.image"
    }
    },
    {
    "code": "mrs_sku",
    "description": "SKU",
    "value": {
    "$ref": "$.product.sku"
    }
    },
    {
    "code": "mrs_ean",
    "description": "EAN",
    "value": {
    "$ref": "$.product.ean"
    }
    },
    {
    "code": "mrs_stock",
    "description": "SOH",
    "value": {
    "$ref": "$.mrs_stock.value"
    }
    },
    {
    "code": "mrs_status",
    "description": "Status",
    "value": {
    "$ref": "$.product.status"
    }
    }
    ]
    },
    {
    "code": "stock_others",
    "description": "${stock}",
    "template": "list",
    "value": {
    "$map": {
    "code": {
    "$ref": "@.store"
    },
    "description": {
    "$ref": "@.store"
    },
    "value": {
    "$ref": "@.stock.stock_on_hand"
    }
    },
    "$ref": "$.erp_stock_stores.value"
    }
    }
    ]
    ```
![image alt <>](assets/product.category-hierarchy.png){:height="300px" width="300px"}

### **reasons** 

Tipo: ``Array<string>`` | **1.2.0-rc.1** (``task-type-definitions``)

Cuando un tipo de tarea tiene motivos asociados al tratamiento de artículos. A continuación, el motivo seleccionado se envía a la tarea en el campo "reason.id".

![image alt <>](assets/reasons.png){:height="300px" width="300px"}

### **regent-price**

Tipo: ``string`` (default: ``"erp"``) | **1.7.0-rc.0** (``application-setting``)

Define el precio que rige, en caso de divergencia de precios.


???+ example "Valores Possíveis:"
    * ``erp``: ``string`` - Define el valor ERP como valor gobernante.
    * ``pos``: ``string`` -  Define el valor POS como valor gobernante.
    * ``label``: ``string`` - Define el valor de LABEL como valor gobernante.

    **Ejemplo**
    ```json
      "regent-price": {
			"pos"
      },
    ```

### **removeItemAdhoc**

Tipo: ``boolean`` (default: ``true``) | **1.4.0-rc.0** (``task-type-definitions``)

Permite eliminar el artículo ad hoc.


!!! note "Ejemplo"
    ```json
    {
      "removeItemAdhoc": true
    }
    ```
![image alt <>](assets/removeItemAdhoc.png){:height="300px" width="300px"}

### **search-item-by-text**

!!! warning "Obsoleto"
    Utilice [searchItem](#searchitem) en su lugar

Tipo: ``object`` (default: "active":``true``) | **1.11.0-rc.0** (``application-setting``)

### **searchItem**

Tipo: ``object`` | **1.11.0-rc.1** (``task-type-definitions``)

Si es posible buscar artículos manualmente dentro de una tarea y cómo. Además, permite configurar el chip de código de barras que contiene EAN o SKU.


???+ example "Valores Possíveis:"
    * ``ean``: ``boolean`` - Define si es posible la búsqueda manual de artículos. por EAN.
    * ``sku``: ``boolean`` - Define si es posible la búsqueda manual de artículos. por SKU.
    * ``camera``: ``boolean`` - Habilita la búsqueda a través de la cámara del dispositivo.
    * ``text``: ``boolean`` - Habilita la búsqueda de texto: ean, sku, descripción, etc.
    * ``scan_sku``: ``boolean`` - Permite el corte de etiquetas con información de sku.
    * ``scan_ean``: ``boolean`` - Permite el corte de etiquetas con información EAN.
    * ``container``: ``boolean`` - Define si es posible la búsqueda manual de contenedores.
    * ``location``: ``boolean`` - Define si es posible la búsqueda de ubicación manual.

    **Ejemplo**
    ```json
      "searchItem": {
			"camera": false,
			"ean": false,
			"sku": false,
			"text": false,
			"scan_sku": true,
			"scan_ean": true,
            "container": true,
            "location": true
		},
    ```
![image alt <>](assets/searchItem.png){:height="300px" width="300px"}

### **sendFutureValidities**

Tipo: ``boolean`` (default: ``false``) | **1.15.0-rc.3** (``task-type-definitions``)

Envía fechas de vencimiento adicionales a un servicio externo.


!!! note "Ejemplo"
    ```json
    {
      "sendFutureValidities": true
    }
    ```

### **show-images**

Tipo: ``object`` | **1.27.1-rc.0** (``application-setting``)

Establece la visibilidad de las imágenes en la aplicación.


!!! note "Ejemplo"
    ```json
    {
      "active": true
    }
    ```
![image alt <>](assets/show-images.png){:height="300px" width="300px"}

### **showScheduledStartHour**

Tipo: ``boolean`` (default: ``true``) | **1.22.0-rc.1** (``task-type-definitions``)

Establece si se muestra la hora de la tarea en la lista.


!!! note "Ejemplo"
    ```json
    {
      "showScheduledStartHour": true
    }
    ```
![image alt <>](assets/showScheduledStartHour.png){:height="300px" width="300px"}

### **sortResources**

Tipo: ``object`` | **1.33.0-rc.0** (``task-type-definitions``)

Permite ordenar los artículos según un conjunto de opciones (todos los campos son opcionales):


???+ example "Valores Possíveis:"
    * ``NAME_A_Z``
    * ``NAME_Z_A``
    * ``UP_DOWN``
    * ``DOWN_UP``
    * ``HIERARCHICAL_LEVEL``
    * ``BRAND_A_Z``
    * ``BRAND_Z_A``
    * ``EXPIRATION_DATE``
    * ``PRIORITY_ASC``
    * ``PRIORITY_DESC``
    * ``UPDATE_DATE``
    * ``PICKING_DATE_ASC``
    * ``PICKING_DATE_DESC``
    * ``MAJOR_QUANTITY``
    * ``LESS_QUANTITY``


    **Estructura**
    
    * "active": ``boolean`` - ndica si se permite a artículos de la orden para el tipo de tarea (valor por defeito: ``true``)
    * "defaultSort": ``string`` - indica si se permite a artículos de la orden para el tipo de tarea
    * "options": ``string``[] - indica las opciones de clasificación en el orden definido en la _array_. Las opciones ``UP_DOWN`` e ``DOWN_UP`` sson exclusivas de las tareas de Rellamada. Las opciones ``PRIORITY_ASC`` e ``PRIORITY_DESC`` son exclusivas de las tareas de Control de Validez. (valor por defeito: ``["UPDATE_DATE", "NAME_A_Z", "NAME_Z_A"]``)

    **Ejemplo**
    ```json
    {
      "sortResources": {
			  "active": true,
			  "defaultSort": "UP_DOWN", 
			  "options": [
				    "UP_DOWN",
				    "DOWN_UP",
				    "BRAND_A_Z",
				    "BRAND_Z_A",
				    "HIERARCHICAL_LEVEL"
			  ]
		  },
    }
    ```
![image alt <>](assets/sortResources.png){:height="300px" width="300px"}

### **synchronousReader**

Tipo: ``boolean`` (default: ``false``) | **1.13.0-rc.1**

!!! warning "Removed"
    

Realiza lecturas de forma sincrónica (solo disponible en modo tarea).



## **TAREA DE APROBACIÓN DE FLUJO**

### **allowLeaving**

Tipo: ``boolean`` (default: ``true``) | **1.14.0-rc.0** (``task-type-definitions``)

Define si dejar una tarea sin liberar / aprobar.


!!! note "Ejemplo"
    ```json
    {
      "allowLeaving": true
    }
    ```
![image alt <>](assets/allowLeaving.png){:height="300px" width="300px"}


### **allowSync**

Tipo: ``boolean`` (default: ``true``) | **1.36.1-rc.0** (``task-type-definitions``)

Define si permitir la sincronización de la tarea.


!!! note "Ejemplo"
    ```json
    {
      "allowSync": 
      {
        "active": true
      }
    }
    ```


### **emptyTaskAction**

Tipo: ``string`` (default: ``"none"``) | **1.25.0-rc.1** (``task-type-definitions``)

Define la acción que se debe realizar al aprobar una tarea sin elementos procesados.


???+ example "Valores Possíveis:"
    * ``"none" ``- permite la aprobación de la tarea (valor por defeito)
    * ``"warning"`` - muestra una alerta, lo que permite aprobar la tarea
    * ``"not_allowed"`` - muestra una alerta y no permite la aprobación de la tarea

    **Ejemplo**
    !!! note "Ejemplo"
    ```json
    {
      "emptyTaskAction": "not_allowed"
    }
    ```
![image alt <>](assets/emptyTaskAction.png){:height="300px" width="300px"}

### **hideResume**

Tipo: ``boolean`` (default: ``false``) | **1.14.0-rc.2** (``task-type-definitions``)

Define si la aplicación presenta el resumen al final de la tarea.


!!! note "Ejemplo"
    ```json
    {
      "hideResume": false
    }
    ```

### **isToShowTaskEmptyWarning**

!!! warning "Obsoleto"
    Utilice [emptyTaskAction](#emptytaskaction) en su lugar

Tipo: ``boolean`` (default: ``false``) | **1.16.0-rc.0** (``task-type-definitions``)


### **notProcessedResourcesModal**

Tipo: ``object`` (default: ``false``) | **1.27.1-rc.0** (``task-type-definitions``)

Define el contenido del modal no procesado (al aprobar una tarea).


???+ example "Valores Possíveis:"
    * "notFoundButton": ``boolean``  - Establecer la posibilidad de dar a los artículos / volúmenes que no se encuentran (default: ``false``)
    * "treatAllButton": ``boolean`` - Establecer la capacidad de los artículos de procesos / volumen con la cantidad esperada (default: ``false``)
    * "resourceType": ``string`` - Define que "recursos" deben aparecer en el modal.
        * "containers": ``string`` - Define que el modal mostrará todos los volúmenes sin procesar.
        * "items": ``string`` -  Define que el modal mostrará todos los artículos sin procesar  (default).
        * "none" - no mostrará el modal (aprueba directamente la tarea)
    
    **Ejemplo**
    ```json
        {
	        "notProcessedResourcesModal": {
		        "notFoundButton": true,
		        "resourceTipo": "none"
	        }
        }
    ```
![image alt <>](assets/notProcessedResourcesModal.png){:height="300px" width="300px"}

### **previousApproveAction**

Tipo: ``object`` (default: {} ) | **1.24.0-rc.0** (``task-type-definitions``)

Le permite definir acciones específicas antes de aprobar una tarea.


???+ example "Valores Possíveis:"
    * "code": ``string`` - iidentifica el tipo de acción a realizar. Valores posibles::
        * "alert": ``string`` - realiza una alerta con un mensaje.
        * "input": ``string`` - obliga al usuario a completar uno o más datos antes de aprobar la tarea.
        * "divergence_alert": ``string`` - similar a alert pero solo ejecuta la alerta si hay divergencias en los artículos cortados.

    * "attributes": ``array`` - conjunto de atributos que se aplicarán a cada acción:
        * "code": ``string`` - el identificador de atributo
        * "type": ``string`` - el tipo de atributo (texto, fecha, radio, seleccionar)
        * "value": ``string`` el tipo de atributo (texto, fecha, radio, seleccionar)
        * "options": ``string`` -   (solo disponible para radio y tipos selectos) - matriz con opciones (código y valor) para el atributo.
    
    **Ejemplo**
    ```json
      {
      "previousApproveAction": {
        "code": "divergence_alert",
        "attributes": [
            {
              "code": "message",
              "type": "text",
              "value": Texto a ser apresentado na app"
            }
          ]
        }
      } 
      ...
      {
      "previousApproveAction": {
        "code": "input",
        "attributes": [
          {
            "type": "text",
            "code": "driver_id",
            "value": "123"
          },
          {
            "type": "text",
            "code": "driver_name"
          }
      } 
      ...
      {
      "previousApproveAction": {
        "type": "select",
        "code": "priority",
        "value": "A",
        "options": [
          {
              "code": "normal",
              "value": "A"
          },
          {
              "code": "urgent",
              "value": "B"
          }
        ]
      },
      ...
      {
      "previousApproveAction": {
        "type": "date",
        "code": "delivery_date",
        "value": 1
      },
      ...
      {
      "previousApproveAction": {
        "code": "alert",
        "attributes": [
          {
              "value": "Text"
          }
        ]
      },
    ```
![image alt <>](assets/previousApproveAction.png){:height="300px" width="300px"}

### **release-on-logout**

Tipo: ``object`` (default: ``false``) | **1.1.0-rc.9** (``application-setting``)

Define si, al cerrar la sesión de la aplicación, libera todas las tareas asociadas con el usuario


!!! note "Ejemplo"
    ```json
    {
      "active": true
    }
    ```


### **showFutureDatesOnApproval**

Tipo: ``boolean`` (default: ``false``) | **1.20.0-rc.2** (``task-type-definitions``)

Permite la presentación del modal de fechas futuras al aprobar fechas de vencimiento.


!!! note "Ejemplo"
    ```json
    {
      "showFutureDatesOnApproval": true
    }
    ```


### **summaryApprovalHandling**

Tipo: ``string`` (default: ``"none"``) | **1.4.0-rc.0** (``task-type-definitions``)

Define las reglas para la aprobación de tareas.


???+ example "Valores Possíveis:"
    * "none": ``string`` - puede aprobar / finalizar la tarea sin restricciones.
    * "AllHasToBeHandled": ``string`` - solo se puede aprobar si se han tratado todos los artículos 

    **Ejemplo**
    !!! note "Ejemplo"
    ```json
    {
      "summaryApprovalHandling": "none"
    }
    ```
![image alt <>](assets/summaryApprovalHandling.png){:height="300px" width="300px"}

## **MODO ARTÍCULO**

### **dropdownOptions**

Tipo: ``array`` (default: []) | **1.2.0-rc.0** (``task-type-definitions``)

Matriz de objetos con las opciones desplegables en el modal de la tarea.


???+ example "Valores Possíveis:"
    * "data": ``string[]`` - identifica los valores del menú desplegable (si no hay valores, no se mostrará el menú desplegable)
    * "mandatory":  ``boolean``  -  indica si es obligatorio para elegir una opción (valor por defeito: ``false``)
    * "type": ``string`` - identifica el tipo de datos del menú desplegable (valores possíveis: "REASONS" e "EXPIRATIONLABEL")

    **Ejemplo**
    ```json
        {
      "dropdownOptions": [
        {
          "data": [
            "linear",
            "other-location"
          ],
          "mandatory": true,
          "type": "REASONS"
        },
        {
          "data": [
            "VUNTIL",
            "VCPB",
            "VUPB"
          ],
          "type": "EXPIRATIONLABEL"
        }
      ]
    }
    ```
![image alt <>](assets/reasons.png){:height="300px" width="300px"}

### **handleQuantities**

!!! warning "Obsoleto"
    Utilice [hideQuantities](#hidequantities) en su lugar

Tipo: ``boolean`` (default: ``true``) | **1.1.0-rc.11** (``task-type-definitions``)


### **hideQuantities**

Tipo: ``string`` (default: ``"none"``) | **1.23.0-rc.2** (``task-type-definitions``)

Define si mostrar la cantidad esperada y / o la cantidad de un artículo, ya sea en la lista o en el modal.




???+ example "Valores Possíveis:"
    * "all": ``string`` - Define si mostrar los valores de cantidad esperada y cantidad tratada en el modal de artículo y en la lista de artículos en las tareas.
    * "expected": ``string`` - Define si mostrar solo los valores de cantidad esperados en el modal de artículo y en la lista de artículos en las tareas.
    * "none": ``string`` - Define que no presenta los valores de cantidad esperada y cantidad tratada en el modal de artículo y en la lista de artículos en las tareas.

    **Ejemplo**
    !!! note "Ejemplo"
    ```json
    {
      "hideQuantities": "all"
    }
    ```
![image alt <>](assets/hideQuantities.png){:height="300px" width="300px"}

### **manageSupplyUnitOnModal**

Tipo: ``object`` (default - "active": ``false``) | **not yet released** (``task-type-definitions``)

Permite presentar el valor de la Unidad de Abastecimiento en el modal del artículo y realizar el cálculo como facilitador de cantidad. (RECUENTO DE STOCK)


!!! note "Ejemplo"
    ```json
    {
      "manageSupplyUnitOnModal": {
        "active": true
      }
    }
    ```

### **maxPercentageDefault**

Tipo:  ``boolean`` (default: ``false``) | **1.27.0-rc.1** (``task-type-definitions``)

Si la parametrización está activa, al abrir el modo de depreciación, el precio final se calcula según el valor del atributo 'maxPercentageDiscount'.


!!! note "Ejemplo"
    ```json
    {
      "maxPercentageDefault": false
    }
    ```

### **openModalOnPicking**

!!! warning "Obsoleto"
    Utilice [openModalOnPickingV2](#openmodalonpickingv2) en su lugar

Tipo: ``boolean`` (default: ``true``) | **1.1.0-rc.1** (``task-type-definitions``)

Define si se abre el modo de tarea al cortar un artículo.


### **openModalOnPickingV2**

!!! warning "Obsoleto"
    Utilice [openModalOnPickingV3](#openmodalonpickingv3) en su lugar

Tipo: ``object`` (default: ``false``) | **1.7.4** (``task-type-definitions``)



### **openModalOnPickingV3**

Tipo: ``object`` (default: ``false``) | **1.40.0** (``task-type-definitions``)

Define si se abre el modo de tarea al cortar un artículo. Permite la definición por plataforma. Además, permite configurar si los artículos de peso variable son obligatorios al abrir el modal.


???+ example "Valores Possíveis:"
    * ``Wince``: ``object`` - Define la apertura modal en la plataforma WinCE.
        * ``active: ``boolean`` - Define si la apertura modal para la plataforma Wince está activa.
        * ``mandatoryVarWeight``: ``boolean`` - Define si la apertura del modal es obligatoria en la plataforma Wince al picar un artículo de peso variable.
    * ``multiplataform``: ``boolean`` - Define la apertura modal en la multiplataforma.
        * ``active: ``boolean`` - Define si la apertura del modal para la multiplataforma está activa.
        * ``mandatoryVarWeight``: ``boolean`` - Define si la multiplataforma es obligatoria para abrir el modal en el astillado de un artículo de peso variable. 

    **Ejemplo**
    !!! note "Ejemplo"
    ```json
    {
	    "openModalOnPickingV3": {
		    "wince": {
			    "active": true,
			    "mandatoryVarWeight": true
		  },
		    "multiplatform": {
			    "active": true,
			    "mandatoryVarWeight": true
		  }
	  }
    ```

### **replenishItemOnModal**

!!! warning "Obsoleto"
    Utilice [replenishItemOnModalV2](#replenishitemonmodalv2) en su lugar

Tipo: ``boolean`` (default: ``false``) | **1.4.0-rc.0** (``task-type-definitions``)

Permite el acceso a reposición urgente vía modal.


### **replenishItemOnModalV2**

Tipo: ``object`` (default: { "active": ``false`` }) | **1.27.1-rc.0** (``task-type-definitions``)

Permite acceder a reposición urgente vía modal y definir el tipo de tarea a crear.


!!! note "Ejemplo"
    ```json
    {
      "replenishItemOnModalV2": {
        "active": true,
        "type": "REPLENISHPREP"
      }
    }
    ```
![image alt <>](assets/replenishItemOnModalV2.png){:height="300px" width="300px"}

### **showContinueOnSelect**

Tipo: ``boolean`` (default: ``true``) - **1.12.0-rc.0** (``task-type-definitions``)

Si es verdadero, el botón CONTINUAR debe estar visible / disponible para procesar el artículo manualmente, sin que suene.

Si es falso, debes ocultar el botón CONTINUAR para que no sea posible procesar el artículo manualmente, lo que te obliga a pitarlo.


!!! note "Ejemplo"
    ```json
    {
      "showContinueOnSelect": true
    }
    ```
![image alt <>](assets/showContinueOnSelect.png){:height="300px" width="300px"}

### **showPrint**

Tipo: ``boolean`` (default: ``true``) | **1.12.0-rc.0** (``task-type-definitions``)

Le permite imprimir etiquetas manualmente usando el icono de la impresora en el modal del artículo.

Nota: esta definición también se aplica a la hoja de elementos cuando se configura para el tipo de tarea "ITEMFILE".

!!! note "Ejemplo"
    ```json
    {
      "showPrint": true
    }
    ```
![image alt <>](assets/showPrint.png){:height="300px" width="300px"}

### **showSOH** 

Tipo: ``boolean`` (default: ``true``) | **1.9.0-rc.0** (``task-type-definitions``)

Muestra información de SOH en el modo de artículo.


!!! note "Ejemplo"
    ```json
    {
      "showSOH": true
    }
    ```
![image alt <>](assets/showPrint.png){:height="300px" width="300px"}

### **searchProductOnModal**

Tipo: ``boolean`` (default: ``false``) | **not yet released** (``task-type-definitions``)

Permite buscar skus con el mismo ID de padre en la tarea STOCKOUTAUDIT y STOCKCOUNT.
!!! note "Ejemplo"
    ```json
    {
      "searchProductOnModal": true
    }
    ```

### **withdrawModalControl**

Tipo: ``object`` | Configuración exclusiva de la aplicación WInCE (``task-type-definitions``)

Le permite configurar todos los campos que componen el modal de retiro en el tipo de tarea EXPIRATIONCONTROL


!!! note "Ejemplo"
    ```json
    {
      "withdrawModalControl": [
        {
          "code": "depreciated",
          "visible": true
        },
        {
          "code": "sales",
          "visible": true
        },
        {
          "code": "expected_withdraw_quantity",
          "visible": true
        },
        {
          "code": "label_quantity",
          "visible": true
        },
        {
          "code": "units_quantity",
          "visible": true
        },
        {
          "code": "reasons",
          "visible": true
        },
        {
          "code": "destinations",
          "visible": true
        },
        {
          "code": "damage_warning",
          "visible": true
        }
      ]
    }
    ```



