# BACKOFFICE - INSTORE

## Historia del documento

| Fecha      | Descripción                                    | Autor        | Versión | Revisión         |
| :--------- | :--------------------------------------------- | :----------- | :------ | :--------------- |
| 01/12/2020 | Creación de documentos.                        | Filipe Silva | 1.0     |                  |
| 12/12/2020 | Creación de formato de documento Introducción. | Filipe Silva | 1.1     |                  |
| 22/12/2020 | Versión final del documento.                   | Filipe Silva | 2.0     |                  |
| 22/01/2021 | Revisión documental final.                     | Filipe Silva | 2.1     | Helena Gonçalves |

## **Configuraciones y tareas**

Para acceder a la funcionalidad para crear y editar configuraciones, el usuario debe acceder a Instore y seleccionar "Configuración de tareas".

![imagen alt <>](assets/instore_acess.gif)

### **Configuración**

La configuración es un agregador de tareas, es decir, le permite agrupar un número ilimitado de tareas dentro del mismo repertorio. Dado que la instalación se puede configurar para la tienda, todas las tareas existentes dentro de una instalación determinada se configuran automáticamente para las tiendas parametrizadas.

Una configuración consta de:

- **Nombre:** Nombrar la instalación es un parámetro obligatorio. Este nombre, aunque totalmente editable, debe contener información que identifique la estructura jerárquica a la que está asociado así como el tipo de tarea correspondiente.
- **Tipo de tarea:** identifica el tipo de tarea que estaba asociada con la configuración en el momento de la creación. Cada instalación tiene un solo tipo de tarea.
- **Tiendas:** Para cada Setup, es posible definir las tiendas asociadas y solo estas tiendas están sujetas a tareas parametrizadas. En cualquier momento es posible eliminar o agregar tiendas a una configuración determinada.

![imagen alt <>](assets/instore_setup.png)

#### Creación de configuración

Para crear una nueva configuración, el usuario debe acceder al botón "Agregar" en la parte superior derecha de la pantalla.

Luego, se le pide al usuario que defina un nombre para la configuración y que defina el tipo de tarea que desea incluir. Para seleccionar las tiendas asociadas con la configuración, el usuario debe seleccionar "Siguiente".

En este último paso aparece un facilitador de selección de tiendas. En este facilitador, el usuario tiene disponibles las tiendas a las que tiene acceso en el lado derecho de la pantalla y en el lado izquierdo un agregador por grupos de tiendas que permite buscar seleccionando cada grupo.

![imagen alt <>](assets/setup_create.gif)

#### Editar una configuración

Para editar una configuración, el usuario debe seleccionar la configuración deseada haciendo clic en el nombre. En la parte superior de la pantalla, debe seleccionar la opción "Editar configuración".

En la siguiente pantalla, el usuario puede editar los campos "Nombre" y "Tipo de tarea" en Configuración. Después de editar la información, el usuario debe hacer clic en "Siguiente" para acceder a la ventana de selección de tiendas.

El proceso de "Selección de tienda" es el mismo que el proceso explicado en el paso anterior de creación de una configuración.

![imagen alt <>](assets/setup_edit.gif)

#### Eliminar una configuración

Para eliminar una configuración, el usuario debe hacer clic en el símbolo "Opciones" en el lado derecho de la misma. Luego seleccione "Eliminar".

![imagen alt <>](assets/setup_delete.gif)

### **Tareas**

Una vez creada la configuración, el usuario puede crear tareas y asociarlas con configuraciones creadas previamente.

#### Asignación

La Tarea es una regla o conjunto de reglas que determinan la asociación de artículos a través de la estructura de marketing. En otras palabras, cuando se define una regla asociándola con una estructura de marketing específica, significa que todos los artículos de esa estructura se asocian automáticamente con esta Tarea.

#### Creación de tareas

Antes de crear una tarea, es necesario elegir el Setup donde incluiremos la Terfa que queremos crear. Si aún no se ha creado ningún programa de instalación, es necesario [crearlo](bo_instore/#criacao-de-setup) para continuar.

Después de seleccionar Configuración, para crear la tarea, el usuario debe hacer clic en "Agregar tarea".

Inmediatamente, se dirige al usuario al modo "Configuración de tareas". Esta es la ventana de creación de tareas y consta de tres pasos distintos:

- **"Nombre de la tarea":** Campo donde el usuario define el nombre de la tarea, este campo es obligatorio.
- **"Tipo de regla":** define el tipo de regla que se utilizará para formatear la tarea.
- **"Configuración de reglas":** Facilitador para seleccionar el nivel de la estructura de marketing.

Después de definir el nombre de la tarea, la selección del tipo de regla está disponible. Finalmente, el usuario tiene disponible el agregador de estructuras jerárquicas donde el usuario puede navegar por los diferentes niveles y seleccionar uno o más deseados.

![imagen alt <>](assets/task_create.gif)

#### Edición de tareas

Una vez creada la Tarea, el usuario puede, en cualquier momento, editar el nombre o las reglas o incluso eliminar la Tarea creada.

Al hacer clic en las opciones de Tarea, aparece una ventana con la opción "Editar nombre". Este campo no tiene un número máximo de caracteres.

Al final de la edición, el usuario debe hacer clic en "Actualizar nombre" para guardar los cambios realizados.

![imagen alt <>](assets/task_edit.gif)

Además de editar el nombre, también es posible que el usuario agregue nuevas reglas, elimine o edite las existentes. Para ello, debe acceder a los detalles de la tarea y en esta pantalla están disponibles las opciones "Agregar regla", "Editar regla" y "Eliminar".

##### **AGREGAR REGLA**

![imagen alt <>](assets/task_edit_add.gif)

##### **EDITAR REGLA**

![imagen alt <>](assets/task_edit_edit.gif)

##### **ELIMINAR REGLA**

![imagen alt <>](assets/task_edit_delete.gif)

#### Eliminar tarea

El usuario tiene la posibilidad de eliminar una Tarea creada previamente. Cuando elimine una tarea, ya no será parte de la configuración donde se inserta y ya no tendrá preponderancia sobre la creación de las tareas.

Para hacer esto, simplemente haga clic en el símbolo "Opciones" en el lado derecho de la tarea y seleccione la opción "Eliminar".

![imagen alt <>](assets/task_delete.gif)

## **Configuración de la impresora**

El Backoffice de TMR InStore permite al usuario configurar las impresoras existentes en la tienda. Una vez parametrizadas, las impresoras están visibles y disponibles en las funciones de TMR que utilizan la impresión.

### **Listado de impresoras**

Para acceder a la lista de impresoras configuradas previamente, el usuario debe seleccionar Instore y luego seleccionar "Impresoras".

![imagen alt <>](assets/instore_printers.gif)

#### Creación de impresora

Para agregar una nueva impresora, el usuario debe seleccionar la opción "Agregar" presente en la lista de impresoras.

Se le dirigirá inmediatamente a la pantalla de creación de impresoras que contiene los siguientes campos:

- **Nombre: le** permite definir un nombre para la impresora. Este nombre será visible en las aplicaciones móviles como un identificador de impresora.
- **Descripción: le** permite definir una descripción detallada de la impresora.
- **IPV4:** Define la IP de comunicación con la impresora.
- **Puerto:** define el puerto de comunicación con la impresora.
- **Dirección Mac:** establece el identificador de dirección Mac para la impresora. Este campo es único y no se permite crear más de una impresora con la misma dirección Mac.
- **EAN: le** permite definir un EAN para la impresora. Este EAN sirve como facilitador de investigación en la aplicación móvil.
- **Tipo:** desplegable de selección del tipo de etiqueta. Los campos disponibles para la selección son configurables por el cliente.
- **Protocolo:** define el protocolo de comunicación con la impresora.
- **Grupo:** define el grupo asociado a la impresora.

Luego de completar toda la información de este formulario, el usuario debe seleccionar "Siguiente" para proceder a la selección de tiendas. **Nota:** Todos los campos del formulario son obligatorios.

Después de seleccionar la tienda (exclusiva), el usuario debe seleccionar "Guardar" para finalizar el proceso de creación de la impresora.

En caso de que la impresora ya exista, la TMR presenta el siguiente mensaje al usuario:

"Se produjo un error: la impresora con el identificador {mac_address} ya existe en la tienda {store id}".

![imagen alt <>](assets/printer_create.gif)

#### Edición de impresora

El formulario de edición es el mismo que para crear una impresora. Para acceder a este formulario, el usuario debe seleccionar la impresora que desea editar.

![imagen alt <>](assets/printer_edit.gif)

#### Eliminar impresora

Para eliminar una impresora presente en la lista, el usuario debe acceder a las "Opciones" y seleccionar la opción "Eliminar".

También puede eliminar varias impresoras al mismo tiempo. Para ello, seleccione las impresoras deseadas y seleccione la opción "Eliminar seleccionados".

![imagen alt <>](assets/printer_delete.gif)

## **Lista de artículos**

El Backoffice de TMR InStore permite al usuario consultar, crear, editar o eliminar la lista de artículos que se utilizarán para crear tareas en la aplicación móvil.

### **Lista de articulos**

Para acceder a la lista de listas de artículos configuradas previamente, el usuario debe seleccionar Instore y luego seleccionar "Lista de artículos".

![imagen alt <>](assets/item_list.gif)

#### Consulta y edición de artículos

La función Lista de artículos permite al usuario consultar las listas de artículos creadas previamente. Además, también te permite consultar los artículos pertenecientes a una determinada lista.

Para hacer esto, simplemente acceda a la lista que desea consultar seleccionándola de la lista de listas existentes.

En el detalle de la Lista de artículos es posible buscar los artículos existentes y agregar nuevos artículos a la lista.

Para ello, es necesario ingresar la información del artículo en el componente "Artículos" y seleccionar el artículo que regresa de la búsqueda. Después de agregar los elementos deseados, el usuario selecciona "Actualizar".

De la misma manera, es posible eliminar artículos previamente existentes en la lista. Para hacer esto, simplemente seleccione uno o más artículos y elija la opción "Eliminar Seleccionados".

![imagen alt <>](assets/item_list_edit.gif)

#### Crear lista de artículos

Para crear una nueva lista de artículos, el usuario debe seleccionar "Agregar". Se le dirigirá inmediatamente a un formulario en el que deberá completar el "Nombre" y la "Descripción" de la Lista de artículos.

Hacer clic en "Siguiente" lo lleva al formulario para agregar artículos.

En este formulario puede insertar artículos de dos formas diferentes:

- Inserción manual vía EAN o introducción SKU.
- Inserción mediante la carga de un archivo de Excel. Este archivo solo debe contener el sku en la columna A. No se aceptará ningún otro formato de Excel.

![imagen alt <>](assets/item_list_create.gif)

#### Eliminar lista

Para eliminar una Lista de elementos existente, el usuario debe hacer clic en "Opciones" de la lista y seleccionar la opción "Eliminar".

![imagen alt <>](assets/item_list_delete.gif)

## **Lista de verificación**

El Backoffice de TMR InStore permite al usuario consultar, crear, editar o eliminar checklist que luego se utilizará en la aplicación móvil. Una lista de verificación es un conjunto de componentes que se componen de "Pregunta" y "Respuesta". La pregunta es completamente configurable por el usuario y cada pregunta se puede asociar con una respuesta. Hay dos posibles respuestas: "SÍ / NO" y "VERIFICADO / NO VERIFICADO".

### Lista de verificación

Para acceder a la lista de listas de verificación disponibles, el usuario debe seleccionar "Instore" y luego seleccionar la función "Lista de verificación". Inmediatamente se dirige a la lista de listas de verificación disponibles.

![imagen alt <>](assets/checklist.gif)

#### Lista de verificación de consulta y edición

La funcionalidad Lista de verificación le permite consultar la lista de verificación existente y también consultar y editar el contenido correspondiente.

Para ello, el usuario debe seleccionar el checklist deseado y en la parte derecha de la pantalla aparecen los componentes ya en uso. En cada componente puede editar la pregunta y la descripción.

En el lado izquierdo de la pantalla están los componentes que puede usar para editar la lista de verificación. Para hacer esto, simplemente arrastre el componente deseado a la parte derecha de la pantalla y complete la pregunta y descripción del componente.

Al seleccionar actualizar, los componentes agregados y editados se guardan en la lista de verificación en tratamiento.

![imagen alt <>](assets/checklist_edit.gif)

#### Creación de lista de verificación

Para crear una nueva lista de verificación, el usuario debe seleccionar "Agregar". Inmediatamente se le dirige a un formulario donde debe completar el "Nombre" y seleccionar los componentes deseados arrastrándolos de izquierda a derecha de la pantalla.

Al hacer clic en "Siguiente" se accede al formulario de selección de tiendas. Después de seleccionar la tienda deseada, el usuario debe seleccionar "Crear". **Nota:** Si no se selecciona ninguna tienda, la lista de verificación estará disponible para todas las tiendas.

![imagen alt <>](assets/checklist_create.gif)

#### Eliminar lista de verificación

Para eliminar una lista de verificación existente, el usuario debe hacer clic en "Opciones" de la lista de verificación y seleccionar la opción "Eliminar".

![imagen alt <>](assets/checklist_delete.gif)

## **Gestión de la validez**

El Backoffice de TMR InStore permite al usuario consultar, crear, editar o eliminar Parámetros de Validez que influyen en la creación de tareas de Control de Validez. Además de los parámetros de validez, también es posible consultar, editar o eliminar Motivos y Destinos de la Retirada / Pausa. Estos motivos y destinos se utilizarán en la aplicación móvil al manejar las tareas de Retiro y Descanso.

### **Parámetros de validez**

Para acceder a la lista de parámetros de validez, el usuario debe seleccionar "Instore" y luego seleccionar la función "Parámetros de validez" que se encuentra en el agregador "Gestión de validez". Inmediatamente se dirige a la lista de parámetros disponibles.

![imagen alt <>](assets/validity_parameters.gif)

#### Crear parámetro de validez

Para agregar un nuevo parámetro de validez, el usuario debe seleccionar la opción "Agregar" presente en la lista.

Se le dirigirá inmediatamente al formulario de creación de parámetros que contiene los siguientes campos:

- **Estructura jerárquica: le** permite definir la estructura jerárquica de un parámetro determinado.
- **Retiro (días):** Le permite definir el número de días para retirar para un parámetro dado.
- **Depreciación (días): le** permite definir el número de días que se depreciará para un parámetro determinado.
- **Descuento (%): le** permite definir el porcentaje máximo de depreciación para un parámetro dado.

Después de completar el formulario, el usuario debe seleccionar "Guardar".

![imagen alt <>](assets/validity_parameters_create.gif)

#### Editar parámetro de validez

La función Gestión de validez le permite editar los parámetros de validez existentes. Para hacer esto, seleccione el parámetro deseado y edite los datos.

![imagen alt <>](assets/validity_parameters_edit.gif)

#### Eliminar parámetro de validez

La función Gestión de validez le permite eliminar los parámetros de validez existentes. Para hacer esto, haga clic en "Opciones" y seleccione la opción "Eliminar".

![imagen alt <>](assets/validity_parameters_delete.gif)

### **Razones**

Para acceder a la lista de Razones, el usuario debe seleccionar "Razones" que se encuentra en el agregador "Gestión de Validez". Inmediatamente se dirige a la lista de motivos disponibles.

#### Crear razones

Para agregar un nuevo motivo, el usuario debe seleccionar la opción "Agregar" presente en la lista.

Se le dirigirá inmediatamente al formulario de creación de motivos que contiene los siguientes campos:

- **Descripción:** Permite definir la descripción del motivo.
- **Código: le** permite definir el código identificador del motivo.
- **Tipo:** permite definir el tipo de motivo.
- **Estado: le** permite definir el estado del asunto.

![imagen alt <>](assets/reason_create.gif)

#### Editar motivo

La función Gestión de validez le permite editar motivos existentes. Para hacer esto, debe seleccionar el motivo deseado y editar los datos.

![imagen alt <>](assets/reason_edit.gif)

#### Eliminar motivo

La función Gestión de validez le permite eliminar las razones existentes. Para hacer esto, haga clic en "Opciones" y seleccione la opción "Eliminar".

![imagen alt <>](assets/reason_delete.gif)

### **Destinos**

Para acceder a la lista de destinos, el usuario debe seleccionar "Destinos" que se encuentran en el agregador "Gestión de validez". Inmediatamente se dirige a la lista de destinos disponibles.

#### Crear destinos

Para agregar un nuevo destino, el usuario debe seleccionar la opción "Agregar" presente en la lista.

Se le dirigirá inmediatamente al formulario de creación de destino que contiene los siguientes campos:

- **Descripción:** Le permite definir la descripción del destino.
- **Código:** Le permite definir el código identificador del destino.
- **Tienda:** permite definir las tiendas asociadas al destino.
- **Estado: le** permite definir el estado del destino.

![imagen alt <>](assets/destinations_create.gif)

#### Editar destino

La función Gestión de validez le permite editar destinos existentes. Para hacer esto, seleccione el destino deseado y edite los datos.

![imagen alt <>](assets/destinations_edit.gif)

#### Eliminar destino

La función Gestión de validez le permite eliminar destinos existentes. Para hacer esto, haga clic en "Opciones" y seleccione la opción "Eliminar".

![imagen alt <>](assets/destinations_delete.gif)