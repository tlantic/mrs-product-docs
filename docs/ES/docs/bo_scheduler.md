# BACKOFFICE - CALENDARIO

![imagen alt <>](assets/scheduler.png)

## Historia del documento

| Fecha      | Descripción                                    | Autor        | Versión | Revisión         |
| :--------- | :--------------------------------------------- | :----------- | :------ | :--------------- |
| 01/12/2020 | Creación de documentos.                        | Filipe Silva | 1.0     |                  |
| 12/12/2020 | Creación de formato de documento Introducción. | Filipe Silva | 1.1     |                  |
| 22/12/2020 | Versión final del documento.                   | Filipe Silva | 2.0     |                  |
| 22/01/2021 | Revisión documental final.                     | Filipe Silva | 2.1     | Helena Gonçalves |

## **CALENDARIO - Vista de tareas**

Para acceder a la función Calendario, el usuario debe acceder a "Calendario" y seleccionar "Calendario".

![imagen alt <>](assets/scheduler_scheduler.gif)

El módulo Calendario de BackOffice TMR le permite crear nuevos horarios y consultar los calendarios de creación de tareas y expiración previamente creados.

### **Vista de mes**

El módulo Calendario muestra los horarios en la vista Mes de forma predeterminada. Cuando el usuario accede a esta funcionalidad, los horarios se presentan en una vista mensual donde, a través de subtítulos, puede comprender todos los horarios creados para el mes actual.

Los subtítulos corresponden a puntos de colores ubicados en la parte inferior de cada día específico. La leyenda del juego de colores siempre está presente en la parte izquierda del calendario.

Además de los subtítulos, el Calendario muestra el día actual con un subrayado en un color diferente para que el usuario pueda identificar más rápidamente los horarios hechos para el día actual.

![imagen alt <>](assets/scheduler_monthly_view.png)

### **Vista de semana**

El módulo Calendario también le permite mostrar horarios en la vista Semana. Esta vista presenta la semana actual y todas las citas realizadas en ella. En esta vista es posible ver los horarios con más detalle dado que la vista de semana presenta la escala de horas en cada día de la semana. De esta forma, las citas se ven agravadas por la hora de inicio.

El juego de colores permanece activo como leyenda, pero en la vista de semana se muestra inmediatamente el nombre del horario, además del color correspondiente al tipo y tarea.

![imagen alt <>](assets/scheduler_week_view.png)

### **Vista del día**

El módulo Calendario también permite visualizar los horarios en la vista Día. En esta vista es posible consultar los horarios con más detalle. La escala de horas y la leyenda de colores mantienen el mismo comportamiento que se muestra en las vistas anteriores.

![imagen alt <>](assets/scheduler_daily_view.png)

### **Selección de tienda**

El módulo Calendario TMR le permite visualizar horarios por tienda o grupo de tiendas previamente definido. De forma predeterminada, todas las tiendas están seleccionadas. Para restringir el grupo de tiendas a mostrar, el usuario debe acceder al facilitador de selección presente en la esquina superior derecha de la pantalla y luego seleccionar las tiendas deseadas.

![imagen alt <>](assets/scheduler_store_select.gif)

## **CALENDARIO - Programación de vencimiento**

### **Creación de horarios**

El módulo Calendario de TMR le permite programar eventos de vencimiento de tareas. Para crear un programa de vencimiento, el usuario debe seleccionar la entrada (+) presente en el calendario. Un formulario está disponible de inmediato con los siguientes campos obligatorios para completar:

- **Título: le** permite definir el nombre del horario.

- Fecha y hora: le

   permite establecer la fecha y la hora para el inicio de la programación. También es posible definir una recurrencia seleccionando "Cambiar a frecuencia". En la programación por frecuencia, es posible definir la siguiente recurrencia:

  - **Horas:** establece el minuto cada hora para que comience la expiración.
  - **Días:** establece la hora de cada día para que comience el vencimiento.
  - **Semana:** establece el día de cada semana para comenzar a expirar.
  - **Mes:** establece el día de cada mes para comenzar a expirar.

- **Intervalo en días: le** permite definir el número de días transcurridos para seleccionar tareas que vencen. Por ejemplo, si el usuario selecciona "Inicio: -5", significa que todas las tareas creadas con 5 días en el pasado expirarán según este programa.

- **Tiendas: le** permite definir las tiendas que estarán cubiertas por el horario.

- **Tipo de tarea: le** permite definir el tipo de tareas que vencerán.

Luego de completar todos los campos, el botón "CREAR" estará disponible y al seleccionar esta opción, se creará el horario y estará disponible en el calendario para su consulta.

![imagen alt <>](assets/scheduler_create_expiration.gif)

### **Programar edición**

El módulo Calendario TMR le permite editar horarios creados previamente. Para ello, el usuario debe seleccionar el horario que desea editar haciendo clic en el nombre del horario. El formulario similar al formulario de creación se abre inmediatamente, con los campos previamente rellenados.

El usuario debe cambiar la información de los campos que quiere editar y al final seleccionar "Actualizar" para guardar la información actualizada.

![imagen alt <>](assets/scheduler_edit_expiration.gif)

### **Eliminando el horario**

El módulo Calendario TMR le permite eliminar horarios creados previamente. Para hacer esto, el usuario debe buscar el horario que desea eliminar y hacer clic en la (X) en el lado izquierdo del nombre del horario. Inmediatamente aparece la siguiente alerta:

**"¿Estas seguro que quieres borrarlo?"**

Si el usuario selecciona "SÍ", la programación se elimina inmediatamente. Una vez eliminado, no es posible revertir esta acción.

![imagen alt <>](assets/scheduler_delete_expiration.gif)

## **CALENDARIO - Programación de tareas**

### **Creación de horarios**

El módulo Calendario TMR le permite programar eventos de creación de tareas. Para crear un cronograma de creación de tareas, el usuario debe seleccionar la entrada (+) presente en el calendario. Un formulario está disponible de inmediato con los siguientes campos obligatorios para completar:

- **Título: le** permite definir el nombre del horario.
- **Tipo de tarea: le** permite definir el tipo de tarea para programar. Esta selección está disponible en el formato de pestaña presente justo después de ingresar el nombre de la cita. Las tareas disponibles son configurables por el cliente.
- **Fecha y hora: le** permite establecer la fecha y la hora para el inicio de la programación. También es posible definir una recurrencia seleccionando "Cambiar a frecuencia". En la programación por frecuencia, es posible definir la siguiente recurrencia:
  - **Horas:** establece el minuto cada hora para la creación de tareas.
  - **Días:** define la hora cada día para la creación de tareas.
  - **Semana:** establece el día de cada semana para la creación de tareas.
  - **Mes:** define el día de cada mes para la creación de tareas.
- **Tiendas: le** permite definir las tiendas que estarán cubiertas por el horario.
- **Estructura jerárquica:** permite definir el nivel de la estructura jerárquica que corresponde a los artículos que se incluirán en la tarea.
- **Tiene elementos:** Casilla de verificación que define si la tarea se creará con artículos o vacía.
- **Lista de artículos:** permite definir una lista de artículos que se incluirán en la tarea en el momento de su creación.
- **Zonas: le** permite definir el número de zonas que se incluirán al crear la tarea. De esta forma, se creará una tarea de agregación con tareas / zonas en su contenido.
- **Ubicación:** menú desplegable que le permite definir la ubicación de la tarea.

Luego de completar todos los campos, el botón "CREAR" estará disponible y al seleccionar esta opción, se creará el horario y estará disponible en el calendario para su consulta.

![imagen alt <>](assets/scheduler_create_task.gif)

### **Programar edición**

El módulo Calendario TMR le permite editar horarios creados previamente. Para ello, el usuario debe seleccionar el horario que desea editar haciendo clic en el nombre del horario. El formulario similar al formulario de creación se abre inmediatamente, con los campos previamente rellenados.

El usuario debe cambiar la información de los campos que quiere editar y al final seleccionar "Actualizar" para guardar la información actualizada.

![imagen alt <>](assets/scheduler_edit_expiration.gif)

### **Eliminando el horario**

El módulo Calendario TMR le permite eliminar horarios creados previamente. Para hacer esto, el usuario debe buscar el horario que desea eliminar y hacer clic en la (X) en el lado izquierdo del nombre del horario. Inmediatamente aparece la siguiente alerta:

**"¿Estas seguro que quieres borrarlo?"**

Si el usuario selecciona "SÍ", la programación se elimina inmediatamente. Una vez eliminado, no es posible revertir esta acción.

![imagen alt <>](assets/scheduler_delete_expiration.gif)

## **Aplicar los cambios**

Después de realizar cualquier cambio en los horarios (creación, edición y eliminación), siempre es necesario aplicar los cambios realizados. Si el usuario no aplica cambios, todas las acciones realizadas anteriormente no tendrán efectos prácticos en el cronograma.

Para aplicar cambios, el usuario debe seleccionar la etiqueta presente en la esquina superior derecha de la pantalla con el nombre "Aplicar cambios". Esta etiqueta muestra el total de cambios realizados y los que deben aplicarse. Al seleccionar "Aplicar cambios", aparece el siguiente mensaje:

**"¿Está seguro de que desea aplicar los cambios?"**

Si el usuario selecciona "SÍ", los cambios se aplican inmediatamente y aparece la siguiente alerta de confirmación:

"Operación realizada con éxito".