# BACKOFFICE - USUARIOS

![imagen alt <>](assets/bo_users.png)

## Historia del documento

| Fecha      | Descripción                                    | Autor        | Versión | Revisión         |
| :--------- | :--------------------------------------------- | :----------- | :------ | :--------------- |
| 01/12/2020 | Creación de documentos.                        | Filipe Silva | 1.0     |                  |
| 12/12/2020 | Creación de formato de documento Introducción. | Filipe Silva | 1.1     |                  |
| 22/12/2020 | Versión final del documento.                   | Filipe Silva | 2.0     |                  |
| 22/01/2021 | Revisión documental final.                     | Filipe Silva | 2.1     | Helena Gonçalves |

## **Gestión de usuarios**

Para acceder a la funcionalidad "Gestión de usuarios", el usuario debe acceder a "Usuarios" y seleccionar "Gestión de usuarios".

![imagen alt <>](assets/users_users.gif)

El módulo de Gestión de Usuarios de BackOffice TMR le permite crear, crear nuevos usuarios, consultar, editar y eliminar usuarios existentes.

### **Consulta y edición de usuarios**

El módulo de Gestión de usuarios le permite buscar usuarios creados anteriormente. Para hacer la búsqueda de usuarios más efectiva, TMR le permite buscar usuarios por nombre y también le permite buscar usuarios consultando la lista de usuarios existentes.

Para buscar por nombre, el usuario debe hacer clic en la lupa en la parte superior de la pantalla y poner el nombre del usuario en el cuadro de texto. Los resultados se presentarán en una lista en la parte inferior de la barra de búsqueda.

Para ver los detalles de los resultados de la búsqueda, el usuario debe seleccionar la línea correspondiente e inmediatamente se abre el modal con la información del usuario.

Los campos presentes en este modal son editables y la información de usuario modificada se actualiza.

![imagen alt <>](assets/users_search.gif)

Si el usuario desea consultar a todos los usuarios creados, el usuario debe hacer clic en el botón "Lista de usuarios". De esta forma, todos los usuarios creados se mostrarán en la lista. De esta forma también es posible consultar / editar la información del usuario seleccionando al usuario de la lista presentada.

![imagen alt <>](assets/users_search_list.gif)

### **Creación de usuarios**

El módulo de Gestión de usuarios le permite crear nuevos usuarios que se utilizarán para acceder a la aplicación móvil y al Backoffice. Para ello, el usuario debe seleccionar "Agregar" y aparece un formulario con los siguientes campos a ser llenados: * **Nombre:** Permite definir el nombre del usuario. Esta información se presenta como identificación del usuario en la aplicación móvil. * **Usuario: le** permite definir el identificador de usuario utilizado en el inicio de sesión de TMR. * **Contraseña:** Le permite definir la contraseña del usuario. * **Dirección de correo electrónico: le** permite definir el correo electrónico asociado con el usuario. * **Estado:**Le permite definir el estado del usuario. Los estados disponibles son "Activo" e "Inactivo". Si el usuario está inactivo, no podrá iniciar sesión en la TMR.

Después de completar los campos del formulario, el usuario debe hacer clic en "Siguiente". De esta manera se le llevará al formulario de selección de tienda. Una vez seleccionada, la tienda debe seleccionar "Crear" para guardar los cambios realizados.

![imagen alt <>](assets/users_create.gif)

### **Cambiar la contraseña**

Para cambiar la contraseña de un usuario, seleccione las opciones en el lado derecho de la línea del usuario y seleccione la opción "Actualizar contraseña". En este modo, el usuario debe insertar la nueva contraseña asociada.

![imagen alt <>](assets/users_edit_password.gif)

### **Borrar usuario**

Para eliminar un usuario en particular, seleccione las opciones en el lado derecho de la línea del usuario y seleccione la opción "Eliminar". El siguiente mensaje aparece inmediatamente:

**"¿Está seguro de que desea eliminar al usuario?"**

Si selecciona "Sí", el usuario se elimina inmediatamente.

![imagen alt <>](assets/users_delete.gif)
