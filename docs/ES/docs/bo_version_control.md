# BACKOFFICE - CONTROLO DE VERSÃO

<!--![image alt <>](assets/under_construction.png){:height="500px" width="500px"}


- Manual Suporte:
    - Arquitetura: support_arquiteture.md
    - Especificidades técnicas dos clientes: support_clients.md
    - Módulo de Validades: modulo_de_validades.md

- BackOffice | Controlo de Versão: bo_version_control.md
- Workflow de tarefas: workflow.md
- Cockpit: cockpit.md-->

![image alt <>](assets/version_control.png)

## Histórico do documento

| Data      | Descrição    | Autor | Versão | Revisão
| ------------- | ---------------------------------------------------------------------------------------- | ---------- | ---------- | -------- | 
| 01/12/2020         | Criação do documento.    |  Filipe Silva  |   1.0   |     |
| 12/12/2020     | Criação de Introdução de formatação de documento.       | Filipe Silva  |  1.1     |      |
| 22/12/2020     | Versão final do documento.       | Filipe Silva  |  2.0    |      |
| 22/01/2021     | Revisão final do documento.       | Filipe Silva  |  2.1    |  Helena Gonçalves|

## **CONTROLO DE VERSÕES**

Para aceder à funcionalidade de "Controlo de Versões", o utilizador deve aceder a "Controlo de Versões" e selecionar "Versões".

![image alt <>](assets/version_control_versions.gif)

O módulo Controlo de Versões do BackOffice TMR permite criar criar novas versões, consultar, editar e eliminar versões existentes.

### **Adiconar nova versão**

O módulo de Controlo de Versões permite criar novas versões da aplicação móvel nas diferentes plataformas e ambientes disponíveis. Estas novas versões servirão para atualização das aplicações móveis mediante um grupo de lojas previamente definido. Desta forma, é possível que o mesmo cliente tenha lojas em versões diferentes da aplicação móvel.

Para adiconar uma nova versão, o utilizador deverá selecionar "ADICIONAR" no canto superior direito do ecrã. De imediato será direcionado para o formulário onde deverá preencher toda a informação necessária para criação da nova versão:

* **Nome:** Define o identificador da versão.
* **Plataforma:**  Define a plataforma da nova versão criada.
* **Ambiente:** Define o ambiente em que a versão será criada.
* **Anexar Ficheiro:** Facilitador que permite o upload do ficheiro da versão e que será posteriormente enviado para a aplicação móvel para respetiva atualização.

Depois de preenchido todos os campos do formulário o utilizador deverá clicar em "Criar" para realizar o upload do ficheiro da nova versão. Após upload com sucesso fica disponível o botão "Fechar".

![image alt <>](assets/version_control_create_version.gif)

### **Consultar, editar e eliminar versão**

O módulo de Controlo de Versões permite consultar, editar e eliminar versões previamente criadas.

Para consultar as versões existentes, o utilizador deve selecionar "versões" na parte esquerda do ecrã e de imediato é apresentada uma listagem com as versões disponíveis. Nesta listagem está presente a informação do nome, plataforma e ambiente correspondente a cada versão. Além desta informação é possível também filtrar as versões apresentadas pela plataforma e pelo ambiente.

![image alt <>](assets/version_control_search.png)

Para editar uma versão existente, o utilizador deve pesquisar a versão que pretende editar e de seguida clicar no nome. De forma imediata é direcionado para o formulário de edição de versão onde o utilizador pode eliminar o ficheiro adicionado anteriormente e adiciona novos ficheiros à versão.

Depois de editados todos os campos desejados, o utilizador deve selecionar "Atualizar".

![image alt <>](assets/version_control_edit.png)

Para eliminar uma versão existente, o utilizador deve pesquisar a versão que pretende eliminar e selecionar as opções presentes na parte direita da versão. De seguida deve selecionar "Apagar". Surge de imediato a pergunta de confirmação à qual o utilizador deve responder com "Sim".

![image alt <>](assets/version_control_delete.gif)


## **GRUPOS DE LOJA**

Para aceder à funcionalidade de "Grupos de lojas", o utilizador deve aceder a "Controlo de Versões" e selecionar "Grupos de lojas".

![image alt <>](assets/version_control_stores.gif)


O módulo Controlo de Versões do BackOffice TMR permite criar criar novos grupos de lojas, consultar, editar e eliminar os grupos de lojas existentes.

### **Criação Grupo de Lojas**

O módulo de Controlo de Versões permite criar novos grupos de lojas nos vários ambientes disponíveis. Estes grupos de lojas definem a que lojas determinada versão será atribuída. Desta forma é possível associar versões diferentes a grupos de lojas distintos.

Antes de criar um novo grupo de lojas é necessário criar a versão previamente. No final do processo de criação de grupos de lojas o utilizador irá associar uma versão e por este motivo esta tem de ser criada antecipadamente.

Para criar um novo grupo de lojas, o utilizador deve selecionar adicionar e de imediato será disponibilizado um formulário que contém os seguintes campos:

* **Nome:** Define o identificador do grupo de lojas
* **Código organização:** Define o cliente para associação de grupo de lojas
* **Endereço V4:** Define o endereço utilizado pela aplicação móvel para fazer pesquisa de novas versões. Utilizado para aplicação móvel híbrida v4 e v5.
* **Endereço V5:** Define o endereço utilizado pela aplicação móvel para fazer pesquisa de novas versões. Utilizado pela aplicação pura V5.
* **Endereço Secundário:** Define o endereço alternativo.
* **Ambiente:** Define o ambiente para o grupo de lojas.

Após preencher todos os campos do formulário, o utilizador deverá selecionar "Próximo" para avançar para o formulário de seleção de lojas.

Por último deverá selecionar "Próximo" para selecionar a versão que pretende adicionar ao grupo de lojas. Neste formúlário existe o campo "Atualização Obrigatória". Este campo define se a atualização da versão é mandatória na aplicação móvel, ou seja, se for selecionada a opção "Atualização Obrigatória" não é possivel cancelar na aplicação móvel o processo de atualização.

![image alt <>](assets/version_control_stores_create.gif)

### **Consultar, editar e eliminar grupos de lojas**

O módulo de Controlo de Versões permite consultar, editar e eliminar grupos de lojas previamente criadas.

Para consultar os grupos de lojas existentes, o utilizador deve selecionar "Grupos de Lojas" na parte esquerda do ecrã e de imediato é apresentada uma listagem com os grupos de lojas disponíveis. Nesta listagem está presente a informação do nome, organização, ambiente, endereços e lojas correspondente a cada grupo de lojas. Além desta informação é possível também filtrar as versões apresentadas pelo ambiente.

Para editar um grupo de loja existente, o utilizador deve pesquisar o grupo de lojas que pretende editar e de seguida clicar no nome. De forma imediata é direcionado para o formulário de edição de versão onde o utilizador pode editar todos os campos previamente preenchidos no momento da criação. 

![image alt <>](assets/version_control_stores_edit.png)

Para eliminar um grupo de lojas existente, o utilizador deve pesquisar o grupo que pretende eliminar e selecionar as opções presentes na parte direita da versão. De seguida deve selecionar "Apagar". Surge de imediato a pergunta de confirmação à qual o utilizador deve responder com "Sim". 

![image alt <>](assets/version_control_stores_delete.gif)

## **DISPOSITIVOS**

Para aceder à funcionalidade de "Dispositivos", o utilizador deve aceder a "Controlo de Versões" e selecionar "Dispositivos".

![image alt <>](assets/version_control_devices.gif)

O módulo Controlo de Versões do BackOffice TMR permite consultar a listagem de dispositivos presente em cada ambiente.

### **Listagem de dispositivos**

A listagem de dispositivos permite consultar o total de dispositivos em funcionamento com a plaicação móvel do TMR. Além dos total de dispositivos é possível também visualizar na listagem existente a plataforma usada no dispositivo, o enderço mac que é o identificador do dispositivo e a organização em que o dispositivo está a ser utilizado. 

![image alt <>](assets/version_control_devices_list.png)
