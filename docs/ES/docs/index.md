# Guía de autonomía

### Bienvenido a la Guía de soporte de Tlantic



El propósito de esta guía es informar los pasos necesarios para el uso correcto de las soluciones y servicios SAAS de Tlantic. Las acciones descritas aquí pueden ser realizadas por socios, equipos de entrega y soporte. 

!!! abstract "Soluciones cubiertas en esta guía"



<table width="40%" border="0" cellspacing="10" cellpadding="4" bgcolor="YELLOW">
   <caption><b></b></caption>
   <tr align="center">
      <td align="center"><a href="#"> <img src="https://www.tlantic.com/assets/public/images/solutions/tlantic-solution-mobile-retail-icon.png" width="100""/></a> <td align="center"><a href="#"> <img src="https://www.tlantic.com/assets/public/images/solutions/tlantic-solution-workforce-management-icon.png" width="100""/></td></a> <td align="center"><a href="#"><img src="https://www.tlantic.com/assets/public/images/solutions/tlantic-solution-store-sales-service-icon.png" width="100""/></a></td align="center">  <td align="center"><a href="#"><img src="https://www.tlantic.com/assets/public/images/home/tlantic-digital-store-processes.png" width="100""/></td></a> 
   </tr>
   <tr align="center"  bgcolor="e9e9e9">
      <td>Tlantic Mobile Retail</td> <td>Tlantic Workforce Management</td> <td>Tlantic Store Sales Services</td><td>Tlantic SmartTask</td>
   </tr>
</table>




    
<br>
<br>
