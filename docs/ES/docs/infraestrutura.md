# Infraestructura

| **REQUISITO DE INFRAESTRUCTURA**                           | **RECOMENDACIÓN MÍNIMA**                                     |
| :--------------------------------------------------------- | :----------------------------------------------------------- |
| Ancho de banda: Store <-> Cloud                            | Desde 600 kbps                                               |
| Ancho de banda: Almacenar <-> Servicios de stock y precios | Desde 600 kbps                                               |
| Cobertura en tienda y depósito                             | - Puede ser parcial ya que el sistema tiene la posibilidad de trabajar en modo offline. - Sin embargo, es necesario tener algunos puntos con una red para sincronizar la información. - Además, para tener consulta de inventario en línea, se debe garantizar la cobertura de WI FI en toda el área de ventas donde se va a activar esta funcionalidad. |
| Cobertura y compatibilidad inalámbrica                     | Colectores x impresoras, alertas de itinerancia              |

### Requisitos informáticos

| **FABRICANTE** | **TLANTIC - OBS.**                                           |
| :------------- | :----------------------------------------------------------- |
| Indiferente    | Capaz de ejecutar el navegador Chrome por encima de la versión 89.0 |
|                |                                                              |

### Requisitos de entrada

| **PROVEEDOR** | **TLANTIC - OBS.**                                           |
| :------------ | :----------------------------------------------------------- |
| Indiferente   | El cliente también debe proporcionar etiquetas de impresión de precios. |