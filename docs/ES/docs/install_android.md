# Guía de instalación de Android 

##### 1 | ACCEDER AL REPOSITORIO DE APLICACIONES

Accede a tu panel de control de Tlantic y descarga el instalador. Este acceso y descarga al panel de control de Tlantic se puede realizar directamente desde el Colector a través del navegador o por computadora, y luego transferir el instalador al colector.

![imagen alt <>](/assets/install_android/1-android%20%281%29.png)

##### 2 | MODELO DE DISPOSITIVO

Seleccione la carpeta "Android"

![imagen alt <>](/assets/install_android/1-android%20%282%29.png)



##### 3 | SELECCIONE LA ÚLTIMA VERSIÓN

Seleccione el archivo APK con el número de versión más alto y luego se descargará

![imagen alt <>](/assets/install_android/1-android%20%283%29.png)



##### 4 | LOCALIZAR LA DESCARGA
Abra la carpeta Descargas del dispositivo

![imagen alt <>](/assets/install_android/1-android%20%284%29.png)



##### 5 | INSTALACIÓN DE LA APLICACIÓN

Selecciona el APK y confirma la instalación:

![imagen alt <>](/assets/install_android/1-android%20%285%29.png)



##### 6 | INSTALACIÓN EN PROGRESO

Elija la carpeta para instalar y haga clic en Aceptar

![imagen alt <>](/assets/install_android/1-android%20%286%29.png)



##### 7 | INSTALACIÓN TERMINADA

Una vez instalado, haga clic en "Abrir" para abrir la TMR

![imagen alt <>](/assets/install_android/1-android%20%287%29.png)



##### 8 | VALIDAR LA APLICACIÓN INSTALADA

Aparecerá la pantalla de inicio de sesión

![imagen alt <>](/assets/install_android/1-android%20%288%29.png)