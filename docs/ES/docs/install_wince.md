# Guía de instalación para Windows CE


**Atención** : es necesario instalar los requisitos previos antes de continuar con el paso a paso a continuación. Instale los componentes de acuerdo con su versión del sistema.

##### Requisitos previos de instalación
!!! info "Componentes"
    - **Wince 6 o Superior**
    
        [NETCFv35.Messages.EN.cab](/assets/install_wince/pre-requirements/NETCFv35.Messages.EN.cab)

    - **Wince 5 o Inferior**
    
        [NETCFv35.Messages.EN.cab](/assets/install_wince/pre-requirements/NETCFv35.Messages.EN.cab)

        [NETCFv2.wce4.ARMV4.cab](/assets/install_wince/pre-requirements/NETCFv2.wce4.ARMV4.cab)

        [NETCFv2.wce5.armv4i.cab](/assets/install_wince/pre-requirements/NETCFv2.wce5.armv4i.cab)

        [System_SR_ENU.CAB](/assets/install_wince/pre-requirements/System_SR_ENU.CAB)
        

##### Requisitos mínimos de hardware



???+ example "Coletor:"
    * **Sistema Operativo:** Windows CE 4.2 ou superior.
    * **Memória ROM:** 60MB ou superior. 
    * **Lector de códigos de barras:** Lector 1D - LÁSER
    * **WI-FI:** 802.11 B/G/N.
    * **Tamaño de pantalla recomendado:** 3” o más.
    * **Resolución mínima recomendada:**  QVGA 320 x 240.
        
**nota**: el dispositivo debe estar aprobado. ver la [lista](/eqpto_homologado/) 
<br>

#### Instalación paso a paso


    .
##### 1 | Acceder al repositorio de aplicaciones
Accede a tu panel de control de Tlantic y descarga el instalador.
Este acceso y descarga al panel de control de Tlantic se puede realizar directamente desde el Colector a través del navegador *Internet Explorer* o por computadora y luego transferir el instalador al colector.
   

![image alt <>](assets/install_wince/1-wince.png)

    
<br>

##### 2 | Modelo de dispositivo
Elija el modelo de su dispositivo y descargue la última versión:
    

![image alt <>](assets/install_wince/2-wince.png)


<br>

##### 3 | Seleccione la última versión
Seleccione la última versión
    

![image alt <>](assets/install_wince/3-wince.png)


<br>

##### 4 | Indique la ubicación de la instalación
Elija "Ejecutar este programa desde su ubicación actual" y haga clic en Aceptar
    

![image alt <>](assets/install_wince/4-wince.png)

<br>

##### 5 | Descarga de la APLICACIÓN
Se realizará la descarga
    

![image alt <>](assets/install_wince/5-wince.png)

<br>

##### 6 | Instalación de la APLICACIÓN
Elija la carpeta para instalar y haga clic en Aceptar

![image alt <>](assets/install_wince/6-wince.png)

<br>

##### 7 | Instalación en progreso
Se realizará la instalación

![image alt <>](assets/install_wince/7-wince.png)

<br>

##### 8 | Validar la APLICACIÓN instalada
El icono se creará en el escritorio.

![image alt <>](assets/install_wince/8-wince.png)