
!!! info  "Áreas"

      [Manual de instalación](../install_wince) <br>
      [Manual de usuario](../multiplataforma) <br>
      [Manual del administrador](../bo_instore) <br>
      [Manual de parametrización](../configuracao_avancada) <br>
      [Integración](http://mrs-docs.s3-eu-west-1.amazonaws.com/index.html) <br>
      [Equipo aprobado](../eqpto_homologado) <br>
      [Notas de la versión](../MP_change)

  
## Módulos que compõem o TMR
[comment]: # (### Ficha de Produto)

!!! abstract "Flujos de precios"
     - Auditoría de precios
     - Precio rebajado
     - Impresión de etiquetas

!!! abstract "Flujos de existencias"
     - Recepción
     - traslados
     - Devolución
     - recuentos
     - inventarios

!!! abstract "Flujos de reemplazo"
     - Auditoría de interrupciones
     - Separación de mercancías
     - Reposición de artículos
     - Reemplazo por ventas
     - Reemplazo urgente

!!! abstract "Flujos de calidad"
     - Lista de verificación para control y verificación

!!! abstract "Flujos de surtido"
     - Auditoría de surtido

!!! abstract "Cockpit"
     - Tablero operativo





