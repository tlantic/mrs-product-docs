# Modelo de implementación

#### Modelo SAAS

Nuestra oferta de soluciones de movilidad implica el hospedaje de la plataforma MRS en Cloud (modelo SaaS), monitoreada por Tlantic para garantizar siempre el mejor desempeño. Sin embargo, es posible permanecer en Private Cloud o en la infraestructura del cliente.

En el modelo SaaS, Tlantic se encarga de toda la estructura necesaria para la disponibilidad del sistema (servidores, conectividad y seguridad de la información), así como de su gestión y mantenimiento. Por tanto, el cliente no necesita crear su propia estructura y formar profesionales para mantener el sistema en funcionamiento.

Este modelo permite un enfoque de implementación gradual (escalabilidad): el cliente puede ajustar de acuerdo a sus necesidades, ya sea el número de tiendas a soportar por el sistema o las funcionalidades a las que quiere acceder.

Adicionalmente, en este modelo, el cliente tiene acceso a la evolución funcional de los módulos que alquila así como a los servicios de soporte de 3ª línea.

El cliente utiliza el software a través de Internet, pagando una tarifa mensual por el servicio ofrecido.



#### Metodología de adopción de proyectos

![imagen alt <>](assets/metodologia/metodologia_projeto.jpg)



#### Metodología de adopción para avanzar hacia la sostenibilidad

![imagen alt <>](assets/metodologia/suporte.png)