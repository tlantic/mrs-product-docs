# **Módulo de Gestão de Validades**

É um Módulo de Servidor para gerir Datas de Validades: em vez de criar logo as tarefas, trata as datas num modelo à parte e despoleta a criação das tarefas um momento mais próximo da sua efetivação, mediante a parametrização.

### **Funções Principais**
- Contenção datas módulo validades;
- Geração de tarefas com base no módulo e nas configurações;
- Gestão de datas futuras (criação e eliminação);
- Backoffice - migração parâmetros.

### **Beneficio**
- Rastreabilidade do ciclo de vida do artigo/Data de Validade;
- Redução do volume de dados em MRS com um menor número de datas de validade criadas em MRS (uma vez que grande parte é eliminada  processo de tratamento da Gestão de Datas Futuras);

### **Componentes envolvidos**
- Server
- Apps WinCE e Multiplataforma


### **Requisitos técnicos**
- Criar tabela Validitycheck 


## **Parametrização**

**Core – Settings**:  ``agent-tasks-task-creating-flow`` 

!!! note "Exemplo"
    ```json
    {
        "contention": {
		    "VALIDITYCHECK": {
			    "days": 60,
			    "mode": "full"
		    }
	    },
    }
    ```

Parâmetro a 60 dias – Significa que irá criar tarefas de Controlo de Validade com os artigos/data de validade, cujos parâmetros de Validade se encontrem dentro dos 60 dias.

**Scheduler** - Jobs com ocorrência diária:

**Template**: ``create_validitycheck_task``

Este Job vai validar todos os dias na tabela ``validity_checks`` se tem artigos/datas de validade no estado **Pending** dentro do parâmetro de contenção. 
Na tabela ``Jobs_params``, o **code:** limit_date com o **value:** 100, corresponde ao nr de dias para a frente que o Job vai considerar as validades, a partir do dia atual. 

## **BD** - Validitycheck: validity_checks

Na tabela ``validity_checks`` vão ficar registados todos os artigos com todas as Datas de Validade eviadas via Integração ou Registo no MRS. 

???+ example "Colunas tabela validity_checks:"
    - **ID**: Ttodos os registos ficam com o ID associado;
    - **Org_code**: Organização;
    - **Stauts**: Pending (não tem tarefa criada), Processed (tem tarefa criada);
    - **Store**: Loja;
    - **Sku**: Artigo;
    - **Validity_date**: Data de Validade do Artigo;
    - **Scheduled_date**: Data em que foi ou vai ser criada a tarefa;
    - **Correlation_id**: Campo que permite tracking do processo associado à criação da validade;
    - **Created_at**: Data de criação do registo na tabela;
    - **Created_by**: User que criou o registo;
    - **Updated_at**: Data da ultima atualização;
    - **Updated_by**: User que atualizou o registo;
    - **Metadata**: Dados do artigo – Sku, data de validades, parâmetros de validade, etc;
        {"sku": "", "date": "2020-03-25T00:00:00Z", "type": "adhoc", "ad_hoc": null, "alert_id": null, "quantity": 2, "sequence": null, "withdrawal": 9, "depreciation": 30, "max_percentage_discount": "50"}
    - **Task_id**: Id da tarefa criada;

## **BD** - Validitycheck: tasks

Na tabela ``Tasks`` ficam registados todos os Artigos/Data de Validade que na tabela ``validity_checks`` estão com o status **Processed**. 
Nesta Tabela fica o registo do **ID** da Task Criada, o respetivo **Status** e o **date_type** (Depreciação ou Retirada). 

???+ example "Colunas tabela tasks:"
    - **Sku**: Artigo;
    - **Store**: Loja;
    - **Org_code**: Organização;
    - **Validity_date**: Data de Validade do Artigo;
    - **Created_at**: Data de criação do registo na tabela;
    - **Updated_at**: Data da ultima atualização;
    - **Updated_by**: User que atualizou o registo;
    - **Task_id**: Id da tarefa criada;
    - **Date_type**: Tipo de data (DEPRECIATION ou WITHDRAWAL);
    - **Status**: Estado da tarefa.

## **BD** - Validitycheck: Skus

Na tabela ``Skus`` ficam registados os artigos com a respetiva informação (ean, sku, eans, name,description, hierarchy_category).

!!! note "Exemplo Metadata:"
    ```json 
    {
    "ean": "5601126003453", 
    "sku": "2004033", 
    "eans": ["5601126039049"],
    "name": "Café Sical C", 
    "description": "Café Sical C", 
    "hierarchy_category": ["10", "10.14", "10.14.3", "10.14.3.1", "10.14.3.1.1"]
    }
    ```

!!! nota
    Posteriormente, o metadata é utilizado na integração dos artigos. 

???+ example "Colunas tabela skus:"
    - **Org_code**: Organização;
    - **Sku**: Artigo;
    - **Created_at**: Data de criação do registo na tabela;
    - **Created_by**: User que criou o registo;
    - **Updated_at**: Data da ultima atualização;
    - **Updated_by**: User que atualizou o registo;
    - **Metadata**: Informação do artigo.

## **BD** - Validitycheck:  deleted_exps

Na tabela ``deleted_exps`` fica o registo dos artigos/data de validade eliminados. Os artigos/data de validade deixam de estar presentes na tabela ``validity_checks`` passando ao estado **Removed** na tabela ``deleted_exps``.

???+ example "Colunas tabela deleted_exps:"
     - **Org_code**: Organização;
     - **Status**: Estado do artigo/data de validade – Removed;
     - **Store**: Loja;
     - **Sku**: Artigo;
     - **Validity_date**: Data de Validade do Artigo;
     - **Scheduled_date**: Data em que foi ou vai ser criada a tarefa;
     - **Correlation_id**: Campo que permite tracking do processo associado à criação da validade;
     - **Created_at**: Data de criação do registo na tabela;
     - **Created_by**: User que criou o registo;
     - **Updated_at**: Data da ultima atualização;
     - **Updated_by**: User que atualizou o registo;
     - **Metadata**: Informação do artigo {"sku": "", "date": "2020-11-30T00:00:00Z", "type": "adhoc", "ad_hoc": null, "alert_id":  null, "quantity": 2, "sequence": null, "withdrawal": 4, "depreciation": 10, "max_percentage_discount": "50"}
     - **Task_id**: ID da tarefa
     - **Date_type**: Tipo de data (DEPRECIATION ou WITHDRAWAL);

## **Apps** – WinCE e Multiplataforma 

???+ example "Pedido Delete de Validades **Request URL**:"
     ``http://s.quality.tlantic.dev.tlantic-saas.com/tlqa2bb782/expirations/2004030:deletebatch?store=2``

^^Data de Validade sem tarefa criada^^

???+ example "Request Payload:"
     ```json 
     {
     "details": [
            {
            "date": "2021-04-02T00:00:00Z",
            "task": "empty"
            }      
        ]
     }
     ```
???+ example "Response:"
     ```json 
     {
         "id":"7dc8820d-38bb-4c48-bc6e-8b89396b3eb8",
         "success":true,
         "message":"200 OK",
         "result":"200"
     }
     ```

^^Data de Validade com tarefa criada^^

???+ example "Request Payload:"
     ```json 
     {
     "details": [
        {
            "type": "DEPRECIATION", 
            "date": "2020-12-31T00:00:00Z", 
            "task": "c5ab198d-ec2c-4142-902a-984b64686a66"
        }
      ]
     }
     ```

???+ example "Response:"
     ```json 
     {
        "id":"d0f2369d-0af9-4a8f-a922-66ddc1c32ac8",
        "success":true,
        "message":"200 OK",
        "result":"200"
     }
     ```
