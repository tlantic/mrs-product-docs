# Configuración del servidor

Esta sección está dedicada a la lista de configuraciones que influyen en el comportamiento de las tareas de

*[Agente]: agent-tasks
*[Integração]: integration-tasks
*[Serviço]: service-tasks
*[stock-default]: "default": integer
*[encontrados]: "include-resources-not-found": boolean

## Tareas del agente

En este capítulo, se presentarán las configuraciones que definen el comportamiento del agente de tareas.

### **agent-tasks-task-creating-flow**

Tipo: ``object`` | Configuración a nivel de **organización**

Configuración que define varios parámetros asociados a la creación de tareas.

???+ example "Valores posibles::"
    * **soh**: ``object`` -  Define el parámetro de stock en el proceso de creación de la tarea:
        * default: ``integer`` - Define el valor de stock predeterminado para todos los tipos de tareas que no tienen parametrización de excepción.
        * {task-type}: ``object`` - permite parametrizar una excepción por tipo de tarea. 
			* default: ``interger`` - Define el stock mínimo predeterminado para un tipo de tarea determinado.
			* include-resources-not-found: ``boolean``  - Define si incluye artículos en los que no se pudo consultar el stock.
		* concurrent-requests: ``interger`` - Define el número de pedidos paralelos para la búsqueda de stock.
		* include-resources-not-found: ``boolean`` - Define si incluye artículos en los que no se pudo consultar el stock.
	* **check-soh**: ``array`` - Define los tipos de tareas para las que está activa la validación de soh.
	* **contention**: ``object`` - Define los tipos de tareas que están sujetas a contención y los parámetros respectivos. Para obtener más información sobre la contención, consulte el manual relacionado con el Módulo de validez.
		* {task-type}: ``object`` - Define el tipo de tarea asociada con la disputa.
			* days: ``interger`` - Define el número de días para crear la tarea.
			* mode: ``string`` - establece el modo de creación.
	* **change-price**: ``array`` - Define los tipos de tareas que utilizan la tabla de precios para recopilar información para su creación.
	* **scheduled-start**: ``object`` - Define el tiempo estimado de creación de tareas a través de la integración.
		* default: ``string`` - Define el tiempo de creación predeterminado previsto. 
		* {task-type}: ``string`` - Define el tiempo de creación estimado para cada tipo de tarea parametrizada.
	* **keep-expirations**: ``array`` - Define los tipos de tareas que manejan los recursos de tipo expirations. 
	* **validation-output**: ``array`` - Define el tipo de tarea que se generará para el tipo de retirada.
	 

!!! note "Ejemplo"
    ```json
    {
	"soh": {
		"default": 1,
		"SCANAUDIT": {
			"default": -1,
			"include-resources-not-found": true
		},
		"EXTERNALAUDIT": {
			"default": -1,
			"include-resources-not-found": true
		},
		"PRESENCECHECK": {
			"default": 0,
			"include-resources-not-found": true
		},
		"include-resources-not-found": true,
		"concurrent-requests": 10
	},
	"check-soh": [
		"PRESENCECHECK",
		"REPLENISHPREP",
		"REPLENISH"
	],
	"contention": {
		"VALIDITYCHECK": {
			"days": 7,
			"mode": "full"
		}
	},
	"change-price": [
		"RELABELLING",
		"CARDDISCOUNT",
		"PRICEUP",
		"PRICEDOWN",
		"PROMOTIONIN",
		"PROMOTIONOUT"
	],
	"scheduled-start": {
		"default": "01:00:00",
		"VALIDITYCHECK": "01:00:00"
	},
	"keep-expirations": [
		"VALIDITYCHECK",
		"EXPIRATIONCONTROL",
		"WITHDRAWAL"
	],
	"validation-output": [
		"EXPIRATIONCONTROL"
	]
    }
    ```

### **agent-tasks-task-closing-flow**

Tipo: ``object`` | Configuración a nivel de **Organización**

Configuración que define varios parámetros asociados al cierre de tareas y consecuente generación.

???+ example "Valores posibles::"
	* **tasks-flow**: ``object`` - Define flujos parametrizados para determinados tipos de tareas.
		* {task-type}: ``array`` - Define el tipo de tarea parametrizada para un flujo dado.
			* flow-type: ``string`` - Define el tipo de caudal parametrizado.
			* output_task: ``string`` - efine la tarea consecuente (si aplica).
			* params: ``object`` - Define los parámetros que influyen en el comportamiento del caudal parametrizado (flow-type).
	* **notify-no-stock**: ``array`` - Define el flujo que notifica al servicio externo cuando hay artículos que han sido excluidos por falta de stock. En el ejemplo, todos los artículos que se excluyeron de la Separación de la verificación de presencia porque no hay stock se notifican al servicio externo (webwook).
	* **use-extradata**: ``object`` - Define los campos que lee el agente dentro del objeto de datos extra a nivel de recursos.
		* validitycheck: ``array`` - Define en qué tipos de tareas el agente mapea la información de validación dentro del objeto de datos extra a nivel de recurso.
	* **scheduled-start**: ``object`` - define la hora de inicio estimada asociada con la tarea.
		* default: ``string`` - Define la hora de inicio predeterminada prevista para los tipos de tareas no parametrizados.
		* {task-type}: ``string`` - define la hora de inicio estimada para el tipo de tarea parametrizada.
	* **inventory-divergences**: ``object`` - Define parámetros en la creación de la tarea "Corrección de divergencias" en tareas del tipo Inventario.
		* name-prefix: ``string`` - Define el prefijo colocado en el nombre de las tareas de "corrección de divergencia".
	* **replenishprep-minimum-soh-required**: ``object`` - Define el valor mínimo de stock al generar tareas de Separación (replenishprep).
		* default: ``interger`` - Define el valor de stock mínimo predeterminado (para todos los tipos de tareas no parametrizados).
		* {task-type}: ``interger`` - Define el valor de stock mínimo para cada tipo de tarea parametrizada.
	* **filter-resources-concurrent-requests**: ``object`` - Define el número de solicitudes que el agente realiza al mismo tiempo para realizar el filtro de artículos que se incluirán en las tareas.
		* default: ``interger`` - Define el número predeterminado de pedidos que el agente realiza simultáneamente para realizar el filtro de elementos que se incluirán en las tareas.
	* **create-stockcount-on-replenish-closing**: ``object`` -Define los parámetros para la inclusión de artículos en las tareas de recuento de existencias (stockcount) al cerrar las tareas de reposición  (replenish).
		* include-resources-not-found: ``boolean`` - Define si los artículos que no se encuentran en las tareas de Reemplazo se incluyen en las tareas de recuento de Inventario.
		* include-resources-untreated: ``boolean`` - Define si incluir artículos sin tratar en las tareas de recuento de inventario en las tareas de reabastecimiento.
		* include-resources-unexpected-quantity: ``boolean`` - 	Define si los artículos en las tareas de recuento de existencias incluyen artículos en los que la cantidad tratada es diferente de la cantidad esperada en las tareas de reabastecimiento.
	* **create-relabelling-on-relabelling-closing**: ``object`` - Define los parámetros para la inclusión de artículos en las tareas de Reetiquetado (relabelling) en el cierre de las tareas de Reetiquetado (relabelling).
		* include-resources-not-found: ``boolean`` - Define si se incluirán los artículos que no se encuentran en las tareas Volver a marcar en las tareas Volver a marcar.
		* include-resources-untreated: ``boolean`` - Define si incluir artículos sin tratar en las tareas de Markdown en las tareas de Markdown.
		* include-resources-unexpected-quantity: ``boolean`` - 	Define si se incluirán artículos en las tareas de Markdown en las que la cantidad tratada es diferente de la cantidad esperada en las tareas de Markdown.
		* sequence-limit: ``interger`` - Define el número límite de tareas que se generarán en la secuencia. 
	* **create-expirationcontrol-on-expirationcontrol-closing**: ``object`` - Define los parámetros para crear Validity Control en el cierre Validity Control.
		* expiration-sequence-limit: ``interger`` - Define el número límite de tareas generadas en la secuencia.

!!! example "Fluxos possíveis:"
	* **FILTER-BY-AVAILABLE-SOH**: Flujo que define el filtro de artículos por indisponibilidad del servicio de consulta de stock.
	* **CREATE-ITEM-LIST**: Flujo que define la generación de lista de artículos en el cierre de la tarea parametrizada.
	* **CHECK-PROCESSED-CONTAINERS**: Flujo que define la gestión de los contenedores tratados en la tarea de cierre parametrizada. Los parámetros configurados influyen en el comportamiento de la inserción de los contenedores tratados en la tarea posterior.
	* **EXTRACT-SNAPSHOT-EXPIRATIONS**:  Flujo que define el tratamiento de los vencimientos al final de la tarea parametrizada.
	* **CHECK-STOCK-DIVERGENCE** Flujo que define la gestión de artículos con el atributo "divergence". 


!!! note "Ejemplo"
    ```json
    {
  		"tasks-flow": {
    		"STOCKOUTAUDIT": [
				{
					"flow_type": "FILTER-BY-AVAILABLE-SOH",
					"output_task": "STANDARDREPLENISHPREP"
				}
    	],
    		"ITEMLIST": [
				{
					"flow_type": "CREATE-ITEM-LIST"
				}
    	],
    		"RECEIVING": [
				{
					"flow_type": "CHECK-PROCESSED-CONTAINERS",
					"output_task": "DESPICKING",
					"params": {
						"include-items-with-parentid": true,
						"include-containers-with-parentid": false
					}
				},
				{
					"flow_type": "USE-EXTRADATA",
					"output_task": "VALIDITYCHECK"
				}
    	],
    		"EXPIRATIONPICKING": [
				{
					"flow_type": "EXTRACT-SNAPSHOT-EXPIRATIONS",
					"output_task": "VALIDITYCHECK"
				},
				{
					"flow_type": "CHECK-STOCK-DIVERGENCE",
					"output_task": "STOCKCOUNT"
				}
    	]
  	},
  		"notify-no-stock": [
    		"REPLENISHPREP-ON-PRESENCECHECK"
  		],
  		"use-extradata": {
    		"validitycheck": [
      			"RECEIVING"
    		]
  		},
  		"scheduled-start": {
    		"default": "01:00:00",
    		"VALIDITYCHECK": "01:00:00"
  		},
  		"inventory-divergences": {
    		"name-prefix": "Correção Divergência"
  		},
  		"replenishprep-minimum-soh-required": {
    		"default": 0,
    		"SCANAUDIT": 3,
    		"RELABELLING": 3,
    		"REPLENISHPREP": 0
  		},
  		"filter-resources-concurrent-requests": {
    		"default": 10
  		},
		"create-stockcount-on-replenish-closing": {
			"include-resources-not-found": false,
			"include-resources-untreated": false,
			"include-resources-unexpected-quantity": false
		},
		"create-relabelling-on-relabelling-closing": {
			"sequence-limit": 0,
			"include-resources-untreated": true,
			"include-resources-unexpected-quantity": true
		},
		"create-expirationcontrol-on-expirationcontrol-closing": {
			"expiration-sequence-limit": 30
		}
	}
    ```
### **agent-task-create-task-soh-unavailable**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Aplicação**

Entorno que define el comportamiento de inserción de artículos en la generación de la tarea ante la falta de respuesta del servicio de stock.

!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-task-soh-unavailable": {"active": true}
    }
    ```

### **agent-task-create-priceaudit-on-relabelling**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si se genera una tarea de auditoría de precios (priceaudit) al final de la reprogramación.


!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-priceaudit-on-relabelling": {"active": true}
    }
    ```

### **agent-task-create-replenishprep-on-relabelling**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si se genera una tarea de separación (replenishprep) al final de la tarea de rellamada prioritaria (relabelling).

!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-replenishprep-on-relabelling": {"active": true}
    }
    ```

### **agent-task-create-replenishprep-on-priorityrelabelling**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si se genera una tarea de Separación (replenishprep) nal final de la tarea de rellamada prioritaria (priorityrelabelling).

!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-replenishprep-on-priorityrelabelling": {"active": true}
    }
    ```

### **agent-task-create-replenishprep-on-priceaudit**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si se genera una tarea de Separación (replenishprep) al cierre de la tarea Auditoría de precios (priceaudit).

!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-replenishprep-on-priceaudit": {"active": true}
    }
    ```

### **agent-task-create-replenish-on-replenishprep**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

SConfiguración que define si se genera una tarea de reabastecimiento (replenish) al final de la tarea de separación (replenishprep).

!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-replenish-on-replenishprep": {"active": true}
    }
    ```

### **agent-task-create-replenishprep-on-externalaudit**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si se genera una tarea de separación (replenishprep) al final de la tarea de auditoría externa (externalaudit).

!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-replenishprep-on-externalaudit": {"active": true}
    }
    ```

### **agent-task-create-replenishprep-on-scanaudit**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si se genera una tarea de separación (replenishprep) al final de la tarea de auditoría de análisis (scanaudit).



!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-replenishprep-on-scanaudit": {"active": true}
    }
    ```

### **agent-task-create-replenishprep-on-stockoutaudit**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si se genera una tarea de separación (replenishprep) al final de la tarea de auditoría de ruptura (stockoutaudit).

!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-replenishprep-on-stockoutaudit": {"active": true}
    }
    ```

### **agent-task-create-stockcount-on-replenishprep**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si una tarea de recuento de existencias (stockcount) se genera al final de la tarea de separación (replenishprep).


!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-stockcount-on-replenishprep": {"active": true}
    }
    ```

### **agent-task-create-stockcount-on-replenish**

!!! warning "Obsoleto"
    Use [agent-tasks-task-closing-flow](#agent-tasks-task-closing-flow) en su lugar

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si se genera una tarea de recuento de existencias (stockcount) al final de la tarea de reabastecimiento. (replenish).

### **agent-task-create-stockcount-on-externalaudit**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si se genera una tarea de recuento de existencias (stockcount) al final de la tarea de auditoría externa (externalaudit).


!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-stockcount-on-externalaudit": {"active": true}
    }
    ```

### **agent-task-create-stockcount-on-scanaudit**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si se genera una tarea de recuento de existencias (stockcount) al cierre de la tarea de auditoría de análisis (scanaudit).



!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-stockcount-on-scanaudit": {"active": true}
    }
    ```


### **agent-task-create-presencecheck-on-externalaudit**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si se genera una tarea de verificación de presencia (presencheck) al final de la tarea de auditoría externa (externalaudit).


!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-presencecheck-on-externalaudit": {"active": true}
    }
    ```

### **agent-task-create-presencecheck-on-scanaudit**

Tipo: ``object`` (default: ``active: false``) | Configuración a nivel de **Organização**

Configuración que define si se genera una tarea de comprobación de presencia (presencheck) al final de la tarea de auditoría de análisis (scanaudit).


!!! note "Ejemplo"
    ```json
    {
      "agent-task-create-presencecheck-on-scanaudit": {"active": true}
    }
    ```

### **agent-task-tasks-change-price**

!!! warning "Obsoleto"
    Use [agent-tasks-task-closing-flow](#agent-tasks-task-closing-flow) en su lugar

Tipo: ``object`` (default: ``{}``) | Configuración a nivel de **Organização**

Configuración que define qué tipos de tareas realizan la integración en la tabla de precios.


### **agent-task-tasks-keep-expirations**

!!! warning "Obsoleto"
    Use [agent-tasks-task-closing-flow](#agent-tasks-task-closing-flow) en su lugar

Tipo: ``object`` (default: ``{}``) | Configuración a nivel de **Organização**

Configuración que define qué tipos de tareas manejan vencimientos.


### **agent-task-tasks-validation-output**

!!! warning "Obsoleto"
    Use [agent-tasks-task-closing-flow](#agent-tasks-task-closing-flow) en su lugar

Tipo: ``object`` (default: ``{}``) | Configuración a nivel de **Organização**

Configuración que define qué tipo de tarea se crea al generar una verificación de validez.



## **Integration Tasks**

Este capítulo presenta la configuración que define el comportamiento del módulo de integración de tareas.


### **integration-setting**

Tipo: ``object`` (default: ``{}``) | Configuración a nivel de **Organização**

Configuración que define el número máximo de artículos que permite la integración en la creación de tareas.


!!! note "Ejemplo"
    ```json
    {
  		"max-items": {
    		"default": 0,
			"SCANAUDIT": 1000,
			"EXTERNALAUDIT": 1000,
			"PRESENCECHECK": 1000
		}
	}
    ```

## **Service Tasks**

Este capítulo presenta la configuración que define el comportamiento del módulo Servicio de tareas.


### **parent-tasks-not-closing**

Tipo: ``object`` (default: ``{}``) | Configuración a nivel de **Organização**

Configuración que define los tipos de tareas que no terminan automáticamente cuando se cierran las tareas secundarias. En el ejemplo, el tipo de tarea "Inventario" está parametrizado. Esto significa que la tarea de inventario no se cierra automáticamente cuando todas las zonas están terminadas.


!!! note "Ejemplo"
    ```json
    {
  		"tasks": [
    		"INVENTORY"
  		]
	}
    ```

### **tasks-ttl**

Tipo: ``object`` (default: ``{}``) | Configuración a nivel de **Organização**

Configuración que define, por tipo de tarea, la cantidad de días marcados para ttl en la base del sofá.


???+ example "Valores posibles::"
	* **default**: ``interger`` - Define el número de días predeterminados para los tipos de tareas no parametrizados.
	* **{task-type}**: ``object`` - Define el número de días parametrizados para el tipo de tarea. 
		* default: ``interger`` - Define el número de días parametrizado por defecto para el tipo de tarea y para los estados no parametrizados
		* {status}: ``interger`` - Define el número de días parametrizados para el estado y tipo de tarea. 

!!! note "Ejemplo"
    ```json
    {
  		"default": 365,
  		"priceup": {
    		"default": 4,
    		"a":40 
  		},
		"inventory": {
			"default": 30,
			"a":40
		},
		"pricedown": {
			"default": 4,
			"a":40
		},
		"scanaudit": {
			"default": 15,
			"a":40
		},
		"promotionin": {
			"default": 4,
			"a":40
		},
		"carddiscount": {
			"default": 4,
			"a":40
		},
		"promotionout": {
			"default": 4,
			"a":40
		},
		"presencecheck": {
			"default": 15,
			"a":40
		},
		"replenishprep": {
			"default": 15,
			"a":40
		}
	}
    ```







<!--![image alt <>](assets/under_construction.png){:height="200px" width="200px"}

![image alt <>](assets/under_construction.png){:height="500px" width="500px"}

| |                                |
| :----------------------: | :-----------------------------------: |
| ![image alt <>](assets/under_construction.png){:height="300px" width="300px"}       | ![image alt <>](assets/under_construction.png){:height="300px" width="300px"}  |-->

