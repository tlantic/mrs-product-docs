## **Notas de la versión versión v5.12.14.1** 
#### Fecha de lanzamiento 18-12-2020

???+ abstract "**Correcciones / Mejoras**"

    - **Auditoría de Sortido**: Solicitud de reemplazo - Error NullReferenceException

## **Notas de la versión versión v5.12.14.0** 
#### Fecha de lanzamiento 14-12-2020

???+ done "**Nuevas características**"

    - **Control de validez** : depreciación del artículo variable de precio / peso a través de la selección de artículo
    - **Control de validez** : Presentación SOH
    - **Control de validez** : Presentación de parámetros
    - **Reemplazo** : la traducción de la cantidad a reponer debe ser "por suministrar"

???+ abstract "**Correcciones / Mejoras**"

    - **Recepciones** : la tarea de aprobación da error NullReferenceException
    - **Transferencias / Devoluciones** : la aplicación se bloquea y deja de conectarse a la red después de seleccionar la fecha para realizar la transferencia
    - **Control de validez** : el botón Continuar está activo con campos obligatorios para completar
    - **Recepción** : se descarta el atributo expiration_control con valor negativo


