
## **Notas de la versión versión 1.37.1-rc.0** 
#### Fecha de lanzamiento 09-02-2021 

???+ done "**Nuevas características**"

    - **Inventarios**: Insertar cantidad cero en el peso indicado en la etiqueta

???+ abstract "**Correcciones / Mejoras**"

    - **Inventarios**: la entrada de cajas libres no hace la suma correctamente

     - **Control de validez**: si emite un pitido o ingresa ean o sku manualmente, las entradas de cantidad tienen una unidad

## **Notas de la versión versión 1.37.0-rc.0** 
#### Fecha de lanzamiento 22-01-2021

???+ done "**Nuevas características**"

    - **Auditoría de validez**: Cambio de etiqueta del botón Fecha de registro

    - **Auditoría de validez**: cambio en las etiquetas de depreciación y retiro

    - **General **:  descarga de la tarea en ejecución

???+ abstract "**Correcciones / Mejoras**"

    - **General **: cuando elimina la entrada total y aumenta la cantidad en el botón "+", el total muestra "NaN"

    - **Control de validez**: si emite un pitido o ingresa ean o sku manualmente, las entradas de cantidad tienen una unidad

    - **Inventarios**: escanear la entrada a la zona de recuento

    - **Token Renew**: la renovación no se realiza solicita un nuevo inicio de sesión

     - **Separación**: Solicitudes de Sustitución de Artículos de Peso Variable presenta modalidad de artículos por unidad

## **Notas de la versión versión 1.36.4** 
#### Fecha de lanzamiento 26-03-2021 

???+ abstract "**Correcciones / Mejoras**"

     - **Control de validez**: impresión de etiquetas de precio / peso variable

    - **Control de validez**: valor PVP en el modal de depreciación

## **Notas de la versión versión 1.36.1-rc.0** 
#### Fecha de lanzamiento 02-02-2021 

???+ done "**Nuevas características**"

    - **Sincronización**: respuesta del servidor de mapas

???+ abstract "**Correcciones / Mejoras**"

    - **Control de validez**: no permita que el artículo se procese como No encontrado si no tiene una etiqueta seleccionada

## **Notas de la versión versión 1.36.0-rc.0** 
#### Fecha de lanzamiento 12-01-2021 

???+ done "**Nuevas características**"

    - **Auditorías de validez** : cantidad y edición por defecto
    - **Auditoría de validez** : bloqueo del registro de fechas anteriores al día actual
    - **Gestión de fechas futuras** : Bloqueo del registro de fechas anteriores al día actual
    - **Auditoría de validez** : aviso del período de depreciación
    - **Auditoría de Validez** : Consultar fechas futuras
    - **Auditoría de validez** : Inserción de nuevas fechas por parte del facilitador
    - **Control de validez** : depreciación del artículo variable de precio / peso a través de la selección de artículo
    - **Control de validez** : Presentación de parámetros
    - **General** : Visibilidad del estado en el modo de artículo.
    - **Aprobación de pausa** : procesamiento de **pausa**
    - **Pausas de búsqueda** : servicio de investigación de pausas
    - **Resumen de tareas** : métrica separada de la cantidad Unidades y el peso de la cantidad
    - **Certificación del dispositivo ED75**

???+ abstract "**Correcciones / Mejoras**"

    - **Recepciones** : Bippling con el modo abierto aumenta la cantidad pero no cambia las entradas.
    - **Recepciones** : cuando se cambia la cantidad en la cantidad de entrada de un artículo de peso variable, no se suma a la entrada total, sino que cambia
    - **Control de validez** : si emite un bip o ingresa ean o sku manualmente, las entradas de cantidad tienen una unidad.

## **Notas de la versión versión 1.35.0.rc1** 
#### Fecha de lanzamiento 12-01-2021

???+ abstract "**Correcciones / Mejoras**"

    - **Recepciones** : En el modal del artículo hay un botón no encontrado en lugar de continuar
    - **Recepciones** : Bippling con el modo abierto aumenta la cantidad pero no cambia las entradas.
    - **Recepciones** : cuando se cambia la cantidad en la cantidad de entrada de un artículo de peso variable, no se suma a la entrada total, sino que cambia
    - **Recibos** : el bippling de artículos de peso variable duplica la cantidad bipulada total
    - **Control de validez** : si emite un pitido o ingresa ean o sku manualmente, las entradas de cantidad tienen una unidad
    - **Recibos** : Al sonar por segunda vez con peso variable, la entrada presenta el primer valor ingresado pero sin decimales
    - **Recuento de existencias / Inventarios** : al eliminar las unidades de entrada, mantienen una unidad más en la entrada total al emitir un pitido

## **Notas de la versión versión 1.35.0-rc.0** 
#### Fecha de lanzamiento 15-12-2020

???+ done "**Nuevas características**"

    - **Control de validez** : Presentación de parámetros
    - **Inventarios** : Solicitud de creación de zona - ID de devolución
    - **Resumen de tareas** : métrica separada de la cantidad Unidades y el peso de la cantidad


???+ abstract "**Correcciones / Mejoras**"

    - **Inventarios** : Artículos de peso / Precio variable varWeight = N
    - **Inventarios** : la tarea de cierre envía datos duplicados
    - **Inventarios** : las cantidades de **artículos** no se envían correctamente
    - **Control de validez** : la cantidad se envía con una cadena
    - **Comprobación de cantidad** : la entrada de **cantidad** en el lineal no aumenta
    - **IGL** : **Separación de** cantidad enviada a nula en pedido de reemplazo urgente

