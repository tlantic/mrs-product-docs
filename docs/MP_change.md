
## **Release Notes Versão 1.37.1-rc.0** 
#### Data de lançamento 09-02-2021 

???+ done "**Novas Funcionalidades**"

    - **Inventários**: Inserir quantidade a zero no peso indicado a etiqueta

???+ abstract "**Correções / Melhorias**"

    - **Inventários**: Input de caixas livres não está a fazer a soma corretamente

     - **Controlo de validade**: se bipar ean ou inserir ean ou sku manualmente, os inputs de quantidade apresenta uma unidade

## **Release Notes Versão 1.37.0-rc.0** 
#### Data de lançamento 22-01-2021 

???+ done "**Novas Funcionalidades**"

    - **Auditoria de Validades**: Alteração da label do botão de Registar data

    - **Auditoria de Validades**: Alteração das labels de depreciação e retirada

    - **Geral**: Download da tarefa em execução

???+ abstract "**Correções / Melhorias**"

    - **Geral**: Quando se faz delete no input total e incrementa quantidade no botão "\+", total apresenta "NaN"

    - **Controlo de validade**: se bipar ean ou inserir ean ou sku manualmente, os inputs de quantidade apresenta uma unidade

    - **Inventários**: Entrada via scan na zona de contagem

    - **Renew do Token**: Não é feito o renew pede novo login

     - **Separação**: Pedidos de Reposição de Artigos de Peso variável apresenta modal de artigos a unidade

## **Release Notes Versão 1.36.4** 
#### Data de lançamento 26-03-2021 

???+ abstract "**Correções / Melhorias**"

     - **Controlo de Validades**: Impressão de etiquetas de preço/peso variável

    - **Controlo de Validades**: Valor de PVP na modal da depreciação

## **Release Notes Versão 1.36.1-rc.0** 
#### Data de lançamento 02-02-2021 

???+ done "**Novas Funcionalidades**"

    - **Sincronização**: mapear resposta do server

???+ abstract "**Correções / Melhorias**"

    - **Controlo de Validade**: Não deixa processar artigo como Não encontrado se não tiver uma etiqueta selecionada

## **Release Notes Versão 1.36.0-rc.0** 
#### Data de lançamento 12-01-2021 

???+ done "**Novas Funcionalidades**"

    - **Auditorias de Validades**: Quantidade default e edição

    - **Auditoria de Validades**: Bloqueio do registo de datas anteriores ao dia corrente

    - **Gestão de datas futuras**: Bloqueio do registo de datas anteriores ao dia corrente

    - **Auditoria de Validades**: Aviso de período de depreciação

    - **Auditoria de Validades**: Consulta datas futuras

    - **Auditoria de Validades**: Facilitador inserção de novas datas

    - **Controlo de Validades**: Depreciação de artigo de preço/peso variável através do select do artigo

    - **Controlo de Validades**: Apresentação dos parâmetros

    - **Geral**: Visibilidade do status na modal de artigo

    - **Aprovação de Quebra**: Processamento das quebras

    - **Pesquisa de Quebras**: Serviço de pesquisa de quebras 

    - **Resumo tarefas**: Separar métrica de quantidade Unidades e quantidade peso

    - **Certificação do dispositivo ED75**

???+ abstract "**Correções / Melhorias**"

    - **Receções**: Bipagem com a modal aberta incrementa quantidade mas não altera inputs

    - **Receções**: Ao alterar quantidade no input quantidade de um artigo de peso variável, não acrescenta no input total, mas altera

    - **Controlo de validade**: se bipar ean ou inserir ean ou sku manualmente, os inputs de quantidade apresenta uma unidade.


## **Release Notes Versão 1.35.0.rc1** 
#### Data de lançamento 12-01-2021

???+ abstract "**Correções / Melhorias**"

    - **Receções**: Na modal do artigo surge botão de não encontrado em vez de continuar

    - **Receções**: Bipagem com a modal aberta incrementa quantidade mas não altera inputs

    - **Receções**: Ao alterar quantidade no input quantidade de um artigo de peso variável, não acrescenta no input total, mas altera

    - **Receções**: Bipagem de artigo de peso variável duplica a quantidade total bipada

    - **Controlo de validade**: se bipar ean ou inserir ean ou sku manualmente, os inputs de quantidade apresenta uma unidade

    - **Receções**: Ao bipar pela segunda vez artigo com peso variável o input apresenta o primeiro valor inserido mas sem casas decimais

    - **Contagem de stock/Inventários**: ao fazer delete no input unidades mantêm uma unidade a mais no input total ao bipar


## **Release Notes Versão 1.35.0-rc.0** 
#### Data de lançamento 15-12-2020

???+ done "**Novas Funcionalidades**"

    - **Controlo de Validades**: Apresentação dos parâmetros 

    - **Inventários**: Pedido de Criação de Zona - Retorno ID 

    - **Resumo tarefas**: Separar métrica de quantidade Unidades e quantidade peso 

???+ abstract "**Correções / Melhorias**"

    - **Inventários**: Artigos de Peso/Preço variável varWeight = N

    - **Inventários**: Fecho da tarefa envia dados duplicados

    - **Inventários**: Quantidades dos artigos não estão a ser enviadas corretamente

    - **Controlo de Validades**: Quantity está a ser enviado com string

    - **Verificação de Quantidade**: Input de quantidade no linear não incrementa

    - **Separação IGL**:  Quantity enviada a null em pedido de reposição urgente

