# **Manual de Integração**

## Objetivo

>Neste manual, você encontra todas as informações necessárias para a integração adequada com a nossa solução, premissas e formatos devem ser seguidos para garantir performance e estabilidade quando a ferramenta está On-Premisses, leia-o por inteiro.
>A Tlantic está à sua disposição para esclarecer qualquer dúvida, bem como
para ouvir sua crítica ou sugestão, consulte a última página deste manual com os nossos contatos.
Guarde este manual para futuras consultas.

![image alt <>](..\img\integration\integration_1.png) 

---

## Integração de aplicações corporativas – Visão Geral

O WFM basicamente disponibiliza tabelas na estrutura abaixo para receber integrações, o emissor (WebService) envia os dados da base origem para nossas tabelas STG.

OBS: O emissor (WebService) deve ser desenvolvido pelo cliente ou um terceiro contratado pelo cliente.

Com as informações em nossas Staging um Scheduller da base faz devidas validações (a cada X horas) e importa os dados para as tabelas CORE onde os usuários terão a acesso pela interface.

Abaixo esquema simplificado da integração:
![image alt <>](..\img\integration\integration_2.png) 

---
## Modelos de Integração

### Sugestões para Integração

>Abaixo 3 sugestões base o formato de emissor que pode ser desenvolvido para integrar ao WFM.
![image alt <>](..\img\integration\integration_3.png) 

---
## Premissas da integração

A integração com o WFM consiste em:

1.	Cliente escolher o modelo de integração que melhor se aplica ao cenário (item 4.1).
2.	Definir a periodicidade do envio de informações, esta definição deverá partir da necessidade da informação para processamento de escala, sendo as mais conhecidas/utilizadas.
a.	ONLINE: Onde o fato gerador envia a informação ao WFM no mesmo instante (recomendado).
b.	AGENDAMENTO: Em determinado período é efetuado o envio automático.
c.	MANUAL: Por ação manual as informações são enviadas.
3.	Não é necessário efetuar DE/PARA em envios, os dados devem ser estar com os códigos e informações que existem no sistema de origem.
4.	Não é necessário efetuar filtros nos dados para o envio, a partir do momento da entrada em Produção os dados deverão vir em conforme forem lançados no respectivo sistema de origem, salvo em casos específicos citados em cada tabela durante este manual.
5.	Quando houver envio de DATA onde a data final de um período ou validade da informação ainda não é conhecida, enviar como data final 26/12/2049.
6.	Cronograma de integração: O Cliente precisa encaminhar um cronograma de desenvolvimento e testes das integrações, sendo com Terceiros ou Próprio, para mobilização de equipes em acompanhamento.
7.	A integração deverá permitir dois formatos de integrar os dados.
a.	Automático: Consiste no envio dos dados de forma sistêmica, este formato deverá compor apenas os envios de CD dos dados.
b.	Manual: A aplicação deverá permitir ação manual de envios em Massa e Unitário (CD) dos dados (item 4.4), estes para validar os envios e ou fazer envios em contingências.

OBS: A integração do WFM está preparada apenas para trabalhar com os itens CREATE, DELETE de um CRUD completo, a função de qualquer envio deve ser conforme exemplo abaixo:

![image alt <>](..\img\integration\integration_4.png) 

---
# Cronograma Modelo

![image alt <>](..\img\integration\integration_5.png) 

---
# Premissas de testes das integrações

O teste de integração com o WFM consiste em simular as funções que serão efetuadas no ambiente de produção, para isso validamos dois pontos:

1.	 Teste em massa: Tem por objetivo o envio completo de uma loja e suas respectivas informações para o WFM gerar escala.
a.	Unidade (uma de cada vez).
b.	Seções referente a unidade.
c.	Cargos (atualizar a lista de cargos).
d.	Colaboradores referente a unidade e as seções da unidade.
e.	Motivos de ausências (atualizar a lista dos motivos de ausências).
f.	Ausências lançadas (férias, faltas, atestados).

2.	 Teste unitário: Tem por objetivo o envio unitário de informações conforme elas são lançadas no sistema de origem.
a.	Colaboradores.
i.	Demissão.
ii.	Admissão.
iii.	Troca de cargo.
iv.	Demitir Loja A e Contratar Loja B.
b.	Ausências.
i.	Férias.
ii.	Atestado.
iii.	Afastamento sem retorno.
iv.	Afastamento determinando data de retorno.
v.	Seções, nova seção para unidade.

---
# Conjunto de integrações do TWFM

>Na seguinte imagem temos os diversos grupos de integrações utilizadas pelo WFM que podem ser divididas pelo tipo e função a que se destinam.

![image alt <>](..\img\integration\integration_6.png) 

---
# Integrações de RH

---
## Tabela de Unidade/Loja (input) - **STG_UNIT**
![image alt <>](..\img\integration\stg_unit_1.png) 

## Exemplo
![image alt <>](..\img\integration\stg_unit_2.png)

---
## Tabela Seção/Departamentos (input) - **STG_SECTION**
![image alt <>](..\img\integration\stg_section_1.png) 

## Exemplo
![image alt <>](..\img\integration\stg_section_2.png) 

---
## Tabela Motivos de Ausências (input) - **STG_MOT_A_P**
![image alt <>](..\img\integration\stg_mot_a_p_1.png) 

## Exemplo
![image alt <>](..\img\integration\stg_mot_a_p_2.png) 

---
## Tabela de Ausências (input) - **STG_ABSENCES**
![image alt <>](..\img\integration\stg_absences_1.png) 

## Exemplo
![image alt <>](..\img\integration\stg_absences_2.png) 

---
## Tabela de Cargos (input) - **STG_ROLE**
![image alt <>](..\img\integration\stg_role_1.png) 

## Exemplo
![image alt <>](..\img\integration\stg_role_2.png) 

---
## Tabela de Colaborador  (input) - **STG_EMPLOYEE**
![image alt <>](..\img\integration\stg_employee_1.png) 

## Exemplo 1/2
![image alt <>](..\img\integration\stg_employee_2.png) 

## Exemplo 2/2
![image alt <>](..\img\integration\stg_employee_3.png) 

---
# Integração de Ponto
## Recebimento de Ponto por WebService (Necessário Módulo de Execução)

•	O recebimento do registro de batida de ponto será através da evocação de um WebService REST no servidor WFM do cliente
•	Este serviço é usado na integração para fazer o registro da batida de ponto do colaborador.
•	A chamada à API (WS) é forma unitária para uma matrícula e uma marcação de ponto.
•	Não é exigido nenhuma ordem na entrada de ponto, e para melhorar o processo aceita chamadas em Multi Thread.
•	Recomenda-se o envio do ponto tão logo ele ocorra de modo a termos o sistema de ponto o mais atualizado possível.
•	Devem ser realizadas tantas quanto forem as batidas de ponto. O cliente poderá fazer o envio massivo de requisições
•	A cada recebimento com sucesso será enviado um HTTP SUCESS pelo sistema do WFM execution para que o cliente acompanhe se suas requisições estão sendo aceitas.

---
!!! note "URL"
    http://<HOST:PORT>/App/execution/api/json/api.php

!!! note "Method"
    POST

!!! note "Parametros"
    - Class=APIPunchExt
    - Method=setPunchExt
    - v=2
    - SessionID=FixSessionIDtoEAIEXTPUNCH
    
    ```json={"data":
        [{
        "status_id": Number, (Tipo Picagem: Entrada, Saída: 10 para entrada, 20 para saída)
        "type_id": Number, (Tipo Picagem: Normal, TS, Intervalo. Manter fixo em 1)
        "punch_time":“HH:MM", (hora da picagem/batida)
        "punch_date":“DD-MM-YYYY", (data da picagem/batida)
        "station_id": "String content", (Identificador do terminal/relógio do ponto)
        "card_id": "String content", (matricula do colaborador que fez a marcação)
        "time_stamp":Epoch (data e hora da batida em formato EPOCH)
        }]}
    ```        

---
## Exemplos
 > O funcionamento do envio é feito através de uma chamada POST contendo todas as informações do registro de ponto conforme exemplo abaixo:

!!! abstract "URL" 
    http://10.11.102.108:8080/App/execution/api/json/api.php?Class=APIPunchExt&Method=setPunchExt&v=2&SessionID=FixSessionIDtoEAIEXTPUNCH&json={"data":[{"status_id":20,"type_id":1,"punch_time":"00:19","time_stamp":1599697140,"punch_date":"11-09-2020","station_id":1234,"card_id":"59309"}]}

!!! abstract "URL" 
    ttp://<HOST:PORT>/App/execution/api/json/api.php?Class=APIPunchExt&Method=setPunchExt&v=2&SessionID=FixSessionIDtoEAIEXTPUNCH&json={"data":[{"status_id":0,"type_id":10,"punch_time":"17:41","time_stamp":1606930860,"punch_date":"02-12-2020","station_id":1234,"card_id":"53866"}]}

---
## Multithread e retorno de erros

O serviço para recebimento do ponto trabalha para ser o mais rápido possível e por isso o retorno de erros acaba sendo apenas dos casos de valores fora de limites ou não esperados.
Os erros mais relativos ao negócio, como período fechado, código da estação, matricula não encontrada e outros são feitos numa etapa posterior para não impactar a performance do serviço.
Segue abaixo um retorno de erro de chamada de serviço em alguns casos.

![image alt <>](..\img\integration\integration_7.png) 


---
# Recebimento de ponto por tabela staging  - **STG_PUNCH**
'Descontinuado'
>A tabela apresentada abaixo serve para referencia dos dados e descrição dos campos semelhantes ao serviço acima descrito.

![image alt <>](..\img\integration\stg_punch_1.png) 

## Exemplo
![image alt <>](..\img\integration\stg_punch_2.png) 


---
# Integração de Vendas
## Premissas 

A integração de vendas/indicador com o WFM deve seguir rigidamente os pontos

1.	A granularidade dos dados enviados deverá ser a mesma definida na geração de horários do WFM, consultar RH para qual granularidade foi definida, por padrão existem duas (10 ou 15) minutos.
2.	Os dados devem estar agrupados diariamente a partir da primeira venda até a última venda registrada no ERP.
3.	Os dados devem estar agrupados por dia e na granularidade determinada (item 1).
4.	Os campos do tipo quantidades e valores devem ser somados dentro do agrupamento.

•	Para cada unidade/loja configurada no WFM deverá ser integrado histórico de vendas.
    * Dados de venda referente a 24 meses a partir do dia da implantação da loja, o mínimo aceitável é 12 meses, abaixo disso não é garantido previsões de necessidades assertivas.
    ii.	Caso não exista ainda a integração automática para a Staging de vendas os dados devem ser enviados no formato desta tabela em arquivo EXCEL (XLSX).
    iii.	Caso a loja por qualquer motivo não tenha 12 meses de dados deverá ser enviado o período existente.

•   Para cada unidade/loja e seção configurada no WFM, deverá ser integrado as vendas realizadas.
    * As vendas realizadas deverão ser integradas a partir do dia seguinte ao último dia do histórico de vendas importado, mesmo que para equalização seja enviado retroativo, o WFM não pode ficar 1 dia sem vendas importadas a partir do inicio do histórico e durante a vida útil da loja no sistema, salvo caso de loja fechada ou feriado fechado.
    *	A periodicidade da integração de vendas realizadas dependerá de quanto em quanto tempo os dados serão consultados no WFM e/ou poderão ser exportados, podendo ser por semana, por dia ou hora, o cliente poderá definir.
•	Para lojas com turnos de 24 horas.
    i.	Quando houver turnos fixos ou variáveis mas a soma dos turnos que iniciam a cada dia D não ultrapasse 24 horas a partir do início do primeiro turno do dia a coluna BASE_DATE deve ser mantida com a data do dia D e para os campos START_TIME e END_TIME quando as vendas ocorrem no dia D devem ser (ex: 2000-01-01 23:50:00) e ao ultrapassar o D+1 (depois da meia noite) o formato deve ser (ex: 2000-01-02 00:00:00). Vide exemplo abaixo

![image alt <>](..\img\integration\stg_unit_dimensions_1.png) 

    ii.	Quando o turno noturno for fixo ou variável mas a soma dos turnos ultrapassa 24 horas a partir do inicio do primeiro turno do dia D deverá optar por ter uma seção com faixa de horário especifica para o noturno: ex: o primeiro turno do dia D inicia as 07:00h e o ultimo turno do dia D começa as 23:00 horas e termina as 07:00h e no noturno inicia as 00:30h e termina as 08:30h sendo assim deverá ter uma seção separada e com envio de vendas especifico para esta seção, caso a loja funcione (portas abertas) nestes horários, e as vendas devem compor apenas essa faixa de horário da seção em especifico.

# Tabela de Vendas/Driver (input) - **STG_UNIT_DIMENSIONS**

![image alt <>](..\img\integration\stg_unit_dimensions_2.png) 

## Exemplos 1/3
![image alt <>](..\img\integration\stg_unit_dimensions_3.png) 

## Exemplos 2/3
![image alt <>](..\img\integration\stg_unit_dimensions_4.png) 

## Exemplos 3/3
![image alt <>](..\img\integration\stg_unit_dimensions_5.png) 


---
# Exportação e Horários para sistema de RH (output) - **STG_WFM_TM**
## Sistema exxterno de controle de ponto

>Caso o sistema de controle de ponto não seja o módulo de execution do WFM, permite que se possa buscar os dados desta tabela que dispõem dos horários para exportação de dados ao sistema de RH efetuar fechamentos de folha.
>STG_WFM_TM – Estrutura da tabela de envio de horários, ausências e rubricas ao colaborador/dia.

![image alt <>](..\img\integration\stg_wfm_tm_1.png) 


