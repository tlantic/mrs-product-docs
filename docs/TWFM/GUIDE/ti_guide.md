# **Manual de Hardware e Software**

## Objetivo

>Neste manual, você encontra todas as informações necessárias de requisitos para dimensionamento dos servidores de forma adequada para a nossa solução, premissas e formatos devem ser seguidos para garantir performance e estabilidade quando a ferramenta está On-Premisses, leia-o por inteiro.

A Tlantic está à sua disposição para esclarecer qualquer dúvida, bem como
para ouvir sua crítica ou sugestão, consulte a última página deste manual com os nossos contatos.

Guarde este manual para futuras consultas.

![image alt <>](..\img\integration\integration_1.png) 

---
## Servidores por Módulo
> Conhecendo os servidores necessários para suportar todos os módulos de TWFM.

!!! note "Servidores por Módulo"
    | Módulos       | Base Oracle 12g (ou superior) | Windows | Linux (centOs) | Gateway Server |
    | :-------- | :----: | :----------------: | :----------------: | :------------------------------------------------------------: |
    | Planejamento  | X | X |  |  |
    | Execução  | X | X |  |  |
    | App Alertas  | X |  | X | X |
    | BFF  | X |  | X | X |
    | Forecast  | X | X |  |  |
    | Birt  | X | X |  |  |
    | Dashboard  | X |  | X |  |

## Ambientes para TWFM

???+ example " "
    * Tipos	
        * On-premises – CAPEX + OPEX
        * Cloud – OPEX
        * Cloud-SaaS – OPEX
    * Modelos On-premises necessitam
        * Infraestrutura de DataCenter para alojamento dos servidores;
        * Técnicos especializados para gestão de Sistema Operativo, Base de Dados e Aplicação;
        * Licenciamento de software que suporte o produto (SO, BD, Application Server);
    * Sistemas de Backup;
    * Manutenção de contratos de suporte de Hardware.
        * Possuem as seguintes limitações:
            * Sempre que se pretenda aumentar a capacidade, implica a aquisição de upgrade (caso seja modular) ou troca total do equipamento;
            * Versões de SO, BD e Application Server limitadas as suportadas pelo Hardware;
        * E permitem os seguintes cenários;
            * Gestão da infraestrutura seguindo procedimentos já existentes na companhia;
            * Reutilização de ferramentas de monitorização.              
    * Os modelos cloud necessitam
        * Contrato com um provider de Cloud;
        * Recursos com capacidade de gestão das plataforma no provider escolhido. 
            * Possuem as seguintes limitações:
                * Dependes do SLA contratado (normalmente são muito agressivos);
                * E permitem os seguintes cenários:
                * Capacidade elástica da hardware;
                * Gestão e monitorização feita pelo provider;
                * Configuração de alertas;
                * Upgrades de SO incluindo;
                * Varias versões da mesma infraestrutura;
    * Os modelos cloud-SaaS necessitam
        * Contrato com uma empresa de software (serviço puro);
        *   Possuem as seguintes limitações:
        *   Dependes do SLA contratado (normalmente são muito agressivos); 
    * E permitem os seguintes cenários:
        * Capacidade elástica da hardware;
        * Monitorização por status page dos serviços;
        * Total transparência na definição de hardware/arquitetura;

---
## Requisitos para ambientes

deve ter os recursos necessários para o perfeito funcionamento e atendimento a todas as demandas do cliente.

Além do ambiente de PRODUÇÃO, o WFM solicita ao cliente a preparação de um ambiente de Testes/HOMOLOGAÇÃO. Este ambiente é utilizado pela equipe de suporte para simulações de situações do cliente, melhorias nos processos atuais, e homologação de novas release antes da disponibilização para o uso no ambiente de PRODUÇÃO.

Abaixo identificamos quais os recuros que podem ser compartilhados entre os ambientes de PRODUÇÃO E HOMOLOGAÇÃO:

!!! note "Servidores por Módulo"
    | Recurso       | Software | Ambiente Compartilhado? (Prod, Hom)  |
    | :-------- | :--------------- | :-----: |
    | Database Oracle | * Oracle DB Standard Editon 12C versão 12.1.X (11gR2 também é suportado) * Recomendado Oracle Enterprise Edition para usufurir de segurança de dados no módulo de Planeamento. | Não |
    | Servidor Aplicacional Windows  | Internet Information Services 7.x + * PHP 5.5+ * R 3.12+ * Oracle Client 12C (11gR2 também é suportado) | Não |
    | Servidor Aplicacional Linux (centOs) | * CentOS 7/8 * Docker * ShinyProxy * Nginx; | Não  |
    | Gateway Server   | * Docker * ShinyProxy * Nginx; | Não |


## Pré Produção

| **Recurso** | **OS** | **CPU** | **RAM** | **Storage** |
| :----- | :---------- | :---------- | :---------- | :---------- |
| Database Oracle | Windows Server 2008R2 64 bits OR AIX 5.2+ OR Redhat Linux | x86-64 , 4 logical processor units , 1800 MHz | Min: 8 GB , dual channel support , DDR3 1066 , ECC (optional) | Min. SATA 3 - 7200 rpm , RAID 1/5 , space available: 512GB |
| Servidor aplicacional windows | Windows (Standard & Advanced) Server 2008R2 64 bits | x86-64 , 4 logical processor units , 1800 MHz | Min: 16 GB , dual channel support , DDR3 1066 , ECC (optional) | Min. SATA 3 - 7200 rpm , RAID 1/5 , space available: 128GB |
| Servidor aplicacional Linux | CentOs 7/8 | x86-64 , 8 logical processor units , 1800 MHz | Min: 32 GB , dual channel support , DDR3 1066 , ECC (optional) | Min. SATA 3 - 7200 rpm , RAID 1/5 , space available: 128GB |
| Gateway Server | Cent Os 7/8 | x86-64 , 4 logical processor units , 1800 MHz | Min: 8 GB , dual channel support , DDR3 1066 , ECC (optional) | Min. SATA 3 - 7200 rpm , RAID 1/5 , space available: 64GB |

---

## Produção

| **Recurso** | **OS** | **CPU** | **RAM** | **Storage** |
| :----- | :---------- | :---------- | :---------- | :---------- |
| Database Oracle | Windows Server 2008R2 64 bits OR AIX 5.2+ OR Redhat Linux | x86-64 , 4 logical processor units , 1800 MHz | Min: 8 GB , dual channel support , DDR3 1066 , ECC (optional) | Min. SATA 3 - 7200 rpm , RAID 1/5 , space available: 512GB |
| Servidor aplicacional windows | Windows (Standard & Advanced) Server 2008R2 64 bits | x86-64 , 4 logical processor units , 1800 MHz | Min: 16 GB , dual channel support , DDR3 1066 , ECC (optional) | Min. SATA 3 - 7200 rpm , RAID 1/5 , space available: 128GB |
| Servidor aplicacional Linux | CentOs 7/8 | x86-64 , 8 logical processor units , 1800 MHz | Min: 32 GB , dual channel support , DDR3 1066 , ECC (optional) | Min. SATA 3 - 7200 rpm , RAID 1/5 , space available: 128GB |
| Gateway Server | Cent Os 7/8 | x86-64 , 4 logical processor units , 1800 MHz | Min: 8 GB , dual channel support , DDR3 1066 , ECC (optional) | Min. SATA 3 - 7200 rpm , RAID 1/5 , space available: 64GB |

## Hardware e Software

!!! note "Info"
    * Aconselhamos o uso de hardware virtualizado para os ambientes de pré-produção e Produção.
    * Conforme a escolha da arquitetura todos os softwares/aplicações deverão seguir o mesmo formato (x86 ou x64).
    * O Servidor físico idealmente deve ser único para cada um dos ambientes, mas partilhado entre servidor de base de dados e servidor aplicacional, se possível utilizar o mesmo sistema operacional. A partilha vai permitir o uso mais eficiente de recursos, pois o software tem processos de duas características principais: intensivos em base de dados e intensivos no servidor aplicacional.
    * Os processos geralmente correm assincronamente a nível temporal pelo que a partilha de recursos poderá ajudar no auto redimensionamento dos mesmos em fases distintas do aplicativo.
    * Os valores indicados para produção têm como pressuposto que as lojas têm dimensão total ou inferior a 100 colaboradores, existem de momento aprox. 400 lojas, e a seção com maior número de colaboradores não excede 50 (por exemplo atendimento). Tempo esperado por geração de escalas para a seção de maior número de colaboradores é aproximadamente 15 min.
    * O sistema fica dimensionado para satisfazer 16 pedidos em simultâneo de geração de horários sem que haja degradação do online (interface), ficando impraticável o uso por parte do utilizador.
    * As ações de forecasting realizam-se no servidor aplicacional e está preparado para correr até 8 seções/variável de histórico em simultâneo.

## Requisitos para Máquinas

### Hardware - Servidor Aplicacional 
* Servidor aplicacional deve ser dedicado ao WFM (máquina física ou virtualizada). É necessário instalar um conjunto de software e de ações de administração que pode por em causa o funcionamento de outras aplicações em caso de servidor partilhado.
* Servidor aplicacional é recomendado a versão Windows Server 2012 R2.
* Para uma fase inicial este servidor deve ter acesso a internet.

   **Nota**
> O dimensionamento da infraestrutura poderá sofrer sugestão de alteração para ajuste, após levantamento das necessidades do cliente em particular na caracterização do uso do aplicativo.

## Requisitos de Acessos
### Acessos a infraestrutura de IT

* Pode ser direto ou por VPN que permita acesso ao(s) servidor(es) aplicacional(ais) e de Base de Dados.
* Os servidores aplicacionais devem ter acesso a base de dados Oracle do WFM (Ip e porta) bem como os schemas com usuário ADMIN.
* Cada servidor aplicacional deve ter liberada a porta 80, 88 e mais uma porta para cada módulo a critério do cliente, caso estejam em uso deve-se sinalizar imediatamente a TLANTIC para validação das portas disponíveis no ambiente.

---
### Acessos para instalação

* Nos servidores Aplicacionais deve ter acesso temporário a internet para carregar pacotes de instalação vindos da TLANTIC.
* Utilizador com acesso de ADMINISTRADOR ao servidor aplicacional para a TLANTIC.
* Utilizador com Permissões de criação de SCHEMAS e respectivos objetos de preferência com permissões de DBA.

---
### Acessos aos Implementadores

* Para Carga de dados.
* Acesso a base de dados Oracle com permissão de admin (possibilidade de disable de triggers e criação de tabelas temporárias).
* Remote desktop ao servidor interno ao IIS: Para poder fazer o restart do IIS em caso de manutenção e para a carga massiva de dados (importação de vendas).
* Acesso a APP da aplicação via browser do PC do utilizador.
* Liberação de regras firewall através do uso de VPN ou endereço público da aplicação.
* Para acesso por https, o cliente deve disponibilizar o certificado durante instalação do WFM, (avisar com antecedência se uso de certificado digital).
* Usuários
    * Os seguintes usuários deverão ter acesso aos ambientes.
    
        | **Usuários** | **Cargo** | **E-mail** |
        | :----- | :----- | :----- |
        | Danilo Bertapelli | Especialista TWFM | danilob@tlantic.com |
        | Inácio Acker | Analista de Integração | iacker@tlantic.com |
        | Hugo Nogueira | Cientista de Dados | hugo.nogueira@tlantic.com |
        | Jorge Abreu | Desenvolvedor | jorge.abreu@tlantic.com |

---
# Instalação do Ambiente
## Atividades 

> Abaixo a discriminação das atividades e quais equipes devem atuar.

| | Atividades | Owner |
| :----- | :----- | :-----: |
| Instalação do ambiente | Base Dados, Servidor Aplicational, InstantClient, Vpn + Acessos | Cliente |
| Instalação do TWFM | Site TWFM (iis), Schemas/Objetos Base Oracle, Rs SCripts, Php 5+, Config. InstantClient, COnfig Forecast | Tlantic |

---
# Processamentos 
> No quadro abaixo está representado os processsamentos que podem gerar picos de uso no servidor.

| Servidor | Processo | Peso Processamento (1 a 5) | Agendamento |
| :----- | :-----: | :-----: | :-----: |
| Dados | Geração de Escala | 5 | Sim |
| Dados | Integração de dados | 5 | Sim |
| Dados | Exportação dados | 3 | Sim |
| Aplicação | Cálculo de Forecast | 5 | Sim | 

| | Legenda |
| :-: | :-----:|
| | Peso 1 não impacta em processamento |
| | Peso 5 impacta em processamento |

---
# Estrutura base do ambiente TWFM

![image alt <>](..\img\ti\ti_01.png) 