# Guia do Usuário

## Histórico do documento

| Data      | Descrição    | Autor | Versão | Revisão
| ------------- | ---------------------------------------------------------------------------------------- | ---------- | ---------- | -------- | 
| 01/12/2020         | Criação do documento.    |  Filipe Silva  |   1.0   |     |
| 12/12/2020     | Criação de Introdução de formatação de documento.       | Filipe Silva  |  1.1     |      |
| 22/12/2020     | Versão final do documento.       | Filipe Silva  |  2.0    |      |
| 22/01/2021     | Revisão final do documento.       | Filipe Silva  |  2.1    |  Helena Gonçalves|

## **Introdução**

### TMR - Instore

Este documento descreve as funcionalidades do sistema Tlantic Mobile Retail (TMR) – InStore. O principal objectivo do documento é fornecer ao utilizador toda a informação necessária para que este possa utilizar o sistema de forma correta e eficaz.

InStore é um dos módulos que compõe o Tlantic Mobile Retail (TMR) voltado para as operações de loja, com foco na solução de problemas comuns do retalho:
???+ example "Principais funcionalidades:"
    * Inventários.
    * Lista de Artigos, Ficha de Artigos, Auditoria de Preço, 
    * Registo e Aprovação de Quebras.
    * Impressão de etiquetas.
    * Auditoria  e Controlo de Validades (Depreciação e Retirada).
    * Auditoria de Varrimento e Verificação de Presença.
    * Auditoria de Rutura, Separação, Reposição. 
    * Receções, Transferências e Devoluções.

O módulo Instore procura simplificar e solucionar problemas do retalho com a execução de tarefas orientadas a um perfil de utilizador.
Cada tarefa está associada a uma funcionalidade com o objetivo de resolver uma determinada necessidade do retalho.
O TMR é composto pelos módulos: **BackOffice**, **Cockpit**, **Aplicações móveis (WinCE e Multiplataforma)**, **Scheduler** e **TMR Server**.

### Arquitetura

O TMR é um sistema constituído por:

* Aplicações Cliente (App WinCE, App Android e App iOS) - Aplicação distribuida pelos dispositivos móveis usados na operação de loja.
* TMR Server - Componente central que contém a lógica e dados e que interage com outros sistemas.
* Backoffice - Componente web que permite definição de dados de referência e configuração de parâmetros.
* Cockpit - Componente web para análises operacionais das tarefas executadas no TMR Instore.


![image alt <>](assets/Arquitetura.png) 

### Aplicação móvel - Multiplataforma

Nos pontos seguintes serão apresentadas todas as funcionalidades presentes na aplicação Multiplataforma e de que forma poderão ser executadas corretamente. Ao longo dos capítulos serão também apresentadas imagens ilustrativas para uma melhor orientação do utilizador em relação ao sistema.

### Requisitos mínimos

Para uma correta instalação do TMR, o dispositivo deve respeitar os seguintes requisitos:

???+ example "Requisitos mínimos:"
    * **Sistema Operativo:** Android 7 ou superior | IOS (???).
    * **Memória ROM:** 16GB ou superior. 
    * **Memória RAM:** 4GB ou superior.
    * **Android WebView:** Versão Chrome 51 ou superior. (Android apenas)
    * **Leitor de Código de Barras:** Leitor 1D - LASER
    * **WI-FI:** 802.11 B/G/N.
    * **Tamanho do Ecrã Recomendado:** 5” ou superior.
    * **Resolução Mínima Recomendada:**  HD 1280 x 720.


## **Login**
Para iniciar sessão é pedido ao utilizador que introduza o seu username e password.  Para verificar o texto introduzido no campo Password, o utilizador pode manter pressionado o ícone  ![foto](assets/eye.png){:height="20px" width="20px"}  para mostrar a informação. 
Após login, é apresentada ao utilizador a lista de lojas associadas sendo obrigatória a sua seleção para continuar. Se o utilizador estiver associado apenas a uma loja, o sistema guarda esta informação automaticamente e avança para o ecrã seguinte.

![image alt <>](assets/login.gif){:height="300px" width="300px"}

## **Menu**

O menu da aplicação está sempre presente no fundo do ecrã para que seja mais fácil e rápido aceder às várias opções. Este é constituído por quatro ligações às páginas mais importantes: INÍCIO, MINHAS TAREFAS, PESQUISA e MAIS.

![image alt <>](assets/menu.png)

Ao selecionar um dos componentes presentes no menú, surge uma barra superior azul para identificar o componente selecionado.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![foto](assets/select_menu.png){:height="250px" width="250px"} ![foto](assets/selected_menu.png){:height="250px" width="250px"}

### Início

O separador "Início" é a página inicial que apresenta o sumário de tarefas agregadas por tipo.  Desta forma, estão vísiveis os tipos de tarefas com tarefas disponíveis para processamento ou configurados para surgir no sumário mesmo quando não existem tarefas por realizar.

![image alt <>](assets/inicio.png)

### Minhas Tarefas

O separador "Minhas Tarefas" contém a lista de tarefas armazenadas localmente no dispositivo. Estas podem ser divididas em dois tipos: PENDENTES (tarefas em que foi efetuado o download mas que ainda não foi efetuada nenhuma operação) e EM PROGRESSO (tarefas que já foram iniciadas pelo utilizador e que ainda não foram terminadas).

![image alt <>](assets/user_tasks.png)

### Pesquisa

O separador "Pesquisa" é um facilitador que permite a pesquisa de artigo por nome, descrição, EAN ou SKU. Ao selecionar um artigo, é apresentada a respetiva ficha do artigo. O template da Ficha de Artigo (as informações apresentadas e a sua ordem de apresentação) são definidas pelo cliente. 

Para aceder a informação de um determinado artigo fora do contexto de tarefa, o utilizador poderá aceder ao facilitador pesquisa presente na parte inferior da aplicação móvel. 

De seguida deverá pesquisar o artigo pretendido mediante a picagem do EAN ou inserção manual da informação. De forma imediata é apresentada a ficha de artigo com toda a informação disponibilizada pelo cliente. De notar que a informação apresentada na Ficha de Artigo é parametrizável por cliente e que pode conter qualquer informação que o cliente pretenda dar visibilidade na operação. 

![image alt <>](assets/search_item.gif){:height="300px" width="300px"}

#### Modal de artigo

Durante o tratamento de uma tarefa é possível consultar a Ficha de Artigo de um determinado produto. Para isso o utilizador deve aceder ao botão "Informação" presente no canto superior direto da modal de artigo. De imediato a aplicação direciona para a Ficha de Artigo.

![image alt <>](assets/search_item_modal.gif){:height="300px" width="300px"}

### Mais

O separador "Mais" contém acessos para formulários da aplicação. As opções disponíveis dependem de parametrizações ao cliente.

???+ example "Formulários disponíveis:"
    * **Relatório de atividades:** Registo de todos os erros que possam ter ocorrido durante a utilização da aplicação, garantindo assim uma análise mais precisa de eventuais problemas. 
    * **Impressoras:** Formulário para seleção de impressora e tipo de etiqueta. 
    * **Impressão de Etiquetas:** Formulário para acesso ao tipo de tarefa de impressão de etiquetas adhoc. Esta opão está disponível mediante [parametrização](app_settings.md#show-labelprint-on-summary).
    * **Alterar Palavra-Passe:** Formulário que permite alterar a palavra-passe associada ao utilizador logado. Esta opção está disponível mediante [parametrização](app_settings.md#allow-change-password).
    * **Terminar sessão:** Formulário para finalizar sessão na aplicação. 

![image alt <>](assets/plus_options_2.png)

## **Tarefas**

### Lista de Tarefas

No menu INÍCIO são apresentados os tipos de tarefa disponíveis para processamento. Quando é selecionado um tipo de tarefa, surge a lista de tarefas disponíveis para o tipo selecionado. Para cada tarefa é apresentado o nome, a sua disponibilidade, a data de criação e o utilizador que está a trabalhar na tarefa (se aplicável).

![image alt <>](assets/available_tasks.png)


### Download da Tarefa
Ao efetuar download  de uma tarefa, esta fica atribuída ao utilizador logado e toda a informação é armazenada localmente no dispositivo. 

Desta forma, é possível processar tarefas sem conectividade à internet, já que todas as informações necessárias ficam guardadas no dispositivo (à exceção de informação que é obtida no momento da picagem, como é o caso do SOH do artigo ou o preço). 

Para realizar o download diretamente sem pergunta de confirmação, o utilizador pode fazer "long press" sobre a tarefa desejada.

Mediante configuração ao cliente é possível definir se é permitido o download de tarefas em execução e que não estejam previamente guardadas na memória do dispositivo. Esta proteção tem como objetivo não permitir ao mesmo utilizador realizar em simultaneo download da tarefa em diversos dispositivos pois, desta forma, poderia implicar perda de trabalho previamente realizado. 

![image alt <>](assets/download_task.gif){:height="300px" width="300px"}

### Processamento da Tarefa
O tratamento de tarefas orientadas seguem todas o mesmo fluxo base, independentemente do tipo de tarefa. 

Após download de uma tarefa, o utilizador tem uma lista de artigos para se guiar durante o tratamento da mesma. Inicialmente, estes artigos surgem todos na tab PENDENTES até que o utilizador faça alguma alteração no artigo.

Ao picar um código de barras ou ao selecionar manualmente um artigo, o sistema verifica se pertence à lista de artigos da tarefa. No caso de não pertencer, existem duas opções:

* Se a tarefa permitir adição de artigos não previstos, o utilizador tem a possibilidade de adicioná-lo à tarefa e tratá-lo com os restantes artigos. (1)
* Se não permitir, o utilizador é informado que o artigo em questão não pertence à tarefa e não pode processar o mesmo. (2)

 ![foto](assets/adhoc_item.png){:height="350px" width="350px"}  ![foto](assets/not_adhoc_item.png){:height="350px" width="350px"}

<!--Se o artigo pertencer à tarefa, surge uma modal com detalhe do artigo, dividido em três secções:

![image alt <>](assets/item_modal.png)-->

### Criação de tarefas Adhoc

O utilizador pode criar tarefas adhoc de três formas distintas: **Com lista**, **Sem lista** ou **Por estrutura hierárquica**. Para isso deve selecionar o ícone botão "mais" (+) presente no canto superior direito da tarefa. 

#### Sem Lista

O utilizador deve pressionar o botão "mais" (+) presente no canto superior direito e escolher a opção **SEM LISTA**. Após a criação da tarefa, o utilizador é reencaminhado para o ecrã de tratamento onde pode iniciar o tratamento de artigos. 

![image alt <>](assets/create_without_list_task.gif){:height="300px" width="300px"}

#### Com Lista

O utilizador deve pressionar o botão "mais" (+) presente no canto superior direito e escolher a opção **COM LISTA**. Após a criação da tarefa, o utilizador é reencaminhado para o ecrã onde vai escolher qual a lista que quer tratar das que estão disponiveis. Após a seleção o utilizador pode começar o tratamento de artigos. 

![image alt <>](assets/create_with_list_task.gif){:height="300px" width="300px"}

#### Por Estrutura Hierárquica

O utilizador deve pressionar o botão "mais" (+) presente no canto superior direito e escolher a opção  **POR ESTRUTURA HIERÁRQUICA**. Após a criação da tarefa, o utilizador é reencaminhado para o ecrã onde vai selecionar o nível da estrutura hierárquica que pretende, sendo o primeiro nível de carácter obrigatório e os seguintes facultativos. Após a seleção, o utilizador pode começar o tratamento de artigos.

![image alt <>](assets/create_hs_task.gif){:height="300px" width="300px"}

## **Configuração de Impressora**

O utilizador enquanto esta a tratar uma tarefa, pode alterar a impressora em qualquer altura sem ter que sair da tarefa. 

Ao realizar um long press sobre  o icone ![foto](assets/printer.png){:height="20px" width="20px"} surge um pop up com a opção de escolha da nova impressora, o número de etiquetas quer imprimir e em alguns casos o tipo de etiqueta. 

Caso queira guardar as novas definições sem imprimir etiqueta também o pode fazer selecionado a opção "Guardar Impressora". 

![image alt <>](assets/printing.png)

## **Resumo**

O utilizador pode aceder ao resumo da tarefa em qualquer momento, através do ícone ![foto](assets/approve_task.png){:height="15px" width="15px"}  presente no canto superior direito no ecrã da tarefa. 

Neste momento é apresentado ao utilizador uma lista de artigos não tratados. Nesta listagem, o utilizador pode avançar para o resumo sem qualquer ação, ou marcar todos os artigos não tratados como não encontrados.

Mediante configuração ao cliente é possível definir se um determinado tipo de tarefa pode ser aprovada com artigos pendentes de tratamento ou se é obrigatório o processamento dos mesmos para aprovação da tarefa. 

![image alt <>](assets/resume.png)

O resumo fornece uma visão geral da tarefa e do seu processamento. A aplicação tem a capacidade de calcular individualmente os resources da tarefa, tais como: artigos, caixas, quantidades e datas de validade. Os algoritmos de cálculo disponíveis no resumos são os seguintes:


* PREVISTOS: resources previstos na tarefa.
* PENDENTES: resources  previstos na tarefa que não foram tratados.
* PROCESSADOS: resources processados pelo utilizador:
    * NÃO ENCONTRADOS,
    * PARCIALMENTE PROCESSADOS,
    * PROCESSADOS EM EXCESSO,
* NÃO PLANEADOS: Resources adiconados adhoc na tarefa.


Esta informação é agrupada por seções e podem ser definidas ao tipo da tarefa. O descritivo do cálculo pode também ser definido consoante o tipo de tarefa, como por exemplo: os artigos tratados podem ser remarcados na Remarcação, recebidos na Receção e auditados numa Auditoria. O detalhe de cada resumo poderá ser consultado no detalhe de cada tipo de tarefa.

Para mais detalhes, consultar a seção Resumo em cada tipo de tarefa.

Neste ecrã também é possível LIBERTAR ou APROVAR a tarefa.

![image alt <>](assets/resume_2.png)


## TAREFAS - PREÇO

### **REMARCAÇÃO**

A funcionalidade de Remarcação permite atualizar diariamente as etiquetas de loja, dos artigos que sofrem alterações de preço e/ou outras características que são comunicadas ao cliente. 

Mediante configuração é possível segmentar os artigos para remarcar em diversos tipos de tarefas de Remarcação: Subida de Preço, Descida de Preço, Entrada em Promoção, Saída de Promoção, Desconto em Cartão e Remarcação Emergencial.

#### Listagem de artigos

Ao entrar no menu Remarcação, o utilizador terá acesso à lista de tarefas disponíveis que pode descarregar para o dispositivo. Após o download, é apresentada uma lista de artigos a tratar com o número de etiquetas a imprimir. Ao selecionar manualmente o artigo, surge a modal com a informação do artigo picado, o preço antigo, preço novo e a quantidade a imprimir.

Se a tarefa for configurada com modo de impressão automática, é impressa a etiqueta, por default, do artigo após picagem do mesmo.  

![image alt <>](assets/relabelling_item_list.png){:height="300px" width="300px"} 

A listagem de artigos das tarefas de Remarcação contém a seguinte informação:

* **Informação do artigo:** Fotografia (se aplicável), descrição, EAN e SKU.
* **Informação da dinâmica de preço:** Da parte esquerda do artigo encontra-se um símbolo composto por uma seta que identifica se o artigo é uma subida ou descida de preço. 
* **Quantidade:** Da parte direita da listagem está a informação de quantidade tratada e quantidade expectável para remarcação. Formato: Quantidade tratada/Quantidade expectável. 

#### Filtro

Na listagem de artigos nas tarefas de Remarcação é possível filtrar os artigos apresentados na lista. Mediante configuração, o TMR apresenta a opção "FILTRAR" que permite filtrar os artigos apresentados na lista mediante as condições "Subidas de Preço" e "Descidas de Preço".

Dependendo da operação de loja, pode ser importante apresentar as subidas de preço e as descidas de preço em separado pois podem ser tratadas em momentos da operação diferentes.

![image alt <>](assets/relabelling_filter.png){:height="300px" width="300px"}

#### Ordenação

Na listagem de artigos nas tarefas de Remarcação é possível ordenar os artigos apresentados na lista. Mediante configuração, o TMR apresenta a opção "ORDENAR" que permite ordenar os artigos apresentados na lista mediante algumas opções também elas configuráveis. Também mediante configuração é possível definir um valor default para ordenação de forma que no download da tarefa os artigos são automáticamente ordenados pelo valor configurado como default. 

Opções de ordenação:

* **Alterados (subidas-descidas):** Apresenta primeiro as subidas e depois as descidas.
* **Alterados (descidas-subidas):** Apresenta primeiro as descidas e depois as subidas.
* **Estrutura Hierárquica:** Apresenta os artigos ordenados pelo identificador da estrutura hierárquica - do menor para o maior.
* **Marca (A-Z:** Apresenta os artigos oredenados por marca de A a Z)
* **Marca (Z-A:** Apresenta os artigos oredenados por marca de Z a A)

![image alt <>](assets/relabelling_order_multi.png){:height="300px" width="300px"}

#### Modal de artigo

Mediante configuração ao cliente é possível definir se a modal de artigo abre de forma imediata na picagem de artigo ou se na picagem imprime de imediato a etiqueta. 

A modal de artigo contém a seguinte informação:

* **Informação do artigo:** Fotografia (se aplicável), status, descrição, EAN, SKU e SOH (se aplicável).
* **Dinâmica de preço:** Alerta de subida ou descida de preço, Preço antigo e Preço novo.
* **Quantidade expectável:** Informação de quantidade de etiquetas a remarcar no campo "A Imprimir".
* **Input de Quantidade:** Input para definir a quantidade de etiquetas remarcadas.
* **Botão Não Encontrei/Continuar:** Permite definir o artigo como Não encontrado. Se a quantidade no input for superior a zero, o botão Não Encontrei é subtituido pelo botão "Continuar".

![image alt <>](assets/relabelling_item_modal.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Remarcação contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos nas tarefas.
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados em que a quantidade remarcada é igual à quantidade expectável.
    * **PARCIAL:** Total de artigos processados em que a quantidade remarcada é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade remarcada é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa.

Em relação às unidades, o ecrã de resumo da tarefa de Remarcação contém a seguinte informação:

* **PREVISTOS:** Total de unidades previstas nas tarefas.
* **PROCESSADOS:** Total de unidades processadas nas tarefas. Nos processados existem as seguintes métricas:
  * **EM EXCESSO:** Total de unidades processadas em que a quantidade remarcada é superior à quantidade expectável.
  * **NÃO ENCONTRADOS:** Total de unidades processadas como não encontradas.

![image alt <>](assets/relabelling_resume.png){:height="300px" width="300px"}

### **IMPRESSÃO DE ETIQUETAS**

A funcionalidade de Impressão de Etiquetas permite ao utilizador realizar impressão de etiquetas adhoc. Desta forma, sempre que a operação de loja identifica artigos que não tenham etiqueta ou que esta esteja danificada, podem utilizar a funcionalidade de Impressão de Etiquetas para realizar a impressão. 

Mediante configuração ao cliente, a funcionalidade de Impressão de Etiquetas pode estar disponível no sumário de tarefas ou no menú da aplicação. 

![image alt <>](assets/labelprint_on_menu.gif){:height="300px" width="300px"}

#### Modal de artigo

Mediante configuração ao cliente é possível definir se a modal de artigo abre de forma imediata na picagem de artigo ou se na picagem imprime de imediato a etiqueta. 

A modal de artigo contém a seguinte informação:

* **Informação do artigo:** Fotografia (se aplicável), status, descrição, EAN, SKU e SOH (se aplicável).
* **Quantidade impressa:** Informação da quantidade de etiquetas já impressas para determinado artigo.
* **Input de Quantidade:** Input para definir a quantidade de etiquetas a imprimir.
* **Botão Imprimir:** Ação de imprimir a quantidade de etiquetas definidas.

Na modal do artigo é possível também alterar a etiqueta a ser impressa e a impressora utilizada. Para isso, o utilizador deve manter pressionado o icon da impressora e de imediato irá surgir um ecrã com as impressoras disponíveis e uma listagem de etiquetas que podem ser utilizadas.

![image alt <>](assets/labelprint_modal_3.gif){:height="300px" width="300px"}

### **AUDITORIA DE PREÇO**

O objectivo do tipo de tarefa Auditoria de Preço é permitir auditar os preços presentes em loja. Através da picagem da etiqueta, o TMR realiza a comparação dos valores da etiqueta (label_price), os valores do ERP (ERP_price) e os valores do POS (POS_price).

*[label_price]: Preço impresso na etiqueta.
*[ERP_price]: Serviço do ERP que informa o preço do artigo em vigor.
*[POS_price]: Serviço do POS/PDV que informa o preço do artigo que será utilizado no momento do registo ao cliente.

Desta forma é possível auditar os preços presentes em loja e no caso de existir alguma divergência, o TMR imprime de imediato uma nova etiqueta com o preço atualizado.

#### Localização da Auditoria de Preço

No processo de [Criação de tarefas Adhoc](#criacao-de-tarefas-adhoc) o utilizador pode definir uma localização para a tarefa. Esta localização fica associada à tarefa e possibilita definir em que zona da loja foi realizada a auditoria. 

Existem duas localizações disponíveis para escolha:

* Loja
* Armazem

A localização escolhida pelo utilizador fica associada à tarefa e fica disponível para consulta no detalhe da tarefa no Cockpit. Com esta informação é possível ao gestor de operação perceber quais os artigos auditados em cada localização e a quantidade total de preços divergentes ou convergentes em cada localização

![image alt <>](assets/priceaudit_location.png){:height="300px" width="300px"}

#### Inserção manual do preço

Durante o tratamento de uma tarefa, quando é picado um EAN ou introduzido manualmente a informação de artigo, o TMR identifica que não foi informado o preço da etiqueta e pede a inserção manual desse valor ao utilizador.

![image alt <>](assets/priceaudit_manual_insert.png){:height="300px" width="300px"}

No caso de ser picada uma etiqueta do TMR que informe o preço do artigo, não é solicitado ao utilizador que informe o preço. Neste caso o TMR direciona o utilizador de forma imediata para a modal do artigo onde realiza a comparação dos preços.

#### Modal de artigo e auditoria de preços

Após obtenção do preço da etiqueta (por leitura de etiqueta ou introdução manual) surge de forma imediata a modal de artigo. Na abertura da modal, o TMR faz o pedido de preço ERP e POS através de uma chamada aos serviços disponibilizados pelo cliente. Realizando a comparaçãos dos valores é possível ao TMR concluir se o preço de etiqueta é Divergente ou Convergente.

*[Divergente]: Preço da etiqueta é diferente de preço ERP ou preço POS
*[Convergente]: Preço da etiqueta é igual a preço ERP e preço POS 

**Convergente**

No caso dos valores serem iguais, o TMR entende que a auditoria é convergente. Neste cenário apresenta a mensagem de "Preços Corretos". O artigo é dado como processado de forma automática sem necessidade de nenhuma ação adicional.

![image alt <>](assets/priceaudit_equal_price.png){:height="300px" width="300px"}

**Divergente**

No caso dos valores serem diferentes, o TMR entende que a auditoria é divergente. Neste cenário apresenta a mensagem de "Divergência Encontrada". De forma imediata é gerada a impressão de uma nova etiqueta e o artigo é considerado processado. Se existir a necessidade de reimpressão da etiqueta, o utilizador tem disponível o botão de impressão no canto superior direito da modal.

![image alt <>](assets/priceaudit_not_equal_price.png){:height="300px" width="300px"}

#### Auditoria de dois preços

A tarefa de Auditoria de Preço permite também auditar em simultâneo dois preços. Isto apenas de aplica a clientes que disponibilizem dois preços na etiqueta e que o serviço de preço que retorne dois preços (ERP 1, ERP 2 POS 1 e POS 2).

A auditoria de dois preços mantem o mesmo fluxo descrito anteriormente apenas com a diferença de auditar em simultâneo dois preços. Neste sentido um artigo é divergente se um ou os dois preços forem diferentes e é convergente se os dois preços forem iguais.

![image alt <>](assets/priceaudit_two_prices.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Auditoria de preço contém a seguinte informação:

* **PREVISTOS:** Total de artigos previstos nas tarefas (apenas se aplica a tarefas orientadas).
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **DIVERGÊNCIAS:** Total de artigos processados com divergência.
    * **SEM ETIQUETA:** Total de artigos processados com informação manual do preço da etiqueta.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa (apenas se aplica a tarefas orientadas).

![image alt <>](assets/priceaudit_resume.png){:height="300px" width="300px"}

## TAREFAS - REPOSIÇÃO

### **AUDITORIA DE VARRIMENTO**

O objectivo do tipo de tarefa Auditoria de Varrimento é permitir ao utilizador verificar a presença de um determinado grupo de artigos em loja. 

Para esse efeito, o utilizador percorre a loja e vai picando os artigos presentes na placa de vendas permitindo ao TMR receber a informação de que o artigo está presente em loja.

Após o término da tarefa, esta informação fica disponível para consulta e exportação no Cockpit. Sobre esta informação serão realizados cálculos adicionais que permitem inferir a percentagem de rutura por tarefa. 

Com esta informação é possível ao gestor de loja saber a quantidade de artigos em rutura e a informação de stock disponível por artigo. 

#### Criação da tarefa de Auditoria de Varrimento

Em relação à criação de tarefas de Auditoria de Varrimento existem as seguintes possibilidades:

* **Adhoc com base em lista:** Possiblidade de criação de tarefas diretamente no dispositivo móvel definindo uma lista de artigos previamente existente. Essa lista de artigos é criada no Backoffice do TMR. Para mais detalhes consultar o capítulo de [criação de tarefas com base em lista](#com-lista).

* **Adhoc com base em estrutura hierárquica:**  Possiblidade de criação de tarefas diretamente no dispositivo móvel definindo o nível pretendido da estrutura hierárquica.  Para mais detalhes consultar o capítulo de [criação de tarefas por estrutura hierárquica](#por-estrutura-hierárquica).

* **Calendarizadas:** Possibilidade de criação de tarefas via TMR Scheduler. Através do Scheduler é possível definir um agendamento único ou uma recorrência automática na criação de tarefas. Por cada agendamento criado é possível definir uma lista de artigos a auditar ou um nível específico da estrutura hierárquica.

#### Auditoria de Varrimento orientada

Mediante configuração ao cliente é possível definir que as tarefas de Auditoria de Varrimento são orientadas. Isto significa que a listagem de artigos pendentes está disponível para visualização durante o tratamento da tarefa. Esta listagem de artigos auxilia o utilizador a perceber quais os artigos previstos para tratamento na tarefa através da fotografia (se aplicável) e descrição do artigo. 

![image alt <>](assets/scanaudit_oriented.png){:height="300px" width="300px"}

#### Auditoria de Varrimento não orientada

Mediante configuração ao cliente é possível definir que as tarefas de Auditoria de Varrimento não são orientadas. Isto significa que a listagem de artigos pendentes não está disponível para visualização durante o tratamento da tarefa. Isto permite não influenciar o utilizador com uma listagem de artigos definidas apesar de o TMR validar em cada picagem se o artigo pertence à tarefa. Também mediante configuração ao cliente é possível definir se o utilizador pode adicionar artigos não previstos nas tarefas ou se apenas pode picar artigos previamente definidos na criação da mesma. 

![image alt <>](assets/scanaudit_not_oriented.png){:height="300px" width="300px"}

Em ambos os cenários (orientada ou não orientada) os artigos picados serão apresentados na aba de Processados.

#### Pedido de reposição urgente

Durante o tratamento da tarefa de Auditoria de Varrimento está disponível ao utilizador o pedido de Reposição urgente. Este facilitador está presente no canto superior direito da modal de artigo. Através deste facilitador é possível ao utilizador definir para determinado artigo um pedido de reposição e a respetiva quantidade.

De forma imediata será gerada uma tarefa de Separação para o artigo pedido.

![image alt <>](assets/replenish_item_on_modal.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Auditoria de Varrimento contém a seguinte informação:

* **PREVISTOS:** Total de artigos previstos na tarefa (apenas se aplica a tarefas orientadas).
* **PROCESSADOS:** Total de artigos processados na tarefa. Nos processados existem as seguintes métricas:
    * **PREVISTOS:** Total de artigos processados previstos na tarefa.
    * **NÃO PREVISTOS:** Total de artigos processados não previstos na tarefa (apenas se aplica nas tarefas que permite adicionar artigos não previstos).
* **PENDENTES:** Total de artigos não processados na tarefa (apenas se aplica a tarefas orientadas).

![image alt <>](assets/scanaudit_resume.png){:height="300px" width="300px"}

### **AUDITORIA EXTERNA**

O objectivo do tipo de tarefa Auditoria Externa é permitir ao utilizador verificar a presença de um determinado grupo de artigos em loja. Por esse motivo, a Auditoria Externa é semelhante a Auditoria de Varrimentos mas com algumas especifícidades:

#### Especificidades

* Foi desenhada para ser utilizada por uma equipa externa aos processos de loja, ou seja, permite um fluxo de processo distinto ao da Auditoria de Varrimento. Tal como na anterior, os cálculos da percentagem de rutura também são realizados com base nos dados recolhidos. 

* Permite que equipa externa à loja audite através deste tipo de tarefa o trabalho da operação realizado na Auditoria de Varrimento. No Cockpit é possível comparar as percentagens de rutura cálculadas entre os dois tipos de tarefa.

* Dado que a Auditoria Externa foi desenhada para ser operada por uma equipa externa à loja, é possível definir (mediante configuração ao cliente) a quantidade de zonas de contagem e a identificação de cada uma. Ao contrário da Auditoria de Varrimento que gera apenas uma tarefa, a Auditoria Externa permite configurar zonas de contagem que poderão ser executadas em simultâneo por operadores diferentes.

* No fecho de todas as zonas da tarefa é realizada a consolidação dos dados e os cálculos seguintes (ex. percentagem rutura) terão como base os dados já consolidados.

#### Zonas de contagem

Mediante configuração ao cliente é possível definir zonas de contagem para as tarefas de Auditoria Externa. 

Desta forma, na geração das tarefas (adhoc ou calendarizadas) são criadas zonas de contagem mediante quantidade configurada e a sua respetiva identificação. As zonas de contagem ficam sempre associadas à tarefa mãe de Auditoria Externa.

As zonas de contagem podem ser orientadas (com artigos previstos visíveis) ou não orientadas (lista de artigos previstos não visíveis ao utilizador) mediante configuração ao cliente. 

![image alt <>](assets/externalaudit_zones_childs.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Auditoria Externa contém a seguinte informação:

* **PREVISTOS:** Total de artigos previstos nas tarefas (apenas se aplica a tarefas orientadas).
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **PREVISTOS:** Total de artigos processados previstos na tarefa.
    * **NÃO PREVISTOS:** Total de artigos processados não previstos na tarefa (apenas se aplica nas tarefas que permite adicionar artigos não previstos).
* **PENDENTES:** Total de artigos não processados na tarefa (apenas se aplica a tarefas orientadas).

![image alt <>](assets/scanaudit_resume.png){:height="300px" width="300px"}

### **VERIFICAÇÃO DE PRESENÇA**

O objectivo do tipo de tarefa Verificação de Presença é permitir confirmar a presença dos artigos não encontrados nas tarefa de Auditoria de Varrimento e Auditoria Externa. Por este motivo, as tarefas de Verificação de Presença não são criadas ahdoc nem calendarizadas mas geradas automáticamente no fecho das tarefas de Auditoria de Varrimento e Externa.

#### Geração de tarefas

A geração das tarefas de Verificação de Presença são configuráveis ao cliente, ou seja, é possível definir por cliente se estas são geradas no fecho das tarefas de Auditoria de Varrimento e Externa.

Se a configuração permitir a geração das tarefas de Verificação de Presença, esta geração segue a seguinte regra:

* Os artigos **não tratados** e **não encontrados** no Varrimento e Auditoria Externa e com stock maior que zero serão automáticamente inseridos na tarefa de Verificação de Presença.

* Existe também a possibilidade de filtrar por estado do artigo. Ou seja, é possível configurar o estado dos artigos que serão incluídos nas tarefas de Verificação de Presença. Por exemplo, é possível definir se os artigos Inativos serão incluídos na tarefa ou apenas artigos Ativos e Descontinuados.

#### Modal de artigo

Durante o processamento de tarefas de Verificação de Presença, ao picar um artigo a modal abre de forma imediata. Este comportamento é configurável por cliente. 

A modal de artigo contém a seguinte informação:

* **Informação do artigo:** No cabeçalho da modal está presenta a fotografia (se aplicável), status, informação de SKU, EAN e SOH (se aplicável).
* **PS e Vendas:** Informação de PS e Vendas do dia (apenas aplicável a clientes que disponibilizem esta informação).
* **Motivo:** Dropdown que disponibiliza dois motivos:
    * Artigo no linear
    * Artigo noutro local

Estes motivos permitem identificar o local em que foi encontrado o artigo. Esta informação fica disponível para consulta no detalhe de artigo no TMR Cockpit. Esta informação permite à operação de loja perceber se os artigos estão nos locais corretos ou se é necessário alterações ao planograma de loja. 

Mediante configuração ao cliente é possível definir se os motivos estão presentes na modal de artigo para seleção. Se estiverem presentes o seu preenchimento é obrigatório pois não é possível processar o artigo sem definir um motivo.

![image alt <>](assets/presencecheck_modal.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Verificação de Presença contém a seguinte informação:

* **PREVISTOS:** Total de artigos previstos nas tarefas.
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **PREVISTOS:** Total de artigos processados previstos na tarefa.
    * **NÃO PREVISTOS:** Total de artigos processados não previstos na tarefa (apenas se aplica nas tarefas que permite adicionar artigos não previstos).
* **PENDENTES:** Total de artigos não processados na tarefa.

![image alt <>](assets/presencecheck_resume.png){:height="300px" width="300px"}

### **AUDITORIA DE RUTURAS**

Uma Auditoria de Rutura permite identificar em loja artigos que estejam em rutura parcial (existe o artigo na loja mas em quantidade menor que o espaço disponível) ou em rutura total (não existe o artigo em loja). Possibilita também a confirmação da quantidade a repor de um artigo no linear.

No momento de criação da tarefa de Auditoria de Ruturas é possível definir uma localização. Esta informação irá transitar para a tarefa de Separação consequente permitindo ao utilizador consultar de forma mais ágil a localização dos artigos para reposição.

![image alt <>](assets/stockoutaudit_location.png){:height="300px" width="300px"}

Existem duas modalidades para Auditoria de Ruptura: 

* Orientada – O utilizador possui uma lista de artigos para orientar o processamento da tarefa.
* Adhoc - Sem lista de artigos previamente defnida. A criação de tarefas adhoc pode ser consultada  na secção [Criação de tarefas Adhoc](#criacao-de-tarefas-adhoc).

#### Orientada

Ao selecionar Auditoria de Rutura no sumário de tarefas, o utilizador terá acesso à lista de tarefas disponíveis e que pode descarregar para o dispositivo, permitindo assim a sua execução em modo offline. Note-se que em modo offline o utilizador não terá acesso a certas informações como Presentation Stock, Vendas do dia e Stock on Hand, visto que são pedidos online durante o tratamento da tarefa.

Após o download da tarefa, é apresentada uma lista de artigos a tratar, que podem ou não ter uma quantidade expectável associada. Neste último caso, é esperado que o utilizador confirme que de facto é essa a quantidade do artigo que deve ser reposta, podendo alterar o valor em qualquer altura.

![image alt <>](assets/stockoutaudit_item_list.png){:height="300px" width="300px"}

#### Modal de artigo

Ao picar um código de barras ou selecionando o artigo da listagem, surge de forma imediata a modal do artigo. Nesta modal existem dois inputs possíveis: 

  * **Quantidade Linear:** Permite introduzir a quantidade de artigos presentes no linear.
  * **Quantidade a Repor:** Permite introduzir a quantidade desejada para reposição. Esta quantidade irá transitar para a consequente tarefa de Separação como quantidade a separar.  

![image alt <>](assets/stockoutaudit_item_modal.png){:height="300px" width="300px"}

Na modal está presente também a informação de "PS" e "VENDAS". Esta informação consultada online permite calcular a quantidade a repor de um determinado artigo mediante a quantidade presente no línear. 

**Nota:** Estes valores apenas estão disponíveis para clientes que detenham um serviço que possibilita a consulta dessa informação.

*[PS]: Presentation Stock - Valor que define a quantidade ideal de stock na prateleira
*[VENDAS]: Serviço que devolve a informação de vendas do dia para determinado produto. 

No caso de existir informação de PS, o utilizador tem apenas que introduzir a Quantidade Linear e a Quantidade a Repor é calculada automaticamente da seguinte forma:

![image alt <>](assets/stockoutaudit_PS.png){:height="500px" width="500px"}

Mesmo sendo calculada de forma automática, o utilizador pode alterar o valor do input "Quantidade a repor" em qualquer altura. 

##### Facilitador Caixas

A modal de artigo do tipo de tarefa Auditoria de Rutura inclui também um facilitador de Unidades/Caixa. Isto permite ao utilizador colocar uma quantidade a repôr informando a quantidade de caixas presentes e a quantidades de unidades por caixa.

**CAIXA**

Este facilitador permite indicar a quantidade de caixas e a quantidade de unidades por caixa dentro da lista pré-existente. O resultado deste cálculo é colocado no input "Quantidade a repor"

![image alt <>](assets/stockoutaudit_box.png){:height="300px" width="300px"}

**CAIXA LIVRES**

Este facilitador é semelhante ao anterior com a particularidade de permitir ao utilizador informar livremente a quantidade de unidades por caixa. O resultado do cálculo tem o mesmo comportamento referido anteriormente.

![image alt <>](assets/stockoutaudit_box_free.png){:height="300px" width="300px"}

Depois de introduzida a quantidade a repor, o botão "Continuar" fica disponível. Ao selecionar "Continuar" a modal é fechada de imediato e o artigo transita para a aba dos processados com a quantidade indicada.

Se o utilizador não informar valor na quantidade a repor, fica disponível o botão "Não Encontrei". Se o utilizador selecionar "Não Encontrei" o artigo é processado como não encontrado e será descartado para a tarefa de separação consequente. 

![image alt <>](assets/stockoutaudit_item_not_found.png){:height="300px" width="300px"}

#### Clientes Fashion

No sentido de adaptar a tarefa de Auditoria de Rutura à especificidade do catálogo fashion, está presente na modal de artigo (apenas para clientes fashion) uma funcionalidade de listagem de artigos skus que pertencem ao mesmo produto. O objetivo desta funcionalidade é permitir auditar a rutura de skus que não estão presentes no linear mediante a picagem de um sku irmão (que pertence ao mesmo produto).

Para isto, o utilizador deverá selecionar o botão "Mais Artigos" presente modal  e o TMR disponibiliza uma listagem de skus associados ao mesmo produto. Nesta listagem o utilizador poderá colocar quantidade a repôr em qualquer artigo e eles serão adiconados como adhoc nas tarefas. 

![image alt <>](assets/stockoutaudit_more_items_modal.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Auditoria de Ruturas contém a seguinte informação:

* **PREVISTOS:** Total de artigos previstos nas tarefas (apenas se aplica a tarefas orientadas).
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados com quantidade tratada igual à quantidade prevista.
    * **PARCIAL:** Total de artigos processados com quantidade tratada inferior à quantidade prevista.
    * **EM EXCESSO:** Total de artigos processados com quantidade tratada superior à quantidade prevista.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa (apenas se aplica a tarefas orientadas).

![image alt <>](assets/stockoutaudit_resume.png){:height="300px" width="300px"}

### **SEPARAÇÃO**

O objectivo do tipo de tarefa Separação é permitir ao utilizador separar os artigos para serem repostos em loja. As tarefas de Separação são sempre geradas por workflow de tarefas, nomeadamente via tarefas de Auditoria de Ruturas, pedidos de reposição urgente, Verificação de Presença, Auditoria de Varrimento ou Auditoria Externa.

Para ver o fluxo completo de tarefas que podem originar Separação consultar o separador [Workflow de tarefas](workflow.md).

#### Separação - Clientes de retalho Alimentar

As tarefas de Separação são sempre orientadas, ou seja, os artigos previstos na tarefa estão sempre visíveis ao utilizador. Desta forma o utilizador consegue perceber mais rapidamente quais os artigos pedidos para separação através do auxílio da fotografia (se aplicável) e da descrição do artigo.

Neste sentido, os artigos estão visíveis na listagem de pendentes e podem conter inclusive informação da quantidade a repôr - "Quantidade expectável". Se a tarefa que deu origem à separação não indicar quantidade esperada para determinado artigo, esta informação não está visível ao operador.

![image alt <>](assets/replenishprep_item_list.png){:height="300px" width="300px"}

##### Modal de artigo - Alimentar

Durante o processamento da tarefa, ao picar um produto é aberta de imediato a modal de artigo. Esta modal contém a seguinte informação:

* **Informação do artigo:** No cabeçalho da modal está presenta a fotografia (se aplicável), status, informação de SKU, EAN e SOH (se aplicável).
* **PS e Vendas:** Contém informação da quantidade a separar, PS e Vendas (se aplicável).
* **Input de Quantidade:** Input para definir a quantidade que vai separar de determinado artigo.
* **Botão Não Encontrei** Permite definir o artigo como Não encontrado. 
* **Botão "Devolução/Retirada:**" Permite definir que o artigo se encontra no local previsto mas que não se encontra em condições de ser separado. (exemplo: Artigo que se encontra em período de retirada e que por isso não pode ser reposto em loja para venda). Desta forma o artigo será processado com quantidade zero.
* **Caixas e Caixas Livres:** Facilitador de inserção de quantidade por "Caixas" e "Caixas Livres".

![image alt <>](assets/replenishprep_item_modal.png){:height="300px" width="300px"}

#### Separação - Clientes Fashion

As tarefas de Separação adaptam-se às necessidades da operação dos clientes fashion mediante configuração. Os clientes fashion têm um catálogo composto por produtos e skus. Neste catálogo os produtos são o modelo e os skus são as diversas cores e tamanhos. Exemplo: A camisola do modelo A é o produto e os skus são o tamanho L, M e XL. Da mesma forma, as diversas cores da camisola são também skus do mesmo produto.

Neste sentido, as tarefas de Separação têm um comportamento distinto para se adaptar às necessidades específicas do catálogo fashion.

##### Modal de artigo - Fashion

As tarefas de Separação dos clientes fashion são orientadas ao sku como as dos clientes alimentar. Ou seja, na listagem de artigos pendentes aparecem os skus (cores e tamanhos) que devem ser separados.

Durante o processamento da tarefa, ao picar um sku previsto abre de imediato a modal de artigo. Esta modal apresenta o sku picado e todos os skus que pertencem ao mesmo produto. Isto permite ao utilizador separar outras cores e tamanhos diferentes das inicialmente previstas na tarefa.

Para isso a modal contém um input de quantidade em cada sku e a informação da quantidade expectável. O utilizador poderá assim colocar quantidade em cada sku que pretende separar. Estes skus não previstos serão adicionados como adhoc na tarefa. 

![image alt <>](assets/replenisprep_fashion.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Separação contém a seguinte informação:

* **PREVISTOS:** Total de artigos previstos na tarefa.
* **PROCESSADOS:** Total de artigos processados na tarefa. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados em que a quantidade separada é igual à quantidade expectável.
    * **PARCIAL:** Total de artigos processados em que a quantidade separada é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade separada é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
    * **SEM QUANTIDADE:** Total de artigos processados Devolução/retirada.
* **PENDENTES:** Total de artigos não processados na tarefa.

![image alt <>](assets/replenishprep_resume.png){:height="300px" width="300px"}

### **REPOSIÇÃO**

O objectivo do tipo de tarefa Reposição é permitir ao utilizador confirmar a separação dos artigos repostos em loja. Por este motivos as tarefas de Reposição são orientadas e exclusivamente geradas no fecho de uma tarefa de Separação.

Os artigos tratados na Separação com quantidade maior que zero geram automáticamente, no fecho da tarefa, uma tarefa de Reposição. 

Tal como na Separação, as tarefas de Reposição são orientadas para permitir ao utilizador reconhecer mais rapidamente os artigos separados e que devem ser colocados em loja. 

Através da consulta no Cockpit é possível perceber se todos os artigos separados foram efetivamente repostos em loja. Este métrica é importante para perceber a qualidade da reposição de loja.

#### Modal de artigo

Durante o processamento da tarefa, ao picar um produto é aberta de imediato a modal de artigo. Esta modal contém a seguinte informação:

* **Informação do artigo:** No cabeçalho da modal está presenta a fotografia (se aplicável), status, informação de SKU, EAN e SOH (se aplicável).
* **Repor, Origem e Localização:** Contém informação da quantidade a repor, Origem do pedido e Localização (se aplicável).
* **Input de Quantidade:** Input para definir a quantidade que vai repôr de determinado artigo.
* **Botão "Devolução/Retirada:**" Permite definir que o artigo se encontra no local previsto mas que não se encontra em condições de ser separado. (exemplo: Artigo que se encontra em período de retirada e que por isso não pode ser reposto em loja para venda). Desta forma o artigo será processado com quantidade zero. Se a quantidade no input for maior que 0, o botão "Devolução/Retirada" desaparece e surge o "Continuar".
* **Caixas e Caixas Livres:** Facilitador de inserção de quantidade por "Caixas" e "Caixas Livres".

![image alt <>](assets/replenish_modal.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Confirmação contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos nas tarefas.
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados em que a quantidade separada é igual à quantidade expectável.
    * **PARCIAL:** Total de artigos processados em que a quantidade separada é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade separada é superior à quantidade expectável.
    * **SEM QUANTIDADE:** Total de artigos processados Devolução/retirada.
* **PENDENTES:** Total de artigos não processados na tarefa.

Em relação às unidades, o ecrã de resumo da tarefa de Confirmação contém a seguinte informação:

* **PREVISTOS:** Total de unidades previstas nas tarefas.
* **PROCESSADOS:** Total de unidades processadas nas tarefas. Nos processados existe a seguinte métrica:
    * **EM EXCESSO:** Total de unidades processadas em que a quantidade reposta é superior à quantidade expectável.
    * **PARCIAL:** Total de unidades processadas em que a quantidade reposta é inferior à quantidade expectável.

![image alt <>](assets/replenish_resume.png){:height="300px" width="300px"}

## TAREFAS - INVENTÁRIOS | RECONTAGEM | CONTAGEM DE STOCK

### **INVENTÁRIO**

A funcionalidade de Inventário permite verificar e corrigir a quantidade de artigos que existem em stock tanto na loja como em armazém. 

**Tarefas de Inventário, Zonas de Contagem, Leituras e Correção de Divergência**

As tarefas de **Inventário** são sempre integradas pelo ERP e o objetivo desta tarefas é o de permitir confirmar o stock de um determinado grupo de artigos mediante a picagem incremental do stock total existente em loja e armazém.

As tarefas de Inventário referidas no ponto anterior são tarefas agregadoras, ou seja, são tarefas que agregam em si uma determinada quantidade de tarefas filhas denominadas por **Zonas de Contagem**. Estas zonas são definidas no momento da integração do inventário e permitem definir uma determinada área geográfica da loja ou armazém onde os artigos serão contados. Isto significa, por exemplo, que na zona de contagem "Mercearia" apenas serão tratados artigos de mercearia que pertençam ao inventário. Da mesma forma, cada zona de contagem pode ser tratada por utilizadores diferentes permitindo uma execução mais ágil do próprio inventário. 

No momento da integração das tarefas de Inventário é ainda possível definir se cada zona de contagem terá **uma ou duas leituras obrigatórias**. Se a zona de contagem incluir segunda, leitura são geradas duas tarefas para essa mesma zona de contagem e no nome da tarefa será incluída a informação a que leitura corresponde determinada tarefa. O benefício de duas leituras obrigatórias consiste na redução significativa do erro de execução do inventário pois cada zona de contagem será tratada duas vezes e todos os artigos serão picados duas vezes.

As duas leituras referidas anteriromente têm a obrigatoriedade de **não divergência**, ou seja, os artigos e quantidades processados nas duas leituras terão de ser exatamente iguais. No caso de haver diferença entre produtos ou quantidades, o TMR gera de imediato uma tarefa de **Correção de Divergência** associada à zona de contagem em tratamento. Esta tarefa é disponibilizada de imediato no fecho da segunda leitura e permite corrigir as diferenças encontradas entre as duas leituras anteriores.

#### Tarefa de Inventário e Zonas de Contagem

Ao entrar no menú Inventário, o utilizador terá acesso à lista de tarefas disponíveis, que pode tratar no momento. Cada tarefa de Inventário é constituida por um conjunto de zonas, o que permite que o mesmo inventário possa ser tratado por mais do que um utilizador em simultâneo.

Na listagem de tarefas de Inventário existe um facilitador de pesquisa que permite ao utilizador pesquisar determinada zona de contagem mediante leitura do código de barras. Este facilitador está disponível para os clientes que incluem código de barras identificador da zona de contagem. O TMR perimite configurar uma regra de código de barras que identifica o inventário e a respetiva zona de contagem. Desta forma, quano o utilizador faz a leitura deste código de barras, a aplicação pesquisa a zona de contagem e o respetivo inventário e realiza de forma automática o download da respetiva tarefa.

![image alt <>](assets/inventory_zones.gif){:height="300px" width="300px"}

#### Criação de Zonas Adhoc

No decorrer do Inventário, e no caso de não existirem zonas de contagem suficientes, o utilizador poderá pedir novas zonas de contagem diretamente na aplicação móvel. Essas zonas pedidas adhoc podem conter uma ou duas leituras obrigatórias.  Além disso, o utilizador pode definir o nome das zonas adhoc criadas na aplicação. Se já existir uma zona criada com o mesmo nome, a aplicação indicará na resposta qual o nome da zona que foi criada.

![image alt <>](assets/inventory_create_adhoc_2.gif){:height="300px" width="300px"}

#### Modal de artigo

No caso do artigo picado for um artigo de **Peso Variável**, deve abrir a modal de informação do artigo e preencher o peso lido da etiqueta no input de peso do artigo. No caso de picar mais artigos iguais, mas com peso diferente, o operador deve indicar sempre o peso do artigo lido neste input. A quantidade do artigo deve ser somada e apresentado no "TOTAL". 


![image alt <>](assets/inventory_modal_insert_price.png){:height="300px" width="300px"}

No caso do artigo picado ser de **Preço Variável**, deve pedir ao utilizador que insira o peso do artigo e, desta forma, calculado o preço por quilo. Nas picagens seguintes do mesmo artigo, este cálculo será usado para obter o peso do artigo lido. Na modal do artigo será apresentada a informação do peso acumulado do artigo, o preço por quilo calculado e o peso do artigo (calculado com base no preço por quilo). Este peso do artigo deve ser possível alterar. 

![image alt <>](assets/inventory_modal_insert_weight.png){:height="300px" width="300px"}

No caso de os artigos não serem de peso ou preço variável, a picagem pode ser feita de forma sucessiva (varrimento) ou incremental. Para que seja feita de forma incremental, é preciso aceder a modal e colocar o valor total de artigos existentes na zona.

Durante o tratamento da tarefa é possível alternar entre modo "Normal" e "Incremental". O utilizador deve aceder ao botão flutuante e clicar no input de alternar modo de picagem. 

![image alt <>](assets/inventory_picking_mode.gif){:height="300px" width="300px"}

Se o utilizador selecionar "Modo Normal" significa que a cada picagem é aberta a modal do artigo. Neste caso, a quantidade picada é apresentada no input da modal. 

Se o utilizador selecionar "Modo Incremental" significa qua a cada picagem não é aberta a modal do artigo. Neste caso, a quantidade picada é apresentada no alerta "Anular" presente na parte inferior do ecrã. 

![image alt <>](assets/inventory_allow_undo.png){:height="300px" width="300px"}

#### Correção de divergência

Quando existe Divergência de artigos entre a 1ª e 2ª leitura, será criada automaticamente uma tarefa para correção de Divergência. As tarefas de Correção de Divergência, são tarefas orientadas com todos os artigos divergentes na zona de contagem em questão. Ao abrir a modal de artigo está presente a informação da contagem da 1º e 2º leitura e o input para por a quantidade final do artigo. 

![image alt <>](assets/inventory_modal_diverngence.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo das tarefas de Inventário contém a seguinte informação em relação aos artigos:

* **PROCESSADOS:** Total de artigos processados nas tarefas. 

Em relação às unidades, o ecrã de resumo da tarefa de Inventário contém a seguinte informação:

* **PROCESSADOS:** Total de unidades processadas nas tarefas. 

![image alt <>](assets/inventory_resume.png){:height="300px" width="300px"}

### **RECONTAGEM**

As tarefas de Recontagem são tarefas geradas após a Crítica do Inventário. Estas tarefas servem para corrigir algum desajuste de stock detetado no momento da Crítica.

As recontagens são tarefas orientadas, em que os artigos foram selecionados durante a Crítica de Inventário para contagem e consequente ajuste de stock. No fecho das tarefas de Recontagem, a informação anterior será substituida pela informação atual.

#### Modal de artigo

A modal de artigo contém a seguinte informação:

* **Informação do artigo:** Fotografia (se aplicável), status, descrição, EAN, SKU e SOH (se aplicável e configurável).
* **Total:** Informação da quantidade previamente processada.
* **Input de Quantidade:** Input para definir a quantidade de unidades ou peso.
* **Botão Não Encontrei/Continuar:** Permite definir o artigo como Não encontrado. Se a quantidade no input for superior a zero, o botão "Não Encontrei" é subtituido pelo botão "Continuar".

![image alt <>](assets/recounting_modal.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Recontagem contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos na tarefa.
* **PROCESSADOS:** Total de artigos processados na tarefa. Nos processados existe a seguinte métrica:
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa.

Em relação às unidades, o ecrã de resumo da tarefa de Recontagem contém a seguinte informação:

* **PROCESSADOS:** Total de unidades processadas na tarefa. Nos processados existe a seguinte métrica:
  * **NÃO ENCONTRADOS:** Total de unidades processadas como não encontradas.


### **CONTAGEM DE STOCK**

A funcionalidade de Contagem de Stock permite contar artigos para acerto de stock em diferentes zonas da loja. De forma a garantir a agregação de tarefas das diferentes zonas, mediante configuração ao cliente,  a aplicação permite a criação de tarefas com afiliação. Isto significa que no momento da criação da tarefa, é gerada uma tarefa de contagem "mãe" com uma tarefa "filha" por cada zona definida.

Exemplo:

* **Contagem de Frescos:** – tarefa "mãe".
    * **Frescos Armazém:** - Tarefa "filha".
    * **Frescos Loja:** – tarefa "filha"

Não é possível fazer o download das tarefas 'mães', servem apenas como agregador de tarefas. Desta forma, as tarefas 'filhas' são tarefas individuais e podem ser executadas por utilizadores diferentes. O estado da tarefa 'mãe' evolui para fechado quando todas as tarefas 'filhas' são terminadas.

#### Processamento da tarefa

As tarefas de Contagem de Stock podem ser Orientadas ou Adhoc. A tarefa adhoc não é orientada (não tem artigos previstos) e permite ao utilizador processar artigos livremente.

As tarefas orientadas (com artigos visíveis ao utilizador) podem ser criadas adhoc com base em [lista](#com-lista) ou estrutura [hierárquica](#por-estrutura-hierárquica). As tarefas orientadas também podem ser geradas de forma automática no fecho de outras tarefas. Para mais detalhes consultar [Workflow de tarefas](workflow.md).

Ao entrar no menu de Contagem de Stock, o utilizador terá acesso à lista de tarefas disponíveis, que pode descarregar para o dispositivo, permitindo assim a execução da tarefa em modo offline. 

Se a tarefa selecionada possuir afiliação, o utilizador é reencaminhado para um ecrã com as tarefas "filhas". Após o download da tarefa, o utilizador é reencaminhado para a listagem de artigos disponíveis (apenas se aplica a tarefas orientadas).

![image alt <>](assets/stockcount_zones.gif){:height="300px" width="300px"}

#### Modal de artigo

Ao picar um código de barras ou ao selecionar manualmente um artigo da lista, surge a modal de informação de artigo onde o utilizador pode colocar as unidades contadas do artigo ou inserir o número de caixas.

A modal de artigo contém a seguinte informação:

* **Informação do artigo:** Fotografia (se aplicável), status, descrição, EAN, SKU e SOH (se aplicável e configurável).
* **Total:** Informação da quantidade previamente processada.
* **Input de Quantidade:** Input para definir a quantidade de unidades ou peso.
* **Botão Não Encontrei/Continuar:** Permite definir o artigo como Não encontrado. Se a quantidade no input for superior a zero, o botão "Não Encontrei" é subtituido pelo botão "Continuar".

![image alt <>](assets/stockcount_modal.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Contagem de Stock contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos nas tarefas. (apenas se aplica a tarefas orientadas)
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existe a seguinte métrica:
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa (apenas se aplica a tarefas orientadas).

Em relação às unidades, o ecrã de resumo da tarefa de Contagem de Stock contém a seguinte informação:

* **PROCESSADOS:** Total de unidades processadas nas tarefas. Nos processados existe a seguinte métrica:
  * **NÃO ENCONTRADOS:** Total de unidades processadas como não encontradas.

![image alt <>](assets/stockcount_resume.png){:height="300px" width="300px"}

## TAREFA - RECEÇÃO

### RECEÇÃO

A funcionalidade de Receção permite rececionar mercadoria originária de entrepostos, fornecedores ou outras lojas. No fecho das tarefas, a informação de artigos ou caixas rececionadas é enviada para ERP e consequente ajuste de stock.

O TMR permite que as Receção sejam relaizadas de duas formas mediante configuração da tarefa:

* **Artigo a artigo:** O utilizador confere todos os artigos presentes em caixa ou suporte.
* **Caixa ou Suporte:** O utilizador confere apenas as caixas ou suportes integrados na tarefa e todos os artigos presentes nessas caixas ou suportes serão automáticamente conferidos.

#### Processamento da tarefa

Ao aceder a Receção presente no sumário de tarefas, o utilizador terá acesso à lista de tarefas disponíveis, que pode descarregar para o dispositivo. Caso o utilizador pretenda rececionar mercadoria que pertença a uma tarefa que não se encontra disponível, a app permite pesquisar tarefas futuras, mediante a informação de Ordem de  Compra, Guia de transporte ou simplesmente pela data da tarefa. 

Desta forma serão apresentadas apenas as tarefas que correspondam aos critérios de pesquisa utilizados.

##### Pesquisa de tarefas

Para aceder à pesquisa avançada, o utilizador deverá manter pressionado o botão de pesquisa. Desta forma a aplicação móvel disponibiliza um ecrã com os inputs para pesquisa. Mediante configuração ao cliente é possível definir quais os inputs que serão apresentados. 


![image alt <>](assets/receiving_task_search.gif){:height="300px" width="300px"}

##### Receção de caixas ou suportes

Ao descarregar a tarefa, o utilizador terá uma lista das caixas ou suportes previstos. O utilizador pode picar o código da caixa por inserção manual ou por bipagem. Deste modo, as caixas ou suportes serão rececionados de forma automática e todos os artigos neles contidos.  

Caso queira conferir artigo a artigo, poderá selecionar a caixa esperada e, consequentemente, processar os artigos presentes. Para adicionar o número de unidades correspondestes ao artigo, o utilizador abre a modal do mesmo, tendo aqui a possibilidade de incrementar a quantidade pelo input de unidades, caixas ou caixas livres. 

![image alt <>](assets/receiving_picking_item.gif){:height="300px" width="300px"}

##### Caixas de segurança

Mediante as necessidades específicas de cada negócio, é possível definir atributos específicos à caixa. O TMR permite distinguir caixas cujo conteúdo é de maior valor e manter regras específicas associadas a essa caixa. Se ao código daquela caixa estiver associada alguma pre-definição de segurança própria, então o utilizador irá rececionar a caixa de forma distinta, assegurando a segurança do seu conteúdo.

A picagem obedece a regras distintas - estas caixas são rececionadas com selos de segurança que têm de ser registados na aplicação. Ao picar a caixa, irá abrir a modal de segurança, onde o utilizador deverá inserir o código dos selos corretamente. Tem 3 oportunidades para registar com sucesso. No caso de falhar, a caixa será dada como danificada, obrigando o utilizador à picagem obrigatória dos artigos.

Durante o processo de Receção de uma caixa de segurança, se os selos não estiverem disponíveis, o utilizador poderá informar o TMR através da opção: "Sem informação de selos de segurança". 

![image alt <>](assets/receiving_container_safety.gif){:height="300px" width="300px"}

##### Caixas não previstas na tarefa

A parametrização no TMR permite também customizar a flexibilidade da tarefa na aceitação e processamento de artigos e caixas não previstas. Quando o utilizador pica ou introduz manualmente um EAN não previsto na tarefa, o TMR questiona se deseja rececionar como artigo ou como caixa. Ao selecionar caixa esta é inserida na tarefa como adhoc, permitindo inclusive rececionar artigos dentro desta caixa ou volume adhoc. 

![image alt <>](assets/receiving_container_adhoc.gif){:height="300px" width="300px"}

##### Caixas sem etiqueta

De forma semelhante à receção de caixas adhoc, também é possível adiconar caixas sem etiqueta à tarefa, mediante configuração ao cliente. Caixas sem etiqueta são caixas que não têm identificação ou que a mesma não está legível. Para que seja possível rececionar essa caixa e todos os artigos nela inseridos, a aplicação móvel dispobiliza ao utilizador um facilitador de inserção de caixa.

Esta opção é parametrizavel ao cliente e permite a criação de uma caixa sem identificação dentro de uma tarefa. Tanto a caixa como os artigos nela inseridos são naturalmente adhoc. Depois de inserida a caixa é necessário a picagem dos artigos e por isso a aplicação direciona para o ecrã de picagem de artigos de forma automática. 

![image alt <>](assets/receiving_container_without_label.gif){:height="300px" width="300px"}

##### Receção de artigo noutra caixa

Durante o tratamento da tarefa se o utilizador picar um artigo que não pertence a determinada caixa, a aplicação móvel pesquisa a que caixas o artigo pertence e pergunta ao utilizador se deseja adicinar na caixa atual como adock ou se deseja rececionar na caixa onde o artigo se encontra.

![image alt <>](assets/receiving_item_in_other_container.gif){:height="300px" width="300px"}

##### Criação de caixa virtual

O conceito de caixa virtual significa que durante o tratamento da tarefa de Receção é possível processar os artigos e associar os mesmo a caixas virtuais criadas durante a sua execução. A diferença para o processo de criação de caixa documentado anteriormente é que no processo de caixa virtual é possível alterar os artigos entre caixas. Neste sentido, o utilizador cria a caixa virtual e esta torna-se a caixa virtual ativa. Todos os artigos processados posteriormente ficarão associados a essa mesma caixa. 

Em qualquer momento é possível criar novas caixas virtuais e também alterar a caixa ativa à qual os artigos processados ficam associados.

No botão de caixa virtual, mediante configuração ao cliente,  estão disponíveis as seguintes opções:

* **Criação de caixa/volume:** Opção + permite criar uma nova caixa e nomear a mesma de imediato. Depois de criada, a caixa atual fica ativa e todos os artigos processados postariormente ficam associados à caixa ativa. 
* **Edição de caixa/volume:** A opção de edição permite renomear uma caixa já existente.
* **Alternar caixa/volume:** Durante o processamento de artigos é possível ao utilizador alternar entre caixas já existentes.
* **Impressão:** Opção que permite impressão móvel da etiqueta identificadora da caixa. 

![image alt <>](assets/receiving_manage_container.gif){:height="300px" width="300px"}


##### Receção ao artigo

As tarefas de Receção podem ser apenas orientadas ao artigo e não conter caixas como vimos nos pontos anteriores. Esta configuração depende unicamente da forma como o cliente entende a sua operação e o TMR comporta qualquer um deste comportamentos.

Se a tarefa for orientada apenas aos artigos, na listagem de pendentes não aparecem caixas nem volumes, apenas os artigos a rececionar. O utilizador deve picar o EAN ou selecionar o artigo da listagem para rececionar. 

![image alt <>](assets/receiving_items.png){:height="300px" width="300px"}

##### Alertas - Caixas e Artigos

Numa tarefa de Receção, as caixas e os artigos podem ser enviados com alertas. Os alertas são identificadores da tipologia de cada item que compõe as tarefas de receção. Por exemplo, um artigo que pertence ao catálogo atual vem com esse alerta associado. 

Por este motivo, o objetivo é que aplicação apresente o(s) alerta(s) associados a cada artigo. Além disso, é objetivo também que apresente o mesmo alerta ao nível da caixa, para que o utilizador tenha informação de que tipos de artigos estão em cada caixa.

Os artigos ou caixas com alertas associados comportam a indicação visual através do simbolo de notificações azul presente na linha de artigo ou caixa.

Para aceder à listagem de alertas, o utilizador deve manter pressionado a linha do produto ou caixa e nessa listagem são apresentados todos os alertas associados. 

![image alt <>](assets/receiving_alerts.gif){:height="300px" width="300px"}

##### Datas de Validade

Durante o tratamento de uma tarefas de Receção, o TMR permite rececionar a informação de data de validade e quantidade associada a determinado artigo. Esta funcionalidade é parametrizável mediante o comportamento definido na integração da tarefa.

![image alt <>](assets/receiving_expiration_audit.gif){:height="300px" width="300px"}

Além da recolha da data de validade é possível também, por configuração na integração de tarefa, o controlo da data de validade inserida. Assim, é possível definir os dias mínimos para receção de data de validade. 

Por exemplo: se estiver definido na tarefa que um artigo não pode ser rececionado com uma data mínima de 5 dias, o utilizador é alertado ou impedido de rececionar uma data com menos de 5 dias no futuro em relação à data atual de receção de tarefa.

Mediante configuração ao cliente é possível definir se é impeditiva a receção de uma data de validade menor que os dias mínimos definidos:

![image alt <>](assets/receiving_control_not_allow.png){:height="300px" width="300px"}

Ou se é permitida a receção apenas com um alerta ao utilizador.

![image alt <>](assets/receiving_control_allow.png){:height="300px" width="300px"}

#### Resumo - Caixas e Volumes

O ecrã de resumo das Caixas ou Volumes nas tarefas de Receção contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos nas tarefas.
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados em que a quantidade rececionada é igual à quantidade expectável.
    * **PARCIAL:** Total de artigos processados em que a quantidade rececionada é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade rececionada é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa.

Em relação às unidades, o ecrã de resumo das tarefas de Receção contém a seguinte informação:

* **PREVISTOS:** Total de unidades previstas nas tarefas.
* **PROCESSADOS:** Total de unidades processadas nas tarefas. Nos processados existem as seguintes métricas:
    * **EM EXCESSO:** Total de unidades processadas em que a quantidade rececionada é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de unidades processadas como não encontrados.

![image alt <>](assets/receiving_resume_container.png){:height="300px" width="300px"}

#### Resumo - Tarefa

O ecrã de resumo da tarefa de Receção contém a seguinte informação em relação às caixas ou volumes:

* **PREVISTOS:** Total de caixas ou volumes previstos nas tarefas.
* **PROCESSADOS:** Total de caixas ou volumes processados nas tarefas. 
* **SEGURANÇA:** Total de caixas ou volumes processados com o atributo caixas de seguraça. Nas caixas de segurança existe a seguinte métrica:
    * **DANIFICADOS:** Total de caixas e volumes processados como danificada.
* **PENDENTES:** Total de artigos não processados na tarefa.

O ecrã de resumo da tarefa de Receção contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos nas tarefas.
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de  processados em que a quantidade rececionada é igual à quantidade expectável.
    * **PARCIAL:** Total de artigos processados em que a quantidade rececionada é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade rececionada é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa.

Em relação às unidades, o ecrã de resumo da tarefa de Receção contém a seguinte informação:

* **PREVISTOS:** Total de unidades previstas nas tarefas.
* **PROCESSADOS:** Total de unidades processadas nas tarefas. Nos processados existem as seguintes métricas:
    * **EM EXCESSO:** Total de unidades processadas em que a quantidade rececionada é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de unidades processadas como não encontrados.

Também no ecrã de resumo da tarefa de Receção está disponível informação sobre detalhes da tarefa como Nota Fiscal e Guias de transporte. Esta informação deve ser integrada pela tarefa.   

![image alt <>](assets/receiving_resume.png){:height="300px" width="300px"}

#### Aprovação de tarefa

Mediante configuração ao cliente, a funcionalidade de Receções permite ao utilizador no fecho da tarefa ter visibilidade de informação anteriormente definida (alertas) ou inputs disponíveis para adicionar informação necessária à tarefa. 

##### Alertas

Mediante configuração ao cliente é possível definir que no fecho de uma tarefa de receção é apresentada uma mensagem informativa ao utilizador. Esta mensagem é definida anteriormente e também ela configurável ao cliente. 

Além da mensagem ser parametrizável por cliente é possível também definir uma mensagem para as tarefas convergentes (todos os artigos tratados) e uma mensagem para tarefas divergentes (com artigos pendentes).

![image alt <>](assets/receiving_approve_action.png){:height="300px" width="300px"}

##### Inputs

As tarefas de Receções, mediante configuração ao cliente, permitem ao utilizador no fecho da tarefa adicionar informação necessária à aprovação da tarefa. Os inputs configuráveis na aprovação de tarefa são os seguinte:

* **Identificação Transportador:** Permite identificar o transportador associado à tarefa.
* **Nome Transportador:** Permite identificar o nome do transportador

![image alt <>](assets/receiving_approve_action_input.png){:height="300px" width="300px"}

## TAREFAS - TRANSFERÊNCIA | DEVOLUÇÃO

### **TRANSFERÊNCIA**

A funcionalidade de Transferência permite ao utilizador processar determinados artigos para movimento de stock entre lojas. Desta forma é possível selecionar uma loja de destino na criação ou download da tarefa e processar artigos para serem transferidos. 

As tarefas de Transferência podem ser criadas adhoc na aplicação móvel sem lista de artigos previstos. Durante este processo de criação é dispobilizado ao utilizador um input de pesquisa que permite pesquisar e selecionar a loja de destino pretendida.

Da mesma forma, as tarefas de Transferência podem ser geradas via integração. Esta integração permite ao cliente realizar gestão de stocks na medida em que é gerada uma tarefa para a loja de origem transferir determinados artigos com uma loja de destino já definida.

![image alt <>](assets/transfer_create_task.gif){:height="300px" width="300px"}

#### Processamento de artigos

##### Criação de caixas/volumes

As tarefas de Transferência, mediante configuração ao cliente, permitem criar caixas volumes durante o tratamento da tarefa. Para isso a aplicação disponibiliza uma botão flutuante no canto inferior direito do ecrã que inclui as seguintes opções:

* **Criação de caixa/volume:** Opção + permite criar uma nova caixa e nomear a mesma de imediato. Depois de criada, a caixa atual fica ativa e todos os artigos processados postariormente ficam associados à caixa ativa. 
* **Edição de caixa/volume:** A opção de edição permite renomear uma caixa já existente.
* **Alternar caixa/volume:** Durante o processamento de artigos é possível ao utilizador alternar entre caixas já existentes.

![image alt <>](assets/transfer_manage_containers.gif){:height="300px" width="300px"}

##### Validação gama loja destino

A funcionalidade de Transferência permite mediante configuração ao cliente validar se o artigo existe na gama da loja de destino. Se o artigo não existir na gama da loja de destino a aplicação retorna um erro não permitindo adicionar o artigo à tarefa. 

![image alt <>](assets/trasnfer_adhoc_item.png){:height="300px" width="300px"}


#### Aprovação de tarefa

Mediante configuração ao cliente, a funcionalidade de Transferência permite ao utilizador no fecho da tarefa ter visibilidade de uma informação anteriormente definida ou inputs disponíveis para adicionar informação necessária à tarefa. 

##### Alertas

Mediante configuração ao cliente é possível definir que no fecho de uma tarefa de Transferência é apresentada uma mensagem informativa ao utilizador. Esta mensagem é definida anteriormente e também ela configurável ao cliente. 

Além da mensagem ser parametrizável por cliente é possível também definir uma mensagem para as tarefas convergentes (todos os artigos tratados) e uma diferente mensagem para tarefas divergentes (com artigos pendentes).

![image alt <>](assets/receiving_approve_action.png){:height="300px" width="300px"}

##### Inputs

As tarefas de Transferência, mediante configuração ao cliente, permitem ao utilizador no momento da aprovação adicionar informação complementar. Os inputs configuráveis na aprovação de tarefa são os seguinte:

* **Identificação Transportador:** Permite identificar o transportador associado à tarefa.
* **Nome Transportador:** Permite identificar o nome do transportador.
* **Prioridade:** Permite identificar a prioridade associada à transferência. Os valores possíveis são "Normal" e "Urgente".
* **Data de entrega:** Permite definir uma data expectável de entrega. 

![image alt <>](assets/receiving_approve_action_input.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Transferência contém a seguinte informação em relação às caixas/volumes:

* **PREVISTOS:** Total de caixas/volumes previstas na tarefa. (aplicavel apenas a tarefas orientadas)
* **PROCESSADOS:** Total de caixas/volumes processadas na tarefa.
* **PENDENTES:** Total de caixas/volumes não processados na tarefa. (aplicavel apenas a tarefas orientadas)

O ecrã de resumo da tarefa de Transferência contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigoss previstos na tarefa. (aplicavel apenas a tarefas orientadas)
* **PROCESSADOS:** Total de artigos processados na tarefa. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados em que a quantidade transferida é igual à quantidade expectável.
    * **PARCIAL:** Total de artigos processados em que a quantidade transferida é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade transferida é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa. (aplicavel apenas a tarefas orientadas)

Em relação às unidades, o ecrã de resumo da tarefa de Transferência contém a seguinte informação:

* **PREVISTOS:** Total de unidades previstas na tarefa.
* **PROCESSADOS:** Total de unidades processadas na tarefa. Nos processados existem as seguintes métricas:
  * **EM EXCESSO:** Total de unidades processadas em que a quantidade separada é superior à quantidade expectável.
  * **NÃO ENCONTRADOS:** Total de unidades processadas como não encontradas.

![image alt <>](assets/trasnfer_resume.png){:height="300px" width="300px"}


### DEVOLUÇÃO

A funcionalidade de Devolução permite ao utilizador processar determinados artigos para retorno de stock para entreposto ou fornecedor. Desta forma é possível selecionar um destino na criação ou download da tarefa e processar artigos para serem devolvidos. Os destinos possíveis são Entreposto e Fornecedor.

As tarefas de Devolução podem ser criadas adhoc na aplicação móvel sem lista de artigos previstos. Durante este processo de criação é disponibilizado ao utilizador a possibilidade de seleção do destino Entreposto ou Fornecedor e um input de pesquisa (aplicável apenas quando selecionado Fornecedor como destino).~

Em relação aos Entrepostos apenas são retornados para a aplicação os entrepostos que estão associados à loja logada. Esta associação é parametrizável ao cliente.

Da mesma forma, as tarefas de Devolução podem ser geradas via integração. Esta integração permite ao cliente realizar gestão de stocks na medida em que é gerada uma tarefa para a loja de origem transferir determinados artigos com destino Fornecedor ou Entreposto já definido.

![image alt <>](assets/itemreturn_create_task.gif){:height="300px" width="300px"}

#### Processamento de artigos

##### Criação de caixas/volumes

As tarefas de Devolução, mediante configuração ao cliente, permitem criar caixas volumes durante o tratamento da tarefa. Para isso a aplicação disponibiliza uma botão flutuante no canto inferior direito do ecrã que inclui as seguintes opções:

* **Criação de caixa/volume:** Opção + permite criar uma nova caixa e nomear a mesma de imediato. Depois de criada, a caixa atual fica ativa e todos os artigos processados postariormente ficam associados à caixa ativa. 
* **Edição de caixa/volume:** A opção de edição permite renomear uma caixa já existente.
* **Alternar caixa/volume:** Durante o processamento de artigos é possível ao utilizador alternar entre caixas já existentes.

![image alt <>](assets/transfer_manage_containers.gif){:height="300px" width="300px"}


#### Aprovação de tarefa

Mediante configuração ao cliente, a funcionalidade de Devolução permite ao utilizador no fecho da tarefa ter visibilidade de uma informação anteriormente definida ou inputs disponíveis para adicionar informação necessária à tarefa. 

##### Alertas

Mediante configuração ao cliente é possível definir que no fecho de uma tarefa de Devolução é apresentada uma mensagem informativa ao utilizador. Esta mensagem é definida anteriormente e também ela configurável ao cliente. 

Além da mensagem ser parametrizável por cliente é possível também definir uma mensagem para as tarefas convergentes (todos os artigos tratados) e uma diferente mensagem para tarefas divergentes (com artigos pendentes).

![image alt <>](assets/receiving_approve_action.png){:height="300px" width="300px"}

##### Inputs

As tarefas de Devolução, mediante configuração ao cliente, permitem ao utilizador no momento da aprovação adicionar informação complementar. Os inputs configuráveis na aprovação de tarefa são os seguinte:

* **Identificação Transportador:** Permite identificar o transportador associado à tarefa.
* **Nome Transportador:** Permite identificar o nome do transportador.
* **Prioridade:** Permite identificar a prioridade associada à transferência. Os valores possíveis são "Normal" e "Urgente".
* **Data de entrega:** Permite definir uma data expectável de entrega. 

![image alt <>](assets/receiving_approve_action_input.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Devolução contém a seguinte informação em relação às caixas/volumes:

* **PREVISTOS:** Total de caixas/volumes prevista na tarefa. (aplicavel apenas a tarefas orientadas)
* **PROCESSADOS:** Total de caixas/volumes processadas na tarefa.
* **PENDENTES:** Total de caixas/volumes não processados na tarefa. (aplicavel apenas a tarefas orientadas)

O ecrã de resumo da tarefa de Devolução contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigoss previstos na tarefa. (aplicavel apenas a tarefas orientadas)
* **PROCESSADOS:** Total de artigos processados na tarefa. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados em que a quantidade devolvida é igual à quantidade expectável.
    * **PARCIAL:** Total de artigos processados em que a quantidade devolvida é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade devolvida é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa. (aplicavel apenas a tarefas orientadas)

Em relação às unidades, o ecrã de resumo da tarefa de Devolução contém a seguinte informação:

* **PREVISTOS:** Total de unidades previstas na tarefa.
* **PROCESSADOS:** Total de unidades processadas na tarefa. Nos processados existem as seguintes métricas:
  * **EM EXCESSO:** Total de unidades processadas em que a quantidade devolvida é superior à quantidade expectável.
  * **NÃO ENCONTRADOS:** Total de unidades processadas como não encontradas.

![image alt <>](assets/trasnfer_resume.png){:height="300px" width="300px"}

## TAREFAS - AUDITORIA E CONTROLO DE VALIDADES

### **AUDITORIA DE VALIDADES**

A funcionalidade de Auditoria de Validades permite registar datas de validades dos artigos presentes em loja ou armazém. Este registo permite que o TMR armazene informação das datas de validades e respetivas quantidades. Esta informação servirá para o TMR gerar as respetivas tarefas de Controlo de Validades. 

#### Processamento da tarefa

As tarefas de Auditoria de Validade são maioritariamente adhoc sem lista de artigos prevista. O utilizador é livre de processar os artigos e respetivas datas autonomamente. No entanto, mediante configuração ao cliente, as tarefas podem ser também criadas adhoc na aplicação móvel com [base em lista](#com-lista) de artigos ou [estrutura hierárquica](#por-estrutura-hierárquica). 

Nas tarefas adhoc sem lista de artigos definida, o utilizador deve picar o artigo ou introduzir manualmente o EAN e a aplicação disponibiliza uma modal com a seguinte estrutura:

* **DIAS DEPREC.:** Informação da quantidade de dias parametrizada para geração das tarefas de  depreciação (Controlo de Validades).
* **DIAS RETIR.:** Informação da quantidade de dias parametrizada para geração das tarefas de retirada (Controlo de Validades).
* **TABELA DATAS:** Informação das datas já resgistadas na tarefa e as datas futuras para o artigo em tratamento.
* **INPUT DATAS:** Input para inserção da data de validade do artigo. Apenas é permitida a inserção de data igual ou superior ao dia atual.
* **INPUT QUANTIDADE:** Input para inserção de quantidade de artigos associados à respetiva data de validade.

![image alt <>](assets/expirationpicking_task_2.gif){:height="300px" width="300px"}

#### Adicionar ou Substituir

Ao introduzir uma nova data de validade, a aplicação verifica se essa data já foi anteriormente registada na tarefa. Se sim, questiona o utilizador se pretende "Adicionar" a quantidade registada à inserida anteriormente ou se pretende "Substituir" a informação de quantidade registada anteriormente.

Além disso, a aplicação validada de a data introduzida já se encontra em período de Depreciação ou Retirada. Se sim, o utilizador é alertado mediante a seguinte mensagem:

"O artigo está em período de depreciação/retirada. Deseja continuar com o registo?"

![image alt <>](assets/expirationpicking_add_quantity.png){:height="300px" width="300px"}

#### Listagem de datas

Depois de registadas as datas para determinado artigo, a aplicação móvel apresenta a listagem das mesmas e as respetivas quantidades. Nesta listagem o utilizador tem também a possibilidade de "Eliminar datas", "Registar Nova Validade" e "Continuar" para a modal de listagem de artigos.


![image alt <>](assets/expirationpicking_delete_date.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Auditoria de Validades contém a seguinte informação em relação aos artigos:

* **REGISTADOS:** Informação da quantidade de artigos processados na tarefa.

O ecrã de resumo da tarefa de Auditoria de Validades contém a seguinte informação em relação às unidades:

* **TOTAL UNIDADES:** Informação de unidades processadas na tarefa.

O ecrã de resumo da tarefa de Auditoria de Validades contém a seguinte informação em relação às datas de validade:

* **REGISTADOS:** Informação do total de datas de validade registadas na tarefa.

![image alt <>](assets/expirationpicking_resume.png){:height="300px" width="300px"}

### **CONTROLO DE VALIDADES**

A funcionalidade de Controlo de Validades permite retirar e depreciar artigos na mesma tarefa. Desta forma, o tratamento de datas de validade torna-se um processo mais rápido e eficiente uma vez que o utilizador processa os artigos e as suas datas de validades num único momento.

As tarefas de Controlo de Validades são exclusivamente orientadas e com listagem de artigos visível.

#### Depreciação

A Depreciação (Markdown) permite aplicar ações promocionais para escoamento de artigos que estejam com aproximação da data de validade.

Após selecionar o artigo da lista, a aplicação apresenta uma lista de datas a tratar. Para iniciar o tratamento de uma data em período de Depreciação, o utilizador deve selecionar a data da listagem com a referência "Depreciar".

![image alt <>](assets/expirationcontrol_markdown.gif){:height="300px" width="300px"}

#### Modal Depreciação

A modal de Depreciação apresenta as seguintes informações e inputs:

* **INFORMAÇÃO DO ARTIGO:** Fotografia (se aplicável), descrição, EAN, SKU e SOH (se aplicável e configurável). 
* **DATA DE VALIDADE:** Informação da data de validade em tratamento.
* **DEPRECIADOS:** Informação da quantidade de artigos depreciados associados à data de validade em tratamento.
* **PVP ATUAL:** Informação do preço atual do artigo (mediante consulta online de serviço de preço).
* **INPUT DESCONTO/VALOR FIXO:** Input que permite ao utilizador definir o preço de depreciação do artigo. Permite inserir o desconto por valor final ou por percentagem. O preço final é calculado pela aplicação e apresentado na parte direita do input. Neste momento é validado também se o preço final está dentro da percentagem máxima de desconto. Se isso não se verificar, a aplicação apresenta uma mensagem de erro e não permite avançar com o processo de depreciação.
* **ETIQUETA:** Input que permite definir o tipo de etiqueta de depreciação a ser impressa.
* **QUANTIDADE DE ETIQUETAS:** Input que permite definir a quantidade de etiquetas de depreciação a serem impressas. 
* **BOTÃO DATA NÃO ENCONTRADA/DEPRECIAR:** Se a quantidade de etiquetas for igual a zero, surge o botão de "Data Não Encontrada" para o utilizador poder definir que a data para tratamento não foi encontrada. Desta forma será uma data processada com quantidade zero. Se a quantidade de etiquetas for maior que zero, surge o botão "Depreciar". Ao clicar em "Depreciar" será gerada a ação de imprimir a quantidade de etiquetas de depreciação definidas pelo utilizador.

![image alt <>](assets/expirationcontrol_markdown_modal.png){:height="300px" width="300px"}

#### Retirada

A Retirada permite recolher da placa de vendas os artigos que estejam com aproximação da data de validade. Desta forma é possível ao utlizador perceber os artigos e respetivas datas que se encontram em fim de validade e retirar os mesmos da loja. 

Os dias a retirar, tal como os dias a depreciar, são parâmetros definidos por artigo ou estrutura hierárquica. No processo de retirada é possível ao utilizador definir um motivo e um destino que são também parâmetros configuráveis por cliente.

![image alt <>](assets/expirationcontrol_withdraw.gif){:height="300px" width="300px"}

#### Modal de Retirada

A modal de Retirada apresenta as seguintes informações e inputs:

* **INFORMAÇÃO DO ARTIGO:** Fotografia (se aplicável), descrição, EAN, SKU e SOH (se aplicável e configurável). 
* **DATA DE VALIDADE:** Informação da data de validade em tratamento.
* **PRECIADOS, VENDAS e A RETIRAR:** Informação de artigos previamente depreciados, da quantidade de vendas para a data em tratamento e a quantidade a retirar da respetiva data de validade. Estas informações são consultadas online e se não retornar informação a aplicação apresenta N/A.
* **QUANTIDADE RETIRAR:** Input que permite definir a quantidade de artigos que serão retirados da data em tratamento.
* **MOTIVO:** Input que permite definir o motivo para retirada. No caso de o artigo ter sido depreciado anteriormente, o motivo "Validades" aparece preenchido por default. Os Motivos que surgem na listagem para seleção são configuráveis ao cliente.
* **DESTINO:** Input que permite definir o destino do artigo após ser retirado de loja. Os Destinos que surgem na listagem para seleção são configuráveis ao cliente.
* **BOTÃO DATA NÃO ENCONTRADA/RETIRAR:** Se a quantidade retirada for igual a zero, surge o botão de "Data Não Encontrada" para o utilizador poder definir que a data para tratamento não foi encontrada. Desta forma será uma data processada com quantidade zero. Se a quantidade de etiquetas for maior que zero, surge o botão "Retirar".


#### Datas futuras

Na modal de artigo é possível consultar e gerir as datas futuras através da opção "Gestão de Datas Futuras". Nesta modal é possível consultar as datas futuras já registadas para determinado artigo e também eliminar as mesmas caso o utilizador perceba durante o tratamento da tarefa que as datas já não se encontram fisicamente em loja. 

Existem três formas de seleção de datas:

* A partir do input de data existente é possível ao utilizador selecionar de uma só vez todas as datas registadas até à data definida no input.
* Diretamente a partir da listagem de datas.
* Através do facilitador "Selecionar todas as datas". 

Depois de selecionadas as datas pretendidas, o utilizador deverá selecionar "Remover". De forma imediata as datas serão removidas do TMR.

![image alt <>](assets/expirationcontrol_show_future_dates.gif){:height="300px" width="300px"}


#### Adicionar data adhoc

Durante o tratamento de uma tarefa de Controlo de Validades é possível adicionar uma data adhoc caso o utilizador identifique uma data em loja que não está na listagem de datas futuras.

Na modal de datas para tratamento e na modal de listagem de datas futuras está presente o input de "Adicionar data adhoc". Ao selecionar esta opção, o utilizador é encaminhado para uma nova modal que permite definir uma nova data através do input disponível e a respetiva quantidade. 

![image alt <>](assets/expirationcontrol_adhoc_date.gif){:height="300px" width="300px"}

No momento em que o utilizador insere uma nova data a aplicação valida se a mesma já existe. Se sim retorna a seguinte mensagem ao utilizador: 

"Não é possível adicionar. Data de validade já existente".

![image alt <>](assets/expirationcontrol_date_already_existes.png){:height="300px" width="300px"}

Da mesma forma, a aplicação valida se a data inserida já se encontra em período de depreciação ou retira. Se sim abre de imediato a modal respetiva para tratamento do artigo. 

![image alt <>](assets/expirationcontrol_adhoc_date_withdral.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Controlo de Validades contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos na tarefa.
* **PROCESSADOS:** Total de artigos processados na tarefa.

Em relação às datas de validade, o ecrã de resumo da tarefa de Controlo de Validades contém a seguinte informação:

* **PREVISTOS:** Total de datas previstas na tarefa.
* **PROCESSADOS:** Total de datas processadas na tarefa. Nos processados existem as seguintes métricas:
  * **DEPRECIADOS:** Total de datas processadas como depreciação.
  * **NÃO ENCONTRADOS:** Total de datas processadas como não encontrados.
  * **RETIRADOS:** Total de datas processadas como retirada.

![image alt <>](assets/expirationcontrol_resume.png){:height="300px" width="300px"}

## TAREFAS - CHECKLIST | LISTA DE ARTIGOS | QUEBRAS

### **CHECKLIST**

A funcionalidade de Checklist permite realizar tarefas diárias em loja mediante informações criadas previamente. As checklist de cada loja são realizadas previamente no Backoffice TMR e posteriormente ficam disponíveis na aplicação móvel para serem utilizados na criação e realização da Checklist.

Além da criação adhoc na aplicação móvel é possível calendarizar via Scheduler as tarefas de Checklist.

As tarefas de Checklist têm por base perguntas e respostas. Neste sentido, as checklist criadas no Backoffice são perguntas que incluem dois tipos de resposta possível e que são definidas na altura da criação da checklist.

Quando a aplicação móvel realiza o download da tarefa apresenta ao utilizador a resposta escolhida na altura da sua criação.

![image alt <>](assets/checklist_create_task.gif){:height="300px" width="300px"}

#### Processamento da tarefa

Depois de realizado o download da tarefa, a aplicação móvel apresenta a checklist associada à tarefa e as respostas associadas às respetivas perguntas. 

As respostas possíveis são as seguintes:

* **Verificado/Não Verificado**
* **Sim/Não**

As respostas são associadas às perguntas na hora de criação da checklist.

![image alt <>](assets/checklist_processing.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Checklist contém a seguinte informação:

* **PREVISTO:** Total de items previstos na tarefa.
* **PROCESSADOS:** Total de item processados na tarefa. 

![image alt <>](assets/checklist_resume.png){:height="300px" width="300px"}

### **LISTA DE ARTIGOS**

A funcionalidade Lista de Artigos permite ao utilizador criar na aplicação móvel uma listagem mediante os artigos que vai processando na tarefa. Os artigos processados vão sendo adicionados à Lista de Artigos e no fecho da tarefa a listagem será criada e disponibilizada no Backoffice TMR para ser utilizada em outros tipos de tarefa. (tarefas adhoc ou calendarizadas com base em lista)

#### Processamento de tarefa

As tarefas são criadas adhoc sem lista definida. Durante o tratamento da tarefa os artigos depois de picados são associados de forma imediata à tarefa como processados. A modal de artigo apenas contém informação do artigo pois o processamento desta tarefa não implica informação de quantidade ou inputs adicionais. 

![image alt <>](assets/itemlist_processing.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Lista de Artigos contém a seguinte informação:


* **PROCESSADOS:** Total de item processados na tarefa. 

![image alt <>](assets/itemlist_resume.png){:height="300px" width="300px"}

### **REGISTO DE QUEBRAS**

A funcionalidade de Registo de Quebras permite ao utilizador processar determinados artigos como quebra. As tarefas de Registo de Quebras poderão ser adhoc sem lista orientada onde o utilizador é livre de processar os artigos que estiverem nestas condições. 

As tarefas poderão ser também orientadas com listagem de artigos visíveis onde o utilizador é orientado a identificar e processar como quebra determinados artigos previstos na tarefa. As tarefas orientadas podem ser criadas via TMR Scheduler ou integração.

#### Modal de artigo

Durante o tratamento da tarefa, o utilizador vai picando o EAN dos artigos ou inserindo manualmente o mesmo e a aplicação de forma imediata abre a modal do artigo com seguinte informação:  

* **INFORMAÇÃO DO ARTIGO:** Fotografia (se aplicável), status, descrição, EAN, SKU e SOH (se aplicável e configurável). 
* **QUANTIDADE RETIRADA:** Input que permite definir a quantidade de artigos que serão processados como quebra.
* **MOTIVO:** Input que permite definir o motivo para quebra.  Os Motivos que surgem na listagem para seleção são configuráveis ao cliente.
* **DESTINO:** Input que permite definir o destino do artigo após ser retirado de loja. Os Destinos que surgem na listagem para seleção são configuráveis ao cliente.
* **BOTÃO NÃO ENCONTREI/CONTINUAR:** Se a quantidade para quebra for igual a zero, surge o botão de "Não Encontrei" para o utilizador poder definir que a data para tratamento não foi encontrada. Desta forma será uma data processada com quantidade zero. Se a quantidade para quebra for maior que zero, surge o botão "Continuar".

![image alt <>](assets/damage_modal.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Registo de Quebra contém a seguinte informação em relação aos artigos:

* **PROCESSADOS:** Total de artigos processados na tarefa. 

Em relação às unidades, o ecrã de resumo contém a seguinte informação:

* **PROCESSADOS:** Total de unidades processadas na tarefa. 

### **APROVAÇÃO DE QUEBRAS**

A funcionalidade Aprovação de Quebras permite ao utilizador pesquisar quebras em aberto e que aguardam respetiva aprovação. Além da pesquisa é possível tratar na aplicação a aprovação, indicando quantidade em cada artigo e confirmando o motivo e destino. No final do tratamento dos artigos, a aplicação apresenta ao utilizador a informação do total de quebra em aprovação. 

#### Pesquisa de Quebras

Para pesquisar as quebras que se encontram a aguardar aprovação, o utilizador deve aceder ao formulário "Aprovação de Quebras" presente no menu da aplicação. 

No formulário de pesquisa deve selecionar o intervalo de datas para a pesquisa e a estrutura mercadológica que pretende pesquisar. No entanto, apenas um dos inputs é obrigatório e apenas é possível realizar a pesquisa se um ou mais inputs estiverem selecionados. 

![image alt <>](assets/damage_approval_search.gif){:height="300px" width="300px"}

#### Processamento de quebras

Após realizada a pesquisa, a aplicação apresenta ao utilizador os artigos que se enquadram nos filtros de pesquisa selecionados. Nesta lista de artigos são apresentados os seguintes campos:

* **Descrição do artigo:** Descrição completa do artigo.
* **SKU:** Identificador do artigo.
* **Custo:** Custo total da quebra do artigo. Este valor é calculado mediante a informação do preço de custo do artigo X quantidade de quebra introduzida pelo utilizador.
* **Motivo:** Motivo associado ao artigo no momento do registo de quebra.
* **Destino:** Destino associado ao artigo no momento do registo de quebra.
* **Quantidade:** Informação da quantidade inserida no momento do registo de quebra. Este campo é o único editável de forma a utilizador poder alterar a quantidade para quebra de um determinado artigo no processo de aprovação.
* **Total de SOH:** Informação da quantidade de stock no momento do registo de quebra.

Para realizar a aprovação de quebra, o utilizador deverá selecionar os artigos que pretende aprovar utilizando o input de seleção presente na parte direita do artigo. Para selecionar todos os artigos existe um facilitador de "selecionar todos" presente na parte superior do ecrã.

Em cada artigo existe o input "Quantidade" que está preenchido por default com a informação de quantidade introduzida no momento de registo de quebra. No entanto, este valor pode ser alterado durante o processamento da aprovação, sendo enviada a informação colocada pelo utilizador no momento da aprovação. 

![image alt <>](assets/damage_approval_processing.gif){:height="300px" width="300px"}

#### Alerta resumo

Depois de processados todos os artigos e selecionadas as quebras para serem aprovadas, o utlizador deve selecionar "Aprovar" presente no parte inferior da listagem. Ao selecionar "Aprovar", a aplicação apresenta um alerta de resumo que informa o utilizador do Custo total da quebra. Este custo total é calculado mediante a soma do custo de todos os artigos selecionados para aprovação.

No alerta resumo, se o utilizador selecionar "Cancelar" é redirecionado para o ecrã anterior de processamento de artigos. Se selecionar "Continuar", as quebras selecionadas serão enviadas para Aprovação. 

![image alt <>](assets/damage_approval_resume.png){:height="300px" width="300px"}

## FLUXO LOCALIZAÇÕES - RECEÇÕES | GESTÃO DE LOCALIZAÇÕES | SEPARAÇÃO | PESQUISA E EDIÇÃO

O TMR como produto suporta o processo de criação, gestão e edição de localizações em loja e armazém. Por este motivo foi criado o fluxo de localizações que permite registar localização física para todos os artigos que entram para stock de loja. 

O objetivo é que seja possível identificar as várias localizações de um determinado artigo para suportar e auxiliar o processo de reposição em loja e os diversos processos de auditoria de stock, tal como Inventários e Contagens de stock.

Este fluxo é composto pelas seguintes etapas:

* **Receções**: Possibilidade de criação de containers durante o processo de receção que irá suportar a agregação e atribuição de artigos a dterminada localização.

* **Gestão de localizações**: Este tipo de tarefa gerada no fecho da receção permite atribuir localizações físicas aos artigos através da assignação de localização aos containers como entendidade agregadora.

* **Separação**: A introdução das localizações nas tarefas de Separação permite ao utilizador consultar as localizações para determinado artigo em processo de reposição. Além da consulta permite também ao utilizador editar a localização de determinado artigo durante o processo de reposição. 

* **Pesquisa/Edição localizações**: Esta funcionalidade permite pesquisa containers criados anteriormente e visualizar a sua localizações e a identificação de todos os artigos nele contidos. Além disso, permite também pesquisa de localizações e visualização dos artigos que nela pertencem.

Os pontos seguintes irão abordar com detalhe cada um dos passos deste fluxo de localizações:

### **Receções - Criação de container**

#### Criação de caixa virtual

O conceito de caixa virtual significa que durante o tratamento da tarefa de Receção é possível processar os artigos e associar os mesmo a caixas virtuais criadas durante a sua execução. A partilcularidade deste processo é o de ser possível criar e alterar os artigos entre caixas. Neste sentido, o utilizador cria a caixa virtual e esta torna-se a caixa virtual ativa. Todos os artigos processados posteriormente ficarão associados a essa mesma caixa. 

Em qualquer momento é possível criar novas caixas virtuais e também alterar a caixa ativa à qual os artigos processados ficam associados.

Esta caixa virtual representa uma entidade agregadora que irá migrar para a tarefa de Gestão de Localização gerada automáticamente no fecho da tarefa de receção. Esta etapa representa o primeiro passo para o fluxo de localizações.

No botão de caixa virtual, mediante configuração ao cliente,  estão disponíveis as seguintes opções:

* **Criação de caixa/volume:** Opção + permite criar uma nova caixa e nomear a mesma de imediato. Depois de criada, a caixa atual fica ativa e todos os artigos processados postariormente ficam associados à caixa ativa. 
* **Edição de caixa/volume:** A opção de edição permite renomear uma caixa já existente.
* **Alternar caixa/volume:** Durante o processamento de artigos é possível ao utilizador alternar entre caixas já existentes.
* **Impressão:** Opção que permite impressão móvel da etiqueta identificadora da caixa. 

![image alt <>](assets/receiving_manage_container.gif){:height="300px" width="300px"}

### **Gestão de localizações**

#### Atribuição de localização a containers

As tarefas de gestão de localizações (separação de paletes) são tarefas orientadas aos containers criados anteriormente nas tarefas de receção. O objetivo desta tarefa é o de atribuir uma localização aos containers criados durante a tarefa de receção. Desta forma, todos os artigos rececionados em determinado container ficarão assignados à localização atribuida a esse mesmo container.

Para atribuir uma localização a determinado container, o utilizador deverá realizar o download da tarefa e na listagem de containers pendentes deverá selecionar o container pretendido. Existe também a possibilidade de realizar a leitura do código de barras identificador do container como forma de processamento do mesmo. 

Após a picagem do código de barras é aberta uma modal de inserção e pesquisa de localizações. 
Nesta modal é possível realizar o scan de um código de barras identificador da localização ou no caso de isso não existir é possível inserir manualmente o código identificador da mesma.

Despois de introduzida a informação de localização, o container é processado imediatamente e a informação da localização assignada fica visível ao nível do container.

![image alt <>](assets/despicking_add_location.gif){:height="300px" width="300px"}

#### Limpar localização ou adicionar à tarefa

No fluxo de atribuição documentado no ponto anterior pode existir o cenário em que o utilizador tenta atribuir um container a uma localização que já contém um outro container associado. Dado que não é possível atribuir mais do que um container a cada localização, a aplicação retorna um ecrã ao utilizador com a seguinte mensagem:

**"A localização já contém o container XXXXXXX. Pretende eliminar a palete XXXXXX ou atribuir-lhe uma nova localização adicionando-a à tarefa?"**

Neste ecrã o utilizador tem duas opções disponíveis:

* **Eliminar container:** Se o utilizador selecionar esta opção, o TMR elimina todos os artigos e o container presentes atualmente na localização e coloca de imediato o container em tratamento. 
* **Atribuir nova localização:** Se o utilizador selecionar esta opção, o TMR não elimina os artigos e containers presentes na localização mas coloca os mesmos como pendentes na tarefa em tratamento. Isto permite que o utilizador possa posteriormente assignar uma nova localização para este container.

![image alt <>](assets/despicking_conflict.png){:height="300px" width="300px"}

#### Pesquisa manual de localizações

Para o caso de o utilizador não ter presente a identificação da localização, o TMR permite pesquisar uma determinada localização através da nevageção entre os diversos níveis.

Na modal de inserção de de localização está disponível a opção pesquisar. Ao clicar neste facilitador a aplicação direciona para um novo ecrã onde surge o primeiro nível de localizações para a loja em tratamento.

Ao selecionar uma opção neste nível, a aplicação disponíbiliza um segundo nível onde estão visíveis as respetivas localizações e assim sucessivamente. Este facilitador permite ir navegando em todos os níveis de localização até o utilizador encontrar a localização pretendida.

Em qualquer altura, quando o utilizador seleciona continuar, a aplicação apresenta as localizações finais e que estão parametrizadas para associação de containers.

A localização selecionada é apresentada na modal de inserção de localização ficando disponível o botão "OK" para avançar com o processo e o botão "Pesquisar" para voltar a pesquisar uma nova localização.

![image alt <>](assets/despicking_mapping.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Gestão de Localizações contém a seguinte informação em relação aos containers:

* **PREVISTOS:** Total de containers previstos na tarefa.
* **PENDENTES:** Total de containers pendentes na tarefa.
* **PROCESSADOS:** Total de containers processados na tarefa. 

![image alt <>](assets/despicking_resume.png){:height="300px" width="300px"}

### **Separação**

Para comportar o fluxo de tratamento de localizações, foram realizadas algumas alterações na modal de artigo das tarefas de Separação. Neste sentido, a modal de artigo das tarefas de Separação passa a incluir a informação da localização e container e inclui também um novo conceito de tips que permite ajudar o utilizador na ação que deve realizar. 

#### Facilitador "tips"

O facilitador "tips" introduz o conceito de tutorial no TMR. Através deste facilitador é possível ao utilizador consultar informação adicional para perceber determinado comportamento.

Na modal de artigos nas tarefas de Separação está presente o facilitador "tips". Ao clicar no "?" presente no canto superior da tabela de localizações, é aberta de imediato um alerta com informação detalhada de cada colona presente na tabela. Isto permite ao utilizador obter informação adicional das ações que deve tomar para processar determinado artigo no âmbito das localizações.

![image alt <>](assets/replenishprep_tips.gif){:height="300px" width="300px"}

#### Edição de Localização

No fluxo do tratamento de localizações nas tarefas de Separação é possível ao utilizador, durante o tratemento da tarefa, consultar a informação da localização de um determinado artigo e editar a informação da quantidade desse mesmo artigo na localização selecionada. 

Para este efeito foram criados novos inputs na modal de artigo das tarefas de localização que contém as seguintes informações:

* **Informação da localização**: Identificação do nome da localização onde o artigo se encontra.
* **Informação do container**: Identificação do EAN do container onde o artigo se encontra.
* **Coluna QT**: Informação da quantidade total presente na localização.
* **Coluna QF**: Informação da quantidade final presente na localização após separação do artigo. O cálculo para esta informação é feito da seguinte forma: Quantidade Total (QT) - Quantidade a separar (SEP) = Quantidade Final (QF).
* **Coluna SEP**: Input de quantidade a separar.

Quando o utilizador introduz no input SEP a quantidade a separar para determinada localização, a aplicação faz o cálculo e atualiza a informação da quantidade final para o artigo. Esta quantidade final é a quantidade que ficará associada ao artigo em determinada localização.

O input de Quantidade Final também é editável para permitir ao utilizador definir manualmente o valor da quantidade final nos casos em que a quantidade real seja diferente da quantidade apresentada pelo TMR.

Se a quantidade final do artigo for zero significa que o artigo será eliminado do container. 

Ao clicar em "Continuar" a informação da localização é atualizada.

![image alt <>](assets/replenishprep_locations_table.gif){:height="300px" width="300px"}

### Pesquisa e Edição

O TMR permite ao utilizador consultar uma determinda localização ou um determinado container e vizualizar os artigos e respetivas quantidades. Ambas as pesquisas podem ser feitas de forma manual ou via scanner através da picagem do identificador de container ou localização. 

#### Pesquisa de containers

Tal como na pesquisa de artigos, o TMR permite a picagem do código de barras de container e de imediato o utilizador é encaminhado para a modal de informação de container. Se não for possível a picagem do código de barras, o utilizador poderá pesquisar essa informação de forma manual.

Para pesquisar um container o utilizador deve aceder a "Pesquisa" e no botão flutuante selecionar "container". De seguida deve introduzir manualmente o EAN do container.

De imediato será encaminhado para a modal de container que contém a seguinte informação:

* **ID**: Identificador do container.
* **Id da Localização**: Identificador da localização.
* **Nome da localização**: Informação do nome atribuído à localização.
* **Listagem de artigos**: Informação de SKU e nome dos artigos presentes no container e a respetiva quantidade. 

Além desta informação existe ainda a possibilidade de impressão da etiqueta identificadora do container. Para isso o utilizador deve selecionar o ícone da impressora presente no canto superior direito do ecrã.


![image alt <>](assets/container_search.gif){:height="300px" width="300px"}

#### Pesquisa de localizações

O TMR permite a picagem do código de barras de localização e de imediato o utilizador é encaminhado para a modal de informação de localização. Se não for possível a picagem do código de barras, o utilizador poderá pesquisar essa informação de forma manual.

Para pesquisar uma localização o utilizador deve aceder a "Pesquisa" e no botão flutuante selecionar "localização". De seguida deve introduzir manualmente o id da localização.

De imediato será encaminhado para a modal de localização que contém a seguinte informação:

* **ID**: Identificador da localização.
* **Nome**: Informação do nome da localização pesquisada.
* **Limpar localização**: Botão que permite apagar todos os artigos e containers presentes na localização.
* **Listagem de artigos**: Informação de SKU e nome dos artigos presentes na localização e a respetiva quantidade. Em cada artigo está também presenta a informação do container a que pertence.

![image alt <>](assets/location_search.gif){:height="300px" width="300px"}


