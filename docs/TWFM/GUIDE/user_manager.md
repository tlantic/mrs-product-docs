# Guia de Gerentes

 ![image alt <>](assets/capa.png) 

## Histórico do documento


| Data      | Descrição    | Autor | Versão | Revisão
| ------------- | ---------------------------------------------------------------------------------------- | ---------- | ---------- | -------- | 
| 01/12/2020         | Criação do documento.    |  Filipe Silva  |   1.0   |     |
| 12/12/2020     | Criação de Introdução de formatação de documento.       | Filipe Silva  |  1.1     |      |
| 22/12/2020     | Versão final do documento.       | Filipe Silva  |  2.0    |      |
| 22/01/2021     | Revisão final do documento.       | Filipe Silva  |  2.1    |  Helena Gonçalves|

## **Introdução**

### TWFM - Tlantic Workforce Management

Este documento descreve as funcionalidades do sistema Tlantic Workforce Management (TWFM). O principal objetivo do documento é fornecer ao utilizador toda a informação necessária para que este possa utilizar o sistema de forma correta e eficaz.
O TWFM padroniza a forma como os horários são planejados em toda a empresa.
Transferindo a responsabilidade da geração de horários para a responsabilidade de gestão e controle de horários.
Permite visualizar por departamento os seguintes temas:
    - Estimativas dos departamentos
    - Quadro ideal de colaboradores
    - Avaliar melhor carga horário para o quadro
    - Analisar a produtividade dos colaboradores
    - Auxilia o gestor nas tomadas de decisão.

???+ example "Principais funcionalidades:"
    * Inventários.
    * Lista de Artigos, Ficha de Artigos, Auditoria de Preço, 
    * Registo e Aprovação de Quebras.
    * Impressão de etiquetas.
    * Auditoria  e Controlo de Validades (Depreciação e Retirada).
    * Auditoria de Varrimento e Verificação de Presença.
    * Auditoria de Rutura, Separação, Reposição. 
    * Receções, Transferências e Devoluções.

O módulo Instore procura simplificar e solucionar problemas do retalho com a execução de tarefas orientadas a um perfil de utilizador.
Cada tarefa está associada a uma funcionalidade com o objetivo de resolver uma determinada necessidade do retalho.
O TMR é composto pelos módulos: **BackOffice**, **Cockpit**, **Aplicações móveis (WinCE e Multiplataforma)**, **Scheduler** e **TMR Server**.

### Arquitetura

O TMR é um sistema constituído por:

* Aplicações Cliente (App WinCE, App Android e App iOS) - Aplicação distribuida pelos dispositivos móveis usados na operação de loja.
* TMR Server - Componente central que contém a lógica e dados e que interage com outros sistemas.
* Backoffice - Componente web que permite definição de dados de referência e configuração de parâmetros.
* Cockpit - Componente web para análises operacionais das tarefas executadas no TMR Instore.