
!!! info  "Áreas"

     [Manual de Integração ](../GUIDE/integration_Guide)<br>
     [Manual de Requisitos TI ](../GUIDE/ti_Guide)<br>

  
## Módulos que compõem o TWFM
!!! abstract "Planejamento"
    - Regras Trabalhistas
    - Lojas, Colaboradores e Polivalências
    - Geração automática de Escalas

!!! abstract "Execução"
    - Espelho de ponto dos colaboradores
    - Ajustes de Horários do ponto
    - Geração de calculo dos horarios para folha de pagamento

!!! abstract "Dashboards"
    - Análise de Ajustes e Trocas
    - Análise de Planejamento de Horários
    - Análise de Execução de Escalas

!!! abstract "App"
    - Visualiação da escalas no celular
    - Solicitação de Ajuste e Troca de Horários
    - Alertas push no celular com (Horários Batidos, Não batidos, Colaborador atrasado e etc...)




