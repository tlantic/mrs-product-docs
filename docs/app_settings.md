# Application Settings
Esta secção é dedicada à listagem de settings que inflenciam o comportamento das aplicações WinCE e Multiplataforma. 



## LOGIN

### **displayStores** ###

Type: array (default: []) | **1.27.1-rc.0** (``task-type-definitions``)

Caso esteja parametrizado, o tipo de tarefa fica disponível apenas para as lojas seleccionadas.

!!! note "Exemplo"
    ```json
    {
      "displayStores": [
        "1",
        "2"
      ]
    }
    ```


### **allow-change-password**

Type: boolean (default: false) | **1.13.0-rc.1** (``application-setting``)

Define se é possivel alterar a password na aplicação.

!!! note "Exemplo"
    ```json
    {
        "allow-change-password": true
    }
    ```

![image alt <>](assets/change_pass.png){:height="300px" width="300px"}

### **allow-download-working-task**

Type: ``boolean`` (default: ``{"active": false}) | **1.37.0-rc.0**

Setting ao nível da aplicação Instore. 

Define se é permitido realizar download de tarefas em estado W. 

!!! note "Exemplo"
    ```json
    {
        "allow-download-working-task": {"active": false}
    }
    ```

### **currency**

Type: ``string`` | (``application-setting``)

Define a moeda por cliente.

!!! note "Exemplo"
    ```json
    {
        "currency": "€"
    }
    ```

### **default-language**

Type: ``string`` | (``application-setting``)

Define a linguagem default por cliente

!!! note "Exemplo"
    ```json
    {
        "default-language": "pt-PT"
    }
    ```

### **default-role**

Type: ``string`` | (``application-setting``)

Define o nível de acesso por defeito ao criar um utilizador

!!! note "Exemplo"
    ```json
    {
        "default-role": "DEFAULT"
    }
    ```

### **get-tasks-options**

Type: ``object`` | **1.4.0-rc.0** (``application-setting``)

Define possíveis opções ao obter tarefas (todos os campos são opcionais)

???+ example "Valores possíveis:"
    * ``status``: ``string []`` - Define todos os estados de tarefas a obter. Valores possíveis:
        * A
        * AW
        * W
        * P 
    * ``fromScheduledStart``: ``string`` - Apenas retorna tarefas com o scheduledStart igual ou superior à data definida - formato ISO standard.
    * ``toScheduledStart``: ``string`` - Apenas retorna tarefas com o scheduledStart igual ou inferior à data definida - formato ISO standard.
    * ``offset``: ``number`` - Define o número de tarefas a ignorar (exemplo: de um conjunto de 20 tarefas, se o offset for 5, só vai retornar as tarefas 6 a 20).
    * ``limit``: ``number`` - Número total de tarefas a retornar.

    **Exemplo**
    ```json
    {
        "get-tasks-options": {
          "status": ["A", "AW", "W"],
          "limit": 20,
          "offset": 5,
          "toScheduledStart": "5",
          "fromScheduledStart": "0"
        }
    }
    ```


### **show-damage-approval**

Type: ``object`` (default: "active": ``false``) | **1.37.0-rc.0**

Setting ao nível da aplicação Instore.

Define se a app apresenta no menú o formulário de Damage Approval.

???+ example "Valores possíveis:"
    * ``active``: ``string`` - Define se a app paresenta no menú o formulário de Damage Approval. (default: ``false``)
    * ``limit``: ``number`` - Define o valor de paginação que a app coloca na chamada do serviço GET. (default: ``null``)

    **Exemplo**
    ```json
    {
        "showDamageApproval": {
          "active": true,
          "limit": 20
        }
    }
    ```


## FLUXO DE CRIAÇÃO DE TAREFA

### **adhocTasks**  

!!! warning "Deprecated"
    Use [adhocTasksV2](#adhoctasksv2) instead
    

Type: string (default: `never`) | **1.1.0-rc.9** (``task-type-definitions``)

Define se é permitido criar tarefas ad-hoc do respectivo tipo de tarefa.

???+ example "Valores possíveis:"

    * `always` - permite criar tarefas ad-hoc para o tipo de tarefa sem restrições.

    * `parent` - permite criar tarefas ad-hoc para o tipo de tarefa se a tarefa não tiver ascendência.

    * `never` - não permite criar tarefas ad-hoc para o tipo de tarefa.

### **adhocTasksV2**

Type: `object` | **1.15.0-rc.0** (``task-type-definitions``)

Define se é permitido criar tarefas adHoc, em que nível e de que forma. Esta definição é um objecto constituído por um campo level (define em que nível se pode criar a tarefa) e um array types (define de que forma pode ser criada a tarefa).

???+ example "Configuração"
    **Campos disponíveis:**

    * ``level`` - "never"

    * ``types`` - ["WITHOUTLIST"]

    **Exemplo**
    
    ```json

    {
      "level": "parent",
      "types": [
        "HS",
        "WITHOUTLIST",
        "WITHLIST"
      ]
    }
    ```
    
    **Valores disponíveis para o campo level:** 

    * `always` - permite criar tarefas adHoc para o tipo de tarefa sem restrições

    * `parent` - permite criar tarefas adHoc para o tipo de tarefa se a tarefa não tiver ascendência

    * `never` - não permite criar tarefas adHoc para o tipo de tarefa


    **"Valores possíveis para o array types:**

    * `HS` - criação de tarefas adHoc com base numa estrutura hierárquica

    * `WITHLIST` - criação de tarefas adHoc com base numa lista

    * `WITHOUTLIST` - criação de tarefas adHoc sem lista

    * `CHECKLIST` - criação de tarefas adHoc com base numa checklist
    
    

![image alt <>](assets/adhocTasksV2.png){:height="300px" width="300px"}

### **checkConnectionOnStart**

Type: `boolean` (default: `false`) | **1.0.0-rc.2** (``task-type-definitions``)

Verifica conexão ao entrar numa tarefa. Se estiver activa para um determinado tipo de tarefa, ao entrar numa tarefa sem rede é mostrado um alerta a indicar que não há rede.

!!! note "Exemplo"
    ```json
    {
        "checkConnectionOnStart": true
    }
    ```


### **destinationSearchInput**

Type: `array` (default: []) | **1.27.0-rc.0** (``task-type-definitions``)

Define quais as opções disponíveis de destino na criação da tarefa  e quais os inputs possíveis para cada destino.

???+ example "Valores possíveis:"
    * ``store``: Opção que define o destino "Loja"
    * ``warehouse``: Opção que define o destino "Entreposto"
    * ``supplier``: Opção que define o destino "Fornecedor"
    
    **Exemplo:**
    ```json
    "destinationSearchInput": [
			{
				"code": "store",
				"input": "text"
			}
		],
    ```

![image alt <>](assets/destinationSearchInput.png){:height="300px" width="300px"}

### **inventory-barcode-rules**

Type: ``object`` | **1.32.0-rc.0** (``application-setting``)

Permite gerir o formato dos parâmetros enviados na picagem de inventários.

???+ example "Campos possíveis:"
    * `zoneIdRightDigits` - define a quantidade de dígitos que a app deve retirar do código de barras e colocar na chamada do serviço tasks. Os dígitos devem ser contados da direita para a esquerda e não fazer trim dos zeros à esquerda.

!!! note "Exemplo"
    ```json
        {
          "inventory-barcode-rules": 
          {
            "zoneIdRightDigits": "3"
          }
        }
    ```

### **preventReplenishingWhen**

Type: `object` | **1.3.0-rc.3** (``task-type-definitions``)

Prevenir a criação de tarefas (separação) mediante condições.

"mandatory": `string` com a condição obrigatória para permitir a criação da tarefa

???+ example "Valores possíveis:"
    * `HIERARCHY_CATEGOR` - obriga à existência de estrutura hierárquica

!!! note "Exemplo"
    ```json
    {
      "preventReplenishingWhen": 
      {
        "mandatory": "hierarchy_category"
      }
    }
    ```



### **adHocTasksDefaultName**

!!! warning "Deprecated" 
    Use [adHocTasksDefaultNameV2](#adhoctasksdefaultnamev2) instead

Define se na criação de tarefas adHoc o nome da tarefa será ou não por defeito. Se o valor desta definição for false, o utilizador tem a possibilidade de definir o nome da tarefa.


### **adHocTasksDefaultNameV2**


Type: `boolean` (default: ``true``) | **1.27.0-rc.0** (``task-type-definitions``)


Define se na criação de tarefas adHoc o nome da tarefa será ou não por defeito. Se o valor do campo active for false, o utilizador tem a possibilidade de definir o nome da tarefa por tipo de criação.

!!! note "Exemplo"
    ```json
    {
      "adHocTasksDefaultNameV2": {
        "active": false,
        "hsExpression": "%hsName% (%hsId%) | %user%",
        "withListExpression": "%withListName% | %user%",
        "withoutListExpression": "%datetime% | %date% | %user%",
        "checklistExpression": "%checklistName% | %user%"
      }
    }
    ```
![image alt <>](assets/default_name.png){:height="300px" width="300px"}

### **handleMode**

Type: ``string`` | **1.29.0-rc.0** (``task-type-definitions``)


Verifica o modo de tratamento da tarefa. 

???+ example "Valores possíveis:"
    * ``normal``:  Define o tratamento de produto do tipo de tarefa REPLENISHPREP (default)
    * ``styled``: Define o comportamento de tratamento de tarefa fashion (comportament já existente para o tipo de tarefa REPLENISHPREP)
    * ``simpler``: (novo campo) Define a versão simplificada no tratamento do tipo de tarefa Receiving para o cliente Marisa.

!!! note "Exemplo"
    ```json
    {
      "handleMode": "styled"
    }
    ```
![image alt <>](assets/handleMode.png){:height="300px" width="300px"}

### **show-labelprint-on-summary**

Type: ``object`` | **1.14.0-rc.0** (``application-setting``)

Define se o task-type LABELPREINT está visível no menu da app ou no sumário de tarefas. Se true e caso existam tarefas disponíveis, o tipo de tarefa LABELPRINT deve estar disponível no sumário de tarefas. Se false, deve estar disponível no menu "Mais".

!!! note "Exemplo"
    ```json
    {
      "active": false
    }
    ```
![image alt <>](assets/show-labelprint-on-summury.png){:height="300px" width="300px"}

### **taskDestination**

Type: ``object`` (default: { ``"active": false`` } ) | **1.29.0-rc.0** (``task-type-definitions``)

Permite ao utilizador definir o destino da tarefa.

!!! note "Exemplo"
    ```json
    {
      "active": true,
      "code": "standard"
    }
    ```

![image alt <>](assets/taskLocationPrint.png){:height="300px" width="300px"}


### **taskLocation**

!!! warning "Deprecated"
    Use [taskLocationV2](#tasklocationv2) instead

Type: ``object`` (default: {} ) | **1.20.0-rc.0** (``task-type-definitions``)

Permite definir e mostrar a localização da tarefa.

???+ example "Valores possíveis:"
    * ``active`` (boolean) - activa a funcionalidade para definir a localização
    * ``display`` (boolean) - caso a localização esteja definida para a tarefa, possibilita a sua consulta
    * ``options`` (array) - as localizações possíveis de escolher para a tarefa

    **Exemplo**

    ```json
    {
      "taskLocation": {
        "active": true,
        "display": true,
        "options": [
          "store",
          "warehouse"
        ]
      }
    }
    ```


### **taskLocationV2**

Type: ``object`` (default: {} ) | **1.33.0-rc.0** (``task-type-definitions``)

Permite definir e mostrar a localização da tarefa.

???+ example "Valores possíveis:"
    * ``active`` (boolean) - activa a funcionalidade para definir a localização
    * ``display`` (boolean) - caso a localização esteja definida para a tarefa, possibilita a sua consulta
    * ``options`` (array) - as localizações possíveis de escolher para a tarefa. Possível definir o Id e o name para cada localização. 

    **Exemplo**
    ```json
    {
      "taskLocationV2": {
	      "active": true,
	      "display": true,
	      "options": [
		        {
		            "id": "store",
			          "description": "Loja"
		        },
		        {
			          "id": "warehouse",
			          "description": "Armazem"
		        }
		      ]
	    }
    }
    ```

![image alt <>](assets/taskLocationV2.png){:height="300px" width="300px"}


### **tasks-query-status**

Type: ``string[]`` | (``application-setting``)

Define o estado das tarefas disponíveis para a visualização na app.

!!! note "Exemplo"
    ```json
    [
        "A",
        "AW",
        "W",
        "P",
        "F
    ]
    ```


### **taskSearchInput**

Type: ``array`` | **1.25.0-rc.1** (``task-type-definitions``)

Permite definir quais os inputs possíveis na pesquisa avançada de tarefas.

???+ example "Valores possíveis:"
    * "document_id": ``text`` - Habilita a pesquisa por id do documento.
    * "document_type": ``text`` - Habilita a pesquisa por tipo de documento.
    * "document_barcode": ``text`` - Habilita a pesquisa por código de barras do documento.
    * "from_scheduled_start": ``date`` - Input de data a partir de.
    * "to_scheduled_start": ``date`` - Input de data até.
    * "vehicle_id": ``text`` - Habilita a pesquisa por id do veículo de transporte.
    * "driver_id": ``text`` - Habilita a pesquisa por id do transportador.
    * "status": ``array`` - Parameteriza os estados possíveis para pesquisa.

    **Exemplo**
    ```json
    "taskSearchInput": [
          {
            "code": "document_id",
            "type": "text"
          },
          {
            "code": "document_type",
            "type": "text"
          },
          {
            "code": "document_barcode",
            "type": "text"
          },
          {
            "code": "from_scheduled_start",
            "type": "date"
          },
          {
            "code": "to_scheduled_start",
            "type": "date"
          },
          {
            "code": "vehicle_id",
            "type": "text"
          },
          {
            "code": "driver_id",
            "type": "text"
          }
        ],
    ```

![image alt <>](assets/taskSearchInput.png){:height="300px" width="300px"}

### **visible**

Type: ``boolean`` (default: ``false``) | **1.0.0-rc.3** (``task-type-definitions``)

Se o tipo de tarefa é visível para o utilizador na APP (no caso de erro ao obter tarefas do servidor ou quando o número de tarefas a executar é 0).

!!! note "Exemplo"
    ```json
    {
        "visible": true
    }
    ```

![image alt <>](assets/visible.png){:height="300px" width="300px"}

### **zones**
Type: ``Array`` [""] (``task-type-definitions``)


Define se é feito o pedido de criação de tarefa com zonas.

!!! note "Exemplo"
    ```json
    {
        "zones":[
            "store",
            "warehouse"
        ]
    }
    ```


## FLUXO PROCESSAMENTO DE ARTIGO ##

### **alertColors**

Type: ``array`` (default: []) | **1.6.0-rc.0** (``task-type-definitions``)

Array de objectos com a definição de cores para um alerta que pertença ao intervalo especificado.

???+ example "Estrutura:"
    * "range": number[``2``] - intervalo de pesos inclusivo (array com duas posições)
    * "color": number[3] - códigos RGB da cor a aplicar para o intervalo (array com três posições)

    **Exemplo**
    ```json
       "alertColors": [
      {
        "range": [0, 10],
        "color": [255, 165, 0]
      },
      {
        "range": [11, 20],
        "color": [255, 187, 149]
      }
    ]
    ```

![image alt <>](assets/alertColors.png){:height="300px" width="300px"}

### **alertIcon**

Type: ``boolean`` (default: ``false``) | **1.27.0-rc.0** (``task-type-definitions``)

Define se apresenta o ícone na presença de alertas em artigos/caixas.

!!! note "Exemplo"
    ```json
    {
        "alertIcon": true
    }
    ```

<!--|          |                          |
| -------------- | ----------------------------------- |
| ![image alt <>](assets/alertIcon.png){:height="400px" width="400px"}       | ![image alt <>](assets/alertIcon_2.png){:height="400px" width="400px"}  |-->

![alt](assets/alertIcon.png){:height="375px" width="375px"} ![alt](assets/alertIcon_2.png){:height="375px" width="375px"}

<!--| Method      | Description                          |
| :----------- | :------------------------------------ |
| `GET`       | :material-check:     Fetch resource  |
| `PUT`       | :material-check-all: Update resource |
| `DELETE`    | :material-close:     Delete resource |-->

### **allowCreateZonesAdhoc**

Type: ``boolean`` (default: ``false``) | **1.22.0-rc.0** (``task-type-definitions``)

Define se é possível criar zonas adhoc nas tarefas de inventário.
Se o valor for 'false' então não permite criar zonas adhoc. Se o valor for 'true', permite criar zonas adhoc ao nível do 'children'. É aplicado exclusivamente nas tarefas de Inventários.

!!! note "Exemplo"
    ```json
    {
        "allowCreateZonesAdhoc": true
    }
    ```

![image alt <>](assets/allowCreateZonesAdhoc.png){:height="300px" width="300px"}

### **allowContainers** ###

!!! warning "Deprecated"
    Use [allowContainersV2](#allowcontainersv2) instead

Type: ``string`` (default: ``never``) | **1.0.0-rc.3** (``task-type-definitions``)

Define se é permitido criar volumes ad-hoc para o tipo de tarefa.

<!--![Placeholder](assets/taskLocationPrint.png){: align=left width=300 }

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

```json
{
    "zones":[
        "store",
        "warehouse"
    ]
}
```
-->

### **allowContainersV2** ###

Type: ``object``(default: { "level": ``"never"`` } ) | **1.19.0-rc.0** (``task-type-definitions``)

Define se é permitido criar volumes ad-hoc para o tipo de tarefa.

* ``"withoutLabel"``: indica se é possível a criação de volumes ad-hoc sem etiqueta, na raiz da tarefa (primeiro nível). Valor por default: ``false``
* ``"level"`` - identifica o nível onde é possível criar volumes.
???+ example "Valores possíveis:"
    * ``always`` - permite criar volumes ad-hoc para o tipo de tarefa em qualquer nível
    * ``first-level`` - permite criar volumes ad-hoc apenas na raiz da tarefa (primeiro nível)
    * ``never`` - não permite criar volumes ah-hoc para o tipo de tarefa (valor por defeito)
    
    **Exemplo**
    ```json
    {
      "allowContainersV2": {
        "level": "first",
        "withoutLabel": true
      }
    }
    ```




<!--![Foto](assets/allowContainersV2.png){: align=left width=300 }
docs\assets\allowContainersV2.png
-->

![alt](assets/alertIcon_3.png){:height="375px" width="375px"} ![alt](assets/allowContainersV2.png){:height="375px" width="375px"}

<!--![image alt <>](assets/allowContainersV2.png){:height="300px" width="300px"}-->


Se a setting estive definida com o campo ``"withoutLabel"``: true, a app disponibiliza no canto superior direito do ecrã o simbolo  ![foto](assets/box_allowContainersV2.png){:height="20px" width="20px"}.

Através deste input, a app permite ao utilizador a criação de um container com Id gerado aleatóriamente.

Apenas é permitido a criação de container adhoc para o nível defenido pelo campo ``"level"``.


### **allowDateOverLimit** ###

Type: ``boolean`` (default: ``true``) | **1.24.0-rc.0** (``task-type-definitions``)

Permite inserir uma data de validade dentro do número de dias parametrizados na tarefa.

!!! note "Exemplo"
    ```json
    {
      "allowDateOverLimit": false
    }
    ```
![image alt <>](assets/allowDateOverLimit.png){:height="300px" width="300px"}

### **allowDepreciateOverSoh** ###

Type: ``boolean`` (default: ``true``) | **1.20.0-rc.0** (``task-type-definitions``)

Impede a depreciação de um artigo se a quantidade a depreciar for superior ao SOH. Se não for possível obter o valor do SOH, a app permite avançar com o processo de depreciação mesmo que o valor da setting seja ``false``.
Se a setting estiver parametrizada com o valor ``true``, a aplicação permite depreciar o artigo mesmo que a quantidade inserida for superior à quantidade de SOH. Neste caso, disponibiliza um alerta ao utilizador mas permite avançar com a depreciação. Se o valor for ``false`` disponibiliza um alerta ao utilizador que é impeditivo de avançar com a depreciação. 

!!! note "Exemplo"
    ```json
    {
      "allowDepreciateOverSoh": true
    }
    ```
![image alt <>](assets/allowDepreciateOverSoh.png){:height="300px" width="300px"}

### **allowExpectedQuantityDefault** ###

Type: ``boolean`` (default: ``false``) | **1.18.1-rc.0**

Quando a quantidade do artigo ainda não foi alterada, permite colocar por defeito na quantidade, a quantidade expectável do artigo. 

!!! note "Exemplo"
    ```json
    {
      "allowExpectedQuantityDefault": false
    }
    ```
![image alt <>](assets/allowExpectedQuantityDefault.png){:height="300px" width="300px"}

<!--:fontawesome-regular-check-circle:{: .medium }-->

### **allowOverQuantity** ###

Type: ``boolean`` (default: ``true``) | **1.1.0-rc.2** (``task-type-definitions``)

Define se é permitido um artigo ter um valor de unidades processadas superior ao valor de unidades previstas.

!!! note "Exemplo"
    ```json
    {
      "allowOverQuantity": false
    }
    ```


### **allowPickingSkus** ###

!!! warning "Deprecated"
    Use [searchItem](#searchitem) instead


Type: ``boolean`` (default: ``true``) 

Esta parametrização permite, por funcionalidade, limitar o tipo de input quando se pretende processar um artigo.

### **askLabelPrice** ###

!!! warning "Deprecated"
    Use [getPricesOnPicking](#getpricesonpicking) instead

Type: ``boolean`` (default: ``false``) | **1.13.0-rc.0** 

Realizar o pedido de introdução manual do preço quando a etiqueta não tem preço incluído.

### **allowUndo** ###

Type: : ``boolean`` (default: ``true``) | **1.19.0-rc.0** (``task-type-definitions``)

Possibilita anular a última alteração feita a um artigo/caixa.

!!! note "Exemplo"
    ```json
    {
      "allowUndo": true
    }
    ```


![image alt <>](assets/allowUndo.png){:height="300px" width="300px"}

### **allowShelfQuantity** 

Type: ``boolean`` (default: ``false``) | **1.17.0-rc.2** (``task-type-definitions``)

Permite habilitar o input de "Quantidade Prateleira" nas tarefas de STOCKOUTAUDIT para clientes que não têm retorno do valor do serviço PS.

!!! note "Exemplo"
    ```json
    {
      "allowShelfQuantity": true
    }
    ```

![image alt <>](assets/allowShelfQuantity.png){:height="300px" width="300px"}


### **askOnEqualPrice**

Type: ``boolean`` (default: ``false``) (``task-type-definitions``)


Esta parametrização permite, na remarcação, rentabilizar a impressão de etiquetas caso o preço da etiqueta seja igual ao preco novo a imprimir.

!!! note "Exemplo"
    ```json
    {
      "askOnEqualPrice": true
    }
    ```

### **askSecondEAN**

Type: ``boolean`` (default: false) | **1.24.0-rc.0** (``task-type-definitions``)

Pede a inserção de um segundo EAN para artigos integrados com atributo ``pair`` com valor  ``true``.

!!! note "Exemplo"
    ```json
    {
      "askSecondEAN": true
    }
    ```

![image alt <>](assets/askSecondEAN.png){:height="300px" width="300px"}


### **checkReaderInput**

Type: ``object`` (default: {}) | **1.14.0-rc.2** (``task-type-definitions``)

Define uma expressão regular a aplicar nas picagens, quer via leitor quer manualmente.

???+ example "Estrutura da setting"

    * ``pattern``: string - o padrão a aplicar ao código de barras/ean/sku picado

    * `manual`: boolean - Define se o padrão vai ser aplicado às picagens manuais (defalt-``false``)

    * `reader`: boolean - Define se o padrão vai ser aplicado às picagens via scanner (defalt-``false``)

    **Exemplo**

    ```json
    "checkReaderInput":
    {
      "pattern": "^\d{1,13}$",
      "manual": true,
      "reader": false
    }
    ```


### **difficulty**

Type: ``string`` (default: ``"Q"``) | **1.20.0-rc.2** (``task-type-definitions``)

Define qual o nível de dificuldade na adição de artigos/volumes a uma tarefa.

???+ example "Valores Possíveis:"
    * ``N`` - "None": Adiciona o artigo/volume à tarefa sem fazer questões.
    * ``DP`` - "Double picking": O utilizador tem que picar duas vezes o artigo para inserir na tarefa.
    * ``Q`` - "Question": Pergunta ao utilizador se pretende adicionar ad-hoc.
    * ``W`` - "Warning": Mostra um alerta ao utilizador e adiciona o artigo/volume.

    **Exemplo**
    ```json
    "difficulty": "N"
    ```


### **filterResources**

Type: ``boolean`` (default: ``false``) | **1.1.0-rc.1** (``task-type-definitions``)

Define se é permitido filtrar a listagem de artigos dentro da tarefa.

???+ example "Valores Possíveis:"
    * ``UP`` - Define o filtro de "Subidas de Preço"
    * ``DOWN`` - Define o filtro de "Descidas de Preço"

    **Exemplo**
    ```json
    "filterResources": true,
    ```
![image alt <>](assets/filterResources.png){:height="300px" width="300px"}

### **getExpirationParameters**

Type: ``boolean`` (default: ``false``) - **1.3.0-rc.1** (``task-type-definitions``)

Ao adicionar um item ad-hoc realiza o pedido de informação dos parâmetros de validade (nº dias de depreciação, retirada e máxima % de desconto).

!!! note "Exemplo"
    ```json
    {
      "getExpirationParameters": true
    }
    ```


### **getItemAdhocInfo**

Type: ``boolean`` (default: ``true``) | **1.0.0-rc.3** (``task-type-definitions``)

!!! warning "Deprecated"
    Use [getItemAdhocInfoV3](#getitemadhocinfov3) instead

Ao adicionar um item ad-hoc faz um pedido ao servidor para complementar a informação do mesmo (necessária conexão).

### **getItemAdhocInfoTimeout**

Type: ``number`` (default: ``5000``) | **1.0.0-rc.3** (``task-type-definitions``)

!!! warning "Deprecated"
    Use [getItemAdhocInfoV3](#getitemadhocinfov3) instead

(só aplicável se "getItemAdhocInfo" = ``true``)

Define o número máximo de milissegundos de espera até resolução do pedido. No caso de timeout o fluxo continua assumindo que a definição getItemAdhocInfo tem o valor ``false``.


### **getItemAdHocInfoV2**

Type: ``object`` (default: { "active": ``true`` }) | **1.24.0-rc.0** (``task-type-definitions``)

!!! warning "Deprecated"
    Use [getItemAdhocInfoV3](#getitemadhocinfov3) instead

Ao adicionar um item ad-hoc faz um pedido ao servidor para complementar a informação do mesmo. Permite ainda definir se valida a existencia do artigo na gama da loja de destino, assim como um timeout para o pedido.


### **getItemAdHocInfoV3**

Type: ``object`` (default: { "active": ``true`` }) | **1.27.1-rc.0** (``task-type-definitions``)

Ao adicionar um item ad-hoc faz um pedido ao servidor para complementar a informação do mesmo. Permite ainda definir parâmetros para envio no pedido (validação do artigo na gama da loja de destino e outras opções), assim como um timeout para o pedido. Permite também definir no caso de erro na resposta do serviço se a aplicação insere o artigo na tarefa.

???+ example "Valores Possíveis:"
    * ``"active"``: ``boolean`` -  Define se a funcionalidade está activa (default: ``true``).
    * ``"timeout"``: ``number`` - Define o número máximo de milissegundos de espera até resolução do pedido. No caso de timeout o fluxo continua assumindo que a funcionalidade não está activa (default: ``5000``).
    * ``"queryParameters"``: ``array``- parâmetros a passar no pedido ("destination" (a loja de destino) e "extra").
    * ``createResourceOnError``: ``boolean`` - Define se adiciona artigo à tarefa no caso de erro na resposta do serviço.

    **Exemplo**
    ```json
    "getItemAdHocInfoV3": {
			"active": true,
      "createResourceOnError": true,
			"queryParameters": [
				{
					"code": "destination",
					"value": true
				},
				{
					"code": "extra",
					"value": "replenish"
				}
			],
			"timeout": 3000
		},
    ```


### **getPricesOnPicking**

Type: ``object`` (default: ``{}``) | **1.17.0-rc.0** (``task-type-definitions``)

Possibilidade de obter preços do artigo na picagem manual ou via leitor. Além disso permite ao utilizador a introdução manual de preço(s) quando a etiqueta picada não retorna preço(s).

???+ example "Valores Possíveis:"
    * ``"active"``: ``boolean`` - Define se a funcionalidade está activa (default: ``false``).
    * ``"askLabelPrice"``: ``boolean`` - caso a funcionalidade esteja activa e quando a etiqueta não tem preço(s) o utilizador pode introduzir manualmente preço(s)  (default: ``false``)

    **Exemplo**
    ```json
    "getPricesOnPicking": {
			"active": true,
			"askLabelPrice": true
		},
    ```
![image alt <>](assets/getPricesOnPicking.png){:height="300px" width="300px"}

### **insertWeightManually**

Type: ``boolean`` (default: ``false``) | **1.8.0-rc.0** (``task-type-definitions``)

Ao ler/picar uma etiqueta de preço variável (que começa por 26 ou 27), pede ao utilizador para inserir manualmente o peso, bloqueando a possibilidade de alterar manualmente este peso ao pedir reposição (apenas com nova picagem).

!!! note "Exemplo"
    ```json
    {
      "insertWeightManually": true
    }
    ```


### **labelChooseByServer**

Type: ``boolean`` (default: ``false``) | **1.8.0-rc.0** (``task-type-definitions``)

!!! warning "Deprecated"
    Use [labelChooseByServerV2](#labelchoosebyserverv2) instead

Define se é permitido escolher o tipo de etiqueta a imprimir ou se esta definição é feita automaticamente pelo servidor.

### **labelChooseByServerV2**

Type:  ``object`` (default: ``{}``) | **1.16.1-rc.0** (``task-type-definitions``)

Se é permitido escolher o tipo de etiqueta a imprimir ou se esta definição é feita automaticamente pelo servidor. Além disso permite definir uma etiqueta por defeito, assim como a opção "check-setting" ao imprimir.

???+ example "Valores Possíveis:"
    * ``"active"``: ``boolean`` - Define se é o servidor a definir o tipo de etiqueta a ser imprimida (valor por defeito: ``false``)
    * ``"defaultLabelCode"``: ``string`` - Define se o tipo de etiqueta por defeito quando o utilizador não escolhe um tipo de etiqueta a imprimir
    * ``"checkSettingOnPrint"``: ``boolean`` - Define se envia o parâmetro "check-setting" na impressão

    **Exemplo**
    ```json
    "labelChooseByServerV2": {
			"active": true,
			"checkSettingOnPrint": false,
			"defaultLabelCode": "30"
		},
    ```
![image alt <>](assets/labelChooseByServerV2.png){:height="300px" width="300px"}

### **manageContainers**

Type: ``boolean`` (default: ``false``) | **1.1.0-rc.1** (``task-type-definitions``)

!!! warning "Deprecated"
    Use [manageContainersV2](#managecontainersv2) instead

Se é permitido gerir volumes para o tipo de tarefa (adicionar e/ou alterar volumes).

### **manageContainersV2**

Type: ``object`` (default: { "active": ``false`` }) | **1.34.0-rc.0** (``task-type-definitions``)

Define se é permitido gerir volumes para o tipo de tarefa (adicionar e/ou alterar volumes).

???+ example "Valores Possíveis:"
    * ``"active"``: ``boolean`` - define se é permitido gerir volumes (valor por defeito: ``false``).
    * ``"name"``: ``boolean`` - define se é permitido definir o nome de um volume (valor por defeito: ``false``).
    * ``"default_container"``: ``boolean`` - define se vai criar automaticamente um volume ao iniciar uma tarefa vazia (valor por defeito: ``false``).
    * ``"print": ``string`` - Define se é possível imprimir a etiqueta identificador do container e define qual o tipo de impressão:
        * ``mobile``: ``string`` - Define a impressão móvel.
        * ``laser``: ``string`` - Define a impressão através de impressora fixa.
    * ``action``: ``string`` - Define que tipo de ação será realizada ao artigo:
        * ``moved``: ``string`` - Artigo movido entre níveis da tarefa.
        * ``delete``: ``string`` - Artigo eliminado da tarefa.

    **Exemplo**
    ```json
    "manageContainersV2": {
			"active": true,
			"name": true,
			"default_container": true,
            "print": "mobile",
            "action": "moved"
		},
    ```
![image alt <>](assets/manageContainersV2.png){:height="300px" width="300px"}

### **maxEanLength**

Type: ``number`` | **1.2.0-rc.1** (``task-type-definitions``)

!!! warning "Deprecated"
    Use [checkReaderInput](#checkreaderinput) instead

Esta parametrização é utilizada apenas no tipo de tarefa em que se pretende limitar o tamanho do EAN lido. Caso o EAN seja superior ao máximo permitido, será mostrada uma mensagem de erro.

!!! note "Exemplo"
    ```json
    {
      "maxEanLength": 13
    }
    ```

### **onAddResourceError**

Type: ``boolean`` (default: ``false``) | **1.0.0-rc.3** (``task-type-definitions``)

!!! warning "Deprecated"


(só aplicável se getItemAdhocInfo = ``true``)

Aplica acções pré-definidas em caso de erro ao adicionar item adhoc.

!!! note "Exemplo"
    ```json
    {
      "onAddResourceError": true
    }
    ```


### **onGetItemAdhocInfo**

Type: ``boolean`` (default: ``false``) | **1.0.0-rc.3** (``task-type-definitions``)

!!! warning "Deprecated"
    Use [getItemAdhocInfoV3](#getitemadhocinfov3) instead

Aplica acções pré-definidas depois de obter informação do item adicionado.

### **onlyChangeQuantityByPicking**

Type: ``boolean`` (default: ``false``) | **1.0.0-rc.3** (``task-type-definitions``)

Se valor ``true``, apenas permite alterar as quantidades de um artigo via picagem.

!!! note "Exemplo"
    ```json
    {
      "onlyChangeQuantityByPicking": true
    }
    ```
![image alt <>](assets/onlyChangeQuantityByPicking.png){:height="300px" width="300px"}

### **processOnPicking**

Type: ``boolean`` (default: ``false``) | **1.1.0-rc.10** (``task-type-definitions``)

Processar a quantidade total de um artigo na picagem. Se o artigo não possuir quantidade expectável, será processada uma unidade. Para esta definição ser aplicada é necessário que o artigo se encontre ao mesmo nível da picagem.

!!! note "Exemplo"
    ```json
    {
      "processOnPicking": false
    }
    ```


### **product-overview**

Type: ``object []`` | **v0.8.0** (``application-setting``)

Define o template para página de entrada de ficha de artigo.

??? note "Exemplo"
    ```json
    [
    {
    "attributes": {
    "condensed": false
    },
    "code": "item_details",
    "template": "map",
    "value": [
    {
    "code": "mrs_name",
    "value": {
    "$ref": "$.product.name"
    }
    },
    {
    "code": "mrs_image_default",
    "description": {
    "$ref": "$.product.name"
    },
    "value": {
    "$ref": "$.product.image"
    }
    },
    {
    "code": "mrs_sku",
    "description": "SKU",
    "value": {
    "$ref": "$.product.sku"
    }
    },
    {
    "code": "mrs_ean",
    "description": "EAN",
    "value": {
    "$ref": "$.product.ean"
    }
    },
    {
    "code": "mrs_stock",
    "description": "SOH",
    "value": {
    "$ref": "$.mrs_stock.value"
    }
    },
    {
    "code": "mrs_status",
    "description": "Status",
    "value": {
    "$ref": "$.product.status"
    }
    }
    ]
    },
    {
    "code": "item_price",
    "description": "${price}",
    "template": "grid",
    "value": [
    {
    "code": "row",
    "value": [
    {
    "code": "col",
    "value": [
    {
    "code": "row",
    "value": [
    {
    "code": "mrs_price",
    "description": "MRS",
    "value": {
    "$ref": "$.price.mrs"
    }
    }
    ]
    }
    ]
    }
    ]
    }
    ]
    },
    {
    "code": "mrs_attributes",
    "description": "Attributes",
    "template": "list",
    "value": {
    "$map": {
    "code": "mrs_attribute",
    "description": {
    "$ref": "@.attribute"
    },
    "value": {
    "$map": {
    "code": "mrs_sibling_attribute",
    "description": {
    "$ref": "@.attribute"
    },
    "value": {
    "$ref": "@.skus"
    }
    },
    "$ref": "@.siblings"
    }
    },
    "$ref": "$.mrs_attributes"
    }
    },
    {
    "code": "item_related_pages",
    "template": "nav_list",
    "value": [
    {
    "code": "stock",
    "description": "${stock}",
    "value": "product-stock"
    },
    {
    "code": "price",
    "description": "${price} & ${promotions}",
    "value": "product-price"
    },
    {
    "code": "features",
    "description": "${features}",
    "must": [
    {
    "$ref": "$.product.fashion"
    },
    {
    "$ref": "$.product.productType"
    }
    ],
    "value": {
    "$ref": "$.mrs_product_features"
    }
    },
    {
    "code": "hierarchy",
    "description": "${hierarchy}",
    "value": "product-category-hierarchy"
    },
    {
    "code": "eans",
    "description": "${eans}",
    "value": "product-eans"
    }
    ]
    }
    ]
    ```
![image alt <>](assets/product-overview.png){:height="300px" width="300px"}

### **product-price**

Type: ``object []`` | **v0.8.0** (``application-setting``)

Define o template para página de preço e promoções do artigo (ficha de artigo).

??? note "Exemplo"
    ```json
    [
    {
    "attributes": {
    "condensed": true
    },
    "code": "item_details",
    "template": "map",
    "value": [
    {
    "code": "mrs_name",
    "value": {
    "$ref": "$.product.name"
    }
    },
    {
    "code": "mrs_image_default",
    "description": {
    "$ref": "$.product.name"
    },
    "value": {
    "$ref": "$.product.image"
    }
    },
    {
    "code": "mrs_sku",
    "description": "SKU",
    "value": {
    "$ref": "$.product.sku"
    }
    },
    {
    "code": "mrs_ean",
    "description": "EAN",
    "value": {
    "$ref": "$.product.ean"
    }
    },
    {
    "code": "mrs_stock",
    "description": "SOH",
    "value": {
    "$ref": "$.mrs_stock.value"
    }
    },
    {
    "code": "mrs_status",
    "description": "Status",
    "value": {
    "$ref": "$.product.status"
    }
    }
    ]
    },
    {
    "code": "item_price",
    "description": "${price}",
    "template": "grid",
    "value": [
    {
    "code": "row",
    "value": [
    {
    "code": "col",
    "value": [
    {
    "code": "row",
    "value": [
    {
    "code": "mrs_price",
    "description": "MRS",
    "value": {
    "$ref": "$.mrs_price.value"
    }
    }
    ]
    },
    {
    "code": "row",
    "value": [
    {
    "code": "mrs_erp_price",
    "description": "ERP",
    "value": {
    "$ref": "$.erp_price.value"
    }
    }
    ]
    }
    ]
    }
    ]
    }
    ]
    },
    {
    "code": "item_promotions",
    "description": "${promotions}",
    "template": "grid",
    "value": {
    "$map": {
    "code": "row",
    "description": {
    "$oneOf": [
    {
    "$ref": "@.group/name"
    },
    {
    "$ref": "@.description"
    }
    ]
    },
    "value": [
    {
    "code": "col",
    "value": [
    {
    "code": "row",
    "value": [
    {
    "code": "mrs_promotion_start",
    "description": "Inicio",
    "value": {
    "$ref": "@.startDate",
    "$transform": "moment|timezone>$.mrs_store.timezone.value|format>DD-MM-YYYY"
    }
    }
    ]
    },
    {
    "code": "row",
    "value": [
    {
    "code": "mrs_promotion_end",
    "description": "Fim",
    "value": {
    "$ref": "@.endDate",
    "$transform": "moment|timezone>$.mrs_store.timezone.value|format>DD-MM-YYYY"
    }
    }
    ]
    }
    ]
    },
    {
    "code": "col",
    "value": [
    {
    "code": "row",
    "value": [
    {
    "code": "mrs_promotion_info",
    "description": "Preço",
    "value": {
    "$ref": "@.price"
    }
    }
    ]
    }
    ]
    }
    ]
    },
    "$ref": "$.promotions"
    }
    }
    ]
    ```
![image alt <>](assets/product-overview.png){:height="300px" width="300px"}

### **product-stock**

Type: ``object[]`` | **v0.8.0** (``application-setting``)

Define o template para página de stock do artigo (ficha de artigo).

??? note "Exemplo"
    ```json
    [
    {
    "attributes": {
    "condensed": true
    },
    "code": "item_details",
    "template": "map",
    "value": [
    {
    "code": "mrs_name",
    "value": {
    "$ref": "$.product.name"
    }
    },
    {
    "code": "mrs_image_default",
    "description": {
    "$ref": "$.product.name"
    },
    "value": {
    "$ref": "$.product.image"
    }
    },
    {
    "code": "mrs_sku",
    "description": "SKU",
    "value": {
    "$ref": "$.product.sku"
    }
    },
    {
    "code": "mrs_ean",
    "description": "EAN",
    "value": {
    "$ref": "$.product.ean"
    }
    },
    {
    "code": "mrs_stock",
    "description": "SOH",
    "value": {
    "$ref": "$.mrs_stock.value"
    }
    },
    {
    "code": "mrs_status",
    "description": "Status",
    "value": {
    "$ref": "$.product.status"
    }
    }
    ]
    },
    {
    "code": "stock",
    "description": "${stock}",
    "template": "list",
    "value": [
    {
    "code": "stock_on_hand",
    "description": "${stock-on-hand}",
    "value": {
    "$ref": "$.erp_stock.stock_on_hand"
    }
    },
    {
    "code": "stock_unavailable",
    "description": "${stock-unavailable}",
    "value": {
    "$ref": "$.erp_stock.stock_unavailable"
    }
    },
    {
    "code": "stock_on_transit",
    "description": "${stock-on-transit}",
    "value": {
    "$ref": "$.erp_stock.stock_on_transit"
    }
    },
    {
    "code": "stock_on_order",
    "description": "${stock-on-order}",
    "value": {
    "$ref": "$.erp_stock.stock_on_order"
    }
    }
    ]
    },
    {
    "code": "item_related_pages",
    "template": "nav_list",
    "value": [
    {
    "code": "stock",
    "description": "${stock-other-stores}",
    "value": "product-stock-others"
    }
    ]
    }
    ]
    ```
![image alt <>](assets/product-stock.png){:height="300px" width="300px"}

### **product-eans**

Type: ``object[]`` | **v0.8.0** (``application-setting``)

Define o template para página de eans (primários e não primários) do artigo (ficha de artigo).

??? note "Exemplo"
    ```json
    [
    {
    "code": "product_eans",
    "description": "EANs",
    "template": "list",
    "value": {
    "$map": {
    "code": {
    "$ref": "mrs_ean"
    },
    "description": {
    "$ref": "@.ean"
    },
    "value": {
    "$ref": "@.primary",
    "$transform": "strictEquals>Y|then>${primary-ean}>"
    }
    },
    "$ref": "$.mrs_eans"
    }
    }
    ]
    ```
![image alt <>](assets/product-overview.png){:height="300px" width="300px"}

### **product-features-f**

Type: ``object[]`` | **v0.8.0** (``application-setting``)

Define o template para características fashion do artigo (ficha de artigo).

??? note "Exemplo"
    ```json
    [
    {
    "attributes": {
    "condensed": true
    },
    "code": "product-features-f",
    "description": "${features}",
    "template": "list",
    "value": [
    {
    "code": "season",
    "description": "${season}",
    "value": {
    "$ref": "$.mrs_features_fashion.season"
    }
    },
    {
    "code": "phase",
    "description": "${phase}",
    "value": {
    "$ref": "$.mrs_features_fashion.phase"
    }
    },
    {
    "code": "theme",
    "description": "${theme}",
    "value": {
    "$ref": "$.mrs_features_fashion.theme"
    }
    }
    ]
    }
    ]
    ```
![image alt <>](assets/product-overview.png){:height="300px" width="300px"}

### **product.category-hierarchy**

Type: ``object[]`` | **v0.8.0** (``application-setting``)

Define o template para a categoria hierarquica do artigo (ficha de artigo).

??? note "Exemplo"
    ```json
    [
    {
    "code": "product_category_hierarchy",
    "description": "${hierarchy}",
    "template": "list",
    "value": {
    "$map": {
    "code": {
    "$ref": "@.code"
    },
    "description": {
    "$ref": "@.name"
    },
    "value": {
    "$ref": "@.code"
    }
    },
    "$ref": "$.mrs_category_hierarchy"
    }
    }
    ]
    ```
![image alt <>](assets/product.category-hierarchy.png){:height="300px" width="300px"}

### **products-stock-others**

Type: ``object[]`` | **v0.8.0** (``application-setting``)

Define o template para stock de outras lojas (ficha de artigo).

??? note "Exemplo"
    ```json
    [
    {
    "attributes": {
    "condensed": true
    },
    "code": "item_details",
    "template": "map",
    "value": [
    {
    "code": "mrs_name",
    "value": {
    "$ref": "$.product.name"
    }
    },
    {
    "code": "mrs_image_default",
    "description": {
    "$ref": "$.product.name"
    },
    "value": {
    "$ref": "$.product.image"
    }
    },
    {
    "code": "mrs_sku",
    "description": "SKU",
    "value": {
    "$ref": "$.product.sku"
    }
    },
    {
    "code": "mrs_ean",
    "description": "EAN",
    "value": {
    "$ref": "$.product.ean"
    }
    },
    {
    "code": "mrs_stock",
    "description": "SOH",
    "value": {
    "$ref": "$.mrs_stock.value"
    }
    },
    {
    "code": "mrs_status",
    "description": "Status",
    "value": {
    "$ref": "$.product.status"
    }
    }
    ]
    },
    {
    "code": "stock_others",
    "description": "${stock}",
    "template": "list",
    "value": {
    "$map": {
    "code": {
    "$ref": "@.store"
    },
    "description": {
    "$ref": "@.store"
    },
    "value": {
    "$ref": "@.stock.stock_on_hand"
    }
    },
    "$ref": "$.erp_stock_stores.value"
    }
    }
    ]
    ```
![image alt <>](assets/product.category-hierarchy.png){:height="300px" width="300px"}

### **reasons** 

Type: ``Array<string>`` | **1.2.0-rc.1** (``task-type-definitions``)

Quando um tipo de tarefa tem motivos associados ao processamento dos artigos. O motivo selecionado é depois enviado na tarefa no campo "reason.id".

![image alt <>](assets/reasons.png){:height="300px" width="300px"}

### **regent-price**

Type: ``string`` (default: ``"erp"``) | **1.7.0-rc.0** (``application-setting``)

Define o preço regente, em caso de divergência de preços.

???+ example "Valores Possíveis:"
    * ``erp``: ``string`` - Define ERP value como valor regente.
    * ``pos``: ``string`` - Define POS value como valor regente.
    * ``label``: ``string`` - Define LABEL value como valor regente.

    **Exemplo**
    ```json
      "regent-price": {
			"pos"
      },
    ```

### **removeItemAdhoc**

Type: ``boolean`` (default: ``true``) | **1.4.0-rc.0** (``task-type-definitions``)

Permite eliminar artigo adhoc.

!!! note "Exemplo"
    ```json
    {
      "removeItemAdhoc": true
    }
    ```
![image alt <>](assets/removeItemAdhoc.png){:height="300px" width="300px"}

### **search-item-by-text**

!!! warning "Deprecated"
    Use [searchItem](#searchitem) instead

Type: ``object`` (default: "active":``true``) | **1.11.0-rc.0** (``application-setting``)

### **searchItem**

Type: ``object`` | **1.11.0-rc.1** (``task-type-definitions``)

Se é possível a pesquisa manual de artigos dentro de uma tarefa e de que forma. Além disso, permite configurar a picagem de códigos de barras que contém EAN ou SKU.

???+ example "Valores Possíveis:"
    * ``ean``: ``boolean`` - Define se é possível a pesquisa manual de artigo. por EAN.
    * ``sku``: ``boolean`` - Define se é possível a pesquisa manual de artigo. por SKU.
    * ``camera``: ``boolean`` - Possibilita a pesquisa via câmara do dispositivo.
    * ``text``: ``boolean`` - Possibilita a pesquisa por texto - ean, sku, descrição, etc.
    * ``scan_sku``: ``boolean`` - Possibilita a picagem de etiquetas com informação de sku.
    * ``scan_ean``: ``boolean`` - Possibilita a picagem de etiquetas com informação de EAN.
    * ``container``: ``boolean`` - Define se é possível a pesquisa manual de container.
    * ``location``: ``boolean`` - Define se é possível a pesquisa manual de localização.

    **Exemplo**
    ```json
      "searchItem": {
			"camera": false,
			"ean": false,
			"sku": false,
			"text": false,
			"scan_sku": true,
			"scan_ean": true,
            "container": true,
            "location": true
		},
    ```
![image alt <>](assets/searchItem.png){:height="300px" width="300px"}

### **sendFutureValidities**

Type: ``boolean`` (default: ``false``) | **1.15.0-rc.3** (``task-type-definitions``)

Envia para serviço externo datas de validade adicionadas.

!!! note "Exemplo"
    ```json
    {
      "sendFutureValidities": true
    }
    ```

### **show-images**

Type: ``object`` | **1.27.1-rc.0** (``application-setting``)

Define a visibilidade das imagens na aplicação.

!!! note "Exemplo"
    ```json
    {
      "active": true
    }
    ```
![image alt <>](assets/show-images.png){:height="300px" width="300px"}

### **showScheduledStartHour**

Type: ``boolean`` (default: ``true``) | **1.22.0-rc.1** (``task-type-definitions``)

Define se mostra a hora da tarefa na listagem.

!!! note "Exemplo"
    ```json
    {
      "showScheduledStartHour": true
    }
    ```
![image alt <>](assets/showScheduledStartHour.png){:height="300px" width="300px"}

### **sortResources**

Type: ``object`` | **1.33.0-rc.0** (``task-type-definitions``)

Permite ordenação de artigos de acordo com um conjunto de opções (todos os campos são opcionais):

???+ example "Valores Possíveis:"
    * ``NAME_A_Z``
    * ``NAME_Z_A``
    * ``UP_DOWN``
    * ``DOWN_UP``
    * ``HIERARCHICAL_LEVEL``
    * ``BRAND_A_Z``
    * ``BRAND_Z_A``
    * ``EXPIRATION_DATE``
    * ``PRIORITY_ASC``
    * ``PRIORITY_DESC``
    * ``UPDATE_DATE``
    * ``PICKING_DATE_ASC``
    * ``PICKING_DATE_DESC``
    * ``MAJOR_QUANTITY``
    * ``LESS_QUANTITY``


    **Estrutura**
    
    * "active": ``boolean`` - indica se é permitido ordenar artigos para o tipo de tarefa (valor por defeito: ``true``)
    * "defaultSort": ``string`` - indica a ordenação por defeito
    * "options": ``string``[] - indica as opções de ordenação na ordem definida no _array_. As opções ``UP_DOWN`` e ``DOWN_UP`` são exclusivas para as tarefas de Remarcação. As opções ``PRIORITY_ASC`` e ``PRIORITY_DESC`` são exclusivas para as tarefas de Controlo de Validades. (valor por defeito: ``["UPDATE_DATE", "NAME_A_Z", "NAME_Z_A"]``)

    **Exemplo**
    ```json
    {
      "sortResources": {
			  "active": true,
			  "defaultSort": "UP_DOWN", 
			  "options": [
				    "UP_DOWN",
				    "DOWN_UP",
				    "BRAND_A_Z",
				    "BRAND_Z_A",
				    "HIERARCHICAL_LEVEL"
			  ]
		  },
    }
    ```
![image alt <>](assets/sortResources.png){:height="300px" width="300px"}

### **synchronousReader**

Type: ``boolean`` (default: ``false``) | **1.13.0-rc.1**

!!! warning "Removed"
    

Realiza leituras de forma síncrona (apenas disponível em modo tarefa).


## **FLUXO APROVAÇÃO TAREFA**

### **allowLeaving**

Type: ``boolean`` (default: ``true``) | **1.14.0-rc.0** (``task-type-definitions``)

Define se permite sair de uma tarefa sem libertar/aprovar.

!!! note "Exemplo"
    ```json
    {
      "allowLeaving": true
    }
    ```
![image alt <>](assets/allowLeaving.png){:height="300px" width="300px"}


### **allowSync**

Type: ``boolean`` (default: ``true``) | **1.36.1-rc.0** (``task-type-definitions``)

Define se permite realizar a sincronização da tarefa.

!!! note "Exemplo"
    ```json
    {
      "allowSync": 
      {
        "active": true
      }
    }
    ```


### **emptyTaskAction**

Type: ``string`` (default: ``"none"``) | **1.25.0-rc.1** (``task-type-definitions``)

Define a acção a tomar ao aprovar uma tarefa sem artigos processados.

???+ example "Valores Possíveis:"
    * ``"none" ``- permite a aprovação da tarefa (valor por defeito)
    * ``"warning"`` - mostra um alerta, permitindo a aprovação da tarefa
    * ``"not_allowed"`` - mostra um alerta e não permite a aprovação da tarefa

    **Exemplo**
    !!! note "Exemplo"
    ```json
    {
      "emptyTaskAction": "not_allowed"
    }
    ```
![image alt <>](assets/emptyTaskAction.png){:height="300px" width="300px"}

### **hideResume**

Type: ``boolean`` (default: ``false``) | **1.14.0-rc.2** (``task-type-definitions``)

Define se a app apresenta o resumo no fecho da tarefa.

!!! note "Exemplo"
    ```json
    {
      "hideResume": false
    }
    ```

### **isToShowTaskEmptyWarning**

!!! warning "Deprecated"
    Use [emptyTaskAction](#emptytaskaction) instead

Type: ``boolean`` (default: ``false``) | **1.16.0-rc.0** (``task-type-definitions``)


### **notProcessedResourcesModal**

Type: ``object`` (default: ``false``) | **1.27.1-rc.0** (``task-type-definitions``)

Define o conteúdo da modal de não processados (ao aprovar uma tarefa).

???+ example "Valores Possíveis:"
    * "notFoundButton": ``boolean``  - Defina a possibilidade de dar os artigos/volumes como não encontrados (default: ``false``)
    * "treatAllButton": ``boolean`` - Defina a possibilidade de processar os artigos/volumes com a sua quantidade expectável (default: ``false``)
    * "resourceType": ``string`` - Define que "resources" devem aparecer na modal.
        * "containers": ``string`` - Define que a modal irá mostrar todos os volumes não processados.
        * "items": ``string`` -  Define que a modal irá mostrar todos os artigos não processados (default).
        * "none" - não irá mostrar a modal (aprova directamente a tarefa)
    
    **Exemplo**
    ```json
        {
	        "notProcessedResourcesModal": {
		        "notFoundButton": true,
		        "resourceType": "none"
	        }
        }
    ```
![image alt <>](assets/notProcessedResourcesModal.png){:height="300px" width="300px"}

### **previousApproveAction**

Type: ``object`` (default: {} ) | **1.24.0-rc.0** (``task-type-definitions``)

Permite definir ações específicas antes de aprovar uma tarefa.

???+ example "Valores Possíveis:"
    * "code": ``string`` - identifica o tipo de acção a executar. Valores possíveis:
        * "alert": ``string`` - executa um alerta com uma mensagem.
        * "input": ``string`` - obriga o utilizador a preencher um ou mais dados antes de aprovar a tarefa.
        * "divergence_alert": ``string`` - semelhante a alert mas apenas executa o alerta se existirem divergências nos artigos picados.
    * "attributes": ``array`` - array de atributos a aplicar para cada acção:
        * "code": ``string`` - o identificador do atributo
        * "type": ``string`` - o tipo do atributo (text, date, radio, select)
        * "value": ``string`` (optional) - caso esteja definido, é o valor apresentado para o atributo (para o tipo date apresenta a data resultante da soma deste campo value com a data de hoje).
        * "options": ``string`` -  (apenas disponível para os tipos radio e select) - array com as opções (code e value) para o atributo.
    
    **Exemplo**
    ```json
      {
      "previousApproveAction": {
        "code": "divergence_alert",
        "attributes": [
            {
              "code": "message",
              "type": "text",
              "value": Texto a ser apresentado na app"
            }
          ]
        }
      } 
      ...
      {
      "previousApproveAction": {
        "code": "input",
        "attributes": [
          {
            "type": "text",
            "code": "driver_id",
            "value": "123"
          },
          {
            "type": "text",
            "code": "driver_name"
          }
      } 
      ...
      {
      "previousApproveAction": {
        "type": "select",
        "code": "priority",
        "value": "A",
        "options": [
          {
              "code": "normal",
              "value": "A"
          },
          {
              "code": "urgent",
              "value": "B"
          }
        ]
      },
      ...
      {
      "previousApproveAction": {
        "type": "date",
        "code": "delivery_date",
        "value": 1
      },
      ...
      {
      "previousApproveAction": {
        "code": "alert",
        "attributes": [
          {
              "value": "Text"
          }
        ]
      },
    ```
![image alt <>](assets/previousApproveAction.png){:height="300px" width="300px"}

### **release-on-logout**

Type: ``object`` (default: ``false``) | **1.1.0-rc.9** (``application-setting``)

Define se no logout da app liberta todas as tarefas associadas ao utilizador.

!!! note "Exemplo"
    ```json
    {
      "active": true
    }
    ```


### **showFutureDatesOnApproval**

Type: ``boolean`` (default: ``false``) | **1.20.0-rc.2** (``task-type-definitions``)

Possibilita a apresentação da modal de datas futuras ao aprovar datas de validade.

!!! note "Exemplo"
    ```json
    {
      "showFutureDatesOnApproval": true
    }
    ```


### **summaryApprovalHandling**

Type: ``string`` (default: ``"none"``) | **1.4.0-rc.0** (``task-type-definitions``)

Define as regras para aprovação de tarefa.

???+ example "Valores Possíveis:"
    * "none": ``string`` - pode aprovar/finalizar tarefa sem restrições.
    * "AllHasToBeHandled": ``string`` - só pode aprovar se todos os artigos tiverem sido tratados 

    **Exemplo**
    !!! note "Exemplo"
    ```json
    {
      "summaryApprovalHandling": "none"
    }
    ```
![image alt <>](assets/summaryApprovalHandling.png){:height="300px" width="300px"}

## **MODAL DE ARTIGO**

### **dropdownOptions**

Type: ``array`` (default: []) | **1.2.0-rc.0** (``task-type-definitions``)

Array de objetos com as as opções para dropdown na modal da tarefa.

???+ example "Valores Possíveis:"
    * "data": ``string[]`` - identifica os valores do dropdown (se não existem valores, o dropdown não será mostrado)
    * "mandatory":  ``boolean``  - indica se é obrigatória a escolha de uma opção (valor por defeito: ``false``)
    * "type": ``string`` - identifica o tipo de dados do dropdown (valores possíveis: "REASONS" e "EXPIRATIONLABEL")

    **Exemplo**
    ```json
        {
      "dropdownOptions": [
        {
          "data": [
            "linear",
            "other-location"
          ],
          "mandatory": true,
          "type": "REASONS"
        },
        {
          "data": [
            "VUNTIL",
            "VCPB",
            "VUPB"
          ],
          "type": "EXPIRATIONLABEL"
        }
      ]
    }
    ```
![image alt <>](assets/reasons.png){:height="300px" width="300px"}

### **handleQuantities**

!!! warning "Deprecated"
    Use [hideQuantities](#hidequantities) instead

Type: ``boolean`` (default: ``true``) | **1.1.0-rc.11** (``task-type-definitions``)


### **hideQuantities**

Type: ``string`` (default: ``"none"``) | **1.23.0-rc.2** (``task-type-definitions``)

Define se mostra a quantidade e/ou quantidade expectável de  um artigo, quer na listagem quer na modal.



???+ example "Valores Possíveis:"
    * "all": ``string`` - Define se apresenta os valores de quantidade esperada e quantidade tratada na modal de artigo e na listagem de artigos nas tarefas.
    * "expected": ``string`` - Define se apresenta apenas os valores de quantidade esperada na modal de artigo e na listagem de artigos nas tarefas.
    * "none": ``string`` - Define que não apresenta os valores de quantidade esperada e quantidade tratada na modal de artigo e na listagem de artigos nas tarefas.

    **Exemplo**
    !!! note "Exemplo"
    ```json
    {
      "hideQuantities": "all"
    }
    ```
![image alt <>](assets/hideQuantities.png){:height="300px" width="300px"}

### **manageSupplyUnitOnModal**

Type: ``object`` (default - "active": ``false``) | **not yet released** (``task-type-definitions``)

Permite apresentar o valor de Supply Unit na modal de artigo e realizar o cálculo como facilitador de quantidade. (STOCKCOUNT)

!!! note "Exemplo"
    ```json
    {
      "manageSupplyUnitOnModal": {
        "active": true
      }
    }
    ```

### **maxPercentageDefault**

Type:  ``boolean`` (default: ``false``) | **1.27.0-rc.1** (``task-type-definitions``)

Caso a parametrização esteja activa, ao abrir a modal de depreciação o preço final é calculado em função do valor do atributo 'maxPercentageDiscount'.

!!! note "Exemplo"
    ```json
    {
      "maxPercentageDefault": false
    }
    ```

### **openModalOnPicking**

!!! warning "Deprecated"
    Use [openModalOnPickingV2](#openmodalonpickingv2) instead

Type: ``boolean`` (default: ``true``) | **1.1.0-rc.1** (``task-type-definitions``)

Define se abre a modal da tarefa ao picar um artigo. 

### **openModalOnPickingV2**

!!! warning "Deprecated"
    Use [openModalOnPickingV3](#openmodalonpickingv3) instead

Type: ``object`` (default: ``false``) | **1.7.4** (``task-type-definitions``)



### **openModalOnPickingV3**

Type: ``object`` (default: ``false``) | **1.40.0** (``task-type-definitions``)

Define se abre a modal da tarefa ao picar um artigo. Permite definição por plataforma. Além disso permite configurar se os artigos de peso variável são mandatórios na abertura da modal.

???+ example "Valores Possíveis:"
    * ``Wince``: ``object`` - Define a abertura de modal na plataforma WinCE.
        * ``active: ``boolean`` - Define se está ativa a abertura da modal para a plataforma Wince.
        * ``mandatoryVarWeight``: ``boolean`` - Define se na plataforma Wince é mandatória a abertura da modal na picagem de artigo de peso variável.
    * ``multiplataform``: ``boolean`` - Define a abertura de modal na multiplataforma.
        * ``active: ``boolean`` - Define se está ativa a abertura da modal para a multiplataforma.
        * ``mandatoryVarWeight``: ``boolean`` - Define se na multiplataforma é mandatória a abertura da modal na picagem de artigo de peso variável. 

    **Exemplo**
    !!! note "Exemplo"
    ```json
    {
	    "openModalOnPickingV3": {
		    "wince": {
			    "active": true,
			    "mandatoryVarWeight": true
		  },
		    "multiplatform": {
			    "active": true,
			    "mandatoryVarWeight": true
		  }
	  }
    ```

### **replenishItemOnModal**

!!! warning "Deprecated"
    Use [replenishItemOnModalV2](#replenishitemonmodalv2) instead

Type: ``boolean`` (default: ``false``) | **1.4.0-rc.0** (``task-type-definitions``)

Permite o acesso à reposição urgente via modal.

### **replenishItemOnModalV2**

Type: ``object`` (default: { "active": ``false`` }) | **1.27.1-rc.0** (``task-type-definitions``)

Permite o acesso à reposição urgente via modal e definir o tipo de tarefa a ser criada.

!!! note "Exemplo"
    ```json
    {
      "replenishItemOnModalV2": {
        "active": true,
        "type": "REPLENISHPREP"
      }
    }
    ```
![image alt <>](assets/replenishItemOnModalV2.png){:height="300px" width="300px"}

### **showContinueOnSelect**

Type: ``boolean`` (default: ``true``) - **1.12.0-rc.0** (``task-type-definitions``)

Se true, o botão CONTINUAR deve estar visível/disponível para processar o artigo manualmente, sem bipar o mesmo.

Se false, deve esconder o botão CONTINUAR para que não seja possível processar o artigo manualmente, obrigando a bipar o mesmo.

!!! note "Exemplo"
    ```json
    {
      "showContinueOnSelect": true
    }
    ```
![image alt <>](assets/showContinueOnSelect.png){:height="300px" width="300px"}

### **showPrint**

Type: ``boolean`` (default: ``true``) | **1.12.0-rc.0** (``task-type-definitions``)

Permite imprimir etiquetas manualmente através do ícone de impressora na modal do artigo.

Nota: esta definição também se aplica à ficha de artigo quando configurada para o tipo de tarefa "ITEMFILE".

!!! note "Exemplo"
    ```json
    {
      "showPrint": true
    }
    ```
![image alt <>](assets/showPrint.png){:height="300px" width="300px"}

### **showSOH** 

Type: ``boolean`` (default: ``true``) | **1.9.0-rc.0** (``task-type-definitions``)

Mostrar informação de SOH na modal de artigo.

!!! note "Exemplo"
    ```json
    {
      "showSOH": true
    }
    ```
![image alt <>](assets/showPrint.png){:height="300px" width="300px"}

### **searchProductOnModal**

Type: ``boolean`` (default: ``false``) | **not yet released** (``task-type-definitions``)

Permite pesquisa de skus com o mesmo Parent Id na tarefa de STOCKOUTAUDIT e STOCKCOUNT.

!!! note "Exemplo"
    ```json
    {
      "searchProductOnModal": true
    }
    ```

### **withdrawModalControl**

Type: ``object`` | Setting exclusiva para a app WInCE (``task-type-definitions``)

Permite configurar todos os campos que constituem a modal da retirada no tipo de tarefa EXPIRATIONCONTROL

!!! note "Exemplo"
    ```json
    {
      "withdrawModalControl": [
        {
          "code": "depreciated",
          "visible": true
        },
        {
          "code": "sales",
          "visible": true
        },
        {
          "code": "expected_withdraw_quantity",
          "visible": true
        },
        {
          "code": "label_quantity",
          "visible": true
        },
        {
          "code": "units_quantity",
          "visible": true
        },
        {
          "code": "reasons",
          "visible": true
        },
        {
          "code": "destinations",
          "visible": true
        },
        {
          "code": "damage_warning",
          "visible": true
        }
      ]
    }
    ```



