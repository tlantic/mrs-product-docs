# BACKOFFICE - INSTORE

![image alt <>](assets/capa_BO_Instore_2.png)

## Histórico do documento

| Data      | Descrição    | Autor | Versão | Revisão
| ------------- | ---------------------------------------------------------------------------------------- | ---------- | ---------- | -------- | 
| 01/12/2020         | Criação do documento.    |  Filipe Silva  |   1.0   |     |
| 12/12/2020     | Criação de Introdução de formatação de documento.       | Filipe Silva  |  1.1     |      |
| 22/12/2020     | Versão final do documento.       | Filipe Silva  |  2.0    |      |
| 22/01/2021     | Revisão final do documento.       | Filipe Silva  |  2.1    |  Helena Gonçalves|

## **Setups e Tarefas**

Para aceder à funcionalidade de criação e edição de Setups, o utilizador deve aceder ao Instore e selecionar "Setup de Tarefas".

![image alt <>](assets/instore_acess.gif)

### **Setup**

O Setup é um agregador de tarefas, ou seja, permite agrupar dentro do mesmo reportório um número ilimitado de tarefas. Dado que o Setup é parametrizável à loja, todas as tarefas existentes dentro de um determinado Setup são configuradas para as lojas parametrizadas, de forma automática.

Um setup é composto por:

* **Nome:** Nomear o Setup é um parâmetro obrigatório. Este nome, apesar de totalmente editável, deve conter informação que identifique a estrutura hierárquica a que está associado bem como o tipo de tarefa correspondente.
* **Tipo de tarefa:** Identifica o tipo de tarefa que foi associado no momento da criação ao Setup. Cada Setup tem exclusivamente um tipo de tarefa.
* **Lojas:** Para cada Setup é possível definir as lojas associadas e apenas estas lojas estão sujeitas às tarefas parametrizadas. Em qualquer altura é possível retirar ou adicionar lojas a determinado Setup.

![image alt <>](assets/instore_setup.png) 

#### Criação de Setup

Para criar um Setup novo, o utilizador deve aceder ao botão “Adicionar” presente na parte superior direita do ecrã.

De seguida é solicitado ao utilizador que defina um Nome para o Setup e que defina o Tipo de tarefa que pretende incluir. Para selecionar as lojas associadas ao setup, o utilizador deve selcionar "Próximo".

Neste último passo surge um facilitador de seleção de loja. Neste facilitador o utilizador tem disponíveis as lojas a que tem acesso no lado direito do ecrã e na parte esquerda um agregador por grupos de loja que permite pesquisar mediante a seleção de cada agrupador. 

![image alt <>](assets/setup_create.gif) 


#### Editar um Setup

Para editar um Setup, o utilizador deve selecionar o Setup pretendido, clicando no nome. Na parte superior do ecrã, deve selecionar opção "Editar Setup".

No ecrã seguinte, o utilizador pode editar os campos "Nome" e "Tipo de Tarefas" do Setup. Depois de editada a informação, o utilizador deve clicar em "Próximo" para aceder à janela de seleção de lojas.

O processo de "Seleção de lojas" é igual ao processo explicado no passo anterior de criação de setup.

![image alt <>](assets/setup_edit.gif) 

#### Eliminar um Setup

Para eliminar um Setup o utilizador deve clicar no símbolo "Opções" presente na parte lateral direita do mesmo. De seguida selecionar "Eliminar".

![image alt <>](assets/setup_delete.gif) 

### **Tarefas**

Depois de criado o Setup, é possível ao utilizador criar tarefas e associar as mesma a setups previamente criados.

#### Tarefa

A Tarefa é uma regra ou conjunto de regras que determinam a associação de artigos através da estrutura mercadológica. Ou seja, ao definir uma regra através da associação a uma determinada estrutura mercadológica, significa que todos os artigos daquela estrutura ficam automáticamente associados a esta Tarefa.

#### Criação de tarefa

Antes de criar um tarefa é necessário escolher o Setup onde vamos incluir a Terfa que queremos criar. Se não existir ainda nenhum Setup criado, é necessário [criar](#criacao-de-setup) para avançar. 

Depois de selecionar o Setup, para criar a Tarefa, o utilizador deve clicar em "Adicionar Tarefa".

De imediato, o utilizador é direcionado para a modal de "Configuração da Tarefa". Esta é a janela de criação da Tarefa e é constituida por três passos distintos: 

* **"Nome da Tarefa”:** Campo onde o utilizador define o nome da tarefa. Este campo é preenchimento obrigatório. 
* **"Tipo de Regra":** Define o tipo de regra que será usado para formatação de tarefa.
* **"Configuração da Regra":** Facilitador para seleção do nivel da estrutura mercadológica.

Depois de definir o nome da Tarefa fica disponível a seleção de Tipo de Regra. Por fim, o utilizador tem disponível o agregador de estrutura hierarquica onde o utilizador poderá navegar pelos diversos niveis e selecionar um ou vários pretendidos.

![image alt <>](assets/task_create.gif)

#### Edição de tarefa

Depois de criada a Tarefa, o utilizador pode, em qualquer altura, editar o nome ou as regras ou mesmo eliminar a Tarefa criada.

Ao clicar nas opções da Tarefa, surge uma janela com a opção de "Editar Nome". Este campo não tem número máximo de caracteres.

No final da edição, o utilizador deve clicar em "Atualizar Nome" para guardar as alterações efetuadas.


![image alt <>](assets/task_edit.gif)

Além da edição do nome, é possivel também ao utilizador adicionar novas regras, eliminar ou editar as existentes. Para isso deve aceder ao detalhe da tarefa e neste ecrã estão disponíveis a opção "Adicionar Regra", "Editar Regra" e "Eliminar".

##### **Adicionar Regra**

![image alt <>](assets/task_edit_add.gif)

##### **Editar regra**

![image alt <>](assets/task_edit_edit.gif)

##### **Eliminar Regra**

![image alt <>](assets/task_edit_delete.gif)

#### Eliminar Tarefa

O utilizador tem a possibilidade de eliminar uma Tarefa criada anteriormente. Ao eliminar uma Tarefa, esta deixa de fazer parte do Setup onde está inserida e deixará de ter preponderância sobre a criação das tarefas.

Para isto, basta clicar no símbolo "Opções" na parte lateral direita da tarefa e selecionar a opção "Eliminar".

![image alt <>](assets/task_delete.gif)

## **Configuração Impressora**

O Backoffice do TMR InStore permite ao utilizador configurar as impressoras existentes em loja. Depois de parametrizadas, as impressoras ficam visíveis e disponíveis nas funcionalidades do TMR que utilizem impressão.

### **Listagem de Impressoras**

Para aceder à listagem de impressoras previamente configuradas, o utilizador deve selecionar Instore e de seguida selecionar "Impressoras".

![image alt <>](assets/instore_printers.gif)

#### Criação Impressora

Para adicionar uma nova impressora, o utilizador deve selecionar a opção "Adicionar" presente na listagem de impressoras.

De imediato é direcionado para o ecrã de criação de impressoras que contém os seguintes campos:

* **Nome:** Permite definir um nome para a impressora. Este nome ficrá visível nas aplicação móvel como identificador da impressora.
* **Descrição:** Permite definir uma descrição detalha para a impressora.
* **IPV4:** Define o IP de comunição com a impressora.
* **Porta:** Define a porta de comunicação com a impressora.
* **Mac Adress:** Define o Mac Adress identificador da impressora. Este campo é único e não é permitido criar mais do que uma impressora com o mesmo Mac Adress.
* **EAN:** Permite definir um EAN para a impressora. Este EAN serve facilitador de pesquisa na aplicação móvel. 
* **Tipo:** Dropdown de seleção do tipo de etiqueta. Os campos disponíveis para seleção são configuráveis por cliente.
* **Protocolo:** Define o protocolo de comunicação com a impressora.
* **Grupo:** Define o grupo associado à impressora.

Depois de preenchida toda a informação deste formulário, o utilizador deve selecionar "Próximo" para avançar para a seleção de lojas. **Nota:** Todos os campos do formulário são de preenchimento obrigatório.

Depois de seleciona a loja (exclusiva), o utilizador deverá selecionar "Guardar" para terminar o processo de criação de impressora.

No caso de a impressora já existir, o TMR apresenta a seguinte mensagem ao utilizador:

"Ocorreu um erro: A impressora com o identificador {mac_address} já existe na loja {id da loja}."

![image alt <>](assets/printer_create.gif)

#### Edição Impressora

O formulário de edição é o mesmo do de criação de impressora. Para aceder a este formulário, o utilizador deverá selecionar a impressora que pretende editar.

![image alt <>](assets/printer_edit.gif)

#### Eliminar Impressora

Para eliminar um impressora presente na listagem, o utilizador deverá aceder às "Opções" e selecionar a opção "Eliminar". 

Poderá também eliminar várias impressoras em simultâneo. Para isso deve selecionar a impressoras pretendidas e selecionar a opção "Eliminar Selecionados". 

![image alt <>](assets/printer_delete.gif)

## **Lista de Artigos**

O Backoffice do TMR InStore permite ao utilizador consultar, criar, editar ou eliminar listagem de artigos que serão utilizadas para criação de tarefas na aplicação móvel.

### **Lista de artigos**

Para aceder à listagem de listas de artigos previamente configuradas, o utilizador deve selecionar Instore e de seguida selecionar "Lista de artigos".

![image alt <>](assets/item_list.gif)

#### Consulta e Edição de Artigos

A funcionalidade de Lista de Artigos permite ao utilizador consultar as listas de artigos previamente criadas. Além disso, permite também consultar os artigos pertencentes a uma determinada lista. 

Para isso basta aceder à lista que pertende consultar selecionando a mesma da listagem de listas existentes. 

No detalhe da Lista de Artigos é possivel pesquisar os artigos existentes e adicionar novos artigos à listagem.

Para isso é necessário introduzir a informação do artigo no componente "Artigos" e selecionar o artigo que retorna da pesquisa efetuada. Depois de adicionados os artigos pretendidos, o utilizador seleciona "Atualizar". 

Da mesma forma é possível eliminar artigos previamente existentes na listagem. Para isso basta selecionar um ou mais artigos e escolher a opção "Eliminar Selecionados".

![image alt <>](assets/item_list_edit.gif)

#### Criação Lista de Artigos

Para criar nova Lista de Artigos, o utilizador deve selecionar "Adicionar". De imediato é direcionado para um formulário onde deve preencher o "Nome" e "Descrição" da Lista de Artigos. 

Ao clicar em "Próximo" é direcionado para o formulário de adição de artigos. 

Neste formulário pode inserir os artigos de duas formas distintas: 

* Inserção manual via introdução de EAN ou SKU.
* Inserção via upload de ficheiro excel. Este ficheiro deve apenas conter os sku na coluna A. Qualquer outro formato de excel não será aceite.  

![image alt <>](assets/item_list_create.gif)

#### Eliminar Lista

Para eliminar uma Lista de Artigos existente, o utilizador deverá clicar em "Opções" da lista e selecionar a opção "Eliminar". 

![image alt <>](assets/item_list_delete.gif)

## **Checklist**

O Backoffice do TMR InStore permite ao utilizador consultar, criar, editar ou eliminar checklist que serão posteriormente utilizadas na aplicação móvel. Uma Checklist é um conjunto de componentes que são constituídos por "Pergunta" e "Resposta". A pergunta é completamente configurável pelo utilizador e a cada pergunta poderá associar uma resposta. Existem duas respostas possíveis: "SIM/NÃO" e "VERIFICADO/NÃO VERIFICADO".

### Lista de Checklist

Para aceder à listagem de Checklists disponíveis, o utilizador deverá selecionar "Instore" e de seguida deve selecionar a funcionalidade "Checklist". De forma imediata é direcionado para a listagem de Checklist disponíveis. 

![image alt <>](assets/checklist.gif)

#### Consulta e Edição Checklist

A funcionalidade de Checklist permite consultar a listagem de Checklist existentes e também de consultar e editar o conteúdo correspondente.

Para isso o utilizador deverá selecionar a checklist pretendida e na parte direita do ecrã surgem os componentes já em utilização. Em cada componente poderá editar a pergunta e a descrição. 

Da parte esquerda do ecrã estão presentes os componentes que poderá utilizar na ediação da checklist. Para isso basta arrastar o componente pretendido para a parte direita do ecrã e preencher a pergunta e descrição do componente. 

Ao selecionar atualizar os componentes adicionados e editados são guardados na checklist em tratamento.

![image alt <>](assets/checklist_edit.gif)

#### Criação Checklist

Para criar nova Checklist, o utilizador deve selecionar "Adicionar". De imediato é direcionado para um formulário onde deve preencher o "Nome" e selecionar os componentes pretendidos arrastando os mesmo da esquerda para a direita do ecrã. 

Ao clicar em "Próximo" é direcionado para o formulário de seleção de lojas. Depois de selecionada a loja pretendida, o utilizador deve selecionar "Criar". **Nota:** Se não for selecionada nenhuma loja, a Checklist ficará disponível para todas as lojas.

![image alt <>](assets/checklist_create.gif)

#### Eliminar Checklist

Para eliminar uma Checklist existente, o utilizador deverá clicar em "Opções" da Checklist e selecionar a opção "Eliminar".

![image alt <>](assets/checklist_delete.gif)

## **Gestão de Validades**

O Backoffice do TMR InStore permite ao utilizador consultar, criar, editar ou eliminar Parâmetros de Validade que influenciam a criação de tarefas de Controlo de Validades. Além dos parâmetros de validade é possível também consultar, editar ou eliminar Motivos e Destinos da Retira/Quebra. Estes motivos e destinos serão utilizados na aplicação móvel durante o tratamento das tarefas de Retirada e Quebra.

### **Parâmetros de Validade**

Para aceder à listagem de parâmetros de validade, o utilizador deverá selecionar "Instore" e de seguida deve selecionar a funcionalidade "Parâmetros de Validade" que se econtra no agregador "Gestão de Validades". De forma imediata é direcionado para a listagem de parâmetros disponíveis.

![image alt <>](assets/validity_parameters.gif)

#### Criar Parâmetro de Validade

Para adicionar um novo parâmetro de validade, o utilizador deve selecionar a opção "Adicionar" presente na listagem.

De imediato é direcionado para o formulário de criação de parâmetro que contém os seguintes campos:

* **Estrutura Hierárquica:** Permite definir a estrutura hierárquica para determinado parâmetro.
* **Retirada (dias):** Permite definir o número de dias a retirar para determinado parâmetro.
* **Depreciação (dias):** Permite definir o número de dias a depreciar para determinado parâmetro.
* **Desconto (%):** Permite definir a percentagem máxima de depreciação para determinado parâmetro.

Depois de preenchido o formulário o utilizador deverá selecionar "Guardar".

![image alt <>](assets/validity_parameters_create.gif)

#### Editar Parâmetro de Validade

A funcionalidade de Gestão de Validades permite editar os parâmetros de validade existentes. Para isso deverá selecionar o parâmetro pretendido e editar os dados. 

![image alt <>](assets/validity_parameters_edit.gif)

#### Eliminar Parâmetro de Validade

A funcionalidade de Gestão de Validades permite eliminar os parâmetros de validade existentes. Para isso deverá clicar em "Opções" e selecionar a opção "Eliminar".

![image alt <>](assets/validity_parameters_delete.gif)

### **Motivos**

Para aceder à listagem de Motivos, o utilizador deverá selecionar "Motivos" que se encontra no agregador "Gestão de Validades". De forma imediata é direcionado para a listagem de motivos disponíveis.

#### Criar Motivos

Para adicionar um novo motivo, o utilizador deve selecionar a opção "Adicionar" presente na listagem.

De imediato é direcionado para o formulário de criação de motivo que contém os seguintes campos:

* **Descrição:** Permite definir a descrição do motivo.
* **Código:** Permite definir o código identificador do motivo.
* **Tipo:** Permite definir o tipo de motivo.
* **Estado:** Permite definir o estado do motivo.

![image alt <>](assets/reason_create.gif)

#### Editar Motivo

A funcionalidade de Gestão de Validades permite editar os motivos existentes. Para isso deverá selecionar o motivo pretendido e editar os dados. 

![image alt <>](assets/reason_edit.gif)

#### Eliminar Motivo

A funcionalidade de Gestão de Validades permite eliminar os motivos existentes. Para isso deverá clicar em "Opções" e selecionar a opção "Eliminar".

![image alt <>](assets/reason_delete.gif)

### **Destinos**

Para aceder à listagem de Destinos, o utilizador deverá selecionar "Destinos" que se encontra no agregador "Gestão de Validades". De forma imediata é direcionado para a listagem de destinos disponíveis.

#### Criar Destinos

Para adicionar um novo destino, o utilizador deve selecionar a opção "Adicionar" presente na listagem.

De imediato é direcionado para o formulário de criação de destino que contém os seguintes campos:

* **Descrição:** Permite definir a descrição do destino.
* **Código:** Permite definir o código identificador do destino.
* **Loja:** Permite definir as lojas associadas ao destino.
* **Estado:** Permite definir o estado do destino.

![image alt <>](assets/destinations_create.gif)

#### Editar Destino

A funcionalidade de Gestão de Validades permite editar os destinos existentes. Para isso deverá selecionar o destino pretendido e editar os dados. 

![image alt <>](assets/destinations_edit.gif)

#### Eliminar Destino

A funcionalidade de Gestão de Validades permite eliminar os destinos existentes. Para isso deverá clicar em "Opções" e selecionar a opção "Eliminar".

![image alt <>](assets/destinations_delete.gif)
















