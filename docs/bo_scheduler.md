# BACKOFFICE - CALENDÁRIO

![image alt <>](assets/scheduler.png)

## Histórico do documento

| Data      | Descrição    | Autor | Versão | Revisão
| ------------- | ---------------------------------------------------------------------------------------- | ---------- | ---------- | -------- | 
| 01/12/2020         | Criação do documento.    |  Filipe Silva  |   1.0   |     |
| 12/12/2020     | Criação de Introdução de formatação de documento.       | Filipe Silva  |  1.1     |      |
| 22/12/2020     | Versão final do documento.       | Filipe Silva  |  2.0    |      |
| 22/01/2021     | Revisão final do documento.       | Filipe Silva  |  2.1    |  Helena Gonçalves|

## **CALENDÁRIO - Visão tarefas**

Para aceder à funcionalidade de Calendário, o utilizador deve aceder ao "Calendário" e selecionar "Calendário".

![image alt <>](assets/scheduler_scheduler.gif)

O módulo Calendário do BackOffice TMR permite criar novos agendamentos e consultar agendamentos de expiração e criação de tarefas previamente criados.

### **Visão Mês**

O módulo Calendário apresenta os agendamentos na visão Mês por default. Quando o utilizador acede a esta funcionalidade os agendamentos são apresentados numa visão mensal onde através de legendas consegue perceber todos os agendamentos criados para o mês atual.

As legendas correspondem a pontos coloridos situados na parte inferior de cada dia específico. A legenda do jogo de cores está sempre presente na parte esquerda do calendário.

Além das legendas, o Calendário apresenta o dia atual com um sublinhado de cor diferente para que o utilizador identifique de forma mais ágil os agendamento realizados para o dia presente. 

![image alt <>](assets/scheduler_monthly_view.png)

### **Visão Semana**

O módulo Calendário permite também apresentar os agendamentos na visão Semana. Nesta visão é apresentada a atual semana e todos os agendamentos nela realizados. Nesta visão é possível visualizar os agendamentos com mais detalhe dado que a visão semana apresenta a escala de horas em cada dia da semana. Desta forma os agendamentos apresentam-se agupados pela hora de início. 

O jogo de cores mantém-se ativo como legenda mas na visão semana já é apresentado de imediato o nome do agendamento além da cor correspondente ao tipo e tarefa.

![image alt <>](assets/scheduler_week_view.png)

### **Visão Dia**

O módulo Calendário permite também apresentar os agendamentos na visão Dia. Nesta visão é possível consultar com mais detalhe os agendamentos. A escala de horas e a legenda de cores mantém o mesmo comportamento que apresentados nas visões anteriores.

![image alt <>](assets/scheduler_daily_view.png)

### **Seleção Lojas**

O módulo Calendário do TMR permite apresentar os agendamentos por loja ou por grupo de lojas previamente definido. Por default todas as lojas aparecem selecionadas. Para restringir o grupo de lojas para visualização, o utilizador deverá aceder ao facilitador de seleção presente no canto superior direto do ecrã e de seguida selecionar a(s) loja(s) pretendida(s).

![image alt <>](assets/scheduler_store_select.gif)

## **CALENDÁRIO - Agendamento Expiração**

### **Criação do agendamento**

O módulo Calendário do TMR permite calendarizar eventos de expiração de tarefas. Para criar um agendamento de expiração, o utilizador deve selecionar o input (+) presente no calendário. De forma imediata é disponibilizado um formulário com os seguintes campos obrigatórios para preenchimento:

* **Título:** Permite definir o nome para o agendamento.
* **Data e Hora:** Permite definir a data e hora para início do agendamento. Também é possível definir uma recorrência selecionando "Alterar para Freq.". No agendamento por frequência é possível definir a seguinte recorrência:
    * **Horas:** Define o minuto a cada hora para início da expiração.
    * **Dias:** Define a hora a cada dia para início da expiração. 
    * **Semana:** Define o dia a cada semana para início da expiração.
    * **Mês:** Define o dia a cada mês para início da expiração. 
* **Intervalo em dias:** Permite definir o número de dias passados para seleção de tarefas para expirar. Por exemplo, se o utilizador selecionar "Início: -5", significa que todas as tarefas criadas com 5 dias no passado serão expirados por este agendamento. 
* **Lojas:** Permite difinir as lojas que serão abrangidas pelo agendamento. 
* **Tipo de tarefa:** Permite definir o tipo de tarefas a expirar.

Após o preenchimento de todos os campos, o botão "CRIAR" ficará disponível e ao selecionar esta opção, o agendamento será criado e ficará disponível no calendário para consulta. 

![image alt <>](assets/scheduler_create_expiration.gif)

### **Edição do agendamento**

O módulo Calendário do TMR permite editar agendamentos previamente criados. Para isso o utilizador deve selecionar o agendamento que pretende editar clicando no nome do agendamento. De forma imediata é aberto o formulário semelhante ao de criação já com os campos previamente preenchidos. 

O utilizador deverá alterar a informação dos campos que pretende editar e no final selecionar "Atualizar" para guardar a informação atualizada. 

![image alt <>](assets/scheduler_edit_expiration.gif)

### **Eliminação do agendamento**

O módulo Calendário do TMR permite eliminar agendamentos previamente criados. Para isso o utilizador deve pesquisar o agendamento que pretende eliminar e clicar na (X) na parte esquerda do nome do agendamento. De forma imediata surge o seguinte alerta:

**"Tem a certeza que pretende eliminar?"**

Se o utilizador selecionar "SIM", o agendamento é eliminado imediatamente. Depois de eliminado não é possível reverter esta ação. 

![image alt <>](assets/scheduler_delete_expiration.gif) 

## **CALENDÁRIO - Agendamento Tarefas**

### **Criação do agendamento**

O módulo Calendário do TMR permite calendarizar eventos de criação de tarefas. Para criar um agendamento de criação de tarefas, o utilizador deve selecionar o input (+) presente no calendário. De forma imediata é disponibilizado um formulário com os seguintes campos obrigatórios para preenchimento:

* **Título:** Permite definir o nome para o agendamento.
* **Tipo de tarefa:** Permite definir o tipo de tarefa para agendamento. Esta seleção está disponível no formato de tab presente logo após o input do nome do agendamento. As tarefas disponíveis são configuráveis por cliente. 
* **Data e Hora:** Permite definir a data e hora para início do agendamento. Também é possível definir uma recorrência selecionando "Alterar para Freq.". No agendamento por frequência é possível definir a seguinte recorrência:
    * **Horas:** Define o minuto a cada hora para criação de tarefa.
    * **Dias:** Define a hora a cada dia para criação de tarefa. 
    * **Semana:** Define o dia a cada semana para criação de tarefa.
    * **Mês:** Define o dia a cada mês para criação de tarefa. 

* **Lojas:** Permite difinir as lojas que serão abrangidas pelo agendamento. 
* **Estrutura Hiérarquica:** Permite definir o nível da estrutura hiérarquica que corresponde aos artigos a serem incluídos na tarefa. 
* **Tem itens:** Checkbox que define se a tarefa será criada com artigos ou vazia. 
* **Lista de artigos:** Permite definir uma lista de artigos que serão incluidos na tarefa no momento da sua criação. 
* **Zonas:** Permite definir o número de zonas que serão incluidas no momento da criação da tarefa. Desta forma, será criada uma tarefa agregadora com tarefas/zonas no seu contéudo. 
* **Localização:** Dropdown que permite definir a localização da tarefa. 

Após o preenchimento de todos os campos, o botão "CRIAR" ficará disponível e ao selecionar esta opção, o agendamento será criado e ficará disponível no calendário para consulta. 

![image alt <>](assets/scheduler_create_task.gif)

### **Edição do agendamento**

O módulo Calendário do TMR permite editar agendamentos previamente criados. Para isso o utilizador deve selecionar o agendamento que pretende editar clicando no nome do agendamento. De forma imediata é aberto o formulário semelhante ao de criação já com os campos previamente preenchidos. 

O utilizador deverá alterar a informação dos campos que pretende editar e no final selecionar "Atualizar" para guardar a informação atualizada. 

![image alt <>](assets/scheduler_edit_expiration.gif)

### **Eliminação do agendamento**

O módulo Calendário do TMR permite eliminar agendamentos previamente criados. Para isso o utilizador deve pesquisar o agendamento que pretende eliminar e clicar na (X) na parte esquerda do nome do agendamento. De forma imediata surge o seguinte alerta:

**"Tem a certeza que pretende eliminar?"**

Se o utilizador selecionar "SIM", o agendamento é eliminado imediatamente. Depois de eliminado não é possível reverter esta ação. 

![image alt <>](assets/scheduler_delete_expiration.gif) 

## **Aplicar Alterações**

Após realizar alguma alteração nos agendamentos (criação, edição e eliminação) é sempre necessário aplicar as alteração efetuadas. Se o utilizador não aplicar alterações, todas as ações realizadas anteriormente não terão efeitos práticos no agendamento. 

Para aplicar alterações o utilizador deve selecionar a label presente no canto superior direiro do ecrã com o nome "Aplicar Alterações". Nesta label é apresentado o total de alterações realizadas e que necessitam de ser aplicadas. Ao selecionar "Aplicar Alterações" surge a seguinte mensagem:

**"Tem a certeza que pretende aplicar as alterações?"**

Se o utilizador selecionar "SIM", as alterações são aplicadas imediatamente e surge a seguinte alerta de confirmação:

"Operação realizada com sucesso."

![image alt <>](assets/scheduler_apply_changes.gif) 




