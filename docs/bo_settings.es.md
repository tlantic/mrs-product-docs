# Configuración de backoffice

Esta sección está dedicada a la lista de configuraciones que influyen en el comportamiento de FE Backoffice.


*[FE]: Front End

## INSTORE SETTINGS

### **instore-app-settings**

Tipo: ``object`` | Configuración a nivel de **Organización**.

Definición responsable de la configuración principal de la aplicación.


???+ example "Valores posibles::"
    * "language": ``string`` - Identifica el idioma utilizado por defecto en la aplicación.
    * "defaultStore": ``string`` - Define la tienda predeterminada utilizada en la aplicación.
        * "displayStructureFilter": ``boolean`` - Le permite mostrar u ocultar el filtro de configuración DOP (árbol) en el componente de tiendas.
    * "itemList": ``object`` - Configuración de la función Lista de elementos.
        * "types": ``array`` -  Define los tipos de lista disponibles
    * "notification": ``object`` - Configuración de la función de notificaciones.
        * "refreshInterval": ``number`` - Define el intervalo de actualización para notificaciones (ms).
    * "printer": ``object`` - Configuración de la función Impresoras
        * "types": ``array`` - define los tipos de impresoras disponibles para su selección. 
    * "support": ``object`` - Configuración de la función de soporte.
        * "actions": ``array`` - Define las acciones disponibles para tickets (listado).
        * "types": ``array`` - Define los códigos para los tipos de tareas que se utilizan en las traducciones.

    **Ejemplo**
    ```json
      {
        "language": "pt-PT",
        "defaultStore": "1",
        "features": {
            "common": {
                "selectionList": { 
                    "displayStructureFilter": true
                }
            },
        "itemList": {
        "types": [
            {
            "code": "default",
            "en-EN": "Default",
            "pt-BR": "Normal",
            "pt-PT": "Normal"
            },
            {
            "code": "replenish",
            "en-EN": "Advanced Replenish",
            "pt-BR": "Reposição Avançada",
            "pt-PT": "Reposição Avançada"
            }
        ]
        },
        "notification":  {
            "refreshInterval": 30000
        },
        "printer": { 
            "types": 
            [ 
                {
                "code": "zebra",
                "en-EN": "Zebra",
                "pt-BR": "Zebra",
                "pt-PT": "Zebra"
                }
            ]
        },
        "support": {
            "issues": [ 
                {
                    "actions": [
                        {
                        "code": "cancel",
                        "en-EN": "Cancel",
                        "icon": "cancel_presentation",
                        "pt-BR": "Cancelar",
                        "pt-PT": "Cancelar"
                        },
                        {
                        "code": "process",
                        "en-EN": "Resend",
                        "icon": "keyboard_tab",
                        "pt-BR": "Reenviar",
                        "pt-PT": "Reenviar"
                        },
                        {
                        "code": "release",
                        "en-EN": "Release",
                        "icon": "reply_all",
                        "pt-BR": "Libertar",
                        "pt-PT": "Libertar"
                        }
                ],
            "types": [
                "receiving",
                "stockcount",
                "relabelling",
                "priceaudit",
                "stockoutaudit",
                "replenishprep",
                "replenish",
                "transfer",
                "stockreturn",
                "scanaudit",
                "presencecheck",
                "priceup",
                "pricedown",
                "expirationcontrol",
                "priorityrelabelling",
                "labelprint",
                "promotionout",
                "promotionin",
                "expirationpicking",
                "carddiscount",
                "itemlist"
                ]
            }
        ]
        }
    }
    }
    ```
![image alt <>](assets/instore-app-settings.png){:height="1000px" width="1000px"}

### **instore-app-task-types**

Tipo: ``object`` | Configuración a nivel de **Organización**

Permite definir los tipos de tareas existentes en la aplicación. Incluye traducciones para diferentes idiomas.


???+ example "Valores posibles::"
    * "taskTipos": ``array`` - Identifica los tipos de tareas disponibles en la aplicación
        * "code": ``string`` - define el código para el tipo de tarea.
        * "name": ``object`` - Define traducciones de tipos de tareas para diferentes idiomas (en-EN, pt-BR, pt-PT).
    
    **Ejemplo**
    ```json
      {
        "taskTipos": [
        {
            "code": "receiving",
            "name": {
                "en-EN": "Receiving",
                "pt-BR": "Recebimento",
                "pt-PT": "Recepção"
            }
        },
        {
            "code": "stockcount",
            "name": {
                "en-EN": "Stock Count",
                "pt-BR": "Contagem de Estoque",
                "pt-PT": "Contagem de Stock"
            }
        },
        {
            "code": "relabelling",
            "name": {
                "en-EN": "Relabelling",
                "pt-BR": "Remarcação",
                "pt-PT": "Remarcação de Preço"
            }
        },
        {
            "code": "priceaudit",
            "name": {
                "en-EN": "Price Audit",
                "pt-BR": "Auditoria de Preço",
                "pt-PT": "Auditoria de Preço"
            }
        },
        {
            "code": "stockoutaudit",
            "name": {
                "en-EN": "Stock Out Audit",
                "pt-BR": "Auditoria de Rutura",
                "pt-PT": "Auditoria de Ruptura"
            }
        },
        {
            "code": "replenishprep",
            "name": {
                "en-EN": "Replenish Prep IGL",
                "pt-BR": "Separação IGL",
                "pt-PT": "Separação IGL"
            }
        },
        {
            "code": "standardreplenishprep",
            "name": {
                "en-EN": "Replenish Prep",
                "pt-BR": "Separação",
                "pt-PT": "Separação"
            }
        },
        {
            "code": "replenish",
            "name": {
                "en-EN": "Replenish",
                "pt-BR": "Reposição",
                "pt-PT": "Reposição"
            }
        },
        {
            "code": "transfer",
            "name": {
                "en-EN": "Transfers",
                "pt-BR": "Transferências",
                "pt-PT": "Transferências"
            }
        },
        {
            "code": "stockreturn",
            "name": {
                "en-EN": "Stock Return",
                "pt-BR": "Devoluções",
                "pt-PT": "Devoluções"
            }
        },
        {
            "code": "scanaudit",
            "name": {
                "en-EN": "Scan Audit",
                "pt-BR": "Auditoria de Sortimento",
                "pt-PT": "Auditoria de Varrimento"
            }
        },
        {
            "code": "externalaudit",
            "name": {
                "en-EN": "External Audit",
                "pt-BR": "Auditoria Externa",
                "pt-PT": "Auditoria Externa"
            }
        },
        {
            "code": "presencecheck",
            "name": {
                "en-EN": "Presence Check",
                "pt-BR": "Verificação de Presença",
                "pt-PT": "Verificação de Presença"
            }
        },
        {
            "code": "priceup",
            "name": {
                "en-EN": "Price Up",
                "pt-BR": "Subidas de Preço",
                "pt-PT": "Subidas de Preço"
            }
        },
        {
            "code": "pricedown",
            "name": {
                "en-EN": "Price Down",
                "pt-BR": "Descidas de Preço",
                "pt-PT": "Descidas de Preço"
            }
        },
        {
            "code": "expirationcontrol",
            "name": {
                "en-EN": "Expiration Control",
                "pt-BR": "Controlo de Validades",
                "pt-PT": "Controlo de Validades"
            }
        },
        {
            "code": "priorityrelabelling",
            "name": {
                "en-EN": "Priority Relabelling",
                "pt-BR": "Emergencial",
                "pt-PT": "Remarcação Prioritária"
            }
        },
        {
            "code": "labelprint",
            "name": {
                "en-EN": "Label Print",
                "pt-BR": "Impressão de Etiquetas",
                "pt-PT": "Impressão de Etiquetas"
            }
        },
        {
            "code": "promotionout",
            "name": {
                "en-EN": "Promotion Out",
                "pt-BR": "Saída de Promoção",
                "pt-PT": "Saída de Promoção"
            }
        },
        {
            "code": "promotionin",
            "name": {
                "en-EN": "Promotion In",
                "pt-BR": "Entrada em Promoção",
                "pt-PT": "Entrada em Promoção"
            }
        },
        {
            "code": "expirationpicking",
            "name": {
                "en-EN": "Expiration Picking",
                "pt-BR": "Recolha de Validades",
                "pt-PT": "Recolha de Validades"
            }
        },
        {
            "code": "carddiscount",
            "name": {
                "en-EN": "Card Discount",
                "pt-BR": "Desconto em Cartão",
                "pt-PT": "Desconto em Cartão"
            }
        },
        {
            "code": "itemlist",
            "name": {
                "en-EN": "Item List",
                "pt-BR": "Lista de Artigos",
                "pt-PT": "Lista de Artigos"
            }
        }
    ]
    }
    ```
![image alt <>](assets/instore-app-task-types.png){:height="800px" width="800px"}

## AJUSTES DE USUARIO

### **users-app-settings**

Tipo: ``object``| Configuración a nivel de **Organización**

Definición responsable de la configuración principal de la aplicación.


???+ example "Valores posibles::"
    * "language": ``string`` - Identifica el idioma utilizado por defecto en la aplicación.

    **Ejemplo**
    ```json
    {
      "language": "pt-PT"
    }
    ```
![image alt <>](assets/users-app-settings.png){:height="800px" width="800px"}

## AJUSTES DEL ACTUALIZADOR

### **updater-app-settings**

Tipo: ``object`` | Configuración a nivel de **Organización**

Definición responsable de la configuración principal de la aplicación.



???+ example "Valores posibles::"
    * "language": ``string`` -  Identifica el idioma utilizado por defecto en la aplicación.
    * "entityGroups": ``object`` -  Define el grupo de tiendas
        * "displayOrganization": ``boolean`` - Definición que le permite ocultar o mostrar la propiedad de la organización (por ejemplo, tlantic).
    * "environments": ``array`` - Identifica los entornos disponibles.
        * "code": ``string`` - Identifica el código del entorno.
        * "name": ``string`` - Identifica el nombre del entorno.
    * "platforms": ``array`` - Definición que permite configurar las plataformas.
        * "code": ``string`` -  Identifica el código de la plataforma (según la base de datos).
        * "icon": ``string`` - Identifica la imagen del icono.
        * "id": ``string`` - identificador (según la base de datos).
    
    **Ejemplo**
    ```json
    {
        "language": "pt-PT",
        "entityGroups": {
            "displayOrganization": true  
        },
        "environments": [
            {
                "code": "DEVELOPMENT",
                "name": "Desenvolvimento"
            },
            {
                "code": "QUALITY",
                "name": "Qualidade"
            },
            {
                "code": "RELEASE",
                "name": "Lançamento (release)"
            },
            {
                "code": "PILOT",
                "name": "Piloto"
            }
        ],
        "platforms": [
            {
                "code": "WINCE",
                "icon": "assets/images/devices/icons8-windows-xp-240.png",
                "id": "1"
            },
            {
                "code": "ANDROID",
                "icon": "assets/images/devices/icons8-android-os-96.png",
                "id": "2"
            },
            {
                "code": "IOS",
                "icon": "assets/images/devices/icons8-apple-logo-90.png",
                "id": "3"
            }
        ]
    }
    ```
![image alt <>](assets/updater-app-settings.png){:height="800px" width="800px"}

## AJUSTES DEL PROGRAMADOR

### **scheduler-app-settings**

Tipo: ``object`` | Configuración a nivel de **Organización**

Definición responsable de la configuración principal de la aplicación.


???+ example "Valores posibles::"
    * "language": ``string`` - Identifica el idioma utilizado por defecto en la aplicación
    * "jobTemplate": ``object`` - Define la plantilla para crear trabajos
        * "destinations": ``array`` - Define los 'destinos' disponibles en la creación de trabajos de 'cuadro combinado' (horarios).
        * ""locations": ``array`` - Define las ubicaciones 'disponibles en la creación de trabajos de' cuadro combinado '(horarios).
        * "zones": ``array`` - Define las 'zonas' disponibles en la creación de trabajos de 'cuadro combinado' (horarios)
    
    **Ejemplo**
    ```json
    {
        "language": "pt-PT",
        "jobTemplate": {
        "destinations": [
            {
                "code": "provider",
                "en-EN": "Provider",
                "pt-BR": "Fornecedor",
                "pt-PT": "Fornecedor"
            },
            {   
                "code": "warehouse",
                "en-EN": "Warehouse",
                "pt-BR": "Armazém",
                "pt-PT": "Armazém"
            }
        ],
        "locations": [
            {
                "code": "store",
                "en-EN": "Store",
                "pt-BR": "Loja",
                "pt-PT": "Loja"
            },
            {
                "code": "warehouse",
                "en-EN": "Warehouse",
                "pt-BR": "Armazém",
                "pt-PT": "Armazém"
            }
        ],
        "zones": [
            {
                "name": "zona 1"
            },
            {
                "name": "zona 2"
            },
            {
                "name": "zona 3"
            }
        ]
    }
    }
    ```
![image alt <>](assets/scheduler-app-settings.png){:height="800px" width="800px"}

## CONFIGURACIÓN DE LAS NOTIFICACIONES

### **notification-app-settings**

Tipo: ``object`` | Configuración a nivel de **Organización**

Definición responsable de la configuración principal de la aplicación.


???+ example "Valores posibles::"
    * "language": ``string`` - Identifica el idioma utilizado por defecto en la aplicación

    **Ejemplo**
    ```json
    {
      "language": "pt-PT"
    }
    ```
![image alt <>](assets/notification-app-settings.png){:height="800px" width="800px"}