# Backoffice Settings

Esta secção é dedicada à listagem de settings que influenciam o comportamento do FE Backoffice.

*[FE]: Front End

## INSTORE SETTINGS

### **instore-app-settings**

Type: ``object`` | Setting ao nível da **Organização**.

Definição responsável pelas principais configurações da aplicação.

???+ example "Valores Possíveis:"
    * "language": ``string`` - Identifica o idioma utilizado por defeito na aplicação.
    * "defaultStore": ``string`` - Define a loja por defeito utilizada na aplicação.
    * "features": ``object``
        * "displayStructureFilter": ``boolean`` - Permite mostrar ou esconder o filtro configuração DOP (árvore) no componente das lojas.
    * "itemList": ``object`` - Definições para a funcionalidade Lista de Artigos.
        * "types": ``array`` - Define os tipos de lista disponíveis.
    * "notification": ``object`` - Definições para a funcionalidade Notificações.
        * "refreshInterval": ``number`` - Define o intervalo de atualização das notificações (ms).
    * "printer": ``object`` - Definições para a funcionalidade Impressoras.
        * "types": ``array`` - Define os tipos de impressoras disponíveis para seleção. 
    * "support": ``object`` - Definições para a funcionalidade Suporte.
        * "actions": ``array`` - Define ações disponíveis para os tickets (listagem).
        * "types": ``array`` - Define os código dos tipos de tarefas utilizados nas traduções.

    **Exemplo**
    ```json
      {
        "language": "pt-PT",
        "defaultStore": "1",
        "features": {
            "common": {
                "selectionList": { 
                    "displayStructureFilter": true
                }
            },
        "itemList": {
        "types": [
            {
            "code": "default",
            "en-EN": "Default",
            "pt-BR": "Normal",
            "pt-PT": "Normal"
            },
            {
            "code": "replenish",
            "en-EN": "Advanced Replenish",
            "pt-BR": "Reposição Avançada",
            "pt-PT": "Reposição Avançada"
            }
        ]
        },
        "notification":  {
            "refreshInterval": 30000
        },
        "printer": { 
            "types": 
            [ 
                {
                "code": "zebra",
                "en-EN": "Zebra",
                "pt-BR": "Zebra",
                "pt-PT": "Zebra"
                }
            ]
        },
        "support": {
            "issues": [ 
                {
                    "actions": [
                        {
                        "code": "cancel",
                        "en-EN": "Cancel",
                        "icon": "cancel_presentation",
                        "pt-BR": "Cancelar",
                        "pt-PT": "Cancelar"
                        },
                        {
                        "code": "process",
                        "en-EN": "Resend",
                        "icon": "keyboard_tab",
                        "pt-BR": "Reenviar",
                        "pt-PT": "Reenviar"
                        },
                        {
                        "code": "release",
                        "en-EN": "Release",
                        "icon": "reply_all",
                        "pt-BR": "Libertar",
                        "pt-PT": "Libertar"
                        }
                ],
            "types": [
                "receiving",
                "stockcount",
                "relabelling",
                "priceaudit",
                "stockoutaudit",
                "replenishprep",
                "replenish",
                "transfer",
                "stockreturn",
                "scanaudit",
                "presencecheck",
                "priceup",
                "pricedown",
                "expirationcontrol",
                "priorityrelabelling",
                "labelprint",
                "promotionout",
                "promotionin",
                "expirationpicking",
                "carddiscount",
                "itemlist"
                ]
            }
        ]
        }
    }
    }
    ```
![image alt <>](assets/instore-app-settings.png){:height="1000px" width="1000px"}

### **instore-app-task-types**

Type: ``object`` | Setting ao nível da **Organização**

Permite definir os tipos de tarefas existentes na aplicação. Inclui traduções para os diferentes idiomas.

???+ example "Valores Possíveis:"
    * "taskTypes": ``array`` - Identifica os tipos de tarefa disponíveis na aplicação. 
        * "code": ``string`` - Define o código do tipo de tarefa.
        * "name": ``object`` - Define as traduções dos tipos de tarefa para os diferentes idiomas (en-EN, pt-BR, pt-PT).
    
    **Exemplo**
    ```json
      {
        "taskTypes": [
        {
            "code": "receiving",
            "name": {
                "en-EN": "Receiving",
                "pt-BR": "Recebimento",
                "pt-PT": "Recepção"
            }
        },
        {
            "code": "stockcount",
            "name": {
                "en-EN": "Stock Count",
                "pt-BR": "Contagem de Estoque",
                "pt-PT": "Contagem de Stock"
            }
        },
        {
            "code": "relabelling",
            "name": {
                "en-EN": "Relabelling",
                "pt-BR": "Remarcação",
                "pt-PT": "Remarcação de Preço"
            }
        },
        {
            "code": "priceaudit",
            "name": {
                "en-EN": "Price Audit",
                "pt-BR": "Auditoria de Preço",
                "pt-PT": "Auditoria de Preço"
            }
        },
        {
            "code": "stockoutaudit",
            "name": {
                "en-EN": "Stock Out Audit",
                "pt-BR": "Auditoria de Rutura",
                "pt-PT": "Auditoria de Ruptura"
            }
        },
        {
            "code": "replenishprep",
            "name": {
                "en-EN": "Replenish Prep IGL",
                "pt-BR": "Separação IGL",
                "pt-PT": "Separação IGL"
            }
        },
        {
            "code": "standardreplenishprep",
            "name": {
                "en-EN": "Replenish Prep",
                "pt-BR": "Separação",
                "pt-PT": "Separação"
            }
        },
        {
            "code": "replenish",
            "name": {
                "en-EN": "Replenish",
                "pt-BR": "Reposição",
                "pt-PT": "Reposição"
            }
        },
        {
            "code": "transfer",
            "name": {
                "en-EN": "Transfers",
                "pt-BR": "Transferências",
                "pt-PT": "Transferências"
            }
        },
        {
            "code": "stockreturn",
            "name": {
                "en-EN": "Stock Return",
                "pt-BR": "Devoluções",
                "pt-PT": "Devoluções"
            }
        },
        {
            "code": "scanaudit",
            "name": {
                "en-EN": "Scan Audit",
                "pt-BR": "Auditoria de Sortimento",
                "pt-PT": "Auditoria de Varrimento"
            }
        },
        {
            "code": "externalaudit",
            "name": {
                "en-EN": "External Audit",
                "pt-BR": "Auditoria Externa",
                "pt-PT": "Auditoria Externa"
            }
        },
        {
            "code": "presencecheck",
            "name": {
                "en-EN": "Presence Check",
                "pt-BR": "Verificação de Presença",
                "pt-PT": "Verificação de Presença"
            }
        },
        {
            "code": "priceup",
            "name": {
                "en-EN": "Price Up",
                "pt-BR": "Subidas de Preço",
                "pt-PT": "Subidas de Preço"
            }
        },
        {
            "code": "pricedown",
            "name": {
                "en-EN": "Price Down",
                "pt-BR": "Descidas de Preço",
                "pt-PT": "Descidas de Preço"
            }
        },
        {
            "code": "expirationcontrol",
            "name": {
                "en-EN": "Expiration Control",
                "pt-BR": "Controlo de Validades",
                "pt-PT": "Controlo de Validades"
            }
        },
        {
            "code": "priorityrelabelling",
            "name": {
                "en-EN": "Priority Relabelling",
                "pt-BR": "Emergencial",
                "pt-PT": "Remarcação Prioritária"
            }
        },
        {
            "code": "labelprint",
            "name": {
                "en-EN": "Label Print",
                "pt-BR": "Impressão de Etiquetas",
                "pt-PT": "Impressão de Etiquetas"
            }
        },
        {
            "code": "promotionout",
            "name": {
                "en-EN": "Promotion Out",
                "pt-BR": "Saída de Promoção",
                "pt-PT": "Saída de Promoção"
            }
        },
        {
            "code": "promotionin",
            "name": {
                "en-EN": "Promotion In",
                "pt-BR": "Entrada em Promoção",
                "pt-PT": "Entrada em Promoção"
            }
        },
        {
            "code": "expirationpicking",
            "name": {
                "en-EN": "Expiration Picking",
                "pt-BR": "Recolha de Validades",
                "pt-PT": "Recolha de Validades"
            }
        },
        {
            "code": "carddiscount",
            "name": {
                "en-EN": "Card Discount",
                "pt-BR": "Desconto em Cartão",
                "pt-PT": "Desconto em Cartão"
            }
        },
        {
            "code": "itemlist",
            "name": {
                "en-EN": "Item List",
                "pt-BR": "Lista de Artigos",
                "pt-PT": "Lista de Artigos"
            }
        }
    ]
    }
    ```
![image alt <>](assets/instore-app-task-types.png){:height="800px" width="800px"}

## USER SETTINGS

### **users-app-settings**

Type: ``object``| Setting ao nível da **Organização**

Definição responsável pelas principais configurações da aplicação.

???+ example "Valores Possíveis:"
    * "language": ``string`` - Identifica o idioma utilizado por defeito na aplicação. 

    **Exemplo**
    ```json
    {
      "language": "pt-PT"
    }
    ```
![image alt <>](assets/users-app-settings.png){:height="800px" width="800px"}

## UPDATER SETTINGS

### **updater-app-settings**

Type: ``object`` | Setting ao nível da **Organização**

Definição responsável pelas principais configurações da aplicação.

???+ example "Valores Possíveis:"
    * "language": ``string`` - Identifica o idioma utilizado por defeito na aplicação. 
    * "entityGroups": ``object`` - Define o grupo de lojas.
        * "displayOrganization": ``boolean`` - Definição que permite esconder ou mostrar o propriedade organização (ex. tlantic).
    * "environments": ``array`` - Identifica ambientes disponíveis.
        * "code": ``string`` - Identifica o código do ambiente.
        * "name": ``string`` - Identifica o nome do ambiente.
    * "platforms": ``array`` - Definição que permite configurar as plataformas.
        * "code": ``string`` - Identifica o código da plataforma (de acordo com base de dados).
        * "icon": ``string`` - Identifica a imagem do ícon.
        * "id": ``string`` - identificador (de acordo com base de dados).
    
    **Exemplo**
    ```json
    {
        "language": "pt-PT",
        "entityGroups": {
            "displayOrganization": true  
        },
        "environments": [
            {
                "code": "DEVELOPMENT",
                "name": "Desenvolvimento"
            },
            {
                "code": "QUALITY",
                "name": "Qualidade"
            },
            {
                "code": "RELEASE",
                "name": "Lançamento (release)"
            },
            {
                "code": "PILOT",
                "name": "Piloto"
            }
        ],
        "platforms": [
            {
                "code": "WINCE",
                "icon": "assets/images/devices/icons8-windows-xp-240.png",
                "id": "1"
            },
            {
                "code": "ANDROID",
                "icon": "assets/images/devices/icons8-android-os-96.png",
                "id": "2"
            },
            {
                "code": "IOS",
                "icon": "assets/images/devices/icons8-apple-logo-90.png",
                "id": "3"
            }
        ]
    }
    ```
![image alt <>](assets/updater-app-settings.png){:height="800px" width="800px"}

## SCHEDULER SETTINGS

### **scheduler-app-settings**

Type: ``object`` | Setting ao nível da **Organização**

Definição responsável pelas principais configurações da aplicação.

???+ example "Valores Possíveis:"
    * "language": ``string`` - Identifica o idioma utilizado por defeito na aplicação. 
    * "jobTemplate": ``object`` - Define o template criação jobs.
        * "destinations": ``array`` - Define os 'destinos' disponíveis na 'combo box' criação job (agendamentos).
        * ""locations": ``array`` - Define as localizações' disponíveis na 'combo box' criação job (agendamentos).
        * "zones": ``array`` - Define as 'zonas' disponíveis na 'combo box' criação job (agendamentos).
    
    **Exemplo**
    ```json
    {
        "language": "pt-PT",
        "jobTemplate": {
        "destinations": [
            {
                "code": "provider",
                "en-EN": "Provider",
                "pt-BR": "Fornecedor",
                "pt-PT": "Fornecedor"
            },
            {   
                "code": "warehouse",
                "en-EN": "Warehouse",
                "pt-BR": "Armazém",
                "pt-PT": "Armazém"
            }
        ],
        "locations": [
            {
                "code": "store",
                "en-EN": "Store",
                "pt-BR": "Loja",
                "pt-PT": "Loja"
            },
            {
                "code": "warehouse",
                "en-EN": "Warehouse",
                "pt-BR": "Armazém",
                "pt-PT": "Armazém"
            }
        ],
        "zones": [
            {
                "name": "zona 1"
            },
            {
                "name": "zona 2"
            },
            {
                "name": "zona 3"
            }
        ]
    }
    }
    ```
![image alt <>](assets/scheduler-app-settings.png){:height="800px" width="800px"}

## NOTIFICATION SETTINGS 

### **notification-app-settings**

Type: ``object`` | Setting ao nível da **Organização**

Definição responsável pelas principais configurações da aplicação.

???+ example "Valores Possíveis:"
    * "language": ``string`` - Identifica o idioma utilizado por defeito na aplicação. 

    **Exemplo**
    ```json
    {
      "language": "pt-PT"
    }
    ```
![image alt <>](assets/notification-app-settings.png){:height="800px" width="800px"}