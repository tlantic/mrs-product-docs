# BACKOFFICE - UTILIZADORES

![image alt <>](assets/bo_users.png)

## Histórico do documento

| Data      | Descrição    | Autor | Versão | Revisão
| ------------- | ---------------------------------------------------------------------------------------- | ---------- | ---------- | -------- | 
| 01/12/2020         | Criação do documento.    |  Filipe Silva  |   1.0   |     |
| 12/12/2020     | Criação de Introdução de formatação de documento.       | Filipe Silva  |  1.1     |      |
| 22/12/2020     | Versão final do documento.       | Filipe Silva  |  2.0    |      |
| 22/01/2021     | Revisão final do documento.       | Filipe Silva  |  2.1    |  Helena Gonçalves|

## **Gestão de Utilizadores**

Para aceder à funcionalidade de "Gestão de Utilizadores", o utilizador deve aceder a "Utilizadores" e selecionar "Gestão de Utilizadores".

![image alt <>](assets/users_users.gif)

O módulo Gestão de Utilizadores do BackOffice TMR permite criar criar novos utilizadores, consultar, editar e eliminar utilizadores existentes.

### **Consulta e edição de utilizadores**

O módulo de Gestão de utilizadores permite pesquisar utilizadores previamente criados. Para que a pesquisa de utilizadores seja mais efetiva, o TMR permite pesquisar utilizador por nome e permite também pesquisar utilizadores mediante consulta na listagem de utilizadores existentes.

Para pesquisar por nome, o utilizador deve clicar na lupa existente na parte superior do ecrã e colocar o nome do utilizador na caixa de texto. Os resultados serão apresentados em listagem na parte inferior da barra da pesquisa. 

Para visualizar o detalhe dos resultados da pesquisa, o utilizador deve selecionar a linha correspondente e de forma imediata é aberta a modal com a informação do utilizador.

Os campos presentes nesta modal são editáveis e a informação do utilizador alterada é atualizada. 


![image alt <>](assets/users_search.gif)

Se o utilizador pretender consultar todos os utilizadores criados, o utilizador deve clicar no botão "Listar Utilizadores". Desta forma todos os utilizadores criados serão apresentados na listagem. Desta forma também é possível consultar/editar a informação do utilizador selecionando o utilizador da listagem apresentada. 

![image alt <>](assets/users_search_list.gif)

### **Criação de utilizadores**

O módulo de Gestão de utilizadores permite criar novos utilizadores para serem utilizados no acesso à aplicação móvel e Backoffice. Para isso o utlizador deverá selecionar "Adicionar" e surge um formulário com os seguintes campos para preenchimento:
* **Nome:** Permite definir o nome do utilizador. Esta informação é apresentada como identificação do utilizador na aplicação móvel.
* **Utilizador:** Permite definir o identificador do utilizador utilizado no login do TMR.
* **Palavra Passe:** Permite definir a palavra passe do utilizador.
* **Endereço de Email:** Permite definir o email associado ao utilizador.
* **Estado:** Permite definir o estado do utilizador. Os estados disponíveis são "Ativos" e "Inativos". Se o utilizador estiver Inativo não conseguirá realizar o login no TMR.

Após preenchimento dos campos do formulário, o utilizador deverá clicar em "Próximo". Desta forma será encaminhado para o formulário de seleção de lojas. Depois de selecionada a loja deverá selecionar "Criar" para guardar as alterações efetuadas. 

![image alt <>](assets/users_create.gif)

### **Alterar Palavra Passe**

Para alterar a palavra passe de um determinado utilizador, deve selecionar as opções presenta na parte direita da linha do utilizador e selecionar a opção "Atualizar a Palavra-Passe". Nesta modal o utilizador deve inserir a nova palavra passe associada. 

![image alt <>](assets/users_edit_password.gif)

### **Eliminar utilizador**

Para eliminar um determinado utilizador, deve selecionar as opções presenta na parte direita da linha do utilizador e selecionar a opção "Eliminar". De imediato surge a seguinte mensagem:

**"Tem a certeza que pretende eliminar o utilizador?"**

Se selecionar "Sim" o utilizador é eliminado de imediato. 

![image alt <>](assets/users_delete.gif)





