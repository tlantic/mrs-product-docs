# Cockpit 

![imagen alt <>](/assets/capa_cockpit.png)

## Historia del documento

| Fecha      | Descripción                                    | Autor          | Versión | Revisión |
| :--------- | :--------------------------------------------- | :------------- | :------ | :------- |
| 21/12/2020 | Creación de documentos.                        | Soraia Meneses | 1.0     |          |
| 01/12/2020 | Creación de formato de documento Introducción. | Soraia Meneses | 1.1     |          |
| 15/1/2021  | Versión final del documento.                   | Filipe Silva   | 2.0     |          |

## **Introducción**

### TMR - Cabina

Este documento describe las características del sistema Cockpit. El principal objetivo del documento es proporcionar al usuario toda la información necesaria para que pueda utilizar el sistema de forma correcta y eficaz.

El Cockpit es uno de los módulos que componen Tlantic Mobile Retail (TMR) dirigido a las operaciones de la tienda, con un enfoque en la solución de los problemas comunes del retail:

<details class="example" open="open" style="box-sizing: inherit; margin: 1.5625em 0px; padding: 0px 0.6rem; overflow: visible; color: rgba(0, 0, 0, 0.87); font-size: 0.64rem; break-inside: avoid; background-color: var(--md-admonition-bg-color); border-left: 0.2rem solid rgb(101, 31, 255); border-radius: 0.1rem; box-shadow: rgba(0, 0, 0, 0.05) 0px 0.2rem 0.5rem, rgba(0, 0, 0, 0.05) 0px 0.025rem 0.05rem; display: block; border-top-color: rgb(101, 31, 255); border-right-color: rgb(101, 31, 255); border-bottom-color: rgb(101, 31, 255); font-family: Roboto, -apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><summary style="box-sizing: inherit; position: relative; margin: 0px -0.6rem 0px -0.8rem; padding: 0.4rem 1.8rem 0.4rem 2rem; font-weight: 700; background-color: rgba(101, 31, 255, 0.1); border-left: 0.2rem solid rgb(101, 31, 255); display: block; min-height: 1rem; border-top-left-radius: 0.1rem; border-top-right-radius: 0.1rem; cursor: pointer; border-top-color: rgb(101, 31, 255); border-right-color: rgb(101, 31, 255); border-bottom-color: rgb(101, 31, 255); outline: none; -webkit-tap-highlight-color: transparent;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Principales características:</font></font></summary><ul style="box-sizing: inherit; margin: 1em 0px 0.6rem 0.625em; list-style-type: disc; padding: 0px;"><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Inventarios.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Lista de artículos, hoja de artículos y auditoría de precios,</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Récord de pausas.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Impresión de etiquetas.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Auditoría y Control de Validez (Depreciaciones y Retiros).</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Auditoría de Escaneo y Verificación de Presencia (Procesos).</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Auditoría de Ruptura, Separación, Reemplazo.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0px; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Recepciones, transferencias y devoluciones.</font></font></li></ul></details>

### Arquitectura

TMR es un sistema que consta de:

- Aplicaciones de cliente (aplicación WinCE, aplicación de Android y aplicación de iOS): aplicación distribuida por los dispositivos móviles utilizados en la operación de la tienda.
- TMR Server: componente central que contiene lógica y datos y que interactúa con otros sistemas.
- Backoffice - Componente web que permite la definición de datos de referencia y configuración de parámetros.
- Cockpit: componente web para el análisis operativo de las tareas realizadas en TMR Instore.

![imagen alt <>](/assets/Arquitetura.png)

### Requerimientos mínimos

Para un correcto uso del TMR Cockpit, el dispositivo debe cumplir con los siguientes requisitos:

<details class="example" open="open" style="box-sizing: inherit; margin: 1.5625em 0px; padding: 0px 0.6rem; overflow: visible; color: rgba(0, 0, 0, 0.87); font-size: 0.64rem; break-inside: avoid; background-color: var(--md-admonition-bg-color); border-left: 0.2rem solid rgb(101, 31, 255); border-radius: 0.1rem; box-shadow: rgba(0, 0, 0, 0.05) 0px 0.2rem 0.5rem, rgba(0, 0, 0, 0.05) 0px 0.025rem 0.05rem; display: block; border-top-color: rgb(101, 31, 255); border-right-color: rgb(101, 31, 255); border-bottom-color: rgb(101, 31, 255); font-family: Roboto, -apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><summary style="box-sizing: inherit; position: relative; margin: 0px -0.6rem 0px -0.8rem; padding: 0.4rem 1.8rem 0.4rem 2rem; font-weight: 700; background-color: rgba(101, 31, 255, 0.1); border-left: 0.2rem solid rgb(101, 31, 255); display: block; min-height: 1rem; border-top-left-radius: 0.1rem; border-top-right-radius: 0.1rem; cursor: pointer; border-top-color: rgb(101, 31, 255); border-right-color: rgb(101, 31, 255); border-bottom-color: rgb(101, 31, 255); outline: none; -webkit-tap-highlight-color: transparent;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Requerimiento mínimo:</font></font></summary><ul style="box-sizing: inherit; margin: 1em 0px 0.6rem 0.625em; list-style-type: disc; padding: 0px;"><li style="box-sizing: inherit; margin-bottom: 0px; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Navegador Chrome</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>: versión 54 o superior.</font></font></li></ul></details>

## **Acceso**

Para iniciar sesión, se le pide al usuario que ingrese su nombre de **usuario** y **contraseña** . Para ver la contraseña ingresada, haga clic en el símbolo![Fotografía](/assets/eye.png)

Después de ingresar las credenciales, haga clic en **"Iniciar sesión"** .

![imagen alt <>](/assets/login_cockpit.png)

## **Selección de fecha y tienda**

Después del inicio de sesión, se dirige al usuario a la página de inicio de la aplicación. Por el momento están disponibles los componentes de la **tienda de selección de** **fecha** y **selección** .

### **Selección de fecha**

El componente de selección de fecha le permite definir el intervalo de tiempo que desea ver. De esta forma, todas las tareas presentadas corresponden únicamente al intervalo de tiempo seleccionado.

El componente de selección de fecha está presente en la esquina superior derecha de la aplicación y muestra el rango de tiempo seleccionado por defecto. Si el usuario desea cambiar este intervalo de tiempo, debe hacer clic en el componente e inmediatamente se presentará un calendario para la selección manual de fechas y un facilitador con los siguientes campos:

- **Fechas futuras** : Facilitador para seleccionar el intervalo de tiempo entre la fecha actual y la fecha futura más lejana con las tareas a realizar.
- **Fechas pasadas** : Facilitador para seleccionar el intervalo de tiempo entre la fecha actual y la fecha pasada más lejana con las tareas a realizar.
- **Hoy** : Facilitador para seleccionar el día actual.
- **Ayer** : Facilitador para la selección del último día.
- **Últimos 7 días** : Facilitador para seleccionar el intervalo de tiempo entre la fecha actual y los últimos siete días.
- **Últimos 30 días** : Facilitador para seleccionar el intervalo de tiempo entre la fecha actual y los últimos 30 días.
- **Últimos 60 días** : Facilitador para seleccionar el intervalo de tiempo entre la fecha actual y los últimos 60 días.
- **Este año** : Facilitador para seleccionar el intervalo de tiempo entre el 1 de enero del año actual y el día actual.
- **Definir** : Facilitador para la selección manual del intervalo de tiempo.

![imagen alt <>](/assets/cockpit_select_date.gif)

### **Selección de tienda**

El componente de selección de tienda le permite definir la tienda o grupo de tiendas que están destinadas a las tareas de visualización. Solo las tiendas con las que el usuario está asociado estarán disponibles para su selección.

El componente de selección de tienda está presente en el lado derecho del componente de selección de fecha y le permite filtrar tiendas a través de tres niveles diferentes:

- **Insignias** : Facilitador que te permite filtrar las tiendas por la insignia a la que pertenecen. (Primer nivel)
- **Grupos** : Facilitador que te permite filtrar las tiendas por el grupo al que pertenecen. (segundo nivel)
- **Tiendas** : Facilitador que te permite filtrar tiendas individualmente.

En cada uno de los grupos está siempre presente el facilitador de "Seleccionar todo / Ninguno", un totalizador que presenta el número total de insignias, grupos o tiendas seleccionados y un insumo de investigación.

![imagen alt <>](/assets/cockpit_select_stores.gif)

## **Resumen y definiciones**

En la esquina superior derecha de la aplicación, están disponibles los siguientes accesos:

- **Resumen** : Facilitador que le permite ver el porcentaje de ejecución por tipo de tarea.
- **Actualizar** : Botón que le permite actualizar los datos visibles en la aplicación.
- **Actualización automática** : Botón para activar la actualización automática.
- Selección de **idioma** : Entrada para seleccionar el idioma de la aplicación.
- **Configuración** : Botón para acceder a la configuración de la aplicación.

![imagen alt <>](/assets/cockpit_definitions.gif)

## **VISIÓN GLOBAL**

Después del inicio de sesión, el usuario se dirige de forma predeterminada a la página "Resumen" de la ejecución y el estado de las tareas.

La "Vista global" le permite mostrar gráficamente el **porcentaje** de tareas y productos realizados. También te permite ver el **detalle de la ejecución** en el resumen de tareas y, finalmente, te permite ver la **lista de tareas** a ejecutar.

### **Gráficos de ejecución**

En la "Vista global", el Cockpit presenta los gráficos de ejecución de tareas y productos. Están presentes en la parte superior de la pantalla para que sean visibles inmediatamente después de iniciar sesión.

#### Tareas

El gráfico de ejecución de tareas muestra el porcentaje (%) de todas las tareas completadas frente a las tareas a realizar. Esta tasa de éxito tiene en cuenta todas las tareas completadas en la (s) tienda (s) y en la (s) fecha (s) seleccionada (s).

Además de la información gráfica (% de éxito de ejecución), también se presentan los detalles del total de tareas planificadas y el total de tareas completadas para las tiendas y el rango de fechas seleccionado.

#### Productos

De la misma forma que el gráfico de ejecución de tareas, el gráfico de ejecución de productos presenta la tasa de éxito de forma gráfica y los detalles del total de artículos esperados y total de artículos procesados.

![imagen alt <>](/assets/cockpit_execution_grafic.png)

### **Resumen**

La tabla "Resumen" presenta información detallada sobre la ejecución de tareas y productos. Esto permite al usuario consultar los detalles de ejecución de cada tipo de tarea o producto utilizando la vista y detalle seleccionados.

Además del detalle de la ejecución, también es posible que el usuario consulte el detalle de tareas o productos por estado, es decir, consultar el total de tareas "Aún no ejecutadas", "En ejecución", etc. .

Finalmente, también es posible exportar los datos que son visibles para que puedan alimentar los indicadores de BI.

#### Resumen de tareas

La tabla "Resumen" por tareas muestra la información sobre el total de tareas disponibles, el total de tareas completadas y el porcentaje de ejecución respectivo. Esta información se presenta en detalle para cada tipo de tarea disponible y su respectivo estado. Los estados visibles en la tabla resumen se pueden cambiar seleccionando el facilitador en la esquina superior derecha.

![imagen alt <>](/assets/cockpit_resume_task_status.gif)

La vista de resumen por tarea también le permite cambiar la agregación de información seleccionando "Tipos de tarea", "Tienda", "Grupo" e "Insignia".

De esta forma, la información presentada en la tabla resumen cambia con la opción "ver" seleccionada.

![imagen alt <>](/assets/cockpit_resume_task_vision.gif)

El detalle de la información presentada en forma agregada también se puede cambiar seleccionando las siguientes opciones de detalle: "Tienda", "Grupo" o "Insignia".

Para ello, el usuario debe ampliar el tipo de tarea deseada y cambiar la opción de detalle disponible.

![imagen alt <>](/assets/cockpit_resume_task_detail.gif)

#### Descripción del producto

La tabla "Resumen" por productos permite visualizar información sobre el total de productos planificados, el total de productos tratados y el porcentaje de ejecución respectivo. Como en el punto anterior, los estados visibles se pueden cambiar seleccionando el facilitador de visualización.

![imagen alt <>](/assets/cockpit_resume_products_vision.gif)

Al igual que en la vista de tareas, la vista de producto también permite cambiar la agregación de la información presentada y sus respectivos detalles.

![imagen alt <>](/assets/cockpit_resume_products_detail.gif)

#### Extracción de datos

Además de consultar la información por artículo o por tarea descrita en los puntos anteriores, el Cockpit también permite la extracción de estos mismos datos. Para ello, el usuario debe seleccionar primero la vista y el detalle deseados en la información y luego seleccionar el botón "Exportar datos". Se descarga inmediatamente un archivo csv con la información solicitada.

![imagen alt <>](/assets/cockpit_resume_export_data.gif)

### **Tareas inconclusas**

La lista de "Tareas sin terminar" en la Vista global muestra todas las tareas que aún no se han completado en el rango de días y tiendas seleccionados.

Esta tabla permite **buscar** , **ordenar** , **filtrar** y **exportar datos** .

#### Investigar

En la parte superior de la tabla "Tareas sin terminar" hay un facilitador de investigación. Este facilitador le permite buscar las tareas listadas por cualquier información asociada a la tarea como nombre, tienda, tipo e incluso artículos que pertenecen a las tareas.

![imagen alt <>](/assets/cockpit_open_task_search.gif)

#### Ordenando

Como facilitador de navegación, la tabla "Tareas sin terminar" permite ordenar la información por las columnas visibles. Por defecto, la cabina tiene ciertas columnas visibles y otras columnas ocultas. Existe la posibilidad de cambiar las columnas visibles utilizando el componente de selección de columnas en la esquina superior derecha de la tabla.

![imagen alt <>](/assets/cockpit_open_task_sort.gif)

#### Filtrar

Como facilitador de la investigación, la tabla "Tareas sin terminar" le permite filtrar por estado de la tarea. Para hacer esto, simplemente acceda al facilitador de selección de estado en la esquina superior derecha de la tabla.

![imagen alt <>](/assets/cockpit_open_task_filter.gif)

#### Exportación de datos

Para fines de informes o simplemente de consultas, la tabla "Tareas sin terminar" le permite exportar los datos visibles en la tabla. Las columnas y filtros para exportar se definen durante el proceso, permitiendo generar un csv con solo la información relevante para el usuario.

![imagen alt <>](/assets/cockpit_open_task_export_data.gif)

## **Vista agregada**

De manera similar a la descripción general, la vista agregada le permite ver el estado de ejecución de las tareas, el resumen y las tareas que deben completarse, pero solo algunos tipos específicos de tareas.

Esta vista agregada solo se aplica a los clientes que desean tener una vista de la ejecución de varios tipos de tareas diferentes y de forma consolidada. De esta forma, a través de la configuración al cliente es posible tener una vista agregada de varios tipos diferentes de tareas.

![imagen alt <>](/assets/cockpit_overview_price.gif)

## **Vista de ejecución**

Además de la "Vista global" y la "Vista agregada", el Cockpit presenta la "Vista de resultados" al seleccionar un tipo específico de tarea. De esta forma, cuando el usuario selecciona un tipo de tarea en el mapa de tareas, el Cockpit presenta por defecto la "Vista de resultados" que permite al usuario monitorear la ejecución de las tareas.

La "Vista de resultados" es idéntica para todo tipo de tareas y contiene información gráfica sobre la ejecución de las tareas, información sobre la ejecución por tienda y listado de tareas y productos.

![imagen alt <>](/assets/cockpit_execution_overview.gif)

### **Gráficos de ejecución**

En la parte superior de la "Vista de resultados", el Cockpit presenta los gráficos de ejecución correspondientes al intervalo de tiempo y las tiendas seleccionadas.

Aquí es posible ver el total de tareas previstas frente al total de tareas completadas. El porcentaje del resultado se presenta gráficamente para una mejor lectura. Además del gráfico, el Cockpit también presenta información detallada sobre el total de tareas para cada estado.

Esta vista gráfica de ejecución se presenta tanto para tareas como para artículos y en cada componente gráfico se presentan los detalles de los respectivos estados.

![imagen alt <>](/assets/cockpit_task_execution_graphic.png)

### **Ejecución por tienda**

En caso de que el usuario tenga más de una tienda seleccionada, el Cockpit presenta un módulo de visualización de ejecución por tienda.

De esta forma es posible que el usuario realice un análisis comparativo de la ejecución de tareas entre las diferentes tiendas.

Este componente presenta la siguiente información:

- **Total** : Total de tareas planificadas del tipo seleccionado y para una tienda determinada.
- **Finalizado** : total de tareas completadas para el tipo seleccionado y para una tienda determinada.
- **Porcentaje (%)** : Porcentaje de finalización de tareas del tipo seleccionado y para una tienda determinada.
- **Por ejecución** : total de tareas sin terminar para el tipo seleccionado y para una tienda determinada.
- **En ejecución** : total **de** tareas en **ejecución** del tipo seleccionado y para una tienda determinada.
- **Hospitalizados** : Total de tareas hospitalizadas del tipo seleccionado y para una tienda específica.
- **Caducadas** : Total de tareas caducadas del tipo seleccionado y para una tienda determinada.
- **Otros** : Total de tareas canceladas y liberadas del tipo seleccionado y para una tienda específica.

![imagen alt <>](/assets/cockpit_task_execution_store.png)

### **Listados**

En el componente "Listados", el Cockpit presenta una lista de tareas y productos y el usuario puede alternar entre ellos seleccionando la pestaña correspondiente.

La información presentada en las columnas de la lista difiere según el tipo de tarea pero el objetivo principal es brindar al usuario la información más relevante a nivel de la tarea y productos como nombre de la tarea, número total de artículos planificados y tratados, duración y estado de la tarea.

El Cockpit también permite al usuario [buscar](cockpit/#pesquisa) una tarea o un producto a través del facilitador de búsqueda y cambiar las columnas visibles por defecto.

Además de la búsqueda, también se encuentran disponibles facilitadores de [filtrado](cockpit/#filtro) y [exportación de datos](cockpit/#exportacao-de-dados) .

La Lista de tareas en la "Vista de ejecución" permite al usuario acceder a información detallada para cada tarea. Para hacer esto, simplemente seleccione una tarea específica haciendo clic en su nombre. El Cockpit le dirige inmediatamente al modo de detalle de la tarea, que se compone de la siguiente información:

- **Detalle de la tarea** : Información detallada sobre el identificador de la tarea, nombre, tipo, consulta de estado e identificación del proceso donde se inserta la tarea. Al hacer clic en el identificador del proceso, el usuario es guiado a una página que informa todas las tareas que constituyen el flujo donde se inserta. Para obtener más información, consulte [Procesos](cockpit/#processos) .
- **Detalle** : información detallada de la tarea que incluye información relacionada con la tienda, totalizador de productos y cantidades e información relacionada con los tiempos y empleados involucrados en la tarea.
- **Productos** : Listado detallado de los productos pertenecientes a la tarea que incluye información sobre el nombre del producto, identificador, cantidad esperada y cantidad tratada y estado del producto. Aquí también es posible acceder a los detalles de un producto haciendo clic en su nombre. En el tipo de tarea "Registro de rupturas", el cockpit muestra la información de "motivo" y "destino" a nivel de artículo. Esta información también está disponible para su extracción.

![imagen alt <>](/assets/cockpit_execution_overview_task_detail.gif)

Asimismo, el Cockpit permite acceder al detalle de un producto a través del listado de productos en la "Vista de Ejecución. Para ello, simplemente seleccione la pestaña de productos y en la lista seleccione el producto deseado haciendo clic en el nombre. El cockpit le dirige inmediatamente al modal de detalle del producto que está compuesto por la siguiente información:

- **Producto** : Información detallada del producto que incluye el identificador, nombre, estado, EAN, SKU, cantidad esperada y cantidad procesada del artículo en la tarea y finalmente la categoría donde el producto encaja dentro de la estructura de mercado del cliente.
- **Detalle** : información detallada del producto que incluye información relacionada con la tienda, identificador, tipo y estado de la tarea. También está presente la información de tiempos y colaboradores asociados al artículo en la tarea en la que se inserta.

![imagen alt <>](/assets/cockpit_execution_overview_item_detail.gif)

## **Resultados de la visión**

La "Vista de Resultados" del Cockpit permite al usuario consultar información gráfica relacionada con la ejecución de un tipo específico de tarea, agregando detalles del tiempo total de ejecución, tiempo promedio y número total de empleados involucrados en el tratamiento de las tareas de los seleccionados. tipo.

Además de la información gráfica, el Cockpit proporciona un resumen gráfico de los resultados por tarea o productos a través de una vista agregada.

Finalmente, la lista de tareas está nuevamente disponible para una consulta más detallada. Sin embargo, ya diferencia de la "Vista de ejecución", el listado de tareas en la "Vista de resultados" solo presenta las tareas completadas ya que son las únicas que influyen en la información presentada en los resultados.

Nota

La "Vista de resultados" tiene una característica importante: hay una vista básica que se presenta en la mayoría de los tipos de tareas, pero también hay una vista que es específica para algunos tipos de tareas. En los siguientes puntos, abordaremos la vista "base" de los resultados y cada una de las vistas particulares.

### **Vista base**

La vista básica se presenta en los siguientes tipos de tareas:

- Recuentos de existencias,
- Comprobación de cantidad,
- Auditoría de escaneo,
- Auditoría externa,
- Verificación de presencia,
- Auditoría y Control de Validez y Recepciones.

#### Resumen gráfico

La vista básica de los resultados presenta una vista gráfica de la ejecución de las tareas con detalles del tiempo total invertido en la realización de las tareas del tipo seleccionado. También muestra el tiempo medio empleado para realizar las tareas y el número total de empleados implicados en la realización de las tareas.

En cuanto a los productos, presenta el gráfico de ejecución y el detalle del tiempo medio empleado en el tratamiento de los productos en las tareas del tipo seleccionado.

![imagen alt <>](/assets/cockpit_graphic_result_view.png)

#### Resumen de tareas, productos y chips

La vista básica de los resultados presenta un gráfico donde es posible consultar el historial de tareas y productos.

En el historial de tareas es posible ver gráficamente el número total de tareas y tareas completadas en un intervalo dado y bajo una vista del día, semana, mes y año. Este gráfico es dinámico por lo que al seleccionar la leyenda correspondiente se ocultará o mostrará la información.

![imagen alt <>](/assets/cockpit_results_overview_resume.gif)

En el historial del producto, es posible mostrar gráficamente el número total de productos procesados, tratados, no encontrados y sin tratar. La información promedio del producto para el intervalo de tiempo seleccionado también está disponible en el gráfico. Este gráfico es dinámico por lo que al seleccionar la leyenda correspondiente se ocultará o mostrará la información.

![imagen alt <>](/assets/cockpit_results_overview_resume_products.gif)

En el historial del producto, es posible ver los detalles del astillado del producto en el rango de fechas y las tiendas seleccionadas. Este resumen de astillado contiene información sobre productos planificados, tratados, no planificados (ad hoc) y productos faltantes. Además de los productos, también está presente información sobre las cantidades respectivas.

![imagen alt <>](/assets/cockpit_results_overview_resume_products_scanned.gif)

#### Listado

La vista básica de los resultados presenta una lista de tareas y productos donde es posible consultar en detalle todas las tareas completadas de un tipo seleccionado.

Esta lista contiene información sobre el nombre de la tarea, estado, cantidad de productos planificados, tratados, no encontrados, inicio de la tarea, duración de la tarea e información del usuario que realizó la tarea.

Al igual que en "Vista de resultados", es posible acceder a una tarea o producto específico haciendo clic en su nombre.

![imagen alt <>](/assets/cockpit_results_overview_list.gif)

### **Vista de rebajas**

La "Vista de resultados" de las tareas de marcado contiene la misma información que está presente en la versión base y agrega información sobre marcas y etiquetas.

#### Rebajas

En el Resumen de las tareas de Rellamada, el Cockpit proporciona una pestaña con la información de Rellamada. Aquí el usuario tiene visibilidad por defecto de la información de stock y las marcas de los artículos tratados, agrupados por estructura de mercado.

Sin embargo, el usuario puede cambiar el estado de los artículos que desea consultar y también cambiar la vista de jerarquía para el stock.

Como ocurre con todos los gráficos de la cabina, los subtítulos son dinámicos y el usuario puede ocultar o mostrar un subtítulo específico. Para una mejor lectura de los gráficos es posible consultar el detalle de cada columna colocando el mouse sobre la columna deseada. Inmediatamente se presenta la información detallada de la información visible.

![imagen alt <>](/assets/cockpit_results_overview_relabelling.gif)

#### Etiquetas colgantes

En el Resumen de las tareas de remarcación, el Cockpit proporciona una pestaña con la información de las etiquetas. Aquí el usuario tiene visibilidad del detalle de las etiquetas impresas durante el proceso de rellamada. La información sobre el número total de ascensos, descensos y etiquetas es visible sin cambiar el precio.

![imagen alt <>](/assets/cockpit_results_overview_relabelling_label.gif)

### **Vista de auditoría de precios**

La "Vista de Resultados" de las tareas de Auditoría de Precios permite al usuario tener visibilidad de la información de la Auditoría a través de alertas y resumen gráfico.

#### Alertas

En la parte superior de la "Vista de resultados", el Cockpit muestra alertas relacionadas con las diferencias de precios resultantes de las auditorías respectivas. En este sentido, existen dos posibles alertas:

- **Productos sujetos a incumplimiento legal** : Información sobre el número de productos auditados donde el precio de la etiqueta es superior al precio de venta. Esta situación puede dar lugar a procesos legales para la tienda / organización.
- **Productos susceptibles de causar pérdidas** : Información sobre el número de productos auditados en los que el precio de la etiqueta es inferior al precio de venta. Esta situación puede ocasionar pérdidas a la empresa ya que la dinámica de precios ha cambiado en el sistema pero no en la etiqueta.

![imagen alt <>](/assets/cockpit_priceaudit_alert.png)

#### Gráfico de auditorías

En el componente Resumen de las tareas de Auditoría de Precios, el Cockpit presenta gráficamente los resultados de las auditorías realizadas en el intervalo de tiempo y tiendas seleccionadas.

En el gráfico de la izquierda, es posible ver el número de auditorías confirmadas (precio de ERP igual al precio de etiqueta), el número de auditorías divergentes (precio de ERP diferente al precio de etiqueta) y el número de auditorías desconocidas (la etiqueta precio no fue informado). En el mismo gráfico también es posible ver el porcentaje correspondiente de auditorías en cada una de las condiciones. Los subtítulos de este gráfico también son dinámicos.

El gráfico de la derecha muestra el número de auditorías confirmadas y desconocidas para cada nivel de la estructura jerárquica.

![imagen alt <>](/assets/cockpit_results_overview_priceaudit_resume.gif)

### **Vista de separación**

La "Vista de Resultados" de las tareas de Separación permite al usuario tener visibilidad de la información de calidad de la ejecución de las tareas.

#### Cuadro resumen

En el componente de resumen de las tareas de Separación, el Cockpit presenta la información de los artículos que fueron separados en su totalidad (Leyenda total) y parcialmente separados (Leyenda parcial).

El gráfico de la izquierda muestra esta información de forma global para todas las tareas por rango de fechas y tiendas seleccionadas. El gráfico de la derecha muestra las separaciones distribuidas por estructura jerárquica.

![imagen alt <>](/assets/cockpit_replenishprep_resumo.png)

### **Vista de reemplazo**

La "Vista de Resultados" de las tareas de Reemplazo permite al usuario tener visibilidad de la información de calidad de la ejecución de las tareas. Al igual que en las tareas de Separación, es posible consultar la información de los reemplazos totales y parciales.

#### Cuadro resumen

En el componente de resumen en las tareas de Reemplazo, el Cockpit presenta la información de los artículos que fueron completamente reemplazados (Leyenda total) y parcialmente restaurados (Leyenda parcial).

El gráfico de la izquierda muestra esta información de forma global para todas las tareas por rango de fechas y tiendas seleccionadas. El gráfico de la derecha muestra los reemplazos distribuidos por estructura jerárquica.

![imagen alt <>](/assets/cockpit_replenish_resume.png)

## **Actuación**

La "Vista de rendimiento" del Cockpit permite al usuario consultar información gráfica relacionada con el desempeño de tareas, productos, empleados y tiendas.

La información presentada en esta vista se puede presentar agregando día, semana, mes y año y utilizando un intervalo de tiempo previamente seleccionado.

### **Tareas**

El gráfico "Duración de la tarea" en la "Vista de rendimiento" muestra el tiempo promedio de las tareas completadas en el intervalo de tiempo seleccionado. Aquí se muestra la información de la tarea de mayor duración, la tarea de menor duración y la información promedio por tarea.

![imagen alt <>](/assets/cockpit_performance_tasks.gif)

### **Productos**

El gráfico "Duración promedio por producto" en la "Vista de rendimiento" muestra el tiempo promedio para tratar un producto en el intervalo de tiempo seleccionado. Aquí puede ver la información del producto con mayor duración, el producto con menor duración y la información promedio por producto.

![imagen alt <>](/assets/cockpit_performance_product.gif)

### **Colaboradores**

El "Visão Performance" presenta la información del "Top 10 de empleados con el mejor promedio por tarea", "Top 10 de las tiendas con el mejor promedio por producto" y una lista con "Tiempos promedio de todas las tiendas".

El gráfico "Los 10 mejores empleados con el mejor promedio por tarea" muestra el tiempo promedio requerido para manejar una tarea por empleado.

El cuadro "Los 10 mejores empleados con el mejor promedio por producto" muestra el tiempo promedio requerido para tratar un producto por empleado.

La lista "Tiempos promedio de todos los empleados" muestra los detalles del tiempo promedio requerido para manejar tareas y productos por empleado.

![imagen alt <>](/assets/cockpit_performance_users.gif)

### **Tienda**

"Visão Performance" presenta la información de "Top 10 de las tiendas con el mejor promedio por tarea", "Top 10 de los empleados con el mejor promedio por producto" y una lista con "Tiempos promedio de todos los empleados".

El gráfico "Las 10 mejores tiendas con el mejor promedio por tarea" muestra el tiempo promedio requerido para manejar una tarea por tienda.

El gráfico "Las 10 mejores tiendas con el mejor promedio por producto" muestra el tiempo promedio requerido para tratar un producto por tienda.

La lista "Tiempos promedio de todas las tiendas" muestra los detalles del tiempo promedio requerido para manejar tareas y productos por tienda.

![imagen alt <>](/assets/cockpit_performance_stores.gif)

## **Inventarios**

El módulo de Inventario en el Cockpit es diferente del módulo para las tareas restantes. En este módulo no hay vistas de resultados, ejecución y desempeño, pero solo hay una vista única que permite consultar la información de todos los inventarios creados para un rango de fechas determinado y grupo de tiendas seleccionadas.

El objetivo de este módulo es presentar los inventarios de forma agregada con la posibilidad de consultar el detalle de zonas y lecturas para cada inventario.

### **Inventarios**

Cuando el usuario selecciona Inventario en el menú de cabina, se muestra la lista de todos los inventarios creados para el intervalo de tiempo seleccionado y el grupo de tiendas.

En esta lista se encuentra la siguiente información:

- **ID** : información del identificador de inventario enviada en el momento de la integración. Este identificador es único y se envía en la integración.
- **Descripción** : Información sobre el nombre del inventario enviado en el momento de la integración.
- **Estado** : información sobre el estado del inventario. El estado del inventario se infiere del estado global de las zonas de inventario. De esta manera, si se completan todas las zonas de inventario, el estado del inventario también se actualizará a "Completado".
- **Fecha de finalización** : información sobre la fecha de cierre del inventario.
- **Store Id** : información del identificador de la tienda en la que se creó el inventario en el momento de la integración.
- **Tienda** : Información a nombre de la tienda donde se creó el inventario en el momento de la integración.

Como en los otros módulos de Cockpit, en el módulo de Inventarios es posible buscar a través del facilitador de búsqueda presente en la parte superior de la tabla.

Además de la búsqueda, también es posible seleccionar las columnas de información en la tabla, filtrar las tareas por su estado y exportar esta información utilizando el facilitador de exportación de datos.

![imagen alt <>](/assets/cockpit_inventory_inventories.gif)

Además de la información de esta tabla, el usuario puede consultar los detalles del inventario respectivo. Para hacer esto, simplemente haga clic en su nombre.

Inmediatamente se dirige al usuario a la pantalla de detalle del inventario donde es posible consultar la siguiente información:

- **Inventario** : Información de identificación, descripción, estado y referencia externa. La información de referencia externa identifica el identificador de inventario en el ERP.
- **Resumen** : En la pestaña resumen se dispone de información sobre la tienda en la que se creó el inventario e información sobre los tiempos y empleados involucrados en su ejecución.
- **Zonas** : en la pestaña de zonas, la información sobre todas las zonas de recuento creadas para un inventario determinado está disponible. En cada zona hay información sobre el nombre, id, estado e información de las lecturas creadas para una zona determinada.
- **Productos** : en la pestaña de productos, la información sobre todos los productos procesados en un inventario determinado está disponible. En cada producto está presente la información del nombre, estado, sku, ean y cantidad.

Al igual que en la pantalla anterior de este listado, también es posible buscar, filtrar por estado, ocultar o mostrar columnas y extraer datos.

![imagen alt <>](/assets/cockpit_inventory_inventories_detail.gif)

### **Zonas de recuento**

En el módulo de inventario es posible ver las zonas de conteo de cada inventario de forma agregada o detallada.

Para ver la información de las zonas de conteo en forma agregada, el usuario debe seleccionar el agregador presente en el lado izquierdo de la línea de inventario correspondiente. De esta forma se presentan las zonas de conteo para el inventario seleccionado y en cada zona se presenta el nombre, id, estado e información de las lecturas de conteo correspondientes.

Para consultar el detalle de una zona de conteo, el usuario debe hacer clic en el nombre de la zona y el Cockpit pasará inmediatamente a la pantalla de detalle. Aquí está la siguiente información:

- **Zona** : información de identificación, descripción, estado e identificación de inventario.
- **Resumen** : En la pestaña de resumen, la información está disponible con respecto a la tienda en la que se creó la zona y la información de lectura disponible para la zona respectiva.
- **Tareas** : en la pestaña de tareas, la información sobre todas las lecturas creadas dentro de una zona determinada está disponible. En cada lectura se presenta información sobre el nombre, estado, número de artículos tratados, no encontrados, inicio de la tarea, colaborador e información sobre la duración de la lectura.
- **Productos** : En la pestaña de productos, está disponible la información sobre todos los productos procesados en un área determinada. En cada producto está presente la información del nombre, estado, SKU, EAN y cantidad.

![imagen alt <>](/assets/cockpit_inventory_zone_detail.gif)

### **Lecturas**

Mediante la integración de inventarios, es posible definir si cada zona de conteo creada tiene solo una lectura o dos lecturas asociadas. En el caso de tener una sola lectura, cuando esta finaliza, la zona de conteo también finaliza automáticamente.

Si hay dos lecturas para una determinada zona de conteo, la misma solo se termina si ambas lecturas están cerradas y sin divergencia, es decir, sin elementos y cantidades diferentes entre las dos lecturas. Si hay una divergencia, se crea una tarea de corrección de divergencia, que también se asocia automáticamente con la zona de conteo.

Para acceder al detalle de cada lectura, el usuario debe seleccionar la zona de conteo respectiva y luego seleccionar la lectura deseada.

La siguiente información aparece en esta pantalla:

- **Tarea** : información de identificación, nombre, descripción, tipo, estado e identificación de inventario.
- **Detalle** : Información de la tienda, productos y cantidades y tiempos y colaboradores.
- **Productos** : Información sobre la lista de productos procesados en la zona de recuento. En cada producto está presente la información del nombre, estado, SKU, EAN y cantidad.

![imagen alt <>](/assets/cockpit_inventory_reading_detail.gif)

## **Caduca**

Para las tareas que tratan con fechas de vencimiento, el Cockpit presenta un módulo específico que permite al usuario ver todas las fechas de vencimiento del rango de fechas seleccionado y grupo de tiendas en forma agregada al SKU o fecha de vencimiento.

Este módulo está disponible en la "Vista de ejecución" de las tareas en la pestaña "Validez" junto a las pestañas "Tareas" y "Productos".

![imagen alt <>](/assets/cockpit_validity_detail_all.gif)

### **Todas las fechas**

Cuando el usuario selecciona la pestaña "Validez", el Cockpit presenta por defecto la vista detallada de todas las fechas presentes en las tareas dentro del intervalo de tiempo seleccionado y grupo de tiendas.

Esta lista de fechas de vencimiento contiene la siguiente información:

- **SKU** : información de **SKU del** artículo asociada con la fecha de vencimiento.
- **Fecha de vencimiento** : información sobre la fecha de **vencimiento** .
- **Tipo de validez** : información sobre el tipo de tarea asociada a la fecha de vencimiento. La fecha puede estar en un período de depreciación o retiro.
- **Días para depreciar** : información sobre la cantidad de días para depreciar el artículo asociado con la fecha.
- **Días para retirar** : información sobre la cantidad de días para retirar del artículo asociado a la fecha.
- **Cantidad** : Cantidad asociada a la fecha de vencimiento.
- Nombre del **producto** : nombre del artículo.
- **Estado del producto** : **estado del** artículo asociado con la fecha de vencimiento.
- **Tarea** : Nombre de la tarea donde se inserta el artículo.
- **Fechas futuras** : Facilitador que le permite buscar todas las fechas asociadas al artículo.

Además de esta información, están presentes el facilitador de búsqueda, el filtro de estado y la exportación de datos.

![imagen alt <>](/assets/cockpit_validity_detail_all_future_dates.gif)

### **Por SKU**

El módulo de validez en el Cockpit le permite ver la lista de fechas disponibles agregadas por SKU. Para ello, el usuario debe seleccionar "Por SKU" e inmediatamente se presenta una lista de SKU's con la siguiente información:

- **SKU** : Identificador de artículo.
- **Nombre** : Nombre del artículo.
- **No. de fechas** : información sobre la cantidad total de fechas asociadas con el SKU dentro del rango de fechas seleccionado y el grupo de tiendas.
- Número de **depreciación** : información sobre el número total de depreciaciones asociadas con el SKU dentro del grupo de fechas seleccionado y el grupo de tiendas.
- **Número de retiros** : información sobre el número total de retiros asociados con el SKU dentro del rango de fechas y grupo seleccionado de tiendas.

Además de esta agregación, es posible consultar los detalles de fechas asociadas a un SKU específico. Para hacer esto, simplemente haga clic en el facilitador de agregación en el lado derecho de la línea.

De esta manera, al usuario se le presentan los detalles de las fechas asociadas asociadas con el SKU seleccionado. En el detalle de la fecha presenta la siguiente información:

- **Fecha de vencimiento** : información sobre la fecha de **vencimiento** .
- **Tipo de validez** : información sobre el tipo de validez asociado a la fecha de caducidad.
- **Días para depreciar** : información sobre la cantidad de días para depreciar el artículo asociado con la fecha.
- **Días para retirar** : información sobre la cantidad de días para retirar del artículo asociado a la fecha.
- **Cantidad** : Cantidad asociada a la fecha de vencimiento.
- Nombre del **producto** : nombre del artículo.
- **Estado del producto** : **estado del** artículo asociado con la fecha de vencimiento.
- **Tarea** : Nombre de la tarea donde se inserta el artículo.
- **Nombre de la tienda** : información sobre el nombre de la tienda asociada a la tarea.

![imagen alt <>](/assets/cockpit_validity_detail_sku.gif)

### **Por fecha**

El módulo de validez en el Cockpit permite visualizar la información de fechas disponibles agregadas por fecha. Para ello, el usuario debe seleccionar "Por fecha" e inmediatamente se presenta un listado de fechas con la siguiente información:

- **Fecha de vencimiento** : información sobre la fecha de **vencimiento** .
- **# SKU** : información sobre el número total de elementos asociados a una fecha determinada.
- **Número de depreciaciones** : información sobre el número total de depreciaciones asociadas con la fecha de vencimiento dentro del rango de fechas y grupo seleccionado de tiendas.
- **No. de retiros** : información sobre el número total de retiros asociados con la fecha de vencimiento dentro del rango de fechas seleccionado y grupo de tiendas.

![imagen alt <>](/assets/cockpit_validity_detail_date.gif)

## **Demanda judicial**

El Cockpit proporciona un área para procesos en el menú. En esta área es posible ver la información del porcentaje de rotura asociada con el Mapa de Escaneo y el Mapa de Auditoría Externa. De la misma forma, también es posible consultar el detalle de las tareas que componen estos mismos procesos.

### **Lista de procesos**

Para acceder a la lista de procesos disponibles, el usuario debe acceder a "Mapa de escaneo" o "Mapa de auditoría externa". El Cockpit presenta inmediatamente la lista de procesos para el tipo de tarea seleccionado.

La siguiente información está presente en esta lista:

- **Nombre** : información sobre el nombre de la tarea que originó el proceso. El nombre de la tarea original es también el nombre del proceso.
- **Store Id** : Información del identificador de la tienda al que está asociado el proceso.
- **En el rango** : Información sobre el número total de artículos previstos en el proceso y que pertenecen al rango de la tienda respectiva.
- **En la estructura** : Información sobre el número total de artículos previstos en el proceso y que pertenecen a la estructura jerárquica de la tienda respectiva.
- **Picado en la estructura** : Información sobre el número total de artículos picados en el proceso y que pertenecen a la estructura jerárquica de la tienda respectiva.
- **Troceado fuera de estructura** : Información sobre la cantidad total de artículos troceados en el proceso y que no pertenecen a la estructura jerárquica de la tienda respectiva.
- **Con stock en la estructura** : Información sobre el número total de artículos en el proceso de stock que pertenecen a la estructura jerárquica de la tienda respectiva.
- **Fuera de stock en la estructura** : Información sobre el número total de artículos en proceso fuera de stock que pertenecen a la estructura jerárquica de la tienda respectiva.
- **% Break** : Cálculo del porcentaje final de break en el proceso. Este cálculo se reprocesa al final de cada tarea que influye en el porcentaje final de la pausa. Ejemplo: auditoría de escaneo y verificación de asistencia.

Rompiendo la distancia:

Cantidad total de artículos sin seleccionar en el rango / cantidad total de artículos con stock en el rango X 100.

Esta lista incluye facilitadores de investigación, entrada de presentación de información y facilitador de exportación de datos.

![imagen alt <>](/assets/cockpit_process_list.gif)

### **Detalle del proceso**

En la lista descrita en el punto anterior, es posible visualizar los detalles de cada proceso. Para ello, el usuario debe hacer clic en el agregador presente en el lado izquierdo de la línea de cada proceso.

De esta forma, el Cockpit presenta todas las tareas que componen un determinado proceso y en cada tarea la siguiente información:

- **Nombre** : Información sobre el nombre de la tarea que compone el proceso.
- **Tipo** : información del tipo de tarea.
- **Estado** : información sobre el estado de la tarea.
- **Programado** : información sobre el número total de elementos programados para la tarea.
- **Tratado** : información sobre la cantidad total de artículos procesados en la tarea.
- **No encontrado** : información sobre la cantidad total de artículos procesados como no encontrados en la tarea.
- **Sin** tratar: información sobre el número total de artículos sin tratar en la tarea.
- **Colaborador** : Información del colaborador que realizó la tarea.

A través de esta información, el usuario puede consultar el número total de ítems en las tareas y visualizar la información que conforma el cálculo de la ruptura.

Para acceder a los detalles de cada una de las tareas en un proceso, simplemente haga clic en el nombre de la tarea e inmediatamente se le dirigirá al detalle respectivo.

![imagen alt <>](/assets/cockpit_process_detail.gif)

## **Hoja de artículo**

El componente "Ficha de artículo" del Cockpit le permite consultar la información de un artículo en particular de forma similar a lo que puede ver en la aplicación móvil.

Para consultar la "Ficha de artículo" el usuario debe identificar el artículo que desea consultar y luego seleccionar la opción "Ver ficha de artículo".

![imagen alt <>](/assets/cockpit_item_file.gif)

En el componente "Hoja de artículo", está presente la siguiente información relacionada con el artículo. La información disponible es configurable por el cliente, lo que significa que esta información puede variar para diferentes clientes.

La información básica presente en la "Ficha del artículo" es la siguiente:

- **Identificación del artículo** : nombre del artículo, foto (si corresponde), estado, SKU e información EAN.
- **Jerarquía** : Información sobre la estructura del mercado donde se inserta el artículo.
- **EAN** : información sobre los EAN primarios y secundarios del artículo.
- **Precios** : información de **precios de** ERP y POS (POS).
- **Stock** : información SOH de la tienda asociada con el artículo investigado.
- **Stock de otras tiendas** : información SOH disponible en todas las tiendas de la organización.

![imagen alt <>](/assets/cockpit_item_file_detail.gif)