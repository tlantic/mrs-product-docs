
# Cockpit 

![image alt <>](assets/capa_cockpit.png) 

## Histórico do documento

| Data      | Descrição    | Autor | Versão | Revisão
| ------------- | ---------------------------------------------------------------------------------------- | ---------- | ---------- | -------- | 
| 21/12/2020         | Criação do documento.    |  Soraia Meneses  |   1.0   |     |
| 01/12/2020     | Criação de Introdução de formatação de documento.       | Soraia Meneses  |  1.1     |      |
| 15/01/2021     | Versão final do documento.       | Filipe Silva  |  2.0     |      |

## **Introdução**

### TMR - Cockpit

Este documento descreve as funcionalidades do sistema Cockpit. O principal objectivo do documento é fornecer ao utilizador toda a informação necessária para que este possa utilizar o sistema de forma correta e eficaz.

O Cockpit é um dos módulos que compõe o Tlantic Mobile Retail (TMR) voltado para as operações de loja, com foco na solução de problemas comuns do retalho:
???+ example "Principais funcionalidades:"
    * Inventários.
    * Lista de Artigos, Ficha de Artigo e Auditoria de Preço, 
    * Registo de Quebras.
    * Impressão de etiquetas.
    * Auditoria  e Controlo de Validades (Depreciação e Retirada).
    * Auditoria de Varrimento e Verificação de Presença (Processos).
    * Auditoria de Rutura, Separação, Reposição. 
    * Receções, Transferências e Devoluções.

### Arquitetura

O TMR é um sistema constituído por:

* Aplicações Cliente (App WinCE, App Android e App iOS) - Aplicação distribuida pelos dispositivos móveis usados na operação de loja.
* TMR Server - Componente central que contém a lógica e dados e que interage com outros sistemas.
* Backoffice - Componente web que permite definição de dados de referência e configuração de parâmetros.
* Cockpit - Componente web para análises operacionais das tarefas executadas no TMR Instore.


![image alt <>](assets/Arquitetura.png) 

### Requisitos mínimos

Para uma correta utilização do Cockpit TMR, o dispositivo deve respeitar os seguintes requisitos:

???+ example "Requisito mínimo:"
    * **Browser Chrome**: Versão 54 ou superior.

## **Login**
Para iniciar sessão, é pedido ao utilizador que introduza o seu **Utilizador** e **Palavra-passe**.  Para visualizar a palavra-passe colocada, deve clicar no símbolo ![foto](assets/eye.png){:height="20px" width="20px"}

Depois de colocadas as credenciais, clicar em **“Iniciar sessão”**. 

![image alt <>](assets/login_cockpit.png) 

## **Seleção de Data e Loja**

Após efetuado o Login, o utilizador é direcionado para a página inicial da aplicação. Neste momento encontram-se disponíveis os componentes de **seleção de data** e **seleção de loja**.  

### **Seleção de Data**

O componente de seleção de data permite definir o intervalo de tempo que é pretendido visualizar. Desta forma, todas as tarefas apresentadas correspondem apenas ao intervalo de tempo selecionado.

O componente de seleção de data está presente no canto superior direito da aplicação e apresenta o intervalo de tempo selecionado por default. Se o utilizador pretender alterar alterar este intervalo de tempo deve clicar sobre o componente e de imediato será apresentado um calendário para seleção manual de datas e um facilitador com os seguintes campos:

* **Datas futuras**: Facilitador para seleção do intervalo de tempo compreendido entre a data atual e a data futura mais distante com tarefas por executar.
* **Datas passadas**: Facilitador para seleção do intervalo de tempo compreendido entre a data atual e a data passada mais distante com tarefas por executar. 
* **Hoje**: Facilitador para selecção do dia atual.
* **Ontem**: Facilitador para seleção do dia passado.
* **Últimos 7 dias**: Facilitador para seleção do intervalo de tempo compreendido entre a data atual e sete dias passados.
* **Últimos 30 dias**: Facilitador para seleção do intervalo de tempo compreendido entre a data atual e trinta dias passados.
* **Últimos 60 dias**: Facilitador para seleção do intervalo de tempo compreendido entre a data atual e sessenta dias passados.
* **Este ano**: Facilitador para seleção do intervalo de tempo compreendido entre dia 1 de Janeiro do ano atual e dia atual.
* **Definir**: Facilitador para seleção manual do intervalo de tempo.

![image alt <>](assets/cockpit_select_date.gif) 

### **Seleção de loja**

O componente de seleção de loja permite definir a loja ou o grupo de lojas que são pretendidas para visualização das tarefas. Apenas estarão disponíveis para seleção as lojas a que o utilizador estiver associado. 

O componente de seleção de lojas está presente na parte direita do componente de seleção de datas e permite realizar o filtro das lojas por três níveis distintos:

* **Insígnias**: Facilitador que permite filtrar as lojas pela insígnia a que pertencem. (primeiro nível)
* **Grupos**: Facilitador que permite filtrar as lojas pelo grupo a que pertencem. (segundo nível)
* **Lojas**: Facilitador que permite filtar as lojas individualmente.

Em cada um dos grupos está sempre presente o facilitador de "Selecionar Todas/Nenhuma", um totalizador que apresenta o número total de insígnias, grupos ou lojas selecionadas e um input de pesquisa.

![image alt <>](assets/cockpit_select_stores.gif) 

## **Resumo e Definições**

No canto superior direto da aplicação estão disponiveis os seguintes acessos:

* **Resumo**: Facilitador que permite visualizar a percentagem de  execução por tipo de tarefa.
* **Refresh**: Botão que permite atualizar os dados visiveis na aplicação.
* **Atualização automática**: Botão para ativação da atualização automática.
* **Seleção de idioma**: Input para seleção de idioma da aplicação.
* **Definições**: Botão para acesso às definições da aplicação.

![image alt <>](assets/cockpit_definitions.gif)

## **VISÃO GLOBAL**  

Após efetuado o Login, o utilizador é direcionado por default para a página de  "Visão Global" da execução e estado das tarefas.  

A "Visão Global" permite visualizar de uma forma gráfica a **percentagem de execução** de tarefas e produtos. Permite também visualizar o **detalhe da execução** no resumo de tarefas e, por fim, permite visualizar a **listagem de tarefas** por executar. 

### **Gráficos de execução**

Na "Visão Global" o Cockpit apresenta os gráficos de execução de tarefas e produtos. Os mesmos estão presentes na parte superior do ecrã de forma a que estejam visíveis de imediato após o login. 

#### Tarefas

O gráfico de execução de tarefas apresenta a percentagem (%) de todas as tarefas terminadas versus tarefas por executar. Esta taxa de sucesso tem em consideração todas as tarefas terminadas na(s) loja(s) e na data(s) selecionadas.

Além da informação gráfica (sucesso % execução) é também apresentado o detalhe do total de tarefas previstas e o total de tarefas terminadas para as lojas e intervalo de datas selecionadas. 

#### Produtos

Do mesmo modo que o gráfico de execução de tarefas, o gráfico de execução de produtos apresenta a taxa de sucesso de uma forma gráfica e o detalhe do total de artigos previstos e total de artigos processados.

![image alt <>](assets/cockpit_execution_grafic.png) 

### **Resumo**

O quadro "Resumo" apresenta a informação detalhada da execução de tarefas e produtos. Isto permite ao utilizador consultar o detalhe de execução de cada tipo de tarefa ou produto mediante a visão e o detalhe selecionados. 

Além do detalhe da execução é possível também ao utilizador consultar o detalhe de tarefas ou produtos por estado, ou seja, consultar o total de tarefas "Por executar", "Em execução", etc...

Por fim é possível também exportar os dados que estão visíveis para que os mesmo possam alimentar indicadores de BI.

#### Resumo tarefas

No quadro "Resumo" por tarefas é apresentada a informação de Total de tarefas disponíveis, o total de tarefas terminadas e a respetiva percentagem de execução. Esta informação é apresentada ao detalhe por cada tipo de tarefa disponível e o seu respetivo estado. Os estados visíveis no quadro de resumo podem ser alterados mediante seleção no facilitador presente no canto superior direito.

![image alt <>](assets/cockpit_resume_task_status.gif) <br>

A visão do resumo por tarefa permite também alterar a agregação da informação selecionando "Tipos de tarefa", "Loja", "Grupo" e "Insígnia". 

Desta forma, a informação apresentada no quadro de resumo vai alterando mediante a opção de "visão" selecionada.

![image alt <>](assets/cockpit_resume_task_vision.gif) <br>


O detalhe da informação apresentada de forma agregada pode também ser alterada mediante a seleção das seguintes opções de detalhe: "Loja", "Grupo" ou "Insígnia". 

Para isso, o utilizador deve expandir o tipo de tarefa pretendido e alterar a opção de detalhe disponível.

![image alt <>](assets/cockpit_resume_task_detail.gif) <br>

#### Resumo produtos

O quadro de "Resumo" por produtos permite visualizar a informação do total de produtos previstos, o total de produtos tratados e a respetiva percentagem de execução. Tal como no ponto anterior, os estados visíveis podem ser alterados mediante seleção no facilitador de visualização.

![image alt <>](assets/cockpit_resume_products_vision.gif) <br>

Tal como na visão por tarefas, a visão por produto também permite alterar a agregação da informação apresentada e o seu respetivo detalhe.

![image alt <>](assets/cockpit_resume_products_detail.gif)

#### Extração de dados

Além da consulta de informação por artigo ou por tarefa descrita nos pontos anteriores, o Cockpit permite também realizar a extração desses mesmos dados. Para isso, o utilizador deve primeiro selecionar a visão e o detalhe pretendido na informação e de seguida seve selecionar o botão de "Exportar dados". De forma imediata é realizado o download de um ficheiro csv com a informação solicitada.

![image alt <>](assets/cockpit_resume_export_data.gif)

### **Tarefas por terminar**

A listagem de "Tarefas por terminar" na Visão Global apresenta todas as tarefas que se encontram por finalizar no intervalo de dias e lojas selecionadas. 

Esta tabela permite **pesquisa**, **ordenação**, **filtro** e **exportação de dados**.

#### Pesquisa

Na parte superior da tabela "Tarefas por terminar" está presente um facilitador de pesquisa. Este facilitador permite pesquisar as tarefas listadas por qualquer informação associada à tarefa como nome, loja, tipo  e até artigos que pertençam às tarefas.

![image alt <>](assets/cockpit_open_task_search.gif)

#### Ordenação

Como facilitador de navegação, a tabela de "Tarefas por terminar" permite ordenação da informação pelas colunas visíveis. Por default, o Cockpit apresenta determinadas colunas visíveis e outras colunas escondidas. Existe a possibilidade de alterar as colunas visíveis através do componente de seleção de colunas presente no canto superior direito da tabela.

![image alt <>](assets/cockpit_open_task_sort.gif)

#### Filtro

Como facilitador de pesquisa, a tabela de "Tarefas por terminar" permite filtro por estado das tarefas. Para isso basta aceder ao facilitador de seleção de estado presente no canto superior direito da tabela.

![image alt <>](assets/cockpit_open_task_filter.gif)

#### Exportação de dados

Para efeitos de report ou simplesmente de consulta, a tabela "Tarefas por terminar" permite exportar os dados visíveis na tabela. As colunas e filtros  para exportação são definidos durante o processo, permitindo gerar um csv apenas com a informação relevante para o utilizador.

![image alt <>](assets/cockpit_open_task_export_data.gif)

## **Visão Agregada**

Semelhante à visão global, a visão agregada permite ter visibilidade do estado de execução das tarefas, do resumo e das tarefas por terminar mas apenas de alguns tipos de tarefa específicos. 

Este visão agregada apenas se aplica a clientes que pretendam ter uma visão de execução de vários tipos de tarefas diferentes e de uma forma consolidada. Desta forma, através de configuração ao cliente é possível ter uma visão agregada de vários tipos de tarefa diferentes. 

![image alt <>](assets/cockpit_overview_price.gif)

## **Visão Execução**

Além da "Visão Global" e "Visão Agregada", o Cockpit apresenta a "Visão Resultados" quando selecionado um tipo específico de tarefa. Desta forma, quando o utilizador seleciona um tipo de tarefa no mapa de tarefas, o Cockpit apresenta por default a "visão Resultados" que permite ao utilizador realizar o acompanhamento da execução das tarefas. 

A "Visão Resultados" é idêntica para todos os tipos de tarefa e contém informação gráfica da execução das tarefas, informação de execução por loja e listagem de tarefas e produtos.

![image alt <>](assets/cockpit_execution_overview.gif)


### **Gráficos de execução**

Na parte superior da "Visão Resultados" o Cockpit apresenta os gráficos de execução correspondente ao intervalo de tempo e lojas selecionadas. 

Aqui é possível visualizar o total de tarefas previstas versus o total de tarefas terminadas. O percentual do resultado é apresentado de forma gráfica para melhor leitura. Além do gráfico, o Cockpit apresenta também a informação detalhada do total de tarefas por cada estado.

Esta visão gráfica de execução é apresentada tanto para as tarefas como para os artigos e em cada componente gráfico é apresentado o detalhe dos respetivos estados. 

![image alt <>](assets/cockpit_task_execution_graphic.png)

### **Execução por loja**

No caso de o utilizador ter mais do que uma loja selecionada, o Cockpit apresenta um módulo de visualização de execução por loja.

Desta forma é possível ao utilizador realizar uma análise comparativa da execução de tarefas entre as diversas lojas.

Este componente apresenta a seguinte informação:

* **Total**: Total de tarefas previstas do tipo selecionado e para determinada loja.
* **Terminadas**: Total de tarefas terminadas do tipo selecionado e para determinada loja. 
* **Percentagem (%)**: Percentagem de excução de tarefas do tipo selecionado e para determinada loja.
* **Por Executar**: Total de tarefas por terminar do tipo selecionado e para determinada loja. 
* **Em execução**: Total de tarefas em execução do tipo selecionado e para determinada loja. 
* **Hospitalizadas**: Total de tarefas hospitalizadas do tipo selecionado e para determinada loja. 
* **Expiradas**: Total de tarefas expiradas do tipo selecionado e para determinada loja.
* **Outras**: Total de tarefas canceladas e libertadas do tipo selecionado e para determinada loja.

![image alt <>](assets/cockpit_task_execution_store.png)

### **Listagens**

No componente "Listagens" o Cockpit apresenta uma listagem de tarefas e produtos podendo o utilizador alternar entre as mesmas selecionando a aba correspondente. 

A informação apresentada nas colunas da listagem difere por tipo de tarefa mas o objetivo principal é disponibilizar ao utilizador a informação mais relevante ao nível da tarefa e dos produtos como nome da tarefa, número total de artigos previstos e tratados, duração e estado da tarefa. 

O Cockpit permite também ao utilizador [pesquisar](cockpit.md#pesquisa) uma tarefa ou um produto através do facilitador de pesquisa e alterar as colunas visíveis por default.

Além da pesquisa, estão também disponíveis os facilitadores de [filtro](#filtro) e [exportação de dados](#exportacao-de-dados).

A Listagem de tarefas na "Visão Execução" permite ao utilizador aceder a informação detalhada de cada tarefa. Para isso basta selecionar uma tarefa especifica clicando no nome da mesma. O Cockpit direciona de imediato para a modal de detalhe de tarefa que é composta pela seguinte informação:

* **Detalhe da tarefa**: Informação detalhada sobre o identificador da tarefa, nome, tipo, consulta de estado e identificação do processo onde a tarefa se insere. Clicando no identificador do processo, o utilizador é orientado para uma página que informa todas as tarefas que constituem o fluxo onde a mesma se insere. Para mais informação consultar [Processos](#processos). 
* **Detalhe**: Informação do detalhe da tarefa que inclui informação relacionada com a loja, totalizador de produtos e quantidades e informação relacionada com tempos e colaboradores envolvidos na tarefa.
* **Produtos**: Listagem detalhada dos produtos pertencentes à tarefa que inclui informação sobre nome do produto, identificador, quantidade prevista e quantidade tratada e estado do produto. Aqui também é possível aceder ao detalhe de um produto clicando no nome do mesmo. No tipo de tarefa "Registo de Quebra", o Cockpit apresenta a informação de "motivo" e "destino" ao nível do artigo. Esta informação está disponível também para extração. 

![image alt <>](assets/cockpit_execution_overview_task_detail.gif)


Da mesma forma, o Cockpit permite aceder ao detalhe de um produto através da listagem de produtos na "Visão Execução. Para isso basta selecionar a aba dos produtos e na listagem selecionar o produto pretendido clicando sobre o nome. O cockpit direciona de imediato para a modal de datalhe do produto que é composta pela seguinte informação:

* **Produto**: Informação detalhada do produto que inclui o identificador, o nome, estado, EAN, SKU, quantidade prevista e quantidade processada do artigo na tarefa e por fim a categoria onde o produto se insere dentro da estrutura mercadológica do cliente.
* **Detalhe**: Informação do detalhe do produto que inclui informação relacionada com a loja, identificador, tipo e estado da tarefa. Esta presente também a informação de tempos e colaboradores associados ao artigo na tarefa onde se insere. 

![image alt <>](assets/cockpit_execution_overview_item_detail.gif)


## **Visão Resultados** 

A "Visão Resultados" do Cockpit permite ao utilizador consultar informação gráfica relacionada com a execução de um tipo de tarefa específico, adicionando detalhe do tempo total de execução, tempo médio e número total de colaboradores envolvidos no tratamento das tarefas do tipo selecionado.

Além da informação gráfica, o Cockpit disponibiliza um resumo gráfico dos resultados por tarefa ou produtos mediante visão agregadora.

Por último está novamente disponível a listagem de tarefas para consulta mais detalhada. No entanto, e ao contrário da "Visão Execução", a listagem de tarefas na "Visão Resultados" apenas apresenta as tarefas finalizadas pois são as únicas que influenciam a informação apresentada nos resultados. 

!!! note "Nota"
    A "Visão Resultados" tem uma particularidade importante: Existe uma visão base que é apresentada na maioria do tipo de tarefas mas existe também uma visão que é específica para alguns tipos de tarefa. Nos pontos seguintes iremos abordar a visão "base" dos resultados e cada uma das visões particulares.

### **Visão base**

!!! example "A visão base é apresentada nos seguintes tipos de tarefa:"
    * Contagens de Stock, 
    * Verificação de Quantidade, 
    * Auditoria de Varrimento, 
    * Auditoria Externa, 
    * Verificação de Presença, 
    * Auditoria e Controlo de Validades e Receções.

#### Resumo gráfico

A visão base dos resultados apresenta uma visão gráfica da execução das tarefa com detalhe do tempo total dispendido na realização das tarefas do tipo selecionado. Apresenta também o tempo médio utilizado na realização das tarefas e o total de colaboradores envolvidos na execução das mesmas.

Em relação aos produtos apresenta o gráfico de execução e o detalhe do tempo médio utilizado no tratamento dos produtos nas tarefas do tipo selecionado. 

![image alt <>](assets/cockpit_graphic_result_view.png)

#### Resumo tarefas, produtos e picagens

A visão base dos resultados apresenta um gráfico onde é possível consultar o históricos das tarefas e produtos.

No histórico das tarefas é possível visualizar graficamente o número total de tarefas e tarefas terminadas em um determinado intervalo e sob uma visão de dia, semana, mês e ano. Este gráfico é dinâmico de forma a que selecionando a legenda correspondente a informação vai sendo ocultada ou apresentada.

![image alt <>](assets/cockpit_results_overview_resume.gif)

No histórico dos produtos é possível visualizar graficamente o número total de produtos processados, tratados, não encontrados e não tratados. No gráfico está também disponível a informação da média de produtos no determinado intervalo de tempo selecionado. Este gráfico é dinâmico de forma a que selecionando a legenda correspondente a informação vai sendo ocultada ou apresentada.

![image alt <>](assets/cockpit_results_overview_resume_products.gif)

No histórico dos produtos é possível visualizar o detalhe da picagem dos produtos no intervalo de datas e lojas selecionadas. Neste resumo de picagens está presente a informação de produtos planeados, tratados, não planeados (adhoc) e produtos em falta. Além dos produtos está também presente a informação das respetivas quantidades.

![image alt <>](assets/cockpit_results_overview_resume_products_scanned.gif)

#### Listagem

A visão base dos resultados apresenta uma listagem de tarefas e produtos onde é possível consultar com detalhe todas as tarefas terminadas de um determinado tipo selecionado.

Nesta listagem está presente a informação do nome da tarefa, o estado, quantidade de produtos previstos, tratados, não encontrados, início da tarefa, duração da tarefa e informação do utilizador que executou a tarefa. 

Tal como na "Visão Resultados" é possível aceder a determinada tarefa ou produto clicando no nome do mesmo. 

![image alt <>](assets/cockpit_results_overview_list.gif)

### **Visão Remarcação**

A "Visão Resultados" das tarefas de Remarcação contém a mesma informação que está presente na versão base e adiciona informação sobre Remarcações e Etiquetas.

#### Remarcações

No Resumo das tarefas de Remarcação, o Cockpit disponiliza uma aba com a informação de Remarcações. Aqui o utilizador tem visibilidade por default da informação do stock e remarcações dos artigos tratados, agrupadas por estrutura mercadológica. 

No entanto, o utilizador pode alterar o estado dos artigos que deseja consultar e alterar também a visão de hierarquia para stock. 

Tal como em todos os gráficos do Cockpit, as legendas são dinâmicas, podendo o utilizador ocultar ou mostrar determinada legenda. Para melhor leitura dos gráficos é possivel consultar o detalhe da cada coluna colocando o rato sobre a coluna pretendida. De imediato é apresentada a informação detalhada da informação visível.

![image alt <>](assets/cockpit_results_overview_relabelling.gif)

#### Etiquetas

No Resumo das tarefas de Remarcação, o Cockpit disponiliza uma aba com a informação de Etiquetas. Aqui o utilizador tem visibilidade do detalhe das etiquetas impressas durante o processo de remarcação. Está visível a informação da quantidade total de subidas, descidas e etiquetas sem alteração de preço. 

![image alt <>](assets/cockpit_results_overview_relabelling_label.gif)

### **Visão Auditoria de Preço**

A "Visão Resultados" das tarefas de Auditoria de Preço permite ao utilizador ter visibilidade da informação de Auditorias através de alertas e resumo gráfico.

#### Alertas

Na parte superior da "Visão Resultados" o Cockpit apresenta alertas relacionados com divergências de preço resultantes das respetivas auditorias. Neste sentido, existem dois possíveis alertas:

* **Produtos passíveis de incumprimento legal**: Informação da quantidade de produtos auditados em que o preço da etiqueta é superior ao preço de venda. Esta situação pode originar processo legais para a loja/organização.
* **Produtos passíveis de originar prejuízos**: Informação da quantidade de produtos auditados em que o preço da etiqueta é inferior ao preço de venda. Esta situação pode originar prejuízos para a empresa visto que a dinâmica de preço foi alterada em sistema mas não na etiqueta.

![image alt <>](assets/cockpit_priceaudit_alert.png)

#### Gráfico de auditorias

No componente de Resumo das tarefas de Auditoria de Preço o Cockpit apresenta de forma gráfica os resultados das auditorias efetuadas no intervalo de tempo e lojas selecionadas.

No gráfico da esquerda é possível visualizar a quantidade de auditorias confirmadas (preço ERP igual a preço etiqueta), quantidade de auditorias divergentes (preço ERP diferente a preço etiqueta) e quantidade de auditorias desconhecidas (preço da etiqueta não foi informado). No mesmo gráfico é possível também visualizar a percentagem correspondente de auditorias em cada uma das condições. As legendas deste gráfico são também dinâmicas.

No gráfico da direita é possível visualizar a quantidade de auditorias confirmadas e desconhecidas por cada nível da estrutura hierarquica.

![image alt <>](assets/cockpit_results_overview_priceaudit_resume.gif)

### **Visão Separação**

A "Visão Resultados" das tarefas de Separação permite ao utilizador ter visibilidade da informação da qualidade da execução das tarefas. 

#### Gráfico de resumo

No componente de resumo nas tarefas de Separação o Cockpit apresenta a informação dos artigos que foram separados na totalidade (legenda Total) e separados parcialmente (legenda Parcial).

No gráfico da esquerda apresenta esta informação de uma forma global a todas as tarefas mediante intervalo de datas e lojas selecionados. O gráfico da direita apresenta as separações agupadas por estrutra hierárquica.

![image alt <>](assets/cockpit_replenishprep_resumo.png)

### **Visão Reposição**

A "Visão Resultados" das tarefas de Reposição permite ao utilizador ter visibilidade da informação da qualidade da execução das tarefas. Tal como nas tarefas de Separação é possível consultar a informação das reposições totais e parciais. 

#### Gráfico de resumo

No componente de resumo nas tarefas de Reposição o Cockpit apresenta a informação dos artigos que foram repostos na totalidade (legenda Total) e repostos parcialmente (legenda Parcial).

No gráfico da esquerda apresenta esta informação de uma forma global a todas as tarefas mediante intervalo de datas e lojas selecionados. O gráfico da direita apresenta as reposições agupadas por estrutra hierárquica.

![image alt <>](assets/cockpit_replenish_resume.png)

## **Performance** 

A "Visão Performance" do Cockpit permite ao utilizador consultar informação gráfica relacionada com a performance de tarefas, produtos, colaboradores e lojas.

A informação apresentada nesta visão pode ser apresentada mediante agregação dia, semana, mês e ano e mediante intervalo de tempo previamente selecionado.

### **Tarefas**

O gráfico "Duração de tarefas" na "Visão Performance" apresenta o tempo médio das tarefas terminadas no intervalo de tempo selecionado. Aqui é visível a informação da tarefa com maior duração, a tarefa com menor duração e a informação de média por tarefa. 

![image alt <>](assets/cockpit_performance_tasks.gif)

### **Produtos**

O gráfico "Duração média por produto" na "Visão Performance" apresenta o tempo médio para tratar um produto no intervalo de tempo selecionado. Aqui é visível a informação do produto com maior duração, do produto com menor duração e a informação de média por produto. 

![image alt <>](assets/cockpit_performance_product.gif)


### **Colaboradores**

A "Visão Performance" apresenta a informação do "Top 10 dos colaboradores com melhor média por tarefa", "Top 10 das lojas com melhor média por produto" e uma listagem com "Tempos médios de todas as lojas".

O gráfico "Top 10 dos colaboradores com melhor média por tarefa" apresenta o tempo médio necessário para tratar uma tarefa por colaborador. 

O gráfico "Top 10 dos colaboradores com melhor média por produto" apresenta o tempo médio necessário para tratar um produto por colaborador.

A listagem de "Tempos médios de todos os colaboradores" apresenta o detalhe do tempo médio necessário para tratar tarefas e produtos por colaborador.

![image alt <>](assets/cockpit_performance_users.gif)

### **Loja**

A "Visão Performance" apresenta a informação do "Top 10 das lojas com melhor média por tarefa", "Top 10 dos colaboradores com melhor média por produto" e uma listagem com "Tempos médios de todos os colaboradores".

O gráfico "Top 10 das lojas com melhor média por tarefa" apresenta o tempo médio necessário para tratar uma tarefa por loja. 

O gráfico "Top 10 das lojas com melhor média por produto" apresenta o tempo médio necessário para tratar um produto por loja.

A listagem de "Tempos médios de todas as lojas" apresenta o detalhe do tempo médio necessário para tratar tarefas e produtos por loja.

![image alt <>](assets/cockpit_performance_stores.gif)

## **Inventários**

O módulo de Inventários no Cockpit é diferente do módulo das restantes tarefas. Neste módulo não existe as visões de resultados, execução e performance mas existe apenas uma visão única que permite consultar a informação de todos os inventários criados para um determinado intervalo de data e grupo de lojas selecionadas. 

O objetivo deste módulo é apresentar os inventários de uma forma agregada com a possibilidade de consulta do detalhe de zonas e leituras para cada inventário.

### **Inventários**

Quando o utilizador seleciona Inventário no menu do Cockpit é apresentada a listagem de todos os inventários criados para o intervalo de tempo e grupo de lojas selecionadas.

Nesta listagem existe a seguinte informação:

* **ID**: Informação do identificador do inventário enviado no momento da integração. Este identificador é único e enviado na integração.
* **Descrição**: Informação do nome do inventário enviado no momento da integração. 
* **Estado**: Informação do estado de inventário. O estado do inventário é inferido pelo estado global das zonas de inventário. Desta forma, se todas as zonas de inventário estiverem terminadas, o estado do inventário também será atualizado para "Terminada".
* **Data de finalização**: Informação da data de fecho de inventário.
* **Id loja**: Informação do identificador de loja em que o inventário foi criado no momento da integração.
* **Loja**: Informação no nome da loja em que o inventário foi criado no momento da integração.

Tal como nos restantes módulos do Cockpit, no módulo de Inventários é possível pesquisa através do facilitador de pesquisa presente na parte superior da tabela. 

Além da pesquisa é possível também selecionar as colunas informativas da tabela, filtrar as tarefa pelo seu estado e exportar essa informação mediante o facilitador de exportação de dados.

![image alt <>](assets/cockpit_inventory_inventories.gif)

Além da informação presente nesta tabela, o utilizador poderá consultar o detalhe de um respetivo inventário. Para isso basta clicar no nome do mesmo.

De forma imediata o utilizador é direcionado para o ecrã de detalhe do inventário onde é possível consultar a seguinte informação:

* **Inventário**: Informação do Id, descrição, estado e referência externa. A informação de referência externa identifica o identificador do inventário no ERP.
* **Resumo**: Na aba de resumo está disponível a informação referente à loja em que o inventário foi criado e a informação de tempos e colaboradores envolvidos na execução do mesmo. 
* **Zonas**: Na aba de zonas está disponível a informação de todas as zonas de contagem criadas para determinado inventário. Em cada zona está presente a informação do nome, id, o estado e a informação das leituras criadas para determinada zona.
* **Produtos**: Na aba de produtos está disponível a informação de todos produtos processados num determinado inventário. Em cada produto está presente a informação do nome, estado, sku, ean e quantidade.

Tal como ecrã anterior nesta listagem também é possível pesquisar, filtrar por estado, esconder ou apresentar colunas e extrair os dados.

![image alt <>](assets/cockpit_inventory_inventories_detail.gif)


### **Zonas de Contagem**

No módulo de inventários é possivel visualizar as zonas de contagem de cada inventário de uma forma agregada ou detalhada.

Para visualizar a informação de zonas de contagem de forma agregada, o utilizador deverá selecionar o agregador presente na parte esquerda da linha do inventário correspondente. Desta forma são apresentadas as zonas de contagem para o inventário selecionado e em cada zona é apresentada a informação nome, id, estado e informação das leituras de contagem correspondentes.

Para consultar o detalhe de uma zona de contagem, o utilizador deverá clicar sobre o nome da zona e o Cockpit direciona de imediato para o ecrã de detalhe. Aqui está presente a seguinte informação:

* **Zona**: Informação do Id, descrição, estado e Id inventário. 
* **Resumo**: Na aba de resumo está disponível a informação referente à loja em que a zona foi criada e a informação das leitura disponíveis para a respetiva zona. 
* **Tarefas**: Na aba de tarefas está disponível a informação de todas as leituras criadas dentro de determinada zona. Em cada leitura está presente a informação do nome, estado, quantidade de artigos tratados, não encontrados, início da tarefa, colaborador  e a informação da duração da leitura.
* **Produtos**: Na aba de produtos está disponível a informação de todos produtos processados numa determinada zona. Em cada produto está presente a informação do nome, estado, SKU, EAN e quantidade.

![image alt <>](assets/cockpit_inventory_zone_detail.gif)

### **Leituras**

Mediante integração de inventários é possível definir se cada zona de contagem criada tem apenas uma leitura ou duas leituras associadas. No caso de ter apenas uma leitura, quando esta é terminada a zona de contagem também termina automaticamente. 

Se existir duas leituras para determinada zona de contagem, a mesma só fica terminada se ambas as leituras estiverem fechadas e sem divergência, ou seja, sem artigos e quantidades diferentes entre as duas leituras. Se existir divergência é criada uma tarefa de correção de divergência que também fica automaticamente associada à zona de contagem.

Para aceder ao detalhe de cada leitura o utilizador deverá selecionar a respetiva zona de contagem e de seguida selecionar a leitura pretendida.

Neste ecrã surge a seguinte informação:

* **Tarefa**: Informação do Id, nome, descrição, tipo,  estado e Id inventário.
* **Detalhe**: Informação de loja, produtos e quantidades e tempos e colaboradores.
* **Produtos**: Informação da listagem de produtos processados na zona de contagem. Em cada produto está presente a informação do nome, estado, SKU, EAN e quantidade.

![image alt <>](assets/cockpit_inventory_reading_detail.gif)

## **Validades**

Para as tarefas que tratam datas de validade, o Cockpit apresenta um módulo específico que permite ao utilizador visualizar todas as datas de validade do intervalo de datas e grupo de lojas selecionadas de uma forma agregada ao SKU ou data de validade.

Este módulo está disponível na "Visão Execução" das tarefas na aba "Validades" junto às abas de "Tarefas" e "Produtos".

![image alt <>](assets/cockpit_validity_detail_all.gif)

### **Todas as datas**

Quando o utilizador seleciona a aba de "Validades", o Cockpit apresenta por default a visão de detalhe de todas as datas presentes em tarefas dentro do intervalo de tempo e grupo de lojas selecionadas.

Nesta listagem de datas de validade está presenta a seguinte informação:

* **SKU**: Informação do SKU do artigo associado à data de validade.
* **Data de Validade**: Informação da data de validade.
* **Tipo de Validade**: Informação do tipo de tarefa associada à data de validade. A data pode estar em período de depreciação ou retirada.
* **Dias a depreciar**: Informação do número de dias a depreciar para o artigo associado à data. 
* **Dias a retirar**: Informação do número de dias a retirar para o artigo associado à data. 
* **Quantidade**: Quantidade associada à data de validade.
* **Nome do produto**: Nome do artigo.
* **Estado do produto**: Estado do artigo associado à data de validade.
* **Tarefa**: Nome da tarefa onde se insere o artigo. 
* **Datas futuras**: Facilitador que permite pesquisar todas as datas associadas ao artigo.

Além desta informação está presente o facilitador de pesquisa, filtro de estado e exportação de dados.

![image alt <>](assets/cockpit_validity_detail_all_future_dates.gif)

### **Por SKU**

O módulo de validades no Cockpit permite visualizar a listagem de datas disponíveis agregadas por SKU. Para isto o utilizador deve selecionar "Por SKU" e de imediato é apresentada uma listagem de SKU's com a seguinte informação:

* **SKU**: Identificador do artigo.
* **Nome**: Nome do artigo.
* **Nº de datas**: Informação do número total de datas associadas ao SKU dentro do intervalo de datas e grupo de lojas selecionado.
* **Nº depreciação**: Informação do número total de depreciações associadas ao SKU dentro do intervalo de datas e grupo de lojas selecionado.
* **Nº retiradas**: Informação do número total de retiradas associadas ao SKU dentro do intervalo de datas e grupo de lojas selecionado.

Além desta agregação é possível consultar o detalhe de datas associadas a determinado SKU. Para isso basta clicar o facilitador de agregação presente na parte direita da linha.

Desta forma é apresentado ao utilizador o detalhe das datas associadas associadas ao SKU selecionado. No detalhe da data apresenta a seguinte informação:

* **Data de validade**: Informação da data de validade.
* **Tipo de validade**: Informação do tipo de validade associado à data de validade.
* **Dias a depreciar**: Informação do número de dias a depreciar para o artigo associado à data. 
* **Dias a retirar**: Informação do número de dias a retirar para o artigo associado à data. 
* **Quantidade**: Quantidade associada à data de validade.
* **Nome do produto**: Nome do artigo.
* **Estado do produto**: Estado do artigo associado à data de validade.
* **Tarefa**: Nome da tarefa onde se insere o artigo. 
* **Nome da loja**: Informação do nome da loja associada à tarefa. 

![image alt <>](assets/cockpit_validity_detail_sku.gif)

### **Por data**

O módulo de validades no Cockpit permite visualizar a informação de datas disponíveis agregadas por data. Para isto o utilizador deve selecionar "Por data" e de imediato é apresentada uma listagem de datas com a seguinte informação:

* **Data de validade**: Informação da data de validade.
* **Nº SKUs**: Informação da quantidade total de artigos associados a determinada data.
* **Nº depreciações**: Informação do número total de depreciações associadas à data de validade dentro do intervalo de datas e grupo de lojas selecionado.
* **Nº retiradas**: Informação do número total de retiradas associadas à data de validade dentro do intervalo de datas e grupo de lojas selecionado.

![image alt <>](assets/cockpit_validity_detail_date.gif)

## **Processos**

O Cockpit disponibiliza no menú uma área destinada aos processos. Nesta área é possivel visualizar a informação da percentagem de rutura associada ao Mapa de Varrimento e Mapa de Auditoria Externa. Da mesma forma é possível também consultar o detalhe das tarefas que compõem esses mesmos processos.

### **Listagem de Processos**

Para aceder à listagem de processos disponíveis o utilizador deve aceder a "Mapa de Varrimento" ou "Mapa de Auditoria Externa". De forma imediata o Cockpit apresenta a listagem de processos do tipo de tarefa selecionada.

Nesta listagem está presente a seguinte informação:

* **Nome**: Informação do nome da tarefa que origina o processo. O nome da tarefa original é também o nome do processo.
* **Id loja**: Informação do identificador de loja a que o processo está associado.
* **Na gama**: Informação da quantidade total de artigos previstos no processo e que pertencem à gama da respetiva loja.
* **Na estrutura**: Informação da quantidade total de artigos previstos no processo e que pertencem à estrutura hierárquica da respetiva loja.
* **Picados na estrutura**: Informação da quantidade total de artigos picados no processo e que pertencem à estrutura hierárquica da respetiva loja.
* **Picados fora da estrutura**: Informação da quantidade total de artigos picados no processo e que não pertencem à estrutura hierárquica da respetiva loja.
* **Com stock na estrutura**: Informação da quantidade total de artigos no processo com stock e que pertencem à estrutura hierárquica da respetiva loja.
* **Sem stock na estrutura**: Informação da quantidade total de artigos no processo sem stock e que pertencem à estrutura hierárquica da respetiva loja.
* **% Ruptura**: Cálculo da percentagem final da rutura no processo. Este cálculo é reprocessado no fecho de cada tarefa que influenciam a percentagem final da rutura. Exemplo: Auditoria de Varrimento e Verificação de Presença.

!!! note "Cáculo da rutura:"
    Quantidade total de artigos não picados na gama / qantidade total de artigos com stock na gama X 100. 



Nesta listagem estão presentes os facilitadores de pesquisa, o input de apresentação de informação e o facilitador de exportação de dados.

![image alt <>](assets/cockpit_process_list.gif)

### **Detalhe de processos**

Na listagem descrita no ponto anterior é possível visualizar o detalhe de cada processo. Para isso o utilizador deve clicar no agregador presente na parte esquerda da linha de cada processo.

Desta forma o Cockpit apresenta todas as tarefas que compõem determinado processo e em cada tarefa a seguinte informação:

* **Nome**: Informação do nome da tarefa que compõem o processo.
* **Tipo**: Informação do tipo de tarefa.
* **Estado**: Informação do estado da tarefa.
* **Previstos**: Informação da quantidade total de artigos previstos na tarefa.
* **Tratados**: Informação da quantidade total de artigos processados na tarefa.
* **Não encontrados**: Informação da quantidade total de artigos processados como não encontrados na tarefa.
* **Não tratados**: Informação da quantidade total de artigos não tratados na tarefa.
* **Colaborador**: Informação do colaborador que executou a tarefa.

Mediante este informação o utilizador pode consultar a quantidade total de artigos nas tarefas e visualizar a informação que compõem o cálculo da rutura.

Para aceder ao detalhe de cada umas das tarefas de um processo basta clicar no nome da tarefa e de imediato é direcionado para o respetivo detalhe. 

![image alt <>](assets/cockpit_process_detail.gif)

## **Ficha de artigo**

O componente "Ficha de artigo" no Cockpit permite consultar as informações respetivas a um determinado artigo de uma forma semelhante ao que é possível consultar na aplicação móvel. 

Para consultar a "Ficha de artigo" o utilizador deve identificar o artigo que pretende consultar e de seguida selecionar a opção "Ver ficha de artigo".

![image alt <>](assets/cockpit_item_file.gif)


No componente "Ficha de artigo" estão presentes as seguinte informações relacionadas com o artigo. As informações disponíveis são configuráveis por cliente o que significa que essa informação pode variar nos vários clientes. 

A informação base presente na "Ficha de artigo" é a seguinte:

* **Identificação de artigo**: Informação do nome do artigo, fotografia (se aplicável), estado, SKU e EAN.
* **Hierarquia**: Informação da estrutura mercadológica onde se insere o artigo.
* **EANs**: Informação dos EANs primários e secundários do artigo.
* **Preços**: Informação de preço ERP e POS (PDV).
* **Stock**: Informação do SOH para a loja associada ao artigo pesquisado.
* **Stock outras lojas**: Informação do SOH disponível em todas as lojas da organização. 

![image alt <>](assets/cockpit_item_file_detail.gif)








