<!--![image alt <>](assets/under_construction.png){:height="500px" width="500px"}-->

# Cockpit Settings 

Esta secção é dedicada à listagem de settings que influenciam o comportamento do Cockpit FE e Cockpit BE

## **cockpit-frontend-settings**

Type: ``object`` | Setting ao nível da **aplicação Cockpit**

O objetivo desta documentação é apresentar a setting referente ao frontend do Cockpit e todas as suas partes constituintes de forma detalhada. 

*[FE]: Front End 
*[BE]: Back End 

!!! note "Visão Global da Setting"
    ```json
    {
    "aggregations": {},
    "custom_messages": [],
    "language": "",
    "navigation": {},
    "options": {
        "actions": {},
        "datatable": {},
        "errors": false,
        "navigation": {},
        "polling": {},
        "search": {}
    },
    "overview": {
        "types": [],
        "processes": []
    },
    "routes": {
        "modules": {
        "type": {
            "paths": []
        }
        }
    }
    }
    ```

### **Aggregations**

O campo "aggregations" é usado para definir grupos de tipos de tarefa que terão uma visão agregada. Desta forma, todos os tipos de tarefa associados ao array "base" serão incluídos não 'Visão Base'.
Esta visão necessita de ser parametrizada no campo [navigation](#navigation).

!!! note "Exemplo" 
    ```json
    {
    "base": [
        "relabelling",
        "scanaudit"
    ]
    }
    ``` 


### **Custom messages**

Através da adição de codigos de tradução no array 'custom messages', permite definir traduções específicas que se irão sobrepor às traduções base da aplicação.  

!!! note "Exemplo" 
    ```json
    {
    "custom_messages": [
		"components-goal-standardreplenishprep-charts-legends-partial",
		"components-goal-standardreplenishprep-charts-legends-total",
		"components-goal-standardreplenishprep-charts-sub-titles-by-hierarchy",
		"components-goal-standardreplenishprep-charts-title",
		"components-goals-overview-tabs-standardreplenishprep-charts"
	],
    }
    ``` 

### **Language**

Define a linguagem default da aplicação. 

!!! note "Exemplo" 
    ```json
    {
    "language": "pt-PT",
    }
    ``` 

### **Navigation**

Define o menu de navegação da aplicação. Para este menu é possível definir o primeiro nível de navegação e um segundo nivel de navegação que será apresentado de forma agregada. 
O array "features" define as configurações disponíveis para cada tipo de tarefa.

!!! note "Exemplo de menu com 1 nível:"
    ```json
    {
    "features": [
        {
            "icon": "fa fa-tasks",
            "key": "execution",
            "path": "/modules/type/relabelling/execution"
        },
        {
            "icon": "fa fa-bullseye",
            "key": "goals",
            "path": "/modules/type/relabelling/goals"
        },
        {
            "icon": "fa fa-bar-chart",
            "key": "performance",
            "path": "/modules/type/relabelling/performance"
        }
    ],
    "key": "relabelling",
    "order": 1,
    "path": "/modules/type/relabelling"
    }
    ```

!!! note "Exemplo de menu com 2 niveis:"
    ```json
    {
    "childs": [
        {
        "features": [
            {
            "icon": "fa fa-tasks",
            "key": "execution",
            "path": "/modules/type/withdrawal/execution"
            }
        ],
        "key": "withdrawal",
        "order": 1,
        "path": "/modules/type/withdrawal"
        },
        {
        "features": [
            {
            "icon": "fa fa-bar-chart",
            "key": "performance",
            "path": "/modules/type/audvaldata/performance"
            }
        ],
        "key": "audvaldata",
        "order": 2,
        "path": "/modules/type/audvaldata"
        }
    ],
    "icon": "fa fa-calendar",
    "key": "expirations",
    "order": 1,
    "path": "/modules/overview"
    }
    ```

### **Options**

#### Actions

Define que ações estão disponíveis por cada estado da tarefa.

!!! note "Exemplo"
    ```json
    {
    "actions": {
        "cancel": [
        "A",
        "W",
        "AW"
        ],
        "finish": [
        "AW",
        "H"
        ],
        "release": [
        "W"
        ]
    }
    }
    ```

#### Datatable

Define as regras de controlo das tabelas de dados da aplicação. 

As colunas parametrizadas na setting (ex. "items_expected") apenas são ordenáveis se o status das tarefas forem diferentes dos status incluídos no array.

Por default todas as colunas são ordenáveis. 


!!! note "Exemplo"
    ````json
    {
    "sort": {
        "items_expected": [
        "A",
        "AW",
        "W",
        "P"
        ],
        "items_picked": [
        "A",
        "AW",
        "W",
        "P"
        ]
    }
    }
    ````

#### Errors

Define se ativa o módulo de erros apresentado no canto superior direito da aplicação (junto ao icone de definições da aplicação).

#### Navigation

Define a visualização e componentes dos tipos de tarefa parametrizados no campo [menu](#navigation)

<!--* "extractions_active": ``boolean``  - Ativa a extração de dados via FE (default: ``false``).
    * "extractions_comma": ``string`` - Define o separador de colunas no ficheiro de exportação (default: ``","``). -->

**Valores Possíveis:**
    
| Campo                        | Descrição  | Default  |
    | ----------------------------- | ------------ | -------- |
    | extractions_active            | Ativa a extração de dados via FE.  | false  |
    | extractions_comma             | Define o separador de colunas no ficheiro de exportação.  |  "," |
    | has_assortments               | Ativa na listagem de tarefas a informação específica relacionada com processos (Na gama, Fora da Gama, % Rutura, etc). <br> Apresenta os gráficos relacionados com os processos no detalhe das tarefas. <br> Apresenta a informação dos processos no detalhe das tarefas.  |  false |
    | has_containers                | Define se está ativa a visão "containers" na listagem de tarefas na visão "Execução" e no detalhe da tarefa.  |  false |
    | has_count_zone                | Define se está ativa a visão "zones" na listagem de tarefas na visão "Execução".  |  false |
    | has_consolidation             | Define se estão visiveis as informações relacionadas com pós-processamento no detalhe da tarefa.  |  false |
    | has_destination               | Define se as informações de ``destination_code`` e ``destination_value`` estão visíveis no detalhe da tarefa.  |  false |
    | has_expiration_dates          | Apresenta as datas de validade no detalhe de artigo.   |  false |
    | has_expiration_list           | Define se está ativa a visão "Validades" na visão "Execução".  |  false |
    | has_expiration_list_detail    | Define se a visão "Validades" inclui os agregadores "SKU" e "Data".   |  false |
    | has_recounts                  | Define se estão visiveis as informação de "recount" tanto na listagem de tarefas como no detalhe das mesmas.  |  false |
    | has_specificity               | Permite ativar as costumizações relacionadas com as colunas apresentadas por default na listagem de tarefas e artigos.  |  false |
    | has_zones                     | Adiciona filtros específicos que permitem navegação da visão de zonas na lista de tarefas com zonas.  |  false |
    | is_relabelling                | Adiciona colunas com informação específica dos processo de relabelling na listagem de artigos. <br> Adiciona gráficos específicos dos processos de relabelling no detalhe das tarefas. <br> Adiciona filtros específicos dos processos de relabelling no detalhe da tarefa e na visão "Resultados". <br> Adiciona informação específica dos processos de relabelling no detalhe das tarefas e artigos. |  false |
    | is_replenish                  | Adiciona colunas com informação específica dos processo de replenish na listagem de artigos. <br> Adiciona gráficos específicos dos processos de replenish no detalhe das tarefas. |  false |
    | is_reschedulable              | Adiciona filtro de reagendamento na listagem de tarefas. <br> Adiciona a ação de reagendamento no detalhe da tarefa. |  false |
    | show_attributes               | Ativa a informação de atributos no detalhe das tarefas. |  false |
    | show_expiration_dates_detail  | Ativa a funcionalidade de expansão de coluna para apresentação das datas de validade. |  false |
    | show_future_dates             | Enables expirations list row feature in order to display future dates modal. |  false |
    | show_resources_charts         | Ativa os gráficos relacionados com histórico de produtos e tarefas no detalhe das tarefas. |  false |
    | show_resources_list           | Ativa a visão "Produtos" na listagem das visões "Execução" e "Resultados". |  false |
    | show_goals_charts             | Ativa a os gráficos de "Resultados" na visão "Resultados".  |  false |
    | show_last_relabellings        | Ativa a funcionalidade presenta na listagem de artigos que permite consultar a ultima remarcação efetuada para determinado artigo.  |  false |
    | show_origin_fields            | Apresenta os campos ``origin_type`` e ``origin_id`` no detalhe da tarefa.  |  false |
    | show_previous_stock           | Apresenta o campo ``stock_previous`` no detalhe do artigo.  |  false |
    | show_stock_totals             | Apresenta os totalizadores de stock presente na parte inferior do gráfico Resumo na visão "Resultados". |  false |
    | show_trend_custom             | Adiciona os componentes ``Relabellings`` e ``Audits`` à tab "Histórico" do gráfico de Resultados.  |  false |
    | show_trend_items_avg_store    | Adiciona a informação da "média por loja" no gráfico de "Resultados" na visão "Produtos".  |  false |
    | show_warnings                 | Define se apresenta "alertas" na visão "Resultados" e no detalhe da tarefa.   |  false |

!!! note "Exemplo"
    ```json
    {
    "inventory": {
        "child": {
            "has_recounts": true,
            "has_specificity": true,
            "show_resources_list": true
        },
        "extractions_active": true,
        "has_count_zone": true,
        "has_recounts": true,
        "has_specificity": true,
        "has_zones": true,
        "parent": {
            "has_recounts": true,
            "has_specificity": true,
            "show_resources_list": true
            },
        "show_resources_list": true,
        "show_trend_items_avg_store": true,
        "search": {}
    }
    }
    ```

!!! note "Nota"
    * Todos as opções parametrizadas dentro do campo "child" apenas tem efeito nas tarefas "filhas" (tarefas com "parent_id").
    * Todas as opções parametrizadas dentro do campo "parent" apenas tem efeito nas tarefas "mãe" (tarefas com "has_children": true) 
    * Toas as opções parametrizadas dentro do objecto "search" apenas tem efeito na visão de detalhe da tarefa.


#### Polling 

Define o tempo (em segundos) que é feito o refresh automático da informação de "datas" e "notificações".  

**Valores Possíveis:**
    
| Campo      |  URL       | Detalhe | Default |
| ------------- | ----------------- | ----------- | ------- |
| dates         | "/base/openDates" | Serviço que retorna as tarefas possíveis nos intervalos de "Datas Passadas" e "Datas Futuras". | 60 |
| notifications | "/notifications"  | Pesquisa por notificações associadas a determinado user. | 30 |

!!! note "Exemplo"
    ```json
    {
    "polling": {
        "dates": 60,
        "notifications": 30
    }
    }
    ```

 
#### Search

Define o caracter possível de ser utilizado como facilitador de pesquisa.

**Valores Possíveis:**

| Campo | Detalhe                           | Default |
| ------------------------- | ------------------------------------- | ------- |
| wildcard      | Caracter usado como facilitador de pesquisa de lojas  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      | "%"     |


!!! note "Exemplo"
    ```json
    {
    "search": {
        "wildcard": "%"
    }
    }
    ```

### **Overview**

Define os tipos de tarefa e processos da aplicação.

**Valores Possíveis:**
    
| Campo      | Detalhe                                                                              |
| ------------- | ---------------------------------------------------------------------------------------- |
| types         | Tipos de tarefa utilizados para o request global da aplicação.    |
| processes     | Os códigos parametrizados serão considerados como processos e terá particularidades específicas.       |


!!! note "Exemplo"
    ```json
    {
    "types": [
        "relabelling",
        "scanaudit",
        "..."
    ],
    "processes": [
        "passortment",
        "..."
    ]
    }
    ```


### **Routes**

Define a navegação permitida com rotas definidas ao tipo de tarefa.

``cockpit/modules/type/:task_type/``

!!! note "Exemplo"
    ```json
    {
    "routes": {
        "modules": {
        "type": {
            "paths": [
            "relabelling",
            "scanaudit",
            "..."
            ]
        }
        }
    }
    }
    ```

??? example "Parametrização exemplo:"
    ```json
    {
        "aggregations": {
            "base": [
                "relabelling",
                "priceup",
                "pricedown",
                "promotionin",
                "promotionout",
                "carddiscount",
                "stockoutaudit",
                "replenishprep",
                "standardreplenishprep",
                "replenish",
                "receiving",
                "presencecheck",
                "scanaudit",
                "stockcount",
                "priceaudit",
                "checklist",
                "buyeraudit",
                "presenceaudit",
                "withdrawal",
                "audvaldata",
                "audvaldath",
                "expirationcontrol",
                "expirationpicking",
                "externalaudit",
                "transfer",
                "damage",
                "itemreturn"
            ],
            "expiration": [
                "withdrawal",
                "audvaldata",
                "audvaldath",
                "expirationcontrol",
                "expirationpicking"
            ],
            "price": [
                "relabelling",
                "priceup",
                "pricedown",
                "promotionin",
                "promotionout",
                "carddiscount"
            ]
        },
        "custom_messages": [
            "components-goal-standardreplenishprep-charts-legends-partial",
            "components-goal-standardreplenishprep-charts-legends-total",
            "components-goal-standardreplenishprep-charts-sub-titles-by-hierarchy",
            "components-goal-standardreplenishprep-charts-title",
            "components-goals-overview-tabs-standardreplenishprep-charts"
        ],
        "language": "pt-PT",
        "navigation": {
            "principal": [
                {
                    "icon": "fa fa-globe",
                    "key": "base",
                    "order": 1,
                    "path": "/modules/overview/base"
                },
                {
                    "childs": [
                        {
                            "key": "price",
                            "order": 1,
                            "path": "/modules/overview/price"
                        },
                        {
                            "features": [
                                {
                                    "icon": "fa fa-tasks",
                                    "key": "execution",
                                    "path": "/modules/type/relabelling/execution"
                                },
                                {
                                    "icon": "fa fa-bullseye",
                                    "key": "goals",
                                    "path": "/modules/type/relabelling/goals"
                                },
                                {
                                    "icon": "fa fa-bar-chart",
                                    "key": "performance",
                                    "path": "/modules/type/relabelling/performance"
                                }
                            ],
                            "key": "relabelling",
                            "order": 1,
                            "path": "/modules/type/relabelling"
                        },
                        {
                            "features": [
                                {
                                    "icon": "fa fa-tasks",
                                    "key": "execution",
                                    "path": "/modules/type/priceup/execution"
                                },
                                {
                                    "icon": "fa fa-bullseye",
                                    "key": "goals",
                                    "path": "/modules/type/priceup/goals"
                                },
                                {
                                    "icon": "fa fa-bar-chart",
                                    "key": "performance",
                                    "path": "/modules/type/priceup/performance"
                                }
                            ],
                            "key": "priceup",
                            "order": 2,
                            "path": "/modules/type/priceup"
                        },
                        {
                            "features": [
                                {
                                    "icon": "fa fa-tasks",
                                    "key": "execution",
                                    "path": "/modules/type/pricedown/execution"
                                },
                                {
                                    "icon": "fa fa-bullseye",
                                    "key": "goals",
                                    "path": "/modules/type/pricedown/goals"
                                },
                                {
                                    "icon": "fa fa-bar-chart",
                                    "key": "performance",
                                    "path": "/modules/type/pricedown/performance"
                                }
                            ],
                            "key": "pricedown",
                            "order": 3,
                            "path": "/modules/type/pricedown"
                        },
                        {
                            "features": [
                                {
                                    "icon": "fa fa-tasks",
                                    "key": "execution",
                                    "path": "/modules/type/promotionin/execution"
                                },
                                {
                                    "icon": "fa fa-bullseye",
                                    "key": "goals",
                                    "path": "/modules/type/promotionin/goals"
                                },
                                {
                                    "icon": "fa fa-bar-chart",
                                    "key": "performance",
                                    "path": "/modules/type/promotionin/performance"
                                }
                            ],
                            "key": "promotionin",
                            "order": 4,
                            "path": "/modules/type/promotionin"
                        },
                        {
                            "features": [
                                {
                                    "icon": "fa fa-tasks",
                                    "key": "execution",
                                    "path": "/modules/type/promotionout/execution"
                                },
                                {
                                    "icon": "fa fa-bullseye",
                                    "key": "goals",
                                    "path": "/modules/type/promotionout/goals"
                                },
                                {
                                    "icon": "fa fa-bar-chart",
                                    "key": "performance",
                                    "path": "/modules/type/promotionout/performance"
                                }
                            ],
                            "key": "promotionout",
                            "order": 5,
                            "path": "/modules/type/promotionout"
                        },
                        {
                            "features": [
                                {
                                    "icon": "fa fa-tasks",
                                    "key": "execution",
                                    "path": "/modules/type/carddiscount/execution"
                                },
                                {
                                    "icon": "fa fa-bullseye",
                                    "key": "goals",
                                    "path": "/modules/type/carddiscount/goals"
                                },
                                {
                                    "icon": "fa fa-bar-chart",
                                    "key": "performance",
                                    "path": "/modules/type/carddiscount/performance"
                                }
                            ],
                            "key": "carddiscount",
                            "order": 6,
                            "path": "/modules/type/carddiscount"
                        }
                    ],
                    "icon": "fa fa-tags",
                    "key": "relabellings",
                    "order": 2,
                    "path": "/modules/overview"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/priceaudit/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/priceaudit/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/priceaudit/performance"
                        }
                    ],
                    "icon": "fa fa-eye",
                    "key": "priceaudit",
                    "order": 3,
                    "path": "/modules/type/priceaudit"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/stockcount/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/stockcount/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/stockcount/performance"
                        }
                    ],
                    "icon": "fa fa-th",
                    "key": "stockcount",
                    "order": 4,
                    "path": "/modules/type/stockcount"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/inventory/execution"
                        }
                    ],
                    "icon": "fa fa-cubes",
                    "key": "inventory",
                    "order": 5,
                    "path": "/modules/inventories"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/stockoutaudit/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/stockoutaudit/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/stockoutaudit/performance"
                        }
                    ],
                    "icon": "fa fa-list-ol",
                    "key": "stockoutaudit",
                    "order": 6,
                    "path": "/modules/type/stockoutaudit"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/damage/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/damage/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/damage/performance"
                        }
                    ],
                    "icon": "fa fa-long-arrow-down",
                    "key": "damage",
                    "order": 7,
                    "path": "/modules/type/damage"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/replenishprep/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/replenishprep/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/replenishprep/performance"
                        }
                    ],
                    "icon": "fa fa-arrows-h",
                    "key": "replenishprep",
                    "order": 8,
                    "path": "/modules/type/replenishprep"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/standardreplenishprep/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/standardreplenishprep/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/standardreplenishprep/performance"
                        }
                    ],
                    "icon": "fa fa-arrows-h",
                    "key": "standardreplenishprep",
                    "order": 9,
                    "path": "/modules/type/standardreplenishprep"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/replenish/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/replenish/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/replenish/performance"
                        }
                    ],
                    "icon": "fa fa-long-arrow-right",
                    "key": "replenish",
                    "order": 10,
                    "path": "/modules/type/replenish"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/receiving/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/receiving/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/receiving/performance"
                        }
                    ],
                    "icon": "fa fa-download",
                    "key": "receiving",
                    "order": 11,
                    "path": "/modules/type/receiving"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/transfer/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/transfer/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/transfer/performance"
                        }
                    ],
                    "icon": "fa fa-exchange",
                    "key": "transfer",
                    "order": 12,
                    "path": "/modules/type/transfer"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/itemreturn/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/itemreturn/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/itemreturn/performance"
                        }
                    ],
                    "icon": "fa fa-long-arrow-left",
                    "key": "itemreturn",
                    "order": 13,
                    "path": "/modules/type/itemreturn"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/checklist/execution"
                        }
                    ],
                    "icon": "fa fa-check-square-o",
                    "key": "checklist",
                    "order": 14,
                    "path": "/modules/type/checklist"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/scanaudit/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/scanaudit/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/scanaudit/performance"
                        }
                    ],
                    "icon": "fa fa-barcode",
                    "key": "scanaudit",
                    "order": 15,
                    "path": "/modules/type/scanaudit"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/externalaudit/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/externalaudit/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/externalaudit/performance"
                        }
                    ],
                    "icon": "fa fa-money",
                    "key": "externalaudit",
                    "order": 16,
                    "path": "/modules/type/externalaudit"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/presenceaudit/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/presenceaudit/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/presenceaudit/performance"
                        }
                    ],
                    "icon": "fa fa-check-circle",
                    "key": "presenceaudit",
                    "order": 17,
                    "path": "/modules/type/presenceaudit"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/buyeraudit/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/buyeraudit/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/buyeraudit/performance"
                        }
                    ],
                    "icon": "fa fa-money",
                    "key": "buyeraudit",
                    "order": 18,
                    "path": "/modules/type/buyeraudit"
                },
                {
                    "features": [
                        {
                            "icon": "fa fa-tasks",
                            "key": "execution",
                            "path": "/modules/type/presencecheck/execution"
                        },
                        {
                            "icon": "fa fa-bullseye",
                            "key": "goals",
                            "path": "/modules/type/presencecheck/goals"
                        },
                        {
                            "icon": "fa fa-bar-chart",
                            "key": "performance",
                            "path": "/modules/type/presencecheck/performance"
                        }
                    ],
                    "icon": "fa fa-check",
                    "key": "presencecheck",
                    "order": 19,
                    "path": "/modules/type/presencecheck"
                },
                {
                    "childs": [
                        {
                            "features": [
                                {
                                    "icon": "fa fa-tasks",
                                    "key": "execution",
                                    "path": "/modules/type/withdrawal/execution"
                                },
                                {
                                    "icon": "fa fa-bullseye",
                                    "key": "goals",
                                    "path": "/modules/type/withdrawal/goals"
                                },
                                {
                                    "icon": "fa fa-bar-chart",
                                    "key": "performance",
                                    "path": "/modules/type/withdrawal/performance"
                                }
                            ],
                            "key": "withdrawal",
                            "order": 1,
                            "path": "/modules/type/withdrawal"
                        },
                        {
                            "features": [
                                {
                                    "icon": "fa fa-tasks",
                                    "key": "execution",
                                    "path": "/modules/type/audvaldata/execution"
                                },
                                {
                                    "icon": "fa fa-bullseye",
                                    "key": "goals",
                                    "path": "/modules/type/audvaldata/goals"
                                },
                                {
                                    "icon": "fa fa-bar-chart",
                                    "key": "performance",
                                    "path": "/modules/type/audvaldata/performance"
                                }
                            ],
                            "key": "audvaldata",
                            "order": 2,
                            "path": "/modules/type/audvaldata"
                        },
                        {
                            "features": [
                                {
                                    "icon": "fa fa-tasks",
                                    "key": "execution",
                                    "path": "/modules/type/audvaldath/execution"
                                },
                                {
                                    "icon": "fa fa-bullseye",
                                    "key": "goals",
                                    "path": "/modules/type/audvaldath/goals"
                                },
                                {
                                    "icon": "fa fa-bar-chart",
                                    "key": "performance",
                                    "path": "/modules/type/audvaldath/performance"
                                }
                            ],
                            "key": "audvaldath",
                            "order": 3,
                            "path": "/modules/type/audvaldath"
                        },
                        {
                            "features": [
                                {
                                    "icon": "fa fa-tasks",
                                    "key": "execution",
                                    "path": "/modules/type/expirationcontrol/execution"
                                },
                                {
                                    "icon": "fa fa-bullseye",
                                    "key": "goals",
                                    "path": "/modules/type/expirationcontrol/goals"
                                },
                                {
                                    "icon": "fa fa-bar-chart",
                                    "key": "performance",
                                    "path": "/modules/type/expirationcontrol/performance"
                                }
                            ],
                            "key": "expirationcontrol",
                            "order": 4,
                            "path": "/modules/type/expirationcontrol"
                        },
                        {
                            "features": [
                                {
                                    "icon": "fa fa-tasks",
                                    "key": "execution",
                                    "path": "/modules/type/expirationpicking/execution"
                                },
                                {
                                    "icon": "fa fa-bullseye",
                                    "key": "goals",
                                    "path": "/modules/type/expirationpicking/goals"
                                },
                                {
                                    "icon": "fa fa-bar-chart",
                                    "key": "performance",
                                    "path": "/modules/type/expirationpicking/performance"
                                }
                            ],
                            "key": "expirationpicking",
                            "order": 5,
                            "path": "/modules/type/expirationpicking"
                        }
                    ],
                    "icon": "fa fa-calendar",
                    "key": "expirations",
                    "order": 20,
                    "path": "/modules/overview"
                },
                {
                    "header": true,
                    "key": "processes",
                    "order": 21
                },
                {
                    "key": "procscanaudit",
                    "order": 22,
                    "path": "/modules/processes/type/procscanaudit"
                },
                {
                    "key": "procexternalaudit",
                    "order": 23,
                    "path": "/modules/processes/type/procexternalaudit"
                }
            ]
        },
        "options": {
            "actions": {
                "cancel": [
                    "A",
                    "W",
                    "AW"
                ],
                "finish": [
                    "AW",
                    "H"
                ],
                "release": [
                    "W"
                ]
            },
            "datatable": {
                "sort": {
                    "items_expected": [
                        "A",
                        "AW",
                        "W",
                        "P"
                    ],
                    "items_picked": [
                        "A",
                        "AW",
                        "W",
                        "P"
                    ]
                }
            },
            "navigation": {
                "audvaldata": {
                    "extractions_active": true,
                    "has_expiration_dates": true,
                    "has_specificity": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "audvaldath": {
                    "extractions_active": true,
                    "has_expiration_dates": true,
                    "has_specificity": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "buyeraudit": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "carddiscount": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "is_relabelling": true,
                    "show_goals_charts": true,
                    "show_previous_stock": true,
                    "show_resources_charts": true,
                    "show_resources_list": true,
                    "show_stock_totals": true,
                    "show_trend_custom": true,
                    "show_trend_items_avg_store": true,
                    "show_warnings": true
                },
                "checklist": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "damage": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "expirationcontrol": {
                    "extractions_active": true,
                    "has_expiration_dates": true,
                    "has_expiration_list": true,
                    "has_expiration_list_detail": true,
                    "has_specificity": true,
                    "is_reschedulable": true,
                    "search": {
                        "has_expiration_list": true,
                        "has_expiration_list_detail": true
                    },
                    "show_expiration_dates_detail": true,
                    "show_future_dates": true,
                    "show_origin_fields": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "expirationpicking": {
                    "extractions_active": true,
                    "has_expiration_dates": true,
                    "has_expiration_list": true,
                    "has_expiration_list_detail": true,
                    "has_specificity": true,
                    "search": {
                        "has_expiration_list": true,
                        "has_expiration_list_detail": true
                    },
                    "show_expiration_dates_detail": true,
                    "show_future_dates": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "externalaudit": {
                    "child": {
                        "show_attributes": true,
                        "show_resources_list": true
                    },
                    "extractions_active": true,
                    "has_assortments": true,
                    "has_consolidation": true,
                    "has_count_map": true,
                    "has_count_zone": true,
                    "has_specificity": true,
                    "has_zones": true,
                    "parent": {
                        "has_consolidation": true,
                        "has_specificity": true,
                        "show_attributes": true,
                        "show_resources_charts": true,
                        "show_resources_list": true
                    },
                    "show_attributes": true,
                    "show_goals_charts": true,
                    "show_resources_charts": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "inventory": {
                    "child": {
                        "has_specificity": true,
                        "show_resources_list": true
                    },
                    "extractions_active": true,
                    "has_count_map": true,
                    "has_count_zone": true,
                    "has_specificity": true,
                    "has_zones": true,
                    "parent": {
                        "has_specificity": true,
                        "show_resources_list": true
                    },
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "itemreturn": {
                    "extractions_active": true,
                    "has_containers": true,
                    "has_destination": true,
                    "has_specificity": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "overview": {
                    "extractions_active": true,
                    "extractions_comma": ";"
                },
                "passortment": {
                    "extractions_active": true,
                    "has_assortments": true,
                    "has_consolidation": true,
                    "has_specificity": true,
                    "parent": {
                        "has_consolidation": true,
                        "has_specificity": true,
                        "show_attributes": true,
                        "show_resources_charts": true,
                        "show_resources_list": true
                    },
                    "show_attributes": true,
                    "show_resources_charts": true,
                    "show_resources_list": true
                },
                "presenceaudit": {
                    "extractions_active": true,
                    "has_assortments": true,
                    "has_consolidation": true,
                    "has_specificity": true,
                    "show_goals_charts": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "presencecheck": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "show_origin_fields": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "priceaudit": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "show_goals_charts": true,
                    "show_resources_charts": true,
                    "show_resources_list": true,
                    "show_trend_custom": true,
                    "show_trend_items_avg_store": true,
                    "show_warnings": true
                },
                "pricedown": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "is_relabelling": true,
                    "show_goals_charts": true,
                    "show_previous_stock": true,
                    "show_resources_charts": true,
                    "show_resources_list": true,
                    "show_stock_totals": true,
                    "show_trend_custom": true,
                    "show_trend_items_avg_store": true,
                    "show_warnings": true
                },
                "priceup": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "is_relabelling": true,
                    "show_goals_charts": true,
                    "show_previous_stock": true,
                    "show_resources_charts": true,
                    "show_resources_list": true,
                    "show_stock_totals": true,
                    "show_trend_custom": true,
                    "show_trend_items_avg_store": true,
                    "show_warnings": true
                },
                "procexternalaudit": {
                    "extractions_active": true,
                    "has_assortments": true,
                    "has_consolidation": true,
                    "has_specificity": true,
                    "parent": {
                        "has_consolidation": true,
                        "has_specificity": true,
                        "show_attributes": true,
                        "show_resources_charts": true,
                        "show_resources_list": true
                    },
                    "show_attributes": true,
                    "show_resources_charts": true,
                    "show_resources_list": true
                },
                "procscanaudit": {
                    "extractions_active": true,
                    "has_assortments": true,
                    "has_consolidation": true,
                    "has_specificity": true,
                    "parent": {
                        "has_consolidation": true,
                        "has_specificity": true,
                        "show_attributes": true,
                        "show_resources_charts": true,
                        "show_resources_list": true
                    },
                    "show_attributes": true,
                    "show_resources_charts": true,
                    "show_resources_list": true
                },
                "promotionin": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "is_relabelling": true,
                    "show_goals_charts": true,
                    "show_previous_stock": true,
                    "show_resources_charts": true,
                    "show_resources_list": true,
                    "show_stock_totals": true,
                    "show_trend_custom": true,
                    "show_trend_items_avg_store": true,
                    "show_warnings": true
                },
                "promotionout": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "is_relabelling": true,
                    "show_goals_charts": true,
                    "show_previous_stock": true,
                    "show_resources_charts": true,
                    "show_resources_list": true,
                    "show_stock_totals": true,
                    "show_trend_custom": true,
                    "show_trend_items_avg_store": true,
                    "show_warnings": true
                },
                "receiving": {
                    "extractions_active": true,
                    "has_containers": true,
                    "has_expiration_dates": true,
                    "has_specificity": true,
                    "search": {
                        "has_expiration_list": true,
                        "has_expiration_list_detail": true
                    },
                    "show_attributes": true,
                    "show_expiration_dates_detail": true,
                    "show_future_dates": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "recounting": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "show_resources_list": true
                },
                "relabelling": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "is_relabelling": true,
                    "is_reschedulable": true,
                    "show_goals_charts": true,
                    "show_previous_stock": true,
                    "show_resources_charts": true,
                    "show_resources_list": true,
                    "show_stock_totals": true,
                    "show_trend_custom": true,
                    "show_trend_items_avg_store": true,
                    "show_warnings": true
                },
                "replenish": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "is_replenish": true,
                    "show_goals_charts": true,
                    "show_origin_fields": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "replenishprep": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "is_replenish": true,
                    "show_goals_charts": true,
                    "show_origin_fields": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "scanaudit": {
                    "child": {
                        "show_attributes": true,
                        "show_resources_list": true
                    },
                    "extractions_active": true,
                    "has_assortments": true,
                    "has_count_map": true,
                    "has_count_zone": true,
                    "has_specificity": true,
                    "has_zones": true,
                    "parent": {
                        "has_specificity": true,
                        "show_attributes": true,
                        "show_resources_charts": true,
                        "show_resources_list": true
                    },
                    "show_attributes": true,
                    "show_goals_charts": true,
                    "show_resources_charts": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "standardreplenishprep": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "is_replenish": true,
                    "show_goals_charts": true,
                    "show_origin_fields": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "stockcount": {
                    "child": {
                        "show_resources_list": true
                    },
                    "extractions_active": true,
                    "has_count_map": true,
                    "has_count_zone": true,
                    "has_specificity": true,
                    "has_zones": true,
                    "parent": {
                        "show_resources_list": true
                    },
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "stockoutaudit": {
                    "extractions_active": true,
                    "has_specificity": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "transfer": {
                    "extractions_active": true,
                    "has_containers": true,
                    "has_destination": true,
                    "has_specificity": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                },
                "withdrawal": {
                    "extractions_active": true,
                    "has_expiration_dates": true,
                    "has_specificity": true,
                    "show_resources_list": true,
                    "show_trend_items_avg_store": true
                }
            },
            "polling": {
                "dates": 60,
                "notifications": 30
            },
            "product": {
                "currency": "€",
                "enabled": true,
                "nav_components": {
                    "product_price_history": {
                        "enabled": true
                    },
                    "product_promotions": {
                        "enabled": true
                    },
                    "product_replenishment": {
                        "enabled": true
                    },
                    "product_stock": {
                        "enabled": true
                    },
                    "product_stock_others": {
                        "chain": true,
                        "enabled": true
                    },
                    "product_stock_store_exposition": {
                        "enabled": true
                    },
                    "product_suppliers": {
                        "enabled": true
                    }
                }
            },
            "search": {
                "wildcard": "%"
            }
        },
        "overview": {
            "processes": [
                "procscanaudit",
                "procexternalaudit",
                "procgeneric"
            ],
            "types": [
                "relabelling",
                "stockoutaudit",
                "replenishprep",
                "standardreplenishprep",
                "replenish",
                "receiving",
                "presencecheck",
                "scanaudit",
                "externalaudit",
                "stockcount",
                "priceaudit",
                "checklist",
                "buyeraudit",
                "presenceaudit",
                "withdrawal",
                "audvaldata",
                "audvaldath",
                "priceup",
                "pricedown",
                "promotionin",
                "promotionout",
                "carddiscount",
                "expirationpicking",
                "expirationcontrol",
                "transfer",
                "damage",
                "itemreturn"
            ]
        },
        "routes": {
            "modules": {
                "type": {
                    "paths": [
                        "relabelling",
                        "stockoutaudit",
                        "replenishprep",
                        "standardreplenishprep",
                        "replenish",
                        "receiving",
                        "presencecheck",
                        "scanaudit",
                        "externalaudit",
                        "stockcount",
                        "priceaudit",
                        "checklist",
                        "buyeraudit",
                        "presenceaudit",
                        "withdrawal",
                        "audvaldata",
                        "audvaldath",
                        "expirationpicking",
                        "expirationcontrol",
                        "priceup",
                        "pricedown",
                        "promotionin",
                        "promotionout",
                        "carddiscount",
                        "transfer",
                        "damage",
                        "itemreturn"
                    ]
                }
            }
        }
    }
    ```

## **cockpit-backend-settings**

Type: ``object`` | Setting ao nível da **aplicação Instore**

Define os parametros que influenciam o service-task que retornam as tarefas para a aplicação Instore.

???+ example "Valores Possíveis:"
    * "elastic": ``string`` - Define a versão do elastic.
    * "options": ``object`` - Define algumas opções. Campos possíveis:
        * "rectify": ``object`` - Define que para os campos ativos (true) o Cockpit vai pesquisar no elastic mais informação e envia ao nível da tarefa.
    * "future_dates": ``number`` - Define a quantidade de dias que pesquisa no intervalo de tarefas futuras
    * "scheduled_start": ``object`` - Define os dias passados e dias futuros das tarefas que envia para a app. Se definido ``gte: 0``, a app Instore apenas receberá tarefas do dia presente não receberá tarefas de dias futuros.

    **Exemplo**
    ```json
    {
	"elastic": "env_org_cockpit_type_*",
	"options": {
            "rectify": {
                "children": false,
                "resources": true
            },
            "future_dates": 365,
            "scheduled_start": {
                "gte": 0,
                "lte": 5
            }
	    }
    }
    ```

## **cockpit-backend-settings**

Type: ``object`` | Setting ao nível da **aplicação Cockpit**

Define os parametros que influenciam o service-task que retornam as tarefas para a aplicação Cockpit.

???+ example "Valores Possíveis:"
    * "elastic": ``string`` - Define a versão do elastic.
    * "options": ``object`` - Define algumas opções. Campos possíveis:
        * "rectify": ``object`` - Define que para os campos ativos (true) o Cockpit vai pesquisar no elastic mais informação e envia ao nível da tarefa.
        * "hierarchies": ``object`` - Define o nivel da estrutura hierarquica apresentada nos gráficos na visão "Resultados".
        * "elastic": ``object`` - Define parametrizações possíveis na consulta ao elastic como ``default_scroll``, ``default_scroll_size``, ``index_mask``, etc... 
    * "data": ``object`` - O campo "inconsistent" (``boolean``) quando ativo (valor: ``true``) define que  consulta no elastic os resources das tarefas em estado A, AW e P.
    * "categories": ``object`` - Define o nível da estrutura em todas as querys que filtrem por categories. 
    * "endpoints": ``object`` - Define os possíveis endpoints para os campos parametrizados. Neste momento apenas existe o campo "gat" criado. Este campo é utilizado para aceder à aplicação "Gestão de Usuários". 
    * "messages": ``array string`` - Define as mensagens utilizadas na aplicação cockpit e criadas na tabela messages.

    **Exemplo**
    ```json
        {
        "categories": {
            "level": 3
        },
        "data": {
            "inconsistent": true
        },
        "elastic": "env_org_cockpit_type_*",
        "endpoints": {
            "gat": {
                "portal": "",
                "token": ""
            }
        },
        "messages": [
            "resource-item-relabelling-d",
            "resource-item-relabelling-e",
            "resource-item-relabelling-u",
            "resource-item-status-c",
            "resource-item-status-e",
            "resource-item-status-p",
            "resource-item-status-nf",
            "resource-item-status-nh",
            "resource-item-status-a",
            "resource-item-status-uck",
            "resource-item-status-ck",
            "resource-item-stock-a",
            "resource-item-stock-e",
            "resource-item-stock-f",
            "resource-item-stock-o",
            "resource-item-stock-p",
            "resource-item-stock-u",
            "resource-items-status-c",
            "resource-items-status-p",
            "resource-items-status-nf",
            "resource-items-status-nh",
            "resource-items-status-a",
            "resource-items-status-e",
            "resource-items-status-uck",
            "resource-items-status-ck",
            "resource-picking-mode-container",
            "resource-picking-mode-item",
            "task-status-w",
            "task-status-h",
            "task-status-a",
            "task-status-aw",
            "task-status-c",
            "task-status-f",
            "task-status-e",
            "task-status-p",
            "tasks-status-w",
            "tasks-status-h",
            "tasks-status-a",
            "tasks-status-aw",
            "tasks-status-c",
            "tasks-status-f",
            "tasks-status-e",
            "tasks-status-p",
            "task-type-base",
            "task-type-price",
            "task-type-expiration",
            "task-type-relabelling",
            "task-type-relabellings",
            "task-type-stockoutaudit",
            "task-type-replenishprep",
            "task-type-replenish",
            "task-type-receiving",
            "task-type-scanaudit",
            "task-type-externalaudit",
            "task-type-presencecheck",
            "task-type-stockcount",
            "task-type-transfer",
            "task-type-priceup",
            "task-type-pricedown",
            "task-type-promotionin",
            "task-type-promotionout",
            "task-type-passortment",
            "task-type-processes",
            "task-type-carddiscount",
            "task-type-expirations",
            "task-type-expirationcontrol",
            "task-type-withdrawal",
            "task-type-expirationpicking",
            "task-type-priceaudit",
            "task-type-checklist",
            "task-type-priorityrelabelling",
            "task-type-buyeraudit",
            "task-type-presenceaudit",
            "task-type-audvaldata",
            "task-type-audvaldath",
            "task-type-inventory",
            "task-type-damage",
            "task-type-standardreplenishprep",
            "task-type-itemreturn",
            "task-type-procscanaudit",
            "task-type-procexternalaudit",
            "cockpit-analysis-execution",
            "cockpit-analysis-goals",
            "cockpit-analysis-performance",
            "cockpit-presence-check-true",
            "cockpit-presence-check-false",
            "destination-code-store",
            "destination-code-warehouse",
            "destination-code-supplier",
            "expiration-type-depreciation",
            "expiration-type-withdrawal",
            "item-status-a",
            "item-status-i",
            "item-status-d",
            "base-true",
            "base-false",
            "checklist-answer-n",
            "checklist-answer-u",
            "checklist-answer-c",
            "checklist-answer-type-c",
            "checklist-answer-type-y",
            "checklist-answer-y",
            "destination-code-store",
            "destination-code-warehouse",
            "destination-code-supplier",
            "ckp-nav-base",
            "ckp-nav-price",
            "ckp-nav-relabelling",
            "ckp-nav-relabellings",
            "ckp-nav-stockoutaudit",
            "ckp-nav-replenishprep",
            "ckp-nav-replenish",
            "ckp-nav-receiving",
            "ckp-nav-scanaudit",
            "ckp-nav-externalaudit",
            "ckp-nav-presencecheck",
            "ckp-nav-stockcount",
            "ckp-nav-transfer",
            "ckp-nav-priceup",
            "ckp-nav-pricedown",
            "ckp-nav-promotionin",
            "ckp-nav-promotionout",
            "ckp-nav-carddiscount",
            "ckp-nav-expirations",
            "ckp-nav-expirationcontrol",
            "ckp-nav-withdrawal",
            "ckp-nav-expirationpicking",
            "ckp-nav-priceaudit",
            "ckp-nav-processes",
            "ckp-nav-passortment",
            "ckp-nav-checklist",
            "ckp-nav-feature-execution",
            "ckp-nav-feature-goals",
            "ckp-nav-feature-performance",
            "ckp-nav-gat",
            "ckp-nav-priorityrelabelling",
            "ckp-nav-buyeraudit",
            "ckp-nav-presenceaudit",
            "ckp-nav-audvaldata",
            "ckp-nav-audvaldath",
            "ckp-nav-inventory",
            "ckp-nav-damage",
            "ckp-nav-standardreplenishprep",
            "ckp-nav-itemreturn",
            "ckp-nav-procscanaudit",
            "ckp-nav-procexternalaudit",
            "resource-price-divergence-status-u",
            "resource-price-divergence-status-d",
            "resource-price-divergence-status-c",
            "resource-replenish-status-t",
            "resource-replenish-status-p",
            "task-type-recepdir",
            "task-type-recepcen",
            "task-type-receptsf",
            "task-type-recepcs",
            "ckp-nav-recepcen",
            "ckp-nav-recepdir",
            "ckp-nav-receptsf",
            "ckp-nav-recepcs",
            "ckp-nav-receptions",
            "missing-category",
            "attribute-code-container_type",
            "attribute-code-document_id",
            "attribute-code-document_type",
            "attribute-code-driver_id",
            "attribute-code-expiration_control",
            "attribute-code-expiration_picking",
            "attribute-code-location",
            "attribute-code-mandatory_final_price",
            "attribute-code-mandatory_read",
            "attribute-code-pair",
            "attribute-code-var_weight",
            "attribute-code-vehicle_id",
            "attribute-value-false",
            "attribute-value-no",
            "attribute-value-safety",
            "attribute-value-store",
            "attribute-value-true",
            "attribute-value-warehouse",
            "attribute-value-yes",
            "process-type-procgeneric",
            "process-type-procscanaudit",
            "process-type-procexternalaudit",
            "item-type-procgeneric",
            "item-type-procscanaudit",
            "item-type-procexternalaudit",
            "stock-status-a",
            "stock-status-o",
            "stock-status-u",
            "stock-status-n"
        ],
        "options": {
            "elastic": {
                "default_scroll": "1m",
                "default_scroll_size": 150,
                "ignore_throttled": false,
                "index_mask": "env_org_cockpit_type_*"
            },
            "hierarchies": {
                "level": 3
            },
            "rectify": {
                "children": true,
                "resources": true
            }
        }
    }
    ```

## **cockpit-agent-settings**

Type: ``object`` | Setting ao nível da **aplicação Cockpit**

Define múltiplas parametrizações ao nível do agente Cockpit.

???+ example "Valores Possíveis:"
    | Campo                             | Tipo         | Detalhe                                                  |
    | ------------------------------------ | ------------ | ------------------------------------------------------------ |
    | extractions.items_allowed_fields     | Array string | Define a lista de campos possíveis de extração (item).                  |
    | extractions.items_date_fields        | Array string | Define a lista de datas a serem validadas no input (item).   |
    | extractions.items_default_fields     | Array string | Define a lista default de campos para extração (item).  |
    | extractions.processes_allowed_fields | Array string | Define a lista de campos válidos para extração. (processes)             |
    | extractions.processes_date_fields    | Array string | Define a lista de datas a serem validadas no input (processes).   |
    | extractions.processes_default_fields | Array string | Define a lista default de campos para extração (processes). |
    | extractions.tasks_allowed_fields     | Array string | Define a lista de campos possíveis de extração (task).                  |
    | extractions.tasks_date_fields        | Array string | Define a lista de datas a serem validadas no input (task).   |
    | extractions.tasks_default_fields     | Array string | Define a lista default de campos para extração (task). |
    | extractions.resources_allowed_fields | Array string | Define a lista de campos válidos para extração (resource).              |
    | extractions.resources_date_fields    | Array string | Define a lista de datas a serem validadas no input (resource).   |
    | extractions.resources_default_fields | Array string | Define a lista default de campos para extração (resource). |
    | flags.exclude_inactive_products      | boolean      | Define se exclui artigos com status Inative (I) no processo de exportação.|
    | flags.include_user_name              | boolean      | Define se inclui o campo ``user_name`` no evento de pós-processamento. |
    | groups.assortment                    | Array string | Define o grupo de tipos de tarefa que constituem o processo.   |
    | groups.assortment_evaluation         | Array string | Define o grupo de tipos de tarefa que influenciam o cálculo da % rutura. |
    | groups.relabelling                   | Array string | Define o grupo de tipos de tarefa considerados para processamento de relabelling.      |
    | groups.stock                         | Array string | Define o grupo de tipos de tarefa que requerem informação de SOH.          |
    | versioning.active                    | boolean      | Define se é obrigatório versionamento na indexação de dados no elastic.|
    | versioning.increment                 | integer      | Define o valor default para incremento no versionamento. |
    | versioning.type                      | string       | Define o tipo de versionamento.                                      |
    | events.delay                     | interger       | Define o tempo de delay entre novas tentativas de consumo de enventos.                                      |
    | events.max_retries                                      |string       | Define o número máximo de tentativas no consumo de enventos.                                      |

??? example "Parametrização exemplo:"
    ```json
        {
        "events": {
            "delay": 30000,
            "max_retries": 5
        },
        "extractions": {
            "items_allowed_fields": [
                "uuid",
                "ad_hoc",
                "app_code",
                "brand",
                "category_desc",
                "category_desc_1",
                "category_desc_2",
                "category_desc_3",
                "category_desc_4",
                "category_desc_5",
                "category_desc_6",
                "category_desc_7",
                "category_desc_8",
                "category_desc_9",
                "category_desc_10",
                "category_id",
                "category_id_1",
                "category_id_2",
                "category_id_3",
                "category_id_4",
                "category_id_5",
                "category_id_6",
                "category_id_7",
                "category_id_8",
                "category_id_9",
                "category_id_10",
                "chain_id",
                "chain_desc",
                "create_date",
                "create_date_store",
                "create_user",
                "create_user_name",
                "correlation_id",
                "ean",
                "expected_quantity",
                "group_id",
                "group_desc",
                "has_negative_stock",
                "has_stock",
                "image_url",
                "is_in_range",
                "is_oriented",
                "is_relapse",
                "is_scanned",
                "item_id",
                "item_desc",
                "item_name",
                "item_status",
                "label_code",
                "org_code",
                "origin_id",
                "origin_type",
                "process_desc",
                "process_id",
                "process_name",
                "process_status",
                "quantity",
                "resource_id",
                "scheduled_start_store",
                "sku",
                "status",
                "source_id",
                "source_type",
                "stock",
                "stock_status",
                "store_id",
                "store_desc",
                "store_timezone",
                "store_timezone_offset",
                "task_id",
                "type",
                "update_date",
                "update_date_store",
                "update_user",
                "update_user_name"
            ],
            "items_date_fields": [
                "create_date",
                "create_date_store",
                "scheduled_start_store",
                "update_date",
                "update_date_store"
            ],
            "items_default_fields": [
                "ad_hoc",
                "category_desc",
                "category_id",
                "create_date",
                "create_user",
                "ean",
                "expected_quantity",
                "has_negative_stock",
                "has_stock",
                "is_in_range",
                "is_oriented",
                "is_relapse",
                "is_scanned",
                "item_id",
                "item_desc",
                "item_name",
                "item_status",
                "origin_id",
                "origin_type",
                "process_desc",
                "process_id",
                "process_name",
                "process_status",
                "quantity",
                "resource_id",
                "scheduled_start_store",
                "sku",
                "status",
                "source_id",
                "source_type",
                "stock",
                "stock_status",
                "store_id",
                "store_desc",
                "task_id",
                "type",
                "update_date",
                "update_user"
            ],
            "processes_allowed_fields": [
                "uuid",
                "app_code",
                "chain_desc",
                "chain_id",
                "correlation_id",
                "group_desc",
                "group_id",
                "items_breakdown_rack_pct",
                "items_breakdown_range_pct",
                "items_breakdown_store_pct",
                "items_in_range",
                "items_in_range_active",
                "items_in_range_with_stock",
                "items_in_range_without_stock",
                "items_scanned_in_range",
                "items_scanned_off_range",
                "items_not_scanned_with_stock",
                "org_code",
                "process_desc",
                "process_id",
                "process_name",
                "process_status",
                "process_type",
                "scheduled_start",
                "scheduled_start_store",
                "store_desc",
                "store_id",
                "update_date",
                "update_date_store",
                "update_user",
                "update_user_name"
            ],
            "processes_date_fields": [
                "scheduled_start",
                "scheduled_start_store",
                "update_date",
                "update_date_store"
            ],
            "processes_default_fields": [
                "items_breakdown_range_pct",
                "items_in_range",
                "items_in_range_active",
                "items_in_range_with_stock",
                "items_in_range_without_stock",
                "items_scanned_in_range",
                "items_scanned_off_range",
                "process_desc",
                "process_id",
                "process_type",
                "scheduled_start",
                "store_desc",
                "store_id",
                "update_date",
                "update_user"
            ],
            "resources_allowed_fields": [
                "uuid",
                "app_code",
                "chain_id",
                "chain_desc",
                "correlation_id",
                "group_id",
                "group_desc",
                "org_code",
                "store_id",
                "store_desc",
                "task_create_date",
                "task_create_date_store",
                "task_desc",
                "task_id",
                "task_name",
                "task_parent_id",
                "task_parent_type",
                "task_scheduled_finish_store",
                "task_scheduled_start_store",
                "task_status",
                "task_type",
                "ad_hoc",
                "brand",
                "category_desc",
                "category_desc_1",
                "category_desc_2",
                "category_desc_3",
                "category_desc_4",
                "category_desc_5",
                "category_desc_6",
                "category_desc_7",
                "category_desc_8",
                "category_desc_9",
                "category_desc_10",
                "category_id",
                "category_id_1",
                "category_id_2",
                "category_id_3",
                "category_id_4",
                "category_id_5",
                "category_id_6",
                "category_id_7",
                "category_id_8",
                "category_id_9",
                "category_id_10",
                "category_task_desc",
                "category_task_id",
                "checklist_answer",
                "checklist_answer_type",
                "checklist_desc",
                "checklist_id",
                "create_date",
                "create_date_store",
                "create_user",
                "create_user_name",
                "destination_code",
                "destination_value",
                "ean",
                "expected_quantity",
                "expiration_date",
                "expiration_depreciation",
                "expiration_discount_value",
                "expiration_discount_type",
                "expiration_final_price",
                "expiration_max_pct_discount",
                "expiration_old_price",
                "expiration_type",
                "expiration_withdrawal",
                "first_picking_date",
                "first_picking_date_store",
                "has_alerts",
                "has_depreciation",
                "has_stock",
                "has_withdrawal",
                "image_url",
                "inventory_desc",
                "inventory_id",
                "inventory_status",
                "is_oriented",
                "item_quantity",
                "item_quantity_dif",
                "item_quantity_dif_pct",
                "item_has_negative_stock",
                "item_has_stock",
                "item_is_in_range",
                "item_is_relapse",
                "item_is_scanned",
                "item_scancode",
                "item_scanned_status",
                "item_status",
                "items_expected",
                "items_expected_quantity",
                "items_picked",
                "items_picked_quantity",
                "label_code",
                "label_is_mandatory",
                "last_picking_date",
                "last_picking_date_store",
                "location_desc",
                "location_id",
                "origin_id",
                "origin_type",
                "picking_mode",
                "presence_status",
                "price_current",
                "price_dif",
                "price_divergence_status",
                "price_erp",
                "price_erp_alt",
                "price_is_divergent",
                "price_label",
                "price_label_alt",
                "price_mrs",
                "price_mrs_alt",
                "price_pos",
                "price_pos_alt",
                "price_previous",
                "price_owner",
                "price_relabelling_status",
                "process_desc",
                "process_id",
                "quantity",
                "reason_desc",
                "reason_id",
                "replenish_status",
                "resource_desc",
                "resource_desc_pt",
                "resource_desc_br",
                "resource_desc_en",
                "resource_desc_es",
                "resource_desc_fr",
                "resource_id",
                "resource_name",
                "resource_name_pt",
                "resource_name_br",
                "resource_name_en",
                "resource_name_es",
                "resource_name_fr",
                "resource_parent_ean",
                "resource_parent_id",
                "resource_parent_picking_mode",
                "resource_parent_sku",
                "resource_parent_type",
                "resource_status",
                "resource_type",
                "sku",
                "source_desc",
                "source_id",
                "stock_current",
                "stock_dif",
                "stock_on_hand",
                "stock_previous",
                "stock_status",
                "store_timezone",
                "store_timezone_offset",
                "task_category_desc",
                "task_category_id",
                "update_date",
                "update_date_store",
                "update_user",
                "update_user_name",
                "zone_desc",
                "zone_id",
                "zone_status"
            ],
            "resources_date_fields": [
                "task_create_date",
                "task_create_date_store",
                "task_scheduled_finish_store",
                "task_scheduled_start_store",
                "create_date",
                "create_date_store",
                "expiration_date",
                "first_picking_date",
                "first_picking_date_store",
                "last_picking_date",
                "last_picking_date_store",
                "update_date",
                "update_date_store"
            ],
            "resources_default_fields": [
                "store_id",
                "store_desc",
                "chain_id",
                "chain_desc",
                "task_id",
                "task_name",
                "task_desc",
                "task_type",
                "task_status",
                "resource_id",
                "resource_name",
                "resource_desc",
                "resource_status",
                "resource_type",
                "sku",
                "ean",
                "expected_quantity",
                "quantity",
                "price_erp",
                "price_pos",
                "price_label",
                "price_previous",
                "price_current",
                "category_id_1",
                "category_desc_1",
                "category_id_2",
                "category_desc_2",
                "category_id_3",
                "category_desc_3",
                "category_id_4",
                "category_desc_4",
                "category_id_5",
                "category_desc_5",
                "update_date",
                "update_user"
            ],
            "tasks_allowed_fields": [
                "uuid",
                "app_code",
                "chain_desc",
                "chain_id",
                "correlation_id",
                "group_desc",
                "group_id",
                "org_code",
                "store_desc",
                "store_id",
                "ad_hoc",
                "assigned_user",
                "assigned_user_name",
                "assort_audit_active_items_in_range",
                "assort_audit_breakdown_rack_pct",
                "assort_audit_breakdown_store_pct",
                "assort_audit_items_in_range",
                "assort_audit_items_in_range_with_stock",
                "assort_audit_items_in_range_without_stock",
                "assort_audit_range_breakdown_pct",
                "assort_audit_scanned_items_in_range",
                "assort_audit_scanned_items_off_range",
                "avg_time_container_sec",
                "avg_time_item_sec",
                "avg_time_unit_sec",
                "checklist_desc",
                "checklist_id",
                "checklist_items_expected",
                "checklist_items_done",
                "containers_expected",
                "containers_handled",
                "containers_missing",
                "containers_not_found",
                "containers_not_handled",
                "containers_picked_over_received",
                "containers_picked",
                "containers_picked_unplanned",
                "containers_status",
                "create_date",
                "create_date_store",
                "create_user",
                "create_user_name",
                "destination_code",
                "destination_value",
                "finish_date",
                "finish_date_store",
                "finish_user",
                "finish_user_name",
                "first_picking_date",
                "first_picking_date_store",
                "group_code",
                "has_alerts",
                "has_children",
                "has_depreciation",
                "has_media",
                "has_withdrawal",
                "items_expected",
                "inventory_desc",
                "inventory_id",
                "items_expected_price_dif",
                "items_expected_price_down",
                "items_expected_price_equal",
                "items_expected_price_up",
                "items_handled",
                "items_missing",
                "items_not_found",
                "items_not_handled",
                "items_picked_partially",
                "items_picked",
                "items_picked_over_received",
                "items_picked_price_dif",
                "items_picked_price_down",
                "items_picked_price_equal",
                "items_picked_price_up",
                "items_picked_unplanned",
                "items_replenish_full",
                "items_replenish_partial",
                "is_extendable",
                "is_on_schedule",
                "is_oriented",
                "is_releasable",
                "last_picking_date",
                "last_picking_date_store",
                "location_desc",
                "location_id",
                "origin_id",
                "origin_type",
                "print_auto",
                "print_mode",
                "priority",
                "process_id",
                "reason_desc",
                "reason_id",
                "scheduled_finish",
                "scheduled_finish_store",
                "scheduled_start",
                "scheduled_start_store",
                "setup_code",
                "skus",
                "source_desc",
                "source_id",
                "start_date",
                "start_date_store",
                "start_user",
                "start_user_name",
                "store_timezone",
                "store_timezone_offset",
                "task_id",
                "task_desc",
                "task_desc_pt",
                "task_desc_br",
                "task_desc_es",
                "task_desc_en",
                "task_desc_fr",
                "task_name",
                "task_name_pt",
                "task_name_br",
                "task_name_es",
                "task_name_en",
                "task_name_fr",
                "task_parent_id",
                "task_parent_type",
                "task_scheduled_finish_store",
                "task_scheduled_start_store",
                "task_status",
                "task_time_sec",
                "task_time_estimated_sec",
                "task_type",
                "units_expected",
                "units_missing",
                "units_picked",
                "units_picked_over_received",
                "units_picked_unplanned",
                "update_date",
                "update_date_store",
                "update_user",
                "update_user_name",
                "zone_id"
            ],
            "tasks_date_fields": [
                "create_date",
                "create_date_store",
                "first_picking_date",
                "first_picking_date_store",
                "last_picking_date",
                "last_picking_date_store",
                "scheduled_finish",
                "scheduled_finish_store",
                "scheduled_start",
                "scheduled_start_store",
                "start_date",
                "start_date_store",
                "task_scheduled_finish_store",
                "task_scheduled_start_store",
                "update_date",
                "update_date_store"
            ],
            "tasks_default_fields": [
                "store_id",
                "store_desc",
                "chain_id",
                "chain_desc",
                "task_id",
                "task_name",
                "task_desc",
                "task_type",
                "task_status",
                "items_expected",
                "origin_id",
                "origin_type",
                "scheduled_start",
                "start_date",
                "finish_date",
                "first_picking_date",
                "last_picking_date",
                "task_time_sec",
                "avg_time_item_sec",
                "update_user",
                "task_parent_id"
            ]
        },
        "flags": {
            "exclude_inactive_products": false,
            "include_user_name": true
        },
        "groups": {
            "assortment": [
                "scanaudit",
                "externalaudit",
                "presenceaudit",
                "presencecheck"
            ],
            "assortment_evaluate": [
                "scanaudit",
                "externalaudit"
            ],
            "assortment_trigger": [
                "presencecheck"
            ],
            "relabelling": [
                "relabelling",
                "priorityrelabelling",
                "priceup",
                "pricedown",
                "promotionin",
                "promotionout",
                "carddiscount"
            ],
            "stock": [
                "relabelling",
                "priorityrelabelling",
                "priceup",
                "pricedown",
                "promotionin",
                "promotionout",
                "carddiscount",
                "scanaudit",
                "externalaudit",
                "presenceaudit",
                "presencecheck"
            ]
        },
        "versioning": {
            "active": true,
            "increment": 1,
            "type": "external_gte"
        }
    }
    ```