# Equipamentos Homologados ao TMR

Segue lista e fabricantes homologados ao TMR.<br>
Caso você possua outro modelo não homologado, procure a sua gestão de conta.

!!! info 
    Para melhores informações sobre características específicas para os equipamentos citados abaixo, por favor acesse diretamente o site de cada fabricante. 


### Coletores



??? abstract "Motorola"

    | MARCA    | S.O. | MODELO           | PART NUMBER      | IMAGEM                                                       |
    | -------- | ---- | ---------------- | ---------------- | :------------------------------------------------------------: |
    | MOTOROLA | CE   | MC2180-AS01E0A   | MC2180-AS01E0A   | ![image alt <>](assets/eqpto_homologado/MC2180-AS01E0A.jpg)  |
    | MOTOROLA | CE   | MC2180-CS01E0A   | MC2180-CS01E0A   | ![image alt <>](assets/eqpto_homologado/MC2180-AS01E0A.jpg)  |
    | MOTOROLA | CE   | MC2180-MS01E0A   | MC2180-MS01E0A   | ![image alt <>](assets/eqpto_homologado/MC2180-MS01E0A.jpg)  |
    | MOTOROLA | CE   | Motorola MC 3090 | Motorola MC 3090 | ![image alt <>](assets/eqpto_homologado/Motorola MC 3090.jpg) |
    | MOTOROLA | CE   | Motorola MC 3190 | Motorola MC 3190 | ![image alt <>](assets/eqpto_homologado/Motorola MC 3190.jpg) |

??? abstract "Zebra"

    | MARCA | S.O.        | MODELO     | PART NUMBER | IMAGEM                                                  |
    | ----- | ----------- | ---------- | ----------- | :-------------------------------------------------------: |
    | Zebra | Android 8.1 | Zebra TC20 | Zebra TC20  | ![image alt <>](assets/eqpto_homologado/Zebra TC20.jpg) |


??? abstract "Datalogic"

    | MARCA     | S.O.        | MODELO    | PART NUMBER | IMAGEM                                                       |
    | --------- | ----------- | --------- | ----------- | :------------------------------------------------------------: |
    | Datalogic | Android 9   | Memor K   | Memor K     | ![image alt <>](assets/eqpto_homologado/Memor Kvprimeira.png) |
    | Datalogic | Android 8.1 | Memor 10  | Memor 10    | ![image alt <>](assets/eqpto_homologado/Memor 10.png)        |
    | DATALOGIC | CE          | 942400002 | 942400002   | ![image alt <>](assets/eqpto_homologado/Skorpio X3.jpg)      |
    | DATALOGIC | CE          | 942350003 | 942350003   | ![image alt <>](assets/eqpto_homologado/Skorpio X3_pistol.jpg) |
    | DATALOGIC | CE          | 942350007 | 942350007   | ![image alt <>](assets/eqpto_homologado/Skorpio X3_Handheld.jpg) |
    | DATALOGIC | CE          | -         | -           | ![image alt <>](assets/eqpto_homologado/Skorpio X4.jpg)      |
    | DATALOGIC | CE          | Memor X3  | Memor X3    | ![image alt <>](assets/eqpto_homologado/Memor X3.jpg)        |


??? abstract "Point Mobile"

    | MARCA        | S.O.   | MODELO         | PART NUMBER    | IMAGEM                                                      |
    | ------------ | ------ | -------------- | -------------- | :-----------------------------------------------------------: |
    | POINT MOBILE | MOBILE | PM60GP74346E0C | PM60GP74346E0C | ![image alt <>](assets/eqpto_homologado/PM60GP74346E0C.jpg) |
    | POINT MOBILE | MOBILE | P450GP76154E0T | P450GP76154E0T | ![image alt <>](assets/eqpto_homologado/P450GP76154E0T.jpg) |
    | POINT MOBILE | MOBILE | P260EP12135E0T | P260EP12135E0T | ![image alt <>](assets/eqpto_homologado/P260EP12135E0T.jpg) |


??? abstract "Denso"

    | MARCA | S.O.       | MODELO         | PART NUMBER    | IMAGEM                                                      |
    | ----- | ---------- | -------------- | -------------- | :-----------------------------------------------------------: |
    | Denso | CE         | n/a            | n/a            | ![image alt <>](assets/eqpto_homologado/BHT1200.jpg)        |
    | Denso | CE         | n/a            | n/a            | ![image alt <>](assets/eqpto_homologado/BHT700.jpg)         |
    | Denso | CE         | n/a            | n/a            | ![image alt <>](assets/eqpto_homologado/BHT260.jpg)         |
    | Denso | Windows CE | BHT-1261BWB-CE | BHT-1261BWB-CE | ![image alt <>](assets/eqpto_homologado/BHT-1261BWB-CE.png) |


??? abstract "Honeywell"

    | MARCA     | S.O.        | MODELO                 | PART NUMBER            | IMAGEM                                                       |
    | --------- | ----------- | ---------------------- | ---------------------- | :------------------------------------------------------------: |
    | Honeywell | CE          | HoneyWell Dolphin 6500 | HoneyWell Dolphin 6500 | ![image alt <>](assets/eqpto_homologado/HoneyWell Dolphin 6500.jpg) |
    | Honeywell | CE          | HoneyWell Dolphin 6100 | HoneyWell Dolphin 6100 | ![image alt <>](assets/eqpto_homologado/HoneyWell Dolphin 6100.png) |
    | Honeywell | Android 8.1 | EDA51                  | EDA51                  | ![image alt <>](assets/eqpto_homologado/EDA51.png)           |

??? abstract "Intermec"

    | MARCA    | S.O.               | MODELO                         | PART NUMBER                    | IMAGEM                                                   |
    | -------- | ------------------ | ------------------------------ | ------------------------------ | :--------------------------------------------------------: |
    | Intermec | CE                 | Intermec honeywell CN50 – CN51 | Intermec honeywell CN50 – CN51 | ![image alt <>](assets/eqpto_homologado/CN50 – CN51.jpg) |
    | Intermec | Windows Mobile 6.1 | CK3                            | CK3                            | ![image alt <>](assets/eqpto_homologado/CK3.png)         |



??? abstract "Tiger"

    | MARCA | S.O.      | MODELO | PART NUMBER | IMAGEM                                            |
    | ----- | --------- | ------ | ----------- | :-------------------------------------------------: |
    | Tiger | Android 9 | SK80   | SK80        | ![image alt <>](assets/eqpto_homologado/SK80.png) |


### Impressoras

??? abstract "Zebra"

    | **Marca** | **Modelo**          | **Conectividade** | **Tipo** | **IMAGEM** |
    | --------- | ------------------- | ----------------- | -------- | ---------- |
    | Zebra     | Qln  220 / 200 Plus | Wifi              | Térmica  |![image alt <>](assets/eqpto_homologado/qln220.jpg)        |
    | Zebra     | Qln 320             | Wifi              | Térmica  |![image alt <>](assets/eqpto_homologado/qln320.jpg)           |
    | Zebra     | Qln 420             | Wifi              | Térmica  |![image alt <>](assets/eqpto_homologado/qln420.jpg)            |
    | Zebra     | ZQ620               | Wifi              | Térmica  |![image alt <>](assets/eqpto_homologado/zq620.jpg)            |

??? abstract "Honeywell"

    | **Marca** | **Modelo**    | **Conectividade** | **Tipo** | **IMAGEM** |
    | --------- | ------------- | ----------------- | -------- | ---------- |
    | Honeywell | Intermec PB22 | Wifi              | Térmica  |![image alt <>](assets/eqpto_homologado/IntermecPB22.jpg)            |

??? abstract "Sewoo"

    | **Marca** | **Modelo** | **Conectividade** | **Tipo** | **IMAGEM** |
    | --------- | ---------- | ----------------- | -------- | ---------- |
    | Sewoo     | LK P12W    | Wifi              | Térmica  |![image alt <>](assets/eqpto_homologado/sewooLK P12W.jpg)           |