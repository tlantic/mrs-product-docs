
!!! info  "Áreas"

      [Manual de instalación](/languages/es/es-install_wince) <br>
      [Manual de usuario](/languages/es/es-multiplataforma) <br>
      [Manual del administrador](/languages/es/es-bo_instore) <br>
      [Manual de parametrización](/languages/es/es-configuracao_avancada) <br>
      [Integración](http://mrs-docs.s3-eu-west-1.amazonaws.com/index.html) <br>
      [Equipo aprobado](/languages/es/es-eqpto_homologado) <br>
      [Notas de la versión](/languages/es/es-MP_change)

  
## Módulos que compõem o TMR
[comment]: # (### Ficha de Produto)

!!! abstract "Flujos de precios"
     - Auditoría de precios
     - Precio rebajado
     - Impresión de etiquetas

!!! abstract "Flujos de existencias"
     - Recepción
     - traslados
     - Devolución
     - recuentos
     - inventarios

!!! abstract "Flujos de reemplazo"
     - Auditoría de interrupciones
     - Separación de mercancías
     - Reposición de artículos
     - Reemplazo por ventas
     - Reemplazo urgente

!!! abstract "Flujos de calidad"
     - Lista de verificación para control y verificación

!!! abstract "Flujos de surtido"
     - Auditoría de surtido

!!! abstract "Cockpit"
     - Tablero operativo





