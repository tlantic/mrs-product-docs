## Historia del documento

| Datos      | Descripción                                    | Autor        | Versión | Revisión         |
| :--------- | :--------------------------------------------- | :----------- | :------ | :--------------- |
| 12/01/2020 | Creación de documentos.                        | Filipe Silva | 1.0     |                  |
| 12/12/2020 | Creación de formato de documento Introducción. | Filipe Silva | 1.1     |                  |
| 22/12/2020 | Versión final del documento.                   | Filipe Silva | 2.0     |                  |
| 22/01/2021 | Revisión documental final.                     | Filipe Silva | 2.1     | Helena Gonçalves |

## **Introducción**

### TMR - Tienda

Este documento describe las funcionalidades del sistema Tlantic Mobile Retail (TMR) - InStore. El principal objetivo del documento es proporcionar al usuario toda la información necesaria para que pueda utilizar el sistema de forma correcta y eficaz.

InStore es uno de los módulos que componen Tlantic Mobile Retail (TMR) orientado a las operaciones de la tienda, con un enfoque en la solución de los problemas habituales del retail:

<details class="example" open="open" style="box-sizing: inherit; margin: 1.5625em 0px; padding: 0px 0.6rem; overflow: visible; color: rgba(0, 0, 0, 0.87); font-size: 0.64rem; break-inside: avoid; background-color: var(--md-admonition-bg-color); border-left: 0.2rem solid rgb(101, 31, 255); border-radius: 0.1rem; box-shadow: rgba(0, 0, 0, 0.05) 0px 0.2rem 0.5rem, rgba(0, 0, 0, 0.05) 0px 0.025rem 0.05rem; display: block; border-top-color: rgb(101, 31, 255); border-right-color: rgb(101, 31, 255); border-bottom-color: rgb(101, 31, 255); font-family: Roboto, -apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><summary style="box-sizing: inherit; position: relative; margin: 0px -0.6rem 0px -0.8rem; padding: 0.4rem 1.8rem 0.4rem 2rem; font-weight: 700; background-color: rgba(101, 31, 255, 0.1); border-left: 0.2rem solid rgb(101, 31, 255); display: block; min-height: 1rem; border-top-left-radius: 0.1rem; border-top-right-radius: 0.1rem; cursor: pointer; border-top-color: rgb(101, 31, 255); border-right-color: rgb(101, 31, 255); border-bottom-color: rgb(101, 31, 255); outline: none; -webkit-tap-highlight-color: transparent;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Principales características:</font></font></summary><ul style="box-sizing: inherit; margin: 1em 0px 0.6rem 0.625em; list-style-type: disc; padding: 0px;"><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Inventarios.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Lista de artículos, hoja de artículos, auditoría de precios,</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Registro y aprobación de rupturas.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Impresión de etiquetas.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Auditoría y Control de Validez (Depreciaciones y Retiros).</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Auditoría de escaneo y verificación de presencia.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Auditoría de Ruptura, Separación, Reemplazo.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0px; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Recepciones, transferencias y devoluciones.</font></font></li></ul></details>

El módulo Instore busca simplificar y solucionar problemas de retail realizando tareas orientadas a un perfil de usuario. Cada tarea está asociada con una característica para resolver una necesidad particular de la aleta. TMR se compone de los módulos: **BackOffice** , **Cockpit** , **Aplicaciones móviles (WinCE y Multiplataforma)** , **Scheduler** y **TMR Server** .

### Arquitectura

TMR es un sistema que consta de:

- Aplicaciones de cliente (aplicación WinCE, aplicación de Android y aplicación de iOS): aplicación distribuida por los dispositivos móviles utilizados en la operación de la tienda.
- TMR Server: componente central que contiene lógica y datos y que interactúa con otros sistemas.
- Backoffice - Componente web que permite la definición de datos de referencia y configuración de parámetros.
- Cockpit: componente web para el análisis operativo de las tareas realizadas en TMR Instore.

![imagen alt <>](assets/Arquitetura.png)

### Aplicación móvil - Multiplataforma

En los siguientes puntos se presentarán todas las funcionalidades presentes en la aplicación Multiplataforma y cómo se pueden ejecutar correctamente. También se presentarán imágenes ilustrativas a lo largo de los capítulos para una mejor orientación al usuario en relación con el sistema.

### Requisitos mínimos

Para una correcta instalación del TMR, el dispositivo debe respetar los siguientes requisitos:

<details class="example" open="open" style="box-sizing: inherit; margin: 1.5625em 0px; padding: 0px 0.6rem; overflow: visible; color: rgba(0, 0, 0, 0.87); font-size: 0.64rem; break-inside: avoid; background-color: var(--md-admonition-bg-color); border-left: 0.2rem solid rgb(101, 31, 255); border-radius: 0.1rem; box-shadow: rgba(0, 0, 0, 0.05) 0px 0.2rem 0.5rem, rgba(0, 0, 0, 0.05) 0px 0.025rem 0.05rem; display: block; border-top-color: rgb(101, 31, 255); border-right-color: rgb(101, 31, 255); border-bottom-color: rgb(101, 31, 255); font-family: Roboto, -apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><summary style="box-sizing: inherit; position: relative; margin: 0px -0.6rem 0px -0.8rem; padding: 0.4rem 1.8rem 0.4rem 2rem; font-weight: 700; background-color: rgba(101, 31, 255, 0.1); border-left: 0.2rem solid rgb(101, 31, 255); display: block; min-height: 1rem; border-top-left-radius: 0.1rem; border-top-right-radius: 0.1rem; cursor: pointer; border-top-color: rgb(101, 31, 255); border-right-color: rgb(101, 31, 255); border-bottom-color: rgb(101, 31, 255); outline: none; -webkit-tap-highlight-color: transparent;">Requisitos mínimos:</summary><ul style="box-sizing: inherit; margin: 1em 0px 0.6rem 0.625em; list-style-type: disc; padding: 0px;"><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Sistema operativo:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>Android 7 o superior |<span>&nbsp;</span></font><font style="box-sizing: inherit; vertical-align: inherit;">IOS (???).</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Memoria ROM:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>16 GB o más.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Memoria RAM:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>4GB o más.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Android WebView:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>Chrome versión 51 o superior.<span>&nbsp;</span></font><font style="box-sizing: inherit; vertical-align: inherit;">(Solo Android)</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Lector de<span>&nbsp;</span></font><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">códigos de barras:</font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>Lector 1D - LASER</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">WI-FI:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>802.11 B / G / N.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Tamaño de pantalla recomendado:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>5 ”o más.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0px; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Resolución mínima recomendada:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>HD 1280 x 720.</font></font></li></ul></details>

## **Acceso**

Para iniciar sesión, se le pide al usuario que ingrese su nombre de usuario y contraseña. Para verificar el texto ingresado en el campo Contraseña, el usuario puede mantener presionado el ícono ![Foto](assets/eye.png) para mostrar la información. Después de iniciar sesión, se le presenta al usuario la lista de tiendas asociadas y su selección es obligatoria para continuar. Si el usuario está asociado con una sola tienda, el sistema guarda esta información automáticamente y pasa a la siguiente pantalla.

![imagen alt <>](assets/login.gif)

## **Menú**

El menú de la aplicación siempre está presente en la parte inferior de la pantalla para que sea más fácil y rápido acceder a las distintas opciones. Consta de cuatro enlaces a las páginas más importantes: INICIO, MIS TAREAS, BÚSQUEDA y MÁS.

![imagen alt <>](assets/menu.png)

Al seleccionar uno de los componentes presentes en el menú, aparece una barra superior azul para identificar el componente seleccionado.

​              ![Foto](assets/select_menu.png) ![Foto](assets/selected_menu.png)

### Comienzo

La pestaña "Inicio" es la página de inicio que presenta el resumen de tareas agregadas por tipo. De esta forma, los tipos de tareas son visibles con tareas disponibles para procesar o configuradas para aparecer en el resumen incluso cuando no hay tareas por realizar.

![imagen alt <>](assets/inicio.png)

### Mis tareas

La pestaña "Mis tareas" contiene la lista de tareas almacenadas localmente en el dispositivo. Estos se pueden dividir en dos tipos: PENDIENTES (tareas que se han descargado pero que aún no se han realizado) y EN CURSO (tareas que ya han sido iniciadas por el usuario y aún no se han completado).

![imagen alt <>](assets/user_tasks.png)

### Investigar

La pestaña "Buscar" es un facilitador que le permite buscar un artículo por nombre, descripción, EAN o SKU. Al seleccionar un artículo, se muestra la hoja del artículo respectivo. La plantilla de la Ficha del Artículo (la información presentada y su orden de presentación) la define el cliente.

Para acceder a información sobre un elemento específico fuera del contexto de la tarea, el usuario podrá acceder al facilitador de búsqueda presente en la parte inferior de la aplicación móvil.

Luego debe buscar el artículo deseado cortando el EAN o ingresando manualmente la información. Inmediatamente se presenta la ficha del artículo con toda la información facilitada por el cliente. Cabe destacar que la información presentada en la Ficha de Artículo es configurable por el cliente y que puede contener cualquier información que el cliente desee dar visibilidad en la operación.

![imagen alt <>](assets/search_item.gif)

#### Artículo modal

Durante el procesamiento de una tarea, es posible consultar la Ficha de artículo de un producto determinado. Para ello, el usuario debe acceder al botón "Información" en la esquina superior derecha del modal del artículo. Inmediatamente la aplicación dirige al formulario de artículo.

![imagen alt <>](assets/search_item_modal.gif)

### Más

La pestaña "Más" contiene acceso a los formularios de solicitud. Las opciones disponibles dependen de las parametrizaciones del cliente.

<details class="example" open="open" style="box-sizing: inherit; margin: 1.5625em 0px; padding: 0px 0.6rem; overflow: visible; color: rgba(0, 0, 0, 0.87); font-size: 0.64rem; break-inside: avoid; background-color: var(--md-admonition-bg-color); border-left: 0.2rem solid rgb(101, 31, 255); border-radius: 0.1rem; box-shadow: rgba(0, 0, 0, 0.05) 0px 0.2rem 0.5rem, rgba(0, 0, 0, 0.05) 0px 0.025rem 0.05rem; display: block; border-top-color: rgb(101, 31, 255); border-right-color: rgb(101, 31, 255); border-bottom-color: rgb(101, 31, 255); font-family: Roboto, -apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><summary style="box-sizing: inherit; position: relative; margin: 0px -0.6rem 0px -0.8rem; padding: 0.4rem 1.8rem 0.4rem 2rem; font-weight: 700; background-color: rgba(101, 31, 255, 0.1); border-left: 0.2rem solid rgb(101, 31, 255); display: block; min-height: 1rem; border-top-left-radius: 0.1rem; border-top-right-radius: 0.1rem; cursor: pointer; border-top-color: rgb(101, 31, 255); border-right-color: rgb(101, 31, 255); border-bottom-color: rgb(101, 31, 255); outline: none; -webkit-tap-highlight-color: transparent;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Formas disponibles:</font></font></summary><ul style="box-sizing: inherit; margin: 1em 0px 0.6rem 0.625em; list-style-type: disc; padding: 0px;"><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Informe de actividad:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>Registro de cualquier error que haya ocurrido durante el uso de la aplicación, asegurando así un análisis más preciso de cualquier problema.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Impresoras:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>Formulario para seleccionar impresora y tipo de etiqueta.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Impresión de etiquetas:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>formulario para acceder al tipo de trabajo de impresión de etiquetas ad hoc.<span>&nbsp;</span></font><font style="box-sizing: inherit; vertical-align: inherit;">Esta opción está disponible mediante<span>&nbsp;</span></font></font><a href="app_settings/#show-labelprint-on-summary" style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; color: var(--md-typeset-a-color); text-decoration: none; word-break: break-word; transition: color 125ms ease 0s;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">parametrización</font></font></a><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Cambiar contraseña:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>Formulario que permite cambiar la contraseña asociada al usuario registrado.<span>&nbsp;</span></font><font style="box-sizing: inherit; vertical-align: inherit;">Esta opción está disponible mediante<span>&nbsp;</span></font></font><a href="app_settings/#allow-change-password" style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; color: var(--md-typeset-a-color); text-decoration: none; word-break: break-word; transition: color 125ms ease 0s;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">parametrización</font></font></a><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0px; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Cerrar sesión:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>formulario para cerrar sesión en la aplicación.</font></font></li></ul></details>

![imagen alt <>](assets/plus_options_2.png)

## **Tareas**

### Lista de tareas

El menú INICIO muestra los tipos de tareas disponibles para procesar. Cuando se selecciona un tipo de tarea, aparece la lista de tareas disponibles para el tipo seleccionado. Para cada tarea, se muestran el nombre, su disponibilidad, la fecha de creación y el usuario que está trabajando en la tarea (si corresponde).

![imagen alt <>](assets/available_tasks.png)

### Descarga de tareas

Al descargar una tarea, se asigna al usuario que ha iniciado sesión y toda la información se almacena localmente en el dispositivo.

De esta forma, es posible procesar tareas sin conectividad a internet, ya que toda la información necesaria se almacena en el dispositivo (a excepción de la información que se obtiene en el momento del chipping, como es el caso del SOH del artículo o el precio).

Para descargar directamente sin una pregunta de confirmación, el usuario puede "presionar prolongadamente" la tarea deseada.

Configurando el cliente, es posible definir si se permite descargar tareas que están en ejecución y que no están guardadas previamente en la memoria del dispositivo. Esta protección tiene como objetivo no permitir que un mismo usuario descargue la tarea al mismo tiempo en varios dispositivos, ya que esto podría resultar en la pérdida del trabajo realizado anteriormente.

![imagen alt <>](assets/download_task.gif)

### Procesamiento de tareas

El manejo de las tareas guiadas sigue el mismo flujo base, independientemente del tipo de tarea.

Después de descargar una tarea, el usuario tiene una lista de artículos para guiarse durante el tratamiento de la tarea. Inicialmente, todos estos artículos aparecen en la pestaña PENDIENTE hasta que el usuario realiza algún cambio en el artículo.

Al cortar un código de barras o seleccionar manualmente un artículo, el sistema verifica si pertenece a la lista de artículos de la tarea. En caso de que no pertenezcas, hay dos opciones:

- Si la tarea permite la adición de elementos inesperados, el usuario tiene la posibilidad de agregarla a la tarea y tratarla con los elementos restantes. (1)
- En caso contrario, se informa al usuario de que el artículo en cuestión no pertenece a la tarea y no puede procesarlo. (dos)

![Foto](assets/adhoc_item.png) ![Foto](assets/not_adhoc_item.png)

### Creación de tareas ad hoc

El usuario puede crear tareas ad hoc de tres formas distintas: **Con lista** , **Sin lista** o **Por estructura jerárquica** . Para hacer esto, seleccione el botón "más" (+) en la esquina superior derecha de la tarea.

#### Sin lista

El usuario debe presionar el botón "más" (+) en la esquina superior derecha y elegir la opción **SIN LISTA** . Después de crear la tarea, el usuario es redirigido a la pantalla de tratamiento donde puede iniciar el tratamiento de los artículos.

![imagen alt <>](assets/create_without_list_task.gif)

#### Con lista

El usuario debe presionar el botón "más" (+) en la esquina superior derecha y elegir la opción **CON LISTA** . Después de crear la tarea, el usuario es dirigido a la pantalla donde elegirá con qué lista quiere tratar y cuáles están disponibles. Después de la selección, el usuario puede comenzar a procesar artículos.

![imagen alt <>](assets/create_with_list_task.gif)

#### Por estructura jerárquica

El usuario debe presionar el botón "más" (+) en la esquina superior derecha y elegir la opción **POR ESTRUCTURA JERÁRQUICA** . Luego de crear la tarea, el usuario es dirigido a la pantalla donde seleccionará el nivel de la estructura jerárquica que desee, siendo el primer nivel obligatorio y el siguiente opcional. Después de la selección, el usuario puede comenzar a procesar artículos.

![imagen alt <>](assets/create_hs_task.gif)

## **Configuración de la impresora**

El usuario, mientras maneja una tarea, puede cambiar la impresora en cualquier momento sin tener que abandonar la tarea.

Cuando haces una pulsación larga sobre el icono, ![Foto](assets/printer.png)aparece un pop up con la opción de elegir la nueva impresora, el número de etiquetas que quieres imprimir y en algunos casos el tipo de etiqueta.

Si desea guardar la nueva configuración sin imprimir una etiqueta, también puede hacerlo seleccionando la opción "Guardar impresora".

![imagen alt <>](assets/printing.png)

## **Resumen**

El usuario puede acceder al resumen de la tarea en cualquier momento, utilizando el icono ![Foto](assets/approve_task.png) en la esquina superior derecha de la pantalla de la tarea.

En este momento, al usuario se le presenta una lista de artículos sin tratar. En esta lista, el usuario puede pasar al resumen sin realizar ninguna acción, o marcar todos los artículos no tratados como no encontrados.

Configurando el cliente, es posible definir si un tipo específico de tarea se puede aprobar con artículos pendientes de tratamiento o si es obligatorio procesarlos para la aprobación de la tarea.

![imagen alt <>](assets/resume.png)

El resumen proporciona una descripción general de la tarea y su procesamiento. La aplicación tiene la capacidad de calcular individualmente los recursos de la tarea, tales como: artículos, cajas, cantidades y fechas de vencimiento. Los algoritmos de cálculo disponibles en los resúmenes son los siguientes:

- PLANIFICADO: recursos previstos en la tarea.
- PENDIENTE: recursos previstos en la tarea que no han sido atendidos.
- PROCESADO: recursos procesados por el usuario:
  - EXTRAVIADO,
  - PARCIALMENTE PROCESADO,
  - DEMASIADO PROCESADO,
- NO PLANIFICADO: Recursos agregados ad hoc en la tarea.

Esta información está agrupada por secciones y se puede definir según el tipo de tarea. La descripción del cálculo también se puede definir según el tipo de tarea, como por ejemplo: los artículos tratados se pueden marcar en el Markdown, recibir en Recepción y auditar en Auditoría. El detalle de cada resumen se puede consultar en el detalle de cada tipo de tarea.

Para obtener más detalles, consulte la sección Resumen de cada tipo de tarea.

En esta pantalla también es posible LIBERAR o APROBAR la tarea.

![imagen alt <>](assets/resume_2.png)

## TAREAS - PRECIO

### **REDUCCIÓN**

La función Markdown le permite actualizar las etiquetas de la tienda, los artículos que sufren cambios de precio y / u otras características que se comunican al cliente a diario.

Al configurarlo, es posible segmentar los artículos para marcar en diferentes tipos de tareas de Markdown: Subida de precio, Bajada de precio, Entrada de promoción, Salida de promoción, Descuento de tarjeta y Cambio de reserva de emergencia.

#### Lista de articulos

Al ingresar al menú Volver a marcar, el usuario tendrá acceso a la lista de tareas disponibles que se pueden descargar al dispositivo. Después de la descarga, se muestra una lista de elementos a tratar con el número de etiquetas a imprimir. Al seleccionar manualmente el artículo, aparece el modal con la información del artículo picado, el precio anterior, el nuevo y la cantidad a imprimir.

Si el trabajo está configurado con modo de impresión automática, la etiqueta se imprime, por defecto, del artículo después de cortarlo.

![imagen alt <>](assets/relabelling_item_list.png)

La lista de artículos de las tareas de Markdown contiene la siguiente información:

- **Información del artículo:** foto (si corresponde), descripción, EAN y SKU.
- **Información sobre la dinámica de precios: En** la parte izquierda del artículo hay un símbolo compuesto por una flecha que identifica si el artículo es un aumento o una disminución de precio.
- **Cantidad: En** el lado derecho de la lista está la información sobre la cantidad tratada y la cantidad esperada para reprogramar. Formato: Cantidad tratada / Cantidad esperada.

#### Filtrar

En la lista de artículos en las tareas de marcado, es posible filtrar los artículos presentados en la lista. Mediante configuración, el TMR presenta la opción "FILTRO" que permite filtrar los artículos presentados en la lista bajo las condiciones "Subidas de precio" y "Rangos de precios".

Dependiendo de la operación de la tienda, puede ser importante presentar los aumentos de precio y las disminuciones de precio por separado, ya que pueden tratarse en diferentes momentos de la operación.

![imagen alt <>](assets/relabelling_filter.png)

#### Ordenando

En la lista de artículos de las tareas de marcado, es posible ordenar los artículos presentados en la lista. A través de la configuración, el TMR presenta la opción "PEDIDO" que permite ordenar los artículos presentados en la lista utilizando algunas opciones que también son configurables. También a través de la configuración es posible definir un valor por defecto de pedido para que al descargar la tarea los artículos sean ordenados automáticamente por el valor configurado por defecto.

Opciones de clasificación:

- **Modificado (ascensos-descensos):** muestra primero los ascensos y luego los descensos.
- **Modificado (descensos-ascensos):** muestra primero los descensos y luego los ascensos.
- **Estructura jerárquica:** muestra los artículos ordenados por el identificador de estructura jerárquica, de menor a mayor.
- **Marca (AZ:** muestra los artículos ordenados por marca de la A a la Z)
- **Marca (ZA:** muestra los artículos ordenados por marca de la Z a la A)

![imagen alt <>](assets/relabelling_order_multi.png)

#### Artículo modal

Al configurar el cliente, es posible definir si el modal del artículo se abre inmediatamente cuando se corta el artículo o si la etiqueta se imprime inmediatamente.

El modal del artículo contiene la siguiente información:

- **Información del artículo:** foto (si corresponde), estado, descripción, EAN, SKU y SOH (si corresponde).
- **Dinámica de precios:** alerta de subida o bajada de precio, precio antiguo y precio nuevo.
- **Cantidad esperada:** Información sobre el número de etiquetas a marcar en el campo "Impresión".
- Entrada de **cantidad:** Entrada para definir el número de etiquetas reprogramadas.
- **Botón No encontrado / Continuar: le** permite establecer el artículo como No encontrado. Si la cantidad en la entrada es mayor que cero, el botón No encontrado se reemplaza por el botón "Continuar".

![imagen alt <>](assets/relabelling_item_modal.png)

#### Resumen

La pantalla de resumen de la tarea Markdown contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en las tareas.

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados en los que la cantidad reprogramada es igual a la cantidad esperada.
  - **PARCIAL:** Total de artículos procesados en los que la cantidad reprogramada es menor que la cantidad esperada.
  - **EN EXCESO:** Número total de artículos procesados en los que la cantidad reprogramada es mayor que la cantidad esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

Para las unidades, la pantalla de resumen de la tarea Volver a marcar contiene la siguiente información:

- **ESPERADOS:** Total de unidades previstas en las tareas.
- **PROCESADOS:** Total de unidades procesadas en las tareas. En los productos procesados existen las siguientes métricas:
- **EN EXCESO:** Total de unidades procesadas en las que la cantidad reprogramada es mayor que la cantidad esperada.
- **NO ENCONTRADO:** Total de unidades procesadas como no encontradas.

![imagen alt <>](assets/relabelling_resume.png)

### **IMPRESIÓN DE ETIQUETAS**

La función de impresión de etiquetas permite al usuario imprimir etiquetas ad hoc. De esta manera, siempre que la operación de la tienda identifique artículos que no tienen etiqueta o que están dañados, pueden usar la función de Impresión de etiquetas para realizar la impresión.

Una vez configurada para el cliente, la función de Impresión de etiquetas puede estar disponible en el resumen de tareas o en el menú de la aplicación.

![imagen alt <>](assets/labelprint_on_menu.gif)

#### Artículo modal

Al configurar el cliente, es posible definir si el modal del artículo se abre inmediatamente cuando se corta el artículo o si la etiqueta se imprime inmediatamente.

El modal del artículo contiene la siguiente información:

- **Información del artículo:** foto (si corresponde), estado, descripción, EAN, SKU y SOH (si corresponde).
- **Cantidad impresa:** información sobre la cantidad de etiquetas ya impresas para un artículo determinado.
- Entrada de **cantidad:** Entrada para definir la cantidad de etiquetas a imprimir.
- **Botón Imprimir:** Acción para imprimir el número de etiquetas definidas.

En el modal del artículo también es posible cambiar la etiqueta a imprimir y la impresora utilizada. Para ello, el usuario debe mantener presionado el icono de la impresora e inmediatamente aparecerá una pantalla con las impresoras disponibles y una lista de etiquetas que se pueden utilizar.

![imagen alt <>](assets/labelprint_modal_3.gif)

### **AUDITORÍA DE PRECIOS**

El propósito del tipo de tarea Auditoría de precios es permitirle auditar los precios presentes en la tienda. Mediante el pinchazo de etiquetas, TMR realiza la comparación de los valores de las etiquetas ( label_price ), los valores de ERP ( ERP_price ) y los valores de POS ( POS_price ).

De esta forma es posible auditar los precios presentes en la tienda y en caso de cualquier divergencia, el TMR imprime inmediatamente una nueva etiqueta con el precio actualizado.

#### Ubicación de la auditoría de precios

En el proceso de [creación de tareas ad hoc](multiplataforma/#criacao-de-tarefas-adhoc) , el usuario puede definir una ubicación para la tarea. Esta ubicación está asociada a la tarea y permite definir en qué área de la tienda se realizó la auditoría.

Hay dos ubicaciones disponibles para elegir:

- Tienda
- Almacenamiento

La ubicación elegida por el usuario está asociada con la tarea y está disponible para consulta en el detalle de la tarea en el Cockpit. Con esta información, el gerente de operaciones puede comprender qué elementos se auditan en cada ubicación y la cantidad total de precios divergentes o convergentes en cada ubicación.

![imagen alt <>](assets/priceaudit_location.png)

#### Inserción manual de precios

Durante el procesamiento de una tarea, cuando se corta un EAN o se ingresa manualmente la información del artículo, el TMR identifica que el precio de la etiqueta no ha sido informado y le pide al usuario que ingrese manualmente ese valor.

![imagen alt <>](assets/priceaudit_manual_insert.png)

En el caso de que se pique una etiqueta TMR que indique el precio del artículo, no se le pedirá al usuario que ingrese el precio. En este caso, el TMR dirige al usuario de inmediato al modal del artículo donde realiza la comparación de precios.

#### Auditoría modal y de precios de artículos

Después de obtener el precio de la etiqueta (mediante la lectura de la etiqueta o la introducción del manual), el artículo modal aparece inmediatamente. Al abrir el modal, TMR solicita precios de ERP y POS a través de una llamada a los servicios prestados por el cliente. Al comparar los valores, es posible que la TMR concluya si el precio de la etiqueta es divergente o convergente .

**Convergente**

En caso de que los valores sean iguales, TMR entiende que la auditoría es convergente. En este escenario, muestra el mensaje "Precios correctos". El artículo se procesa automáticamente sin necesidad de realizar ninguna otra acción.

![imagen alt <>](assets/priceaudit_equal_price.png)

**Divergente**

En caso de que los valores sean diferentes, TMR entiende que la auditoría es diferente. En este escenario presenta el mensaje "Divergencia encontrada". Inmediatamente, se imprime una nueva etiqueta y el artículo se considera procesado. Si es necesario volver a imprimir la etiqueta, el usuario tiene el botón de impresión disponible en la esquina superior derecha del modal.

![imagen alt <>](assets/priceaudit_not_equal_price.png)

#### Auditoría de dos precios

La tarea Auditoría de precios también le permite auditar dos precios simultáneamente. Esto solo se aplica a los clientes que ofrecen dos precios en la etiqueta y el servicio de precios que devuelve dos precios (ERP 1, ERP 2 POS 1 y POS 2).

La auditoría de dos precios mantiene el mismo flujo descrito anteriormente con la diferencia de auditar dos precios al mismo tiempo. En este sentido, un artículo es divergente si uno o los dos precios son diferentes y es convergente si los dos precios son iguales.

![imagen alt <>](assets/priceaudit_two_prices.png)

#### Resumen

La pantalla de resumen de la tarea Auditoría de precios contiene la siguiente información:

- **PLANIFICADO:** Total de artículos previstos en las tareas (solo aplica para tareas orientadas).

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **DIVERGENCIAS:** Total de artículos procesados con divergencia.
  - **SIN ETIQUETA:** Total de artículos procesados con información manual sobre el precio de la etiqueta.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea (solo aplica para tareas orientadas).

![imagen alt <>](assets/priceaudit_resume.png)

## TAREAS - REEMPLAZO

### **AUDITORÍA DE ESCANEO**

El propósito del tipo de tarea Scan Audit es permitir al usuario verificar la presencia de un determinado grupo de artículos en la tienda.

Para ello, el usuario pasa por la tienda y recoge los artículos en el tablero de ventas, permitiendo que el TMR reciba información de que el artículo está presente en la tienda.

Después de completar la tarea, esta información está disponible para su consulta y exportación en el Cockpit. Sobre esta información se realizarán cálculos adicionales que permitan inferir el porcentaje de pausa por tarea.

Con esta información, es posible que el gerente de la tienda conozca la cantidad de artículos rotos y la información de stock disponible por artículo.

#### Creación de la tarea de auditoría de análisis

En cuanto a la creación de tareas de Scan Audit, existen las siguientes posibilidades:

- Adhoc **basado en** listas **:** Posibilidad de crear tareas directamente en el dispositivo móvil mediante la definición de una lista de artículos previamente existente. Esta lista de artículos se crea en TMR Backoffice. Para obtener más detalles, consulte el capítulo sobre la [creación de tareas basadas en la lista](multiplataforma/#com-lista) .
- **Adhoc basado en estructura jerárquica:** Posibilidad de crear tareas directamente en el dispositivo móvil definiendo el nivel deseado de la estructura jerárquica. Para obtener más detalles, consulte el capítulo sobre la [creación de tareas por estructura jerárquica](multiplataforma/#por-estrutura-hierárquica) .
- **Programado:** Posibilidad de crear tareas a través de TMR Scheduler. A través del Programador es posible definir un horario único o una recurrencia automática en la creación de tareas. Para cada cronograma creado, es posible definir una lista de artículos a auditar o un nivel específico de la estructura jerárquica.

#### Auditoría de análisis dirigida

Configurando el cliente, es posible definir que las tareas de Scan Audit estén orientadas. Esto significa que la lista de artículos pendientes está disponible para su visualización mientras se realiza la tarea. Esta lista de artículos ayuda al usuario a comprender qué artículos se espera que sean tratados en la tarea a través de la fotografía (si corresponde) y la descripción del artículo.

![imagen alt <>](assets/scanaudit_oriented.png)

#### Auditoría de escaneo no dirigida

Configurando el cliente, es posible definir que las tareas de Scan Audit no estén orientadas. Esto significa que la lista de artículos pendientes no está disponible para su visualización mientras se realiza la tarea. Esto permite no influir en el usuario con una lista de artículos definidos, aunque la TMR valida en cada foto si el artículo pertenece a la tarea. También mediante la configuración del cliente, es posible definir si el usuario puede agregar ítems no previstos en las tareas o si solo puede recoger ítems previamente definidos en la creación de las mismas.

![imagen alt <>](assets/scanaudit_not_oriented.png)

En ambos escenarios (orientado o no orientado) los artículos triturados se presentarán en la pestaña Procesados.

#### Orden de reemplazo urgente

Durante el manejo de la tarea de auditoría de análisis, se solicita al usuario que solicite un reinicio urgente. Este facilitador está presente en la esquina superior derecha del modo de artículo. A través de este facilitador, es posible que el usuario defina un pedido de reemplazo y la cantidad respectiva para un artículo dado.

Inmediatamente se generará una tarea de Separación para el artículo solicitado.

![imagen alt <>](assets/replenish_item_on_modal.gif)

#### Resumen

La pantalla de resumen de la tarea de auditoría de análisis contiene la siguiente información:

- **ESPERADOS:** Total de artículos previstos en la tarea (solo aplica para tareas orientadas).

- PROCESADOS:

   Total de artículos procesados en la tarea. En los productos procesados existen las siguientes métricas:

  - **ESPERADOS:** Número total de artículos procesados previstos en la tarea.
  - **INESPERADOS:** Total de elementos procesados no previstos en la tarea (solo aplica para tareas que permiten agregar elementos inesperados).

- **PENDIENTE:** Total de elementos no procesados en la tarea (solo aplica para tareas orientadas).

![imagen alt <>](assets/scanaudit_resume.png)

### **AUDITORÍA EXTERNA**

El propósito del tipo de tarea Auditoría externa es permitir al usuario verificar la presencia de un determinado grupo de artículos en la tienda. Por esta razón, la Auditoría Externa es similar a la Auditoría de Escaneo pero con algunas especificidades:

#### Especificidades

- Fue diseñado para ser utilizado por un equipo externo a los procesos de la tienda, es decir, permite un flujo de proceso diferente al del Scan Audit. Como en el anterior, los cálculos del porcentaje de rotura también se realizan en base a los datos recopilados.
- Permite al equipo externo a la tienda auditar a través de este tipo de tareas el trabajo de la operación realizada en el Scan Audit. En el Cockpit es posible comparar los porcentajes de rotura calculados entre los dos tipos de tarea.
- Dado que la Auditoría Externa fue diseñada para ser operada por un equipo externo a la tienda, es posible definir (configurando al cliente) el número de zonas de conteo y la identificación de cada una. A diferencia de la auditoría de escaneo, que genera una sola tarea, la auditoría externa le permite configurar zonas de conteo que pueden ser realizadas simultáneamente por diferentes operadores.
- Cuando todas las zonas de tareas están cerradas, se lleva a cabo la consolidación de datos y los siguientes cálculos (por ejemplo, porcentaje de desglose) se basarán en datos ya consolidados.

#### Contando zonas

Al configurar el cliente, es posible definir zonas de conteo para tareas de Auditoría Externa.

De esta forma, al generar tareas (ad hoc o programadas), se crean zonas de conteo según la cantidad configurada y su respectiva identificación. Las zonas de recuento siempre están asociadas con la tarea principal de Auditoría externa.

Las zonas de recuento pueden estar orientadas (con los artículos predichos visibles) o no orientadas (lista de artículos predichos no visibles para el usuario) configurando el cliente.

![imagen alt <>](assets/externalaudit_zones_childs.gif)

#### Resumen

La pantalla de resumen de la tarea de Auditoría externa contiene la siguiente información:

- **PLANIFICADO:** Total de artículos previstos en las tareas (solo aplica para tareas orientadas).

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **ESPERADOS:** Número total de artículos procesados previstos en la tarea.
  - **INESPERADOS:** Total de elementos procesados no previstos en la tarea (solo aplica para tareas que permiten agregar elementos inesperados).

- **PENDIENTE:** Total de elementos no procesados en la tarea (solo aplica para tareas orientadas).

![imagen alt <>](assets/scanaudit_resume.png)

### **VERIFICACIÓN DE PRESENCIA**

El propósito del tipo de tarea Verificación de asistencia es permitirle confirmar la presencia de elementos que no se encuentran en las tareas Auditoría de análisis y Auditoría externa. Por este motivo, las tareas de Verificación de asistencia no se crean ahdoc ni se programan, sino que se generan automáticamente cuando se cierran las tareas de Escaneo y Auditoría Externa.

#### Generación de tareas

La generación de las tareas de Verificación de Asistencia son configurables por el cliente, es decir, es posible definir por cliente si se generan cuando se cierran las tareas de Escaneo y Auditoría Externa.

Si la configuración permite la generación de las tareas de Verificación de presencia, esta generación sigue la siguiente regla:

- Los artículos **no tratados** y **no encontrados** en el análisis y la auditoría externa y el stock mayor que cero se ingresarán automáticamente en la tarea de análisis de presencia.
- También existe la posibilidad de filtrar por estado del artículo. Es decir, es posible configurar el estado de los elementos que se incluirán en las tareas de Verificación de asistencia. Por ejemplo, puede definir si los elementos inactivos se incluyen en la tarea o solo los elementos activos y descontinuados.

#### Artículo modal

Durante el procesamiento de las tareas de Presence Check, al cortar un artículo, el modal se abre inmediatamente. Este comportamiento lo puede configurar el cliente.

El modal del artículo contiene la siguiente información:

- **Información del artículo:** El encabezado del modal muestra la fotografía (si aplica), estado, información SKU, EAN y SOH (si aplica).

- **PS y ventas:** PS de informacióny ventas diarias (solo se aplica a los clientes que brindan esta información).

- Motivo:

   menú desplegable que ofrece dos motivos:

  - Artículo no lineal
  - Artículo en otro lugar

Estos motivos permiten identificar el lugar donde se encontró el artículo. Esta información está disponible para su consulta en el detalle del artículo en TMR Cockpit. Esta información permite que la operación de la tienda comprenda si los artículos están en las ubicaciones correctas o si son necesarios cambios en el planograma de la tienda.

Configurando el cliente, es posible definir si los motivos están presentes en el artículo modal para la selección. Si están presentes, su llenado es obligatorio porque no es posible procesar el artículo sin definir un motivo.

![imagen alt <>](assets/presencecheck_modal.gif)

#### Resumen

La pantalla de resumen de la tarea Verificación de asistencia contiene la siguiente información:

- **ESPERADOS:** Total de artículos previstos en las tareas.

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **ESPERADOS:** Número total de artículos procesados previstos en la tarea.
  - **INESPERADOS:** Total de elementos procesados no previstos en la tarea (solo aplica para tareas que permiten agregar elementos inesperados).

- **PENDIENTE:** Total de elementos no procesados en la tarea.

![imagen alt <>](assets/presencecheck_resume.png)

### **AUDITORÍA DE RUTUROS**

Una Auditoría de Ruptura permite identificar artículos en la tienda que están parcialmente rotos (hay un artículo en la tienda pero en menor cantidad que el espacio disponible) o en ruptura total (no hay ningún artículo en la tienda). También permite la confirmación de la cantidad a reponer de un artículo en el lineal.

Al crear la tarea Auditoría de rupturas, es posible definir una ubicación. Esta información se transferirá a la tarea de Separación consiguiente, lo que permitirá al usuario consultar la ubicación de los artículos para su reemplazo más rápidamente.

![imagen alt <>](assets/stockoutaudit_location.png)

Hay dos tipos de auditoría de rupturas:

- Guiado: el usuario tiene una lista de artículos para guiar el procesamiento de la tarea.
- Adhoc: sin lista de artículos previamente definida. La creación de tareas ad hoc se puede consultar en el apartado [Creación](multiplataforma/#criacao-de-tarefas-adhoc) de tareas ad hoc .

#### Orientada

Al seleccionar Auditoría de Fallos en el resumen de tareas, el usuario tendrá acceso a la lista de tareas disponibles que se pueden descargar al dispositivo, permitiendo así su ejecución en modo offline. Tenga en cuenta que en el modo fuera de línea el usuario no tendrá acceso a cierta información como Stock de presentación, Ventas del día y Stock disponible, ya que se ordenan en línea durante el manejo de la tarea.

Después de descargar la tarea, se presenta una lista de artículos a tratar, que pueden tener o no una cantidad esperada asociada. En este último caso, se espera que el usuario confirme que efectivamente esta es la cantidad del artículo que debe ser reemplazado, pudiendo cambiar el valor en cualquier momento.

![imagen alt <>](assets/stockoutaudit_item_list.png)

#### Artículo modal

Al cortar un código de barras o seleccionar el artículo de la lista, el modal del artículo aparece inmediatamente. En este modal hay dos posibles entradas:

- **Cantidad lineal: le** permite ingresar la cantidad de artículos presentes en el lineal.
- **Cantidad para restablecer: le** permite ingresar la cantidad deseada para restablecer. Esta cantidad se transferirá a la tarea de separación consiguiente como una cantidad a separar.

![imagen alt <>](assets/stockoutaudit_item_modal.png)

La información " PS " y " VENTAS " también está presente en el modal . Esta información consultada online permite calcular la cantidad a reponer de un determinado artículo utilizando la cantidad presente en la línea.

**Nota:** Estos valores solo están disponibles para los clientes que tengan un servicio que permita consultar esta información.

En el caso de la información de PS , el usuario solo tiene que ingresar la Cantidad Lineal y la Cantidad a Reemplazar se calcula automáticamente de la siguiente manera:

![imagen alt <>](assets/stockoutaudit_PS.png)

Aunque se calcula automáticamente, el usuario puede cambiar el valor de la entrada "Cantidad a reponer" en cualquier momento.

##### CAJAS PARA FACILITADORES

El tipo de elemento del tipo de tarea Auditoría de ruptura también incluye un facilitador de unidad / efectivo. Esto permite al usuario colocar una cantidad a reponer informando la cantidad de cajas presentes y la cantidad de unidades por caja.

**CAJA**

Este facilitador le permite indicar el número de cajas y el número de unidades por caja dentro de la lista preexistente. El resultado de este cálculo se coloca en la entrada "Cantidad a reponer".

![imagen alt <>](assets/stockoutaudit_box.png)

**DINERO GRATIS**

Este facilitador es similar al anterior, con la particularidad de permitir al usuario informar libremente el número de unidades por caja. El resultado del cálculo tiene el mismo comportamiento que el mencionado anteriormente.

![imagen alt <>](assets/stockoutaudit_box_free.png)

Después de ingresar la cantidad a reiniciar, el botón "Continuar" está disponible. Al seleccionar "Continuar", el modal se cierra inmediatamente y el artículo pasa a la pestaña de procesado con la cantidad indicada.

Si el usuario no ingresa un valor en la cantidad a reponer, el botón "No he encontrado" está disponible. Si el usuario selecciona "No he encontrado", el artículo se procesa como no encontrado y se descarta para la consiguiente tarea de separación.

![imagen alt <>](assets/stockoutaudit_item_not_found.png)

#### Moda Clientes

Para adaptar la tarea Rupture Audit a la especificidad del catálogo de moda, en el modal del artículo está presente una función para enumerar los artículos skus que pertenecen al mismo producto (solo para clientes de moda). El propósito de esta característica es permitir auditar la rotura de skus que no están presentes en el lineal mediante el astillado de un sku hermano (que pertenece al mismo producto).

Para ello, el usuario debe seleccionar el botón "Más artículos" presente en el modal y TMR proporciona una lista de skus asociados con el mismo producto. En esta lista el usuario puede poner una cantidad para ser reemplazada en cualquier artículo y se agregarán como adhoc en las tareas.

![imagen alt <>](assets/stockoutaudit_more_items_modal.gif)

#### Resumen

La pantalla de resumen de la tarea Auditoría de rupturas contiene la siguiente información:

- **PLANIFICADO:** Total de artículos previstos en las tareas (solo aplica para tareas orientadas).

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados con cantidad tratada igual a la cantidad esperada.
  - **PARCIAL:** Número total de artículos procesados con una cantidad menor a la esperada.
  - **EN EXCESO:** Número total de artículos procesados con una cantidad superior a la esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea (solo aplica para tareas orientadas).

![imagen alt <>](assets/stockoutaudit_resume.png)

### **SEPARACIÓN**

El propósito del tipo de tarea Separación es permitir que el usuario separe los artículos que se reemplazarán en la tienda. Las tareas de separación siempre se generan por flujo de trabajo de tareas, es decir, a través de tareas de Auditoría de rupturas, solicitudes de reemplazo urgentes, Verificación de presencia, Auditoría de escaneo o Auditoría externa.

Para ver el flujo completo de tareas que pueden conducir a la Separación, consulte la pestaña [Flujo de trabajo de tareas](workflow/) .

#### Separación: clientes minoristas de alimentos

Las tareas de separación siempre están orientadas, es decir, los elementos previstos en la tarea siempre son visibles para el usuario. De esta manera, el usuario puede comprender más rápidamente qué artículos se solicitan para la separación mediante la ayuda de la fotografía (si corresponde) y la descripción del artículo.

En este sentido, los artículos son visibles en la lista de artículos pendientes e incluso pueden contener información sobre la cantidad a reemplazar - "Cantidad esperada". Si la tarea que dio lugar a la separación no indica la cantidad esperada para un determinado artículo, esta información no es visible para el operador.

![imagen alt <>](assets/replenishprep_item_list.png)

##### MODAL DE ARTÍCULO - ALIMENTACIÓN

Durante el procesamiento de la tarea, al cortar un producto, el modo de artículo se abre inmediatamente. Este modal contiene la siguiente información:

- **Información del artículo:** El encabezado del modal muestra la fotografía (si aplica), estado, información SKU, EAN y SOH (si aplica).
- **PS y Ventas:** Contiene información sobre la cantidad a separar, PS y Ventas (si corresponde).
- Entrada de **cantidad:** Entrada para definir la cantidad que se separará de un artículo determinado.
- **Botón No encontrado** Le permite establecer el artículo como No encontrado.
- Botón **"Devolver / Retirar:** " Le permite definir que el artículo está en la ubicación esperada pero que no está en condiciones de separarse. (ejemplo: artículo que se encuentra en período de desistimiento y por lo tanto no puede ser reemplazado en una tienda a la venta) De esta forma el artículo se procesará con cantidad cero.
- **Cajas y Cajas Gratis:** Facilitador de insertar cantidad por "Cajas" y "Cajas Gratis".

![imagen alt <>](assets/replenishprep_item_modal.png)

#### Separación - Clientes de moda

Las tareas de separación se adaptan a las necesidades de operación de los clientes de moda a través de la configuración. Los clientes de moda disponen de un catálogo compuesto por productos y skus. En este catálogo los productos son el modelo y los skus son los diferentes colores y tamaños. Ejemplo: El modelo de camiseta es el producto y los skus son de talla L, M y XL. Asimismo, los diferentes colores de la camiseta también son skus del mismo producto.

En este sentido, las tareas de Separación tienen un comportamiento diferente para adaptarse a las necesidades específicas del catálogo de moda.

##### ARTÍCULO MODAL - MODA

Las tareas de separación para los clientes de moda están orientadas al sku como las de los clientes de alimentación. Es decir, en la lista de artículos pendientes aparecen los skus (colores y tallas), los cuales deben estar separados.

Durante el procesamiento de la tarea, al cortar un sku predicho, se abre inmediatamente el modo de elemento. Este modal presenta el sku picado y todos los skus que pertenecen al mismo producto. Esto permite al usuario separar colores y tamaños distintos a los previstos inicialmente en la tarea.

Para esto, el modal contiene una entrada de cantidad en cada sku y la información de la cantidad esperada. El usuario podrá así poner cantidad en cada sku que quiera separar. Estos skus imprevistos se agregarán como ad hoc en la tarea.

![imagen alt <>](assets/replenisprep_fashion.gif)

#### Resumen

La pantalla de resumen de la tarea de separación contiene la siguiente información:

- **ESPERADOS:** Total de artículos previstos en la tarea.

- PROCESADOS:

   Total de artículos procesados en la tarea. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados en los que la cantidad separada es igual a la cantidad esperada.
  - **PARCIAL:** Total de artículos procesados en los que la cantidad separada es menor que la cantidad esperada.
  - **EN EXCESO:** Número total de artículos procesados en los que la cantidad separada es mayor que la cantidad esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.
  - **SIN CANTIDAD:** Total de artículos procesados Devolución / retirada.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

![imagen alt <>](assets/replenishprep_resume.png)

### **REEMPLAZO**

El propósito del tipo de tarea Reemplazo es permitir al usuario confirmar la separación de los artículos que se vuelven a almacenar. Por este motivo, las tareas de Reemplazo se orientan y generan exclusivamente al final de una tarea de Separación.

Los elementos tratados en la Separación con una cantidad mayor que cero generan automáticamente, al final de la tarea, una tarea de Reemplazo.

Al igual que en Separación, las tareas de Reemplazo están orientadas a permitir al usuario reconocer más rápidamente los artículos separados que deben ser almacenados.

Al consultar el Cockpit, es posible ver si todos los artículos separados realmente se han reemplazado en la tienda. Esta métrica es importante para comprender la calidad del reemplazo de la tienda.

#### Artículo modal

Durante el procesamiento de la tarea, al cortar un producto, el modo de artículo se abre inmediatamente. Este modal contiene la siguiente información:

- **Información del artículo:** El encabezado del modal muestra la fotografía (si aplica), estado, información SKU, EAN y SOH (si aplica).
- **Restablecer, origen y ubicación:** contiene información sobre la cantidad a restablecer, el origen del pedido y la ubicación (si corresponde).
- Entrada de **cantidad:** Entrada para definir la cantidad que reemplazará un determinado artículo.
- Botón **"Devolver / Retirar:** " Le permite definir que el artículo está en la ubicación esperada pero que no está en condiciones de separarse. (ejemplo: artículo que se encuentra en período de desistimiento y por lo tanto no puede ser reemplazado en una tienda a la venta) De esta forma el artículo se procesará con cantidad cero. Si la cantidad en la entrada es mayor que 0, el botón "Devolver / Retirar" desaparece y aparece "Continuar".
- **Cajas y Cajas Gratis:** Facilitador de insertar cantidad por "Cajas" y "Cajas Gratis".

![imagen alt <>](assets/replenish_modal.png)

#### Resumen

La pantalla de resumen de la tarea de confirmación contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en las tareas.

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados en los que la cantidad separada es igual a la cantidad esperada.
  - **PARCIAL:** Total de artículos procesados en los que la cantidad separada es menor que la cantidad esperada.
  - **EN EXCESO:** Número total de artículos procesados en los que la cantidad separada es mayor que la cantidad esperada.
  - **SIN CANTIDAD:** Total de artículos procesados Devolución / retirada.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

Para las unidades, la pantalla de resumen de la tarea de confirmación contiene la siguiente información:

- **ESPERADOS:** Total de unidades previstas en las tareas.

- PROCESADOS:

   Total de unidades procesadas en las tareas. En productos procesados existe la siguiente métrica:

  - **EN EXCESO:** Total de unidades procesadas en las que la cantidad reabastecida es mayor que la cantidad esperada.
  - **PARCIAL:** Total de unidades procesadas en las que la cantidad reabastecida es menor que la cantidad esperada.

![imagen alt <>](assets/replenish_resume.png)

## TAREAS - INVENTARIOS | RECONTAJE | RECUENTO DE STOCK

### **INVENTARIO**

La función Inventario le permite verificar y corregir la cantidad de artículos que hay en stock tanto en la tienda como en el almacén.

**Tareas de inventario, zonas de recuento, lecturas y corrección de divergencias**

**Las** tareas de **inventario** siempre están integradas por ERP y el propósito de esta tarea es permitirle confirmar el stock de un determinado grupo de artículos cortando gradualmente el stock total existente en la tienda y el almacén.

Las tareas de Inventario a las que se hace referencia en el punto anterior son tareas de agregación, es decir, son tareas que agregan un cierto número de tareas secundarias llamadas **Zonas de Conteo** . Estas zonas se definen al momento de integrar el inventario y permiten definir un área geográfica específica de la tienda o almacén donde se contarán los artículos. Esto significa, por ejemplo, que en la zona de recuento "Abarrotes", solo se tratarán los productos comestibles que pertenezcan al inventario. De la misma forma, cada zona de conteo puede ser manejada por diferentes usuarios permitiendo una ejecución más ágil del propio inventario.

Al integrar las tareas de Inventario, también es posible definir si cada zona de conteo tendrá **una o dos lecturas obligatorias** . Si la zona de recuento incluye una segunda, se generan dos tareas de lectura para esa misma zona de recuento y el nombre de la tarea incluirá la información a la que corresponde la lectura a una determinada tarea. El beneficio de dos lecturas obligatorias es una reducción significativa en el error de ejecución del inventario, ya que cada zona de recuento se tratará dos veces y todos los artículos se cortarán dos veces.

Las dos lecturas mencionadas anteriormente tienen la obligación de **no divergencia** , es decir, los artículos y cantidades procesadas en las dos lecturas deberán ser exactamente iguales. En caso de diferencia entre productos o cantidades, el TMR genera inmediatamente una tarea de **Corrección de Divergencia** asociada a la zona de recuento en tratamiento. Esta tarea está disponible inmediatamente al cierre de la segunda lectura y permite corregir las diferencias encontradas entre las dos lecturas anteriores.

#### Tarea de inventario y zonas de recuento

Al ingresar al menú Inventario, el usuario tendrá acceso a la lista de tareas disponibles, que puede manejar en el momento. Cada tarea de Inventario consta de un conjunto de zonas, lo que permite que el mismo inventario sea manejado por más de un usuario al mismo tiempo.

En la lista de tareas de Inventario hay un facilitador de búsqueda que permite al usuario buscar un área de conteo específica leyendo el código de barras. Este facilitador está disponible para los clientes que incluyan un código de barras que identifique la zona de conteo. El TMR le permite configurar una regla de código de barras que identifica el inventario y la zona de conteo respectiva. De esta forma, cuando el usuario lee este código de barras, la aplicación busca en la zona de conteo y su inventario y automáticamente descarga la tarea respectiva.

![imagen alt <>](assets/inventory_zones.gif)

#### Creación de Zonas Adhoc

Durante el Inventario, y si no hay suficientes zonas de recuento, el usuario puede solicitar nuevas zonas de recuento directamente en la aplicación móvil. Estas zonas solicitadas para ad hoc pueden contener una o dos lecturas obligatorias. Además, el usuario puede definir el nombre de las zonas ad hoc creadas en la aplicación. Si ya existe una zona creada con el mismo nombre, la aplicación indicará en la respuesta cuál es el nombre de la zona creada.

![imagen alt <>](assets/inventory_create_adhoc_2.gif)

#### Artículo modal

En caso de que el artículo triturado sea un artículo de **peso variable** , debe abrir el modal de información del artículo y completar el peso leído en la etiqueta en la entrada de peso del artículo. En caso de picar artículos más iguales, pero con diferente peso, el operador siempre debe indicar el peso del artículo leído en esta entrada. La cantidad del artículo debe agregarse y presentarse en el "TOTAL".

![imagen alt <>](assets/inventory_modal_insert_price.png)

En caso de que el artículo triturado sea de **Precio Variable** , se debe solicitar al usuario que ingrese el peso del artículo y así se calcula el precio por kilo. En los siguientes apartados del mismo artículo, este cálculo se utilizará para obtener el peso del artículo leído. En el modal del artículo se presentará la información del peso acumulado del artículo, el precio por kilo calculado y el peso del artículo (calculado en base al precio por kilo). Este peso del artículo debería ser posible cambiar.

![imagen alt <>](assets/inventory_modal_insert_weight.png)

Si los artículos no son de peso o precio variable, el astillado se puede realizar de forma sucesiva (barrido) o incremental. Para que se haga de forma incremental es necesario acceder al modal y poner el valor total de los artículos existentes en el área.

Durante el tratamiento de la tarea, es posible cambiar entre el modo "Normal" y el "Incremental". El usuario debe acceder al botón flotante y hacer clic en la entrada para cambiar el modo de corte.

![imagen alt <>](assets/inventory_picking_mode.gif)

Si el usuario selecciona "Modo normal", significa que el modal del artículo se abre en cada picadura. En este caso, la cantidad picada se muestra en la entrada modal.

Si el usuario selecciona "Modo incremental", significa que el modal del artículo no se abre en cada chip. En este caso, la cantidad picada se muestra en la alerta "Cancelar" en la parte inferior de la pantalla.

![imagen alt <>](assets/inventory_allow_undo.png)

#### Corrección de divergencia

Cuando haya una divergencia de artículos entre la 1ª y la 2ª lectura, se creará automáticamente una tarea para corregir la divergencia. Las tareas de Corrección de divergencias son tareas orientadas con todos los artículos divergentes en el área de conteo en cuestión. Al abrir el modo artículo, está presente la información de recuento de la 1ª y 2ª lectura y la entrada para poner la cantidad final del artículo.

![imagen alt <>](assets/inventory_modal_diverngence.png)

#### Resumen

La pantalla de resumen de la tarea de inventario contiene la siguiente información en relación con los artículos:

- **PROCESADOS:** Total de artículos procesados en las tareas.

Para las unidades, la pantalla de resumen de la tarea de Inventario contiene la siguiente información:

- **PROCESADOS:** Total de unidades procesadas en las tareas.

![imagen alt <>](assets/inventory_resume.png)

### **RECUENTO**

Las tareas de recuento son tareas generadas después de la crítica de inventario. Estas tareas se utilizan para corregir cualquier discrepancia de stock detectada en el momento de la revisión.

Los recuentos son tareas guiadas, en las que los artículos fueron seleccionados durante el Inventario Crítica para el recuento y consecuente ajuste de stock. Al cerrar las tareas de Recuento, la información anterior será reemplazada por la información actual.

#### Artículo modal

El modal del artículo contiene la siguiente información:

- **Información del artículo:** Fotografía (si corresponde), estado, descripción, EAN, SKU y SOH (si corresponde y configurable).
- **Total:** Información de la cantidad procesada previamente.
- Entrada de **cantidad:** Entrada para definir el número de unidades o peso.
- **Botón No encontrado / Continuar: le** permite establecer el artículo como No encontrado. Si la cantidad en la entrada es mayor que cero, el botón "No he encontrado" se reemplaza por el botón "Continuar".

![imagen alt <>](assets/recounting_modal.png)

#### Resumen

La pantalla de resumen de la tarea Recuento contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en la tarea.

- PROCESADOS:

   Total de artículos procesados en la tarea. En productos procesados existe la siguiente métrica:

  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

Para las unidades, la pantalla de resumen de la tarea de recuento contiene la siguiente información:

- **PROCESADO:** Total de unidades procesadas en la tarea. En productos procesados existe la siguiente métrica:
- **NO ENCONTRADO:** Total de unidades procesadas como no encontradas.

### **RECUENTO DE STOCK**

La función de recuento de existencias le permite contar artículos para el ajuste de existencias en diferentes áreas de la tienda. Para garantizar la agregación de tareas de diferentes zonas, mediante la configuración del cliente, la aplicación permite la creación de tareas con afiliación. Esto significa que cuando se crea la tarea, se genera una tarea de conteo "madre" con una tarea "hija" para cada zona definida.

Ejemplo:

- Contando Fresco:

   \- Tarea "madre".

  - **Almacén de Frescos:** - Tarea "hija".
  - **Tienda Frescos:** - tarea "hija"

No es posible descargar tareas 'principales', solo sirven como un agregador de tareas. De esta manera, las tareas 'secundarias' son tareas individuales y pueden ser realizadas por diferentes usuarios. El estado de la tarea 'principal' evoluciona a cerrada cuando se terminan todas las tareas 'secundarias'.

#### Procesamiento de tareas

Las tareas de recuento de existencias pueden ser específicas o ad hoc. La tarea ad hoc no está orientada (no tiene artículos previstos) y permite al usuario procesar artículos libremente.

Las tareas guiadas (con artículos visibles para el usuario) se pueden crear ad hoc en base a una [lista](multiplataforma/#com-lista) o estructura [jerárquica](multiplataforma/#por-estrutura-hierárquica) . Las tareas guiadas también se pueden generar automáticamente cuando se cierran otras tareas. Para obtener más detalles, consulte [Flujo de trabajo de tareas](workflow/) .

Al ingresar al menú Stock Count, el usuario tendrá acceso a la lista de tareas disponibles, la cual podrá descargar al dispositivo, permitiendo así que la tarea se ejecute fuera de línea.

Si la tarea seleccionada tiene afiliación, el usuario es redirigido a una pantalla con tareas "secundarias". Después de descargar la tarea, el usuario es redirigido a la lista de artículos disponibles (solo se aplica a las tareas guiadas).

![imagen alt <>](assets/stockcount_zones.gif)

#### Artículo modal

Al cortar un código de barras o seleccionar manualmente un artículo de la lista, aparece el modo de información del artículo donde el usuario puede colocar las unidades contadas del artículo o ingresar el número de casillas.

El modal del artículo contiene la siguiente información:

- **Información del artículo:** Fotografía (si corresponde), estado, descripción, EAN, SKU y SOH (si corresponde y configurable).
- **Total:** Información de la cantidad procesada previamente.
- Entrada de **cantidad:** Entrada para definir el número de unidades o peso.
- **Botón No encontrado / Continuar: le** permite establecer el artículo como No encontrado. Si la cantidad en la entrada es mayor que cero, el botón "No he encontrado" se reemplaza por el botón "Continuar".

![imagen alt <>](assets/stockcount_modal.png)

#### Resumen

La pantalla de resumen de la tarea Recuento de existencias contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en las tareas. (solo se aplica a orientado a tareas)

- PROCESADOS:

   Total de artículos procesados en las tareas. En productos procesados existe la siguiente métrica:

  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea (solo aplica para tareas orientadas).

Para las unidades, la pantalla de resumen de la tarea Recuento de existencias contiene la siguiente información:

- **PROCESADOS:** Total de unidades procesadas en las tareas. En productos procesados existe la siguiente métrica:
- **NO ENCONTRADO:** Total de unidades procesadas como no encontradas.

![imagen alt <>](assets/stockcount_resume.png)

## TAREA - RECEPCIÓN

### RECEPCIÓN

La función de Recepción le permite recibir mercadería proveniente de almacenes, proveedores u otras tiendas. Al finalizar las tareas, la información de los artículos o cajas recibidos se envía a ERP y consecuente ajuste de stock.

TMR permite volver a listar Recepción de dos formas configurando la tarea:

- **Artículo por artículo:** El usuario revisa todos los artículos presentes en una caja o soporte.
- **Caja o Soporte:** El usuario marca solo las cajas o soportes integrados en la tarea y todos los elementos presentes en esas cajas o soportes serán marcados automáticamente.

#### Procesamiento de tareas

Al acceder a la Recepción presente en el resumen de tareas, el usuario tendrá acceso a la lista de tareas disponibles, que podrá descargar al dispositivo. Si el usuario desea recibir mercadería que pertenece a una tarea que no está disponible, la aplicación permite buscar tareas futuras, utilizando la Orden de Compra, Guía de Transporte o simplemente por la fecha de la tarea.

De esta forma, solo se presentarán las tareas que coincidan con los criterios de búsqueda utilizados.

##### BÚSQUEDA DE TAREAS

Para acceder a la búsqueda avanzada, el usuario debe mantener presionado el botón de búsqueda. De esta forma, la aplicación móvil proporciona una pantalla con los insumos para la investigación. Configurando el cliente, es posible definir qué entradas se presentarán.

![imagen alt <>](assets/receiving_task_search.gif)

##### RECEPCIÓN DE CAJAS O SOPORTES

Al descargar la tarea, el usuario tendrá una lista de las cajas o soportes proporcionados. El usuario puede ingresar el código de la caja mediante inserción manual o mediante bip. De esta forma, se recibirán automáticamente las cajas o soportes y todos los artículos contenidos en ellos.

Si desea consultar artículo por artículo, puede seleccionar la casilla esperada y, en consecuencia, procesar los artículos presentes. Para sumar el número de unidades correspondientes al artículo, el usuario abre el modal del artículo, teniendo aquí la posibilidad de incrementar la cantidad mediante el ingreso de unidades, cajas o cajas libres.

![imagen alt <>](assets/receiving_picking_item.gif)

##### CAJAS DE SEGURIDAD

Dependiendo de las necesidades específicas de cada negocio, es posible definir atributos específicos para el cajero. TMR permite distinguir cajas cuyo contenido es de mayor valor y mantener reglas específicas asociadas a esa caja. Si el código de esa caja está asociado a una pre-definición de su propia seguridad, entonces el usuario recibirá la caja de una forma diferente, garantizando la seguridad de su contenido.

El astillado sigue reglas diferentes: estas cajas se reciben con sellos de seguridad que deben registrarse en la aplicación. Al pinchar la caja se abrirá el modo de seguridad, donde el usuario deberá insertar correctamente el código de los sellos. Tienes 3 oportunidades para registrarte con éxito. En caso de avería, la caja se considerará dañada, obligando al usuario a cortar obligatoriamente los artículos.

Durante el proceso de Recepción de una caja de seguridad, si los precintos no están disponibles, el usuario puede informar al TMR a través de la opción: "Sin información de precintos de seguridad".

![imagen alt <>](assets/receiving_container_safety.gif)

##### CAJAS NO PREVISTAS EN LA TAREA

La parametrización en el TMR también le permite personalizar la flexibilidad de la tarea en la aceptación y procesamiento de artículos y cajas no previstas. Cuando el usuario pincha o ingresa manualmente un EAN no previsto en la tarea, el TMR pregunta si quiere recibirlo como un artículo o como una caja. Al seleccionar un cuadro, se inserta en la tarea como un ad hoc, lo que le permite incluso recibir artículos dentro de este cuadro o volumen ad hoc.

![imagen alt <>](assets/receiving_container_adhoc.gif)

##### CAJAS SIN ETIQUETA

De manera similar a la recepción de buzones ad hoc, también es posible agregar cuadros sin etiquetar a la tarea, configurando el cliente. Las cajas sin etiqueta son cajas que no tienen identificación o que no se pueden leer. Para poder recibir esta caja y todos los elementos insertados en ella, la aplicación móvil proporciona al usuario un facilitador de inserción de cajas.

Esta opción es configurable para el cliente y permite la creación de un cuadro no identificado dentro de una tarea. Tanto la caja como los elementos insertados en ella son naturalmente ad hoc. Después de insertar la caja, es necesario cortar los artículos y, por lo tanto, la aplicación dirige automáticamente a la pantalla de corte de artículos.

![imagen alt <>](assets/receiving_container_without_label.gif)

##### RECIBO DE ARTÍCULO EN OTRA CAJA

Durante el procesamiento de la tarea si el usuario escoge un ítem que no pertenece a una determinada casilla, la aplicación móvil busca a qué casillas pertenece el ítem y le pregunta al usuario si quiere agregar en la casilla actual como una carpeta o si quiere recibir en la caja donde se encuentra el artículo.

![imagen alt <>](assets/receiving_item_in_other_container.gif)

##### CREACIÓN DE CAJA VIRTUAL

El concepto de caja virtual significa que durante el procesamiento de la tarea de Recepción es posible procesar los artículos y asociarlos a cajas virtuales creadas durante su ejecución. La diferencia con el proceso de creación de cajero previamente documentado es que en el proceso de cajero virtual es posible cambiar artículos entre cajas. En este sentido, el usuario crea la caja virtual y se convierte en la caja virtual activa. Todos los artículos procesados posteriormente se asociarán con esa misma caja.

En cualquier momento es posible crear nuevas cajas virtuales y también cambiar la caja activa con la que están asociados los elementos procesados.

En el botón de cajero virtual, previa configuración al cliente, se encuentran disponibles las siguientes opciones:

- **Creación de caja / volumen:** Option + le permite crear una nueva caja y nombrarla inmediatamente. Una vez creado, el cuadro actual está activo y todos los elementos procesados posteriormente se asocian con el cuadro activo.
- Edición de **caja / volumen:** la opción de edición le permite cambiar el nombre de una caja existente.
- **Cambiar caja / volumen: Al** procesar artículos, el usuario puede cambiar entre cajas existentes.
- **Impresión:** Opción que permite la impresión móvil de la etiqueta identificativa de la caja.

![imagen alt <>](assets/receiving_manage_container.gif)

##### RECIBIENDO EL ARTICULO

Las tareas de recepción solo pueden estar orientadas al artículo y no contienen recuadros como vimos en los puntos anteriores. Esta configuración depende únicamente de cómo el cliente entiende su funcionamiento y la TMR admite cualquiera de estos comportamientos.

Si la tarea está orientada solo a artículos, la lista de ítems pendientes no aparece en recuadros o volúmenes, solo los artículos a recibir. El usuario debe verificar el EAN o seleccionar el artículo de la lista para recibirlo.

![imagen alt <>](assets/receiving_items.png)

##### ALERTAS: RECUADROS Y ARTÍCULOS

En una tarea de Recepción, se pueden enviar cajas y artículos con alertas. Las alertas son identificadores de la tipología de cada artículo que conforma las tareas de recepción. Por ejemplo, un artículo que pertenece al catálogo actual viene con esta alerta asociada.

Por este motivo, el objetivo es que la aplicación muestre las alertas asociadas a cada artículo. Además, también es objetivo presentar la misma alerta a nivel de caja, para que el usuario tenga información sobre qué tipos de artículos hay en cada caja.

Los artículos o casillas con alertas asociadas incluyen la indicación visual a través del símbolo de notificación azul presente en la línea o casilla del artículo.

Para acceder a la lista de alertas, el usuario debe mantener presionada la línea o casilla del producto y en esa lista se muestran todas las alertas asociadas.

![imagen alt <>](assets/receiving_alerts.gif)

##### FECHAS DE VENCIMIENTO

Durante el manejo de una tarea de Recepción, el TMR permite recibir la fecha de vencimiento y la información de cantidad asociada a un determinado artículo. Esta funcionalidad es configurable mediante el comportamiento definido en la integración de la tarea.

![imagen alt <>](assets/receiving_expiration_audit.gif)

Además de la recopilación de la fecha de vencimiento, también es posible, por configuración en la integración de tareas, controlar la fecha de vencimiento ingresada. Así, es posible definir los días mínimos para recibir la fecha de vencimiento.

Por ejemplo: si se define en la tarea que no se puede recibir un artículo con una fecha mínima de 5 días, se alerta al usuario o se le impide recibir una fecha menor a 5 días en el futuro en relación a la fecha actual de recepción de la tarea.

Configurando el cliente es posible definir si se impide la recepción de una fecha de caducidad menor a los días mínimos definidos:

![imagen alt <>](assets/receiving_control_not_allow.png)

O si la recepción está permitida solo con una alerta al usuario.

![imagen alt <>](assets/receiving_control_allow.png)

#### Resumen: cajas y volúmenes

La pantalla resumen de las Cajas o Volúmenes en las tareas de Recepción contiene la siguiente información en relación a los artículos:

- **ESPERADOS:** Total de artículos previstos en las tareas.

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados en los que la cantidad recibida es igual a la cantidad esperada.
  - **PARCIAL:** Total de artículos procesados en los que la cantidad recibida es menor a la esperada.
  - **EN EXCESO:** Total de artículos procesados en los que la cantidad recibida es mayor a la esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

En relación a las unidades, la pantalla de resumen de la tarea de recepción contiene la siguiente información:

- **ESPERADOS:** Total de unidades previstas en las tareas.

- PROCESADOS:

   Total de unidades procesadas en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN EXCESO:** Total de unidades procesadas en las que la cantidad recibida es mayor a la esperada.
  - **NO ENCONTRADO:** Total de unidades procesadas como no encontradas.

![imagen alt <>](assets/receiving_resume_container.png)

#### Resumen: tarea

La pantalla de resumen de la tarea de recepción contiene la siguiente información sobre cajas o volúmenes:

- **ESPERADOS:** Total de cajas o volúmenes previstos en las tareas.

- **PROCESADOS:** Total de cajas o volúmenes procesados en las tareas.

- SEGURIDAD:

   Total de cajas o volúmenes procesados con el atributo de cajas de seguridad. En las cajas de seguridad se encuentra la siguiente métrica:

  - **DAÑADOS:** Total de cajas y volúmenes procesados como dañados.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

La pantalla de resumen de la tarea de recepción contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en las tareas.

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Número total de artículos procesados en los que la cantidad recibida es igual a la cantidad esperada.
  - **PARCIAL:** Total de artículos procesados en los que la cantidad recibida es menor a la esperada.
  - **EN EXCESO:** Total de artículos procesados en los que la cantidad recibida es mayor a la esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

Para las unidades, la pantalla de resumen de la tarea de recepción contiene la siguiente información:

- **ESPERADOS:** Total de unidades previstas en las tareas.

- PROCESADOS:

   Total de unidades procesadas en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN EXCESO:** Total de unidades procesadas en las que la cantidad recibida es mayor a la esperada.
  - **NO ENCONTRADO:** Total de unidades procesadas como no encontradas.

También en la pantalla de resumen de la tarea de Recepción, se encuentra disponible información sobre detalles de la tarea, como guías de facturación y transporte. Esta información debe estar integrada por la tarea.

![imagen alt <>](assets/receiving_resume.png)

#### Aprobación de la tarea

Tras la configuración al cliente, la funcionalidad de Recepciones permite al usuario al final de la tarea tener visibilidad de información previamente definida (alertas) o entradas disponibles para agregar la información necesaria a la tarea.

##### ALERTAS

Configurando al cliente, es posible definir que al final de una tarea de recepción, se muestre al usuario un mensaje informativo. Este mensaje está previamente definido y también configurable para el cliente.

Además de que el mensaje sea configurable por el cliente, también es posible definir un mensaje para tareas convergentes (todos los artículos tratados) y un mensaje para tareas divergentes (con artículos pendientes).

![imagen alt <>](assets/receiving_approve_action.png)

##### ENTRADAS

La recepción de tareas, una vez configuradas para el cliente, permite al usuario al final de la tarea agregar la información necesaria para la aprobación de la tarea. Las entradas configurables para la aprobación de tareas son las siguientes:

- **Identificación del transportista: le** permite identificar el transportista asociado con la tarea.
- **Nombre del transportista: le** permite identificar el nombre del transportista

![imagen alt <>](assets/receiving_approve_action_input.png)

## TAREAS - TRASLADO | DEVOLUCIÓN

### **TRANSFERIR**

La función Transferir permite al usuario procesar ciertos artículos para el movimiento de existencias entre tiendas. De esta forma es posible seleccionar una tienda de destino al crear o descargar la tarea y procesar los artículos a transferir.

Las tareas de transferencia se pueden crear ad hoc en la aplicación móvil sin que se proporcione una lista de elementos. Durante este proceso de creación, se le proporciona al usuario una entrada de búsqueda que permite buscar y seleccionar la tienda de destino deseada.

Asimismo, las tareas de transferencia se pueden generar mediante integración. Esta integración permite al cliente realizar la gestión de stock a medida que se genera una tarea para que la tienda de origen transfiera determinados artículos con una tienda de destino ya definida.

![imagen alt <>](assets/transfer_create_task.gif)

#### Procesamiento de artículos

##### CREACIÓN DE CAJAS / VOLÚMENES

Las tareas de Transferencia, previa configuración al cliente, permiten la creación de cajas de volumen durante el tratamiento de la tarea. Para ello, la aplicación cuenta con un botón flotante en la esquina inferior derecha de la pantalla que incluye las siguientes opciones:

- **Creación de caja / volumen:** Option + le permite crear una nueva caja y nombrarla inmediatamente. Una vez creado, el cuadro actual está activo y todos los elementos procesados posteriormente se asocian con el cuadro activo.
- Edición de **caja / volumen:** la opción de edición le permite cambiar el nombre de una caja existente.
- **Cambiar caja / volumen: Al** procesar artículos, el usuario puede cambiar entre cajas existentes.

![imagen alt <>](assets/transfer_manage_containers.gif)

##### VALIDACIÓN DEL RANGO DE TIENDA OBJETIVO

La funcionalidad Transferir permite al cliente configurar si el artículo existe en el rango de la tienda de destino. Si el artículo no existe en el rango de la tienda de destino, la aplicación devuelve un error que no permite agregar el artículo a la tarea.

![imagen alt <>](assets/trasnfer_adhoc_item.png)

#### Aprobación de la tarea

Una vez configurada para el cliente, la funcionalidad Transferir permite que el usuario al final de la tarea tenga visibilidad de la información previamente definida o de las entradas disponibles para agregar la información necesaria a la tarea.

##### ALERTAS

Al configurar el cliente, es posible definir que al final de una tarea de transferencia, se muestra un mensaje informativo al usuario. Este mensaje está previamente definido y también configurable para el cliente.

Además de configurar el mensaje por cliente, también es posible definir un mensaje para tareas convergentes (todos los artículos tratados) y un mensaje diferente para tareas divergentes (con artículos pendientes).

![imagen alt <>](assets/receiving_approve_action.png)

##### ENTRADAS

Las tareas de transferencia, una vez configuradas para el cliente, permiten al usuario en el momento de la aprobación agregar información adicional. Las entradas configurables para la aprobación de tareas son las siguientes:

- **Identificación del transportista: le** permite identificar el transportista asociado con la tarea.
- **Nombre del transportista: le** permite identificar el nombre del transportista.
- **Prioridad: le** permite identificar la prioridad asociada con la transferencia. Los valores posibles son "Normal" y "Urgente".
- **Fecha de entrega: le** permite definir una fecha de entrega esperada.

![imagen alt <>](assets/receiving_approve_action_input.png)

#### Resumen

La pantalla de resumen de la tarea Transferir contiene la siguiente información sobre cajas / volúmenes:

- **ESPERADOS:** Total de cajas / volúmenes previstos en la tarea. (aplicable solo a tareas orientadas)
- **PROCESADO:** Total de cajas / volúmenes procesados en la tarea.
- **PENDIENTE:** Total de cajas / volúmenes no procesados en la tarea. (aplicable solo a tareas orientadas)

La pantalla de resumen de la tarea de transferencia contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en la tarea. (aplicable solo a tareas orientadas)

- PROCESADOS:

   Total de artículos procesados en la tarea. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados en los que la cantidad transferida es igual a la cantidad esperada.
  - **PARCIAL:** Total de artículos procesados en los que la cantidad transferida es menor que la cantidad esperada.
  - **EN EXCESO:** Total de artículos procesados en los que la cantidad transferida es mayor que la cantidad esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea. (aplicable solo a tareas orientadas)

Para las unidades, la pantalla de resumen de la tarea Transferir contiene la siguiente información:

- **ESPERADOS:** Total de unidades previstas en la tarea.
- **PROCESADO:** Total de unidades procesadas en la tarea. En los productos procesados existen las siguientes métricas:
- **EN EXCESO:** Total de unidades procesadas en las que la cantidad separada es mayor que la cantidad esperada.
- **NO ENCONTRADO:** Total de unidades procesadas como no encontradas.

![imagen alt <>](assets/trasnfer_resume.png)

### DEVOLUCIÓN

La función Devolución permite al usuario procesar ciertos artículos para devolución de existencias al almacén o al proveedor. De esta forma es posible seleccionar un destino al crear o descargar la tarea y procesar los artículos a devolver. Los posibles destinos son Almacén y Proveedor.

Las tareas de devolución se pueden crear ad hoc en la aplicación móvil sin que se proporcione una lista de elementos. Durante este proceso de creación, se le ofrece al usuario la posibilidad de seleccionar el Almacén o Proveedor de destino y una entrada de búsqueda (aplicable solo al seleccionar Proveedor como destino). ~

En cuanto a los almacenes, solo se devuelven a la aplicación los almacenes que están asociados con la tienda que inició sesión. Esta asociación es configurable para el cliente.

Asimismo, las tareas de devolución se pueden generar mediante integración. Esta integración permite al cliente realizar la gestión de stock a medida que se genera una tarea para que la tienda de origen transfiera determinados artículos a un Proveedor o Almacén de destino ya definido.

![imagen alt <>](assets/itemreturn_create_task.gif)

#### Procesamiento de artículos

##### CREACIÓN DE CAJAS / VOLÚMENES

Las tareas de Devolución, previa configuración al cliente, permiten la creación de cajas de volumen durante el tratamiento de la tarea. Para ello, la aplicación cuenta con un botón flotante en la esquina inferior derecha de la pantalla que incluye las siguientes opciones:

- **Creación de caja / volumen:** Option + le permite crear una nueva caja y nombrarla inmediatamente. Una vez creado, el cuadro actual está activo y todos los elementos procesados posteriormente se asocian con el cuadro activo.
- Edición de **caja / volumen:** la opción de edición le permite cambiar el nombre de una caja existente.
- **Cambiar caja / volumen: Al** procesar artículos, el usuario puede cambiar entre cajas existentes.

![imagen alt <>](assets/transfer_manage_containers.gif)

#### Aprobación de la tarea

Una vez configurada para el cliente, la funcionalidad Devolución permite que el usuario al final de la tarea tenga visibilidad de la información previamente definida o entradas disponibles para agregar la información necesaria a la tarea.

##### ALERTAS

Al configurar el cliente, es posible definir que al cierre de una tarea de devolución se muestre al usuario un mensaje informativo. Este mensaje está previamente definido y también configurable para el cliente.

Además de configurar el mensaje por cliente, también es posible definir un mensaje para tareas convergentes (todos los artículos tratados) y un mensaje diferente para tareas divergentes (con artículos pendientes).

![imagen alt <>](assets/receiving_approve_action.png)

##### ENTRADAS

Las tareas de Devolución, al configurar el cliente, permiten al usuario en el momento de la aprobación agregar información adicional. Las entradas configurables para la aprobación de tareas son las siguientes:

- **Identificación del transportista: le** permite identificar el transportista asociado con la tarea.
- **Nombre del transportista: le** permite identificar el nombre del transportista.
- **Prioridad: le** permite identificar la prioridad asociada con la transferencia. Los valores posibles son "Normal" y "Urgente".
- **Fecha de entrega: le** permite definir una fecha de entrega esperada.

![imagen alt <>](assets/receiving_approve_action_input.png)

#### Resumen

La pantalla de resumen de la tarea de devolución contiene la siguiente información sobre cajas / volúmenes:

- **ESPERADOS:** Total de cajas / volúmenes previstos en la tarea. (aplicable solo a tareas orientadas)
- **PROCESADO:** Total de cajas / volúmenes procesados en la tarea.
- **PENDIENTE:** Total de cajas / volúmenes no procesados en la tarea. (aplicable solo a tareas orientadas)

La pantalla de resumen de la tarea de devolución contiene la siguiente información en relación con los elementos:

- **ESPERADOS:** Total de artículos previstos en la tarea. (aplicable solo a tareas orientadas)

- PROCESADOS:

   Total de artículos procesados en la tarea. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados en los que la cantidad devuelta es igual a la cantidad esperada.
  - **PARCIAL:** Total de artículos procesados en los que la cantidad devuelta es menor que la cantidad esperada.
  - **EN EXCESO:** Total de artículos procesados en los que la cantidad devuelta es mayor que la cantidad esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea. (aplicable solo a tareas orientadas)

Para las unidades, la pantalla de resumen de la tarea Devolver contiene la siguiente información:

- **ESPERADOS:** Total de unidades previstas en la tarea.
- **PROCESADO:** Total de unidades procesadas en la tarea. En los productos procesados existen las siguientes métricas:
- **EN EXCESO:** Total de unidades procesadas en las que la cantidad devuelta es mayor que la cantidad esperada.
- **NO ENCONTRADO:** Total de unidades procesadas como no encontradas.

![imagen alt <>](assets/trasnfer_resume.png)

## TAREAS - AUDITORÍA Y CONTROL DE VALIDEZ

### **AUDITORÍA DE validada**

La función de auditoría de validez le permite registrar las fechas de vencimiento de los artículos presentes en una tienda o almacén. Este registro permite al TMR almacenar información sobre fechas de vencimiento y cantidades respectivas. Esta información será utilizada por el TMR para generar las respectivas tareas de Control de Validez.

#### Procesamiento de tareas

Las tareas de Auditoría de validez son en su mayoría ad hoc sin una lista esperada de artículos. El usuario es libre de procesar los artículos y las fechas respectivas de forma autónoma. Sin embargo, al configurar el cliente, las tareas también se pueden crear ad hoc en la aplicación móvil en [función de una lista](multiplataforma/#com-lista) de artículos o [estructura jerárquica](multiplataforma/#por-estrutura-hierárquica) .

En tareas ad hoc sin una lista definida de artículos, el usuario debe recoger el artículo o ingresar manualmente el EAN y la aplicación proporciona un modal con la siguiente estructura:

- **DÍAS DE DEPRECED:** Información sobre el número de días parametrizados para la generación de tareas de depreciación (Validity Control).
- **DÍAS DE JUBILACIÓN:** Información sobre el número de días parametrizados para generar las tareas de baja (Control de Validez).
- **TABLA DE FECHAS:** Información de las fechas ya registradas en la tarea y las fechas futuras del artículo en tratamiento.
- **FECHAS DE ENTRADA:** Entrada para insertar la fecha de caducidad del artículo. Solo se permite una fecha igual o mayor que el día actual.
- **CANTIDAD DE ENTRADA:** Entrada para insertar la cantidad de artículos asociados a la respectiva fecha de vencimiento.

![imagen alt <>](assets/expirationpicking_task_2.gif)

#### Agregar o reemplazar

Al ingresar una nueva fecha de vencimiento, la aplicación verifica si esa fecha ya se ha registrado en la tarea. Si es así, pregunte al usuario si quiere "Sumar" la cantidad registrada a la previamente ingresada o si quiere "Reemplazar" la información de cantidad previamente registrada.

Además, la aplicación validada de la fecha ingresada ya se encuentra en el período de Depreciación o Retiro. Si es así, el usuario recibe una alerta con el siguiente mensaje:

"El artículo está en período de depreciación / retiro. ¿Quieres continuar con el registro?"

![imagen alt <>](assets/expirationpicking_add_quantity.png)

#### Lista de fechas

Una vez registradas las fechas para un determinado artículo, la aplicación móvil presenta la lista del mismo y las cantidades respectivas. En esta lista, el usuario también tiene la posibilidad de "Eliminar fechas", "Registrar nueva validez" y "Continuar" para el modo de listado de artículos.

![imagen alt <>](assets/expirationpicking_delete_date.gif)

#### Resumen

La pantalla de resumen de la tarea Auditoría de validez contiene la siguiente información en relación con los artículos:

- **REGISTRADO:** Información sobre la cantidad de artículos procesados en la tarea.

La pantalla de resumen de la tarea Auditoría de validez contiene la siguiente información en relación con las unidades:

- **UNIDADES TOTALES:** Información de las unidades procesadas en la tarea.

La pantalla de resumen de la tarea Auditoría de validez contiene la siguiente información en relación con las fechas de vencimiento:

- **REGISTRADO:** Información sobre las fechas de caducidad totales registradas en la tarea.

![imagen alt <>](assets/expirationpicking_resume.png)

### **CONTROL DE validada**

La función Control de validez le permite eliminar y depreciar artículos en la misma tarea. De esta forma, el tratamiento de las fechas de caducidad se convierte en un proceso más rápido y eficiente ya que el usuario procesa los artículos y sus fechas de caducidad en un solo momento.

Las tareas de Validity Control están orientadas exclusivamente y con una lista de artículos visibles.

#### Depreciación

La depreciación (Markdown) le permite aplicar acciones promocionales para deshacerse de los artículos que se acercan a la fecha de vencimiento.

Después de seleccionar el elemento de la lista, la aplicación muestra una lista de fechas para abordar. Para comenzar a procesar una fecha en el período de depreciación, el usuario debe seleccionar la fecha de la lista con la referencia "depreciación".

![imagen alt <>](assets/expirationcontrol_markdown.gif)

#### Depreciación modal

El modal Depreciación presenta la siguiente información e insumos:

- **INFORMACIÓN DEL ARTÍCULO:** Foto (si aplica), descripción, EAN, SKU y SOH (si aplica y configurable).
- **FECHA DE VIGENCIA:** Información sobre la fecha de caducidad en tratamiento.
- **DEPRECIADO:** Información sobre el número de artículos depreciados asociados a la fecha de vencimiento en tratamiento.
- **PVP ACTUAL:** Información del precio actual del artículo (previa consulta del servicio de precios online).
- **ENTRADA DE DESCUENTO / CANTIDAD FIJA:** Entrada que permite al usuario definir el precio de depreciación del artículo. Le permite ingresar el descuento por valor final o porcentaje. El precio final lo calcula la aplicación y se muestra en la parte derecha de la entrada. En este momento, también se valida si el precio final se encuentra dentro del porcentaje máximo de descuento. Si este no es el caso, la aplicación muestra un mensaje de error y no le permite continuar con el proceso de depreciación.
- **ETIQUETA:** Entrada que permite definir el tipo de etiqueta de depreciación a imprimir.
- **CANTIDAD DE ETIQUETAS:** Entrada que permite definir la cantidad de etiquetas de depreciación a imprimir.
- **BOTÓN FECHA NO ENCONTRADA / DEPRECIAR:** Si el número de etiquetas es igual a cero, aparece el botón "Fecha no encontrada" para que el usuario pueda definir que no se ha encontrado la fecha para el tratamiento. De esta forma será una fecha procesada con cantidad cero. Si el número de etiquetas es mayor que cero, aparece el botón "Depreciar". Hacer clic en "Depreciar" generará la acción de imprimir la cantidad de etiquetas de depreciación definidas por el usuario.

![imagen alt <>](assets/expirationcontrol_markdown_modal.png)

#### Retiro

El retiro le permite recolectar del tablero de ventas los artículos que se acercan a la fecha de vencimiento. De esta forma es posible que el usuario comprenda los artículos y respectivas fechas que se encuentran al final de su vigencia y los retire de la tienda.

Los días a eliminar, así como los días a depreciar, son parámetros definidos por artículo o estructura jerárquica. En el proceso de baja, es posible que el usuario defina un motivo y un destino, que también son parámetros configurables por cliente.

![imagen alt <>](assets/expirationcontrol_withdraw.gif)

#### Modal de retirada

El modal de Retiro presenta la siguiente información y entradas:

- **INFORMACIÓN DEL ARTÍCULO:** Foto (si aplica), descripción, EAN, SKU y SOH (si aplica y configurable).
- **FECHA DE VIGENCIA:** Información sobre la fecha de caducidad en tratamiento.
- **PRECIO, VENTAS y RETIRO:** Información sobre artículos previamente depreciados, la cantidad de ventas para la fecha en trámite y la cantidad a retirar de la respectiva fecha de vencimiento. Esta información se consulta en línea y si no se devuelve información, la aplicación muestra N / A.
- **CANTIDAD A RETIRAR:** Entrada que permite definir la cantidad de artículos que serán removidos a partir de la fecha en tratamiento.
- **MOTIVO:** Entrada que le permite definir el motivo del retiro. Si el artículo se ha depreciado anteriormente, el motivo "Validez" parece estar completado de forma predeterminada. Los motivos que aparecen en la lista de selección son configurables por el cliente.
- **DESTINO:** Entrada que le permite definir el destino del artículo después de que sea retirado de la tienda. Los destinos que aparecen en la lista de selección son configurables por el cliente.
- **BOTÓN FECHA NO ENCONTRADA / RETIRO:** Si la cantidad extraída es igual a cero, aparece el botón "Fecha no encontrada" para que el usuario pueda definir que no se ha encontrado la fecha para el tratamiento. De esta forma será una fecha procesada con cantidad cero. Si el número de etiquetas es mayor que cero, aparece el botón "Eliminar".

#### Fechas futuras

En el modal de artículo es posible consultar y gestionar las fechas futuras a través de la opción "Gestión de Fechas Futuras". En este modal es posible consultar las fechas futuras ya registradas para un determinado artículo y también borrarlas si el usuario se da cuenta durante el tratamiento de la tarea que las fechas ya no están físicamente en la tienda.

Hay tres formas de seleccionar fechas:

- A partir de la entrada de fecha existente, el usuario puede seleccionar todas las fechas registradas hasta la fecha definida en la entrada a la vez.
- Directamente de la lista de fechas.
- A través del facilitador "Seleccionar todas las fechas".

Después de seleccionar las fechas deseadas, el usuario debe seleccionar "Eliminar". Inmediatamente, las fechas se eliminarán de la TMR.

![imagen alt <>](assets/expirationcontrol_show_future_dates.gif)

#### Agregar fecha ad hoc

Al manejar una tarea de Control de Validez, es posible agregar una fecha ad hoc si el usuario identifica una fecha en la tienda que no está en la lista de fechas futuras.

En el modal de fechas para tratamiento y en el modal de listado de fechas futuras, está presente la entrada de "Agregar fecha ad hoc". Al seleccionar esta opción, el usuario es dirigido a un nuevo modal que permite definir una nueva fecha a través del input disponible y la cantidad respectiva.

![imagen alt <>](assets/expirationcontrol_adhoc_date.gif)

En el momento en que el usuario ingresa una nueva fecha, la aplicación valida si ya existe. Si es así, el siguiente mensaje se devuelve al usuario:

"No se puede agregar. La fecha de vencimiento ya existe".

![imagen alt <>](assets/expirationcontrol_date_already_existes.png)

Asimismo, la aplicación es válida si la fecha ingresada ya se encuentra en período de depreciación o se retira. En caso afirmativo, abra inmediatamente el respectivo modal para manipular el artículo.

![imagen alt <>](assets/expirationcontrol_adhoc_date_withdral.gif)

#### Resumen

La pantalla de resumen de la tarea Control de validez contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en la tarea.
- **PROCESADOS:** Total de artículos procesados en la tarea.

Con respecto a las fechas de vencimiento, la pantalla de resumen de la tarea Control de validez contiene la siguiente información:

- **ESPERADOS:** Fechas totales previstas en la tarea.
- **PROCESADO:** Total de fechas procesadas en la tarea. En los productos procesados existen las siguientes métricas:
- **DEPRECIADO:** Fechas totales procesadas como depreciación.
- **NO ENCONTRADO:** Total de fechas procesadas como no encontradas.
- **RETIROS:** Fechas totales procesadas como retiros.

![imagen alt <>](assets/expirationcontrol_resume.png)

## TAREAS - LISTA DE VERIFICACIÓN | LISTA DE ARTÍCULOS | ROTOS

### **LISTA DE VERIFICACIÓN**

La función Lista de verificación le permite realizar tareas diarias en la tienda utilizando información creada previamente. Las listas de verificación de cada tienda se realizan previamente en el Backoffice de TMR y luego están disponibles en la aplicación móvil para ser utilizadas en la creación y realización de la Lista de Verificación.

Además de la creación ad hoc en la aplicación móvil, es posible programar las tareas de la lista de verificación a través del Programador.

Las tareas de la lista de verificación se basan en preguntas y respuestas. En este sentido, las listas de verificación creadas en el Backoffice son preguntas que incluyen dos tipos de posibles respuestas y que se definen en el momento de la creación de la lista de verificación.

Cuando la aplicación móvil descarga la tarea, le presenta al usuario la respuesta elegida en el momento de su creación.

![imagen alt <>](assets/checklist_create_task.gif)

#### Procesamiento de tareas

Una vez descargada la tarea, la aplicación móvil presenta la lista de verificación asociada con la tarea y las respuestas asociadas con las preguntas respectivas.

Las posibles respuestas son las siguientes:

- **Verificado / No verificado**
- **Sí No**

Las respuestas se asocian con las preguntas al crear la lista de verificación.

![imagen alt <>](assets/checklist_processing.gif)

#### Resumen

La pantalla de resumen de la tarea Lista de verificación contiene la siguiente información:

- **PROPORCIONADO:** Total de elementos previstos en la tarea.
- **PROCESADO:** Total de elementos procesados en la tarea.

![imagen alt <>](assets/checklist_resume.png)

### **LISTA DE ARTÍCULOS**

La función Lista de artículos permite al usuario crear una lista en la aplicación móvil utilizando los artículos que se están procesando en la tarea. Los artículos procesados se agregan a la Lista de artículos y al final de la tarea, la lista se creará y estará disponible en el Backoffice TMR para ser utilizada en otros tipos de tareas. (tareas ad hoc o programadas basadas en la lista)

#### Procesamiento de tareas

Las tareas se crean ad hoc sin una lista definida. Durante el tratamiento de la tarea, los artículos después de ser cortados se asocian inmediatamente con la tarea tal como se procesan. El modal de artículo solo contiene información de artículo ya que el procesamiento de esta tarea no implica información de cantidad o entradas adicionales.

![imagen alt <>](assets/itemlist_processing.gif)

#### Resumen

La pantalla de resumen de la tarea Lista de elementos contiene la siguiente información:

- **PROCESADO:** Total de elementos procesados en la tarea.

![imagen alt <>](assets/itemlist_resume.png)

### **ROMPER REGISTRO**

La función Break Log permite al usuario procesar ciertos elementos como un descanso. Las tareas de Break Registration pueden ser ad hoc sin una lista guiada donde el usuario es libre de procesar artículos que se encuentran en estas condiciones.

Las tareas también se pueden orientar con una lista de artículos visibles donde se indica al usuario que identifique y procese cómo romper ciertos artículos previstos en la tarea. Las tareas guiadas se pueden crear mediante TMR Scheduler o mediante la integración.

#### Artículo modal

Durante el tratamiento de la tarea, el usuario pincha el EAN de los artículos o inserta manualmente el mismo y la aplicación abre inmediatamente el modal del artículo con la siguiente información:

- **INFORMACIÓN DEL ARTÍCULO:** Foto (si aplica), estado, descripción, EAN, SKU y SOH (si aplica y configurable).
- **CANTIDAD RETIRADA:** Entrada que permite definir la cantidad de artículos que se procesarán como ruptura.
- **MOTIVO:** Entrada que le permite definir el motivo de la rotura. Los motivos que aparecen en la lista de selección son configurables por el cliente.
- **DESTINO:** Entrada que le permite definir el destino del artículo después de que sea retirado de la tienda. Los destinos que aparecen en la lista de selección son configurables por el cliente.
- **BOTÓN NO ENCONTRAR / CONTINUAR:** Si el importe a romper es igual a cero, aparece el botón "No encontré" para que el usuario pueda definir que no se ha encontrado la fecha de tratamiento. De esta forma será una fecha procesada con cantidad cero. Si la cantidad a romper es mayor que cero, aparece el botón "Continuar".

![imagen alt <>](assets/damage_modal.png)

#### Resumen

La pantalla de resumen de la tarea Registro de resumen contiene la siguiente información en relación con los artículos:

- **PROCESADOS:** Total de artículos procesados en la tarea.

Para las unidades, la pantalla de resumen contiene la siguiente información:

- **PROCESADO:** Total de unidades procesadas en la tarea.

### **APROBACIÓN DE AVERÍAS**

La función de Aprobación de rupturas permite al usuario buscar rupturas abiertas que están esperando aprobación. Además de la investigación es posible tramitar la aprobación en la solicitud, indicando cantidad en cada artículo y confirmando el motivo y destino. Al final del tratamiento de los artículos, la aplicación presenta al usuario información sobre la interrupción total en la aprobación.

#### Búsqueda de pausas

Para buscar pausas que están esperando aprobación, el usuario debe acceder al formulario "Aprobación de pausas" presente en el menú de la aplicación.

En el formulario de búsqueda debe seleccionar el rango de fechas para la búsqueda y la estructura de mercado que desea buscar. Sin embargo, solo una de las entradas es obligatoria y solo es posible realizar la búsqueda si se seleccionan una o más entradas.

![imagen alt <>](assets/damage_approval_search.gif)

#### Procesamiento de rotura

Una vez realizada la búsqueda, la aplicación presenta al usuario los artículos que caen bajo los filtros de búsqueda seleccionados. En esta lista de artículos se presentan los siguientes campos:

- **Descripción del artículo:** Descripción completa del artículo.
- **SKU:** Identificador de artículo.
- **Costo:** costo total de romper el artículo. Este valor se calcula mediante la información sobre el precio de coste del artículo X cantidad de rotura introducida por el usuario.
- **Razón:** Razón asociada con el artículo en el momento del récord.
- **Destino:** Destino asociado con el artículo en el momento del récord.
- **Cantidad:** Información de la cantidad ingresada en el momento del récord de ruptura. Este campo es el único editable para que el usuario pueda cambiar la cantidad para romper un determinado artículo en el proceso de aprobación.
- **Total SOH:** Información sobre la cantidad de existencias en el momento del break record.

Para llevar a cabo la aprobación de la ruptura, el usuario debe seleccionar los artículos que desea aprobar utilizando la entrada de selección presente en la parte derecha del artículo. Para seleccionar todos los elementos hay un facilitador de "seleccionar todo" en la parte superior de la pantalla.

En cada artículo está la entrada "Cantidad" que se completa por defecto con la información de cantidad ingresada en el momento del récord de ruptura. Sin embargo, este valor se puede cambiar durante el proceso de aprobación, con la información enviada por el usuario en el momento de enviar la aprobación.

![imagen alt <>](assets/damage_approval_processing.gif)

#### Alerta de resumen

Después de que se hayan procesado todos los artículos y se hayan seleccionado las pausas a aprobar, el usuario debe seleccionar "Aprobar" presente al final de la lista. Al seleccionar "Aprobar", la aplicación muestra una alerta de resumen que informa al usuario del costo total de la infracción. Este costo total se calcula sumando el costo de todos los artículos seleccionados para aprobación.

En la alerta de resumen, si el usuario selecciona "Cancelar", se le redirige a la pantalla de procesamiento del artículo anterior. Si selecciona "Continuar", las pausas seleccionadas se enviarán para su aprobación.

![imagen alt <>](assets/damage_approval_resume.png)

## UBICACIONES DE FLUJO - RECEPCIONES | GESTIÓN DE UBICACIONES | SEPARACIÓN | INVESTIGACIÓN Y EDICIÓN

TMR como producto respalda el proceso de creación, gestión y edición de ubicaciones de tiendas y almacenes. Por esta razón, se creó el flujo de ubicación, que le permite registrar la ubicación física de todos los artículos que ingresan al stock de la tienda.

El objetivo es hacer posible la identificación de las distintas ubicaciones de un artículo determinado para respaldar y ayudar en el proceso de reposición de tiendas y los distintos procesos de auditoría de existencias, como Inventarios y Recuento de existencias.

Este flujo consta de los siguientes pasos:

- **Recepciones** : Posibilidad de crear contenedores durante el proceso de recepción que apoyarán la agregación y asignación de artículos a una ubicación determinada.
- **Gestión de ubicación** : este tipo de tarea que se genera al cierre de la recepción permite asignar ubicaciones físicas a los artículos asignando ubicaciones a los contenedores como una entidad agregadora.
- **Separación** : La inserción de las ubicaciones en las tareas de Separación permite al usuario consultar las ubicaciones de un artículo determinado en el proceso de reemplazo. Además de la consulta, también permite al usuario editar la ubicación de un determinado artículo durante el proceso de reemplazo.
- **Buscar / editar ubicaciones** : esta función le permite buscar contenedores creados previamente y ver sus ubicaciones y la identificación de todos los elementos contenidos en ellos. Además, también permite buscar ubicaciones y visualizar los artículos que le pertenecen.

Los siguientes puntos cubrirán cada uno de los pasos en este flujo de ubicaciones en detalle:

### **Recepciones: creación de contenedores**

#### Creación de caja virtual

El concepto de caja virtual significa que durante el procesamiento de la tarea de Recepción es posible procesar los artículos y asociarlos a cajas virtuales creadas durante su ejecución. La naturaleza compartida de este proceso es que es posible crear y cambiar elementos entre cajas. En este sentido, el usuario crea la caja virtual y se convierte en la caja virtual activa. Todos los artículos procesados posteriormente se asociarán con esa misma caja.

En cualquier momento es posible crear nuevas cajas virtuales y también cambiar la caja activa con la que están asociados los elementos procesados.

Esta caja virtual representa una entidad de agregación que migrará a la tarea de Gestión de Ubicación generada automáticamente cuando se cierre la tarea de recepción. Este paso representa el primer paso en el flujo de ubicación.

En el botón de cajero virtual, previa configuración al cliente, se encuentran disponibles las siguientes opciones:

- **Creación de caja / volumen:** Option + le permite crear una nueva caja y nombrarla inmediatamente. Una vez creado, el cuadro actual está activo y todos los elementos procesados posteriormente se asocian con el cuadro activo.
- Edición de **caja / volumen:** la opción de edición le permite cambiar el nombre de una caja existente.
- **Cambiar caja / volumen: Al** procesar artículos, el usuario puede cambiar entre cajas existentes.
- **Impresión:** Opción que permite la impresión móvil de la etiqueta identificativa de la caja.

![imagen alt <>](assets/receiving_manage_container.gif)

### **Gestión de la ubicación**

#### Asignación de ubicación a contenedores

Las tareas de gestión de ubicación (separación de palets) son tareas orientadas a los contenedores creados anteriormente en las tareas de recepción. El propósito de esta tarea es asignar una ubicación a los contenedores creados durante la tarea de recepción. De esta forma, todos los artículos recibidos en un contenedor determinado serán asignados a la ubicación asignada a ese mismo contenedor.

Para asignar una ubicación a un contenedor en particular, el usuario debe descargar la tarea y en la lista de contenedores pendientes, seleccionar el contenedor deseado. También existe la posibilidad de leer el código de barras del identificador del contenedor como forma de procesarlo.

Una vez cortado el código de barras, se abre un modal para ingresar y buscar ubicaciones. En este modal es posible escanear un código de barras identificando la ubicación o en caso de que no exista es posible ingresar manualmente el código identificador del mismo.

Después de ingresar la información de ubicación, el contenedor se procesa inmediatamente y la información de la ubicación asignada es visible al nivel del contenedor.

![imagen alt <>](assets/despicking_add_location.gif)

#### Borrar ubicación o agregar a la tarea

En el flujo de asignación documentado en el punto anterior, puede haber un escenario en el que el usuario intente asignar un contenedor a una ubicación que ya contiene otro contenedor asociado. Dado que no es posible asignar más de un contenedor a cada ubicación, la aplicación devuelve una pantalla al usuario con el siguiente mensaje:

**"La ubicación ya contiene el contenedor XXXXXXX. ¿Desea eliminar la paleta XXXXXX o asignarle una nueva ubicación agregándola a la tarea?"**

En esta pantalla el usuario tiene dos opciones disponibles:

- **Eliminar contenedor:** Si el usuario selecciona esta opción, TMR elimina todos los artículos y el contenedor actualmente presente en la ubicación e inmediatamente pone el contenedor en tratamiento.
- **Asignar nueva ubicación:** Si el usuario selecciona esta opción, el TMR no elimina los artículos y contenedores presentes en la ubicación, sino que los coloca como pendientes en la tarea en tratamiento. Esto permite al usuario asignar posteriormente una nueva ubicación para este contenedor.

![imagen alt <>](assets/despicking_conflict.png)

#### Búsqueda de ubicación manual

En caso de que el usuario no tenga en mente la identificación de la ubicación, el TMR permite buscar una ubicación específica a través de la nevada entre los diferentes niveles.

En el modal de inserción de ubicación, se encuentra disponible la opción de búsqueda. Al hacer clic en este facilitador, la aplicación lo dirige a una nueva pantalla donde aparece el primer nivel de ubicaciones de la tienda en tratamiento.

Al seleccionar una opción en este nivel, la aplicación pone a disposición un segundo nivel donde las respectivas ubicaciones son visibles y así sucesivamente. Este facilitador le permite navegar por todos los niveles de ubicación hasta que el usuario encuentre la ubicación deseada.

En cualquier momento, cuando el usuario opta por continuar, la aplicación muestra las ubicaciones finales y que están parametrizadas para la asociación de contenedores.

La ubicación seleccionada se muestra en el modo de inserción de ubicación, con el botón "Aceptar" disponible para continuar con el proceso y el botón "Buscar" para buscar una nueva ubicación nuevamente.

![imagen alt <>](assets/despicking_mapping.gif)

#### Resumen

La pantalla de resumen de la tarea Gestión de ubicaciones contiene la siguiente información en relación con los contenedores:

- **ESPERADOS:** Contenedores totales previstos en la tarea.
- **PENDIENTE:** Total de contenedores pendientes en la tarea.
- **PROCESADOS:** Total de contenedores procesados en la tarea.

![imagen alt <>](assets/despicking_resume.png)

### **Separación**

Para respaldar el flujo de tratamiento de la ubicación, se realizaron algunos cambios en la modalidad de ítem de las tareas de Separación. En este sentido, la modalidad de ítem de las tareas de Separación ahora incluye la información de ubicación y contenedor y también incluye un nuevo concepto de tips que permite ayudar al usuario en la acción que debe realizar.

#### Facilitador "consejos"

El facilitador de "consejos" introduce el concepto de tutorial en TMR. A través de este facilitador es posible que el usuario consulte información adicional para comprender un determinado comportamiento.

En el modal de artículos en las tareas de Separación, el facilitador "tips" está presente. Haciendo clic en "?" presente en la esquina superior de la tabla de ubicaciones, se abre inmediatamente una alerta con información detallada de cada colono presente en la tabla. Esto permite al usuario obtener información adicional sobre las acciones que debe realizar para procesar un determinado artículo dentro del alcance de las ubicaciones.

![imagen alt <>](assets/replenishprep_tips.gif)

#### Edición de ubicación

En el flujo del tratamiento de ubicaciones en las tareas de Separación, es posible que el usuario, durante el tratamiento de la tarea, consulte la información de la ubicación de un determinado artículo y edite la información de la cantidad de ese mismo artículo. en la ubicación seleccionada.

Para ello, se crearon nuevos insumos en el ítem modal de las tareas de localización, que contiene la siguiente información:

- **Información de ubicación** : identificación del nombre de la ubicación donde se encuentra el artículo.
- **Información del contenedor** : identificación EAN del contenedor donde se encuentra el artículo.
- **Columna QT** : Información de la cantidad total presente en la ubicación.
- **Columna QF** : Información de la cantidad final presente en la ubicación después de la separación del artículo. El cálculo de esta información se realiza de la siguiente manera: Cantidad total (QT) - Cantidad a separar (SEP) = Cantidad final (QF).
- **Columna SEP** : Entrada de la cantidad a separar.

Cuando el usuario ingresa la cantidad a separar para una ubicación determinada en la entrada SEP, la aplicación calcula y actualiza la información de la cantidad final para el artículo. Esta cantidad final es la cantidad que se asociará con el artículo en una ubicación determinada.

La entrada de Cantidad final también se puede editar para permitir al usuario definir manualmente el valor de la cantidad final en los casos en que la cantidad real sea diferente de la cantidad mostrada por el TMR.

Si la cantidad final del artículo es cero, significa que el artículo será eliminado del contenedor.

Al hacer clic en "Continuar", se actualiza la información de ubicación.

![imagen alt <>](assets/replenishprep_locations_table.gif)

### Investigación y edición

El TMR permite al usuario consultar una determinada ubicación o un determinado contenedor y visualizar los artículos y las cantidades respectivas. Ambas búsquedas se pueden realizar manualmente o mediante un escáner pinchando el identificador o la ubicación del contenedor.

#### Búsqueda de contenedores

Al igual que en la búsqueda de artículos, el TMR permite cortar el código de barras del contenedor y el usuario es dirigido inmediatamente al modo de información del contenedor. Si no es posible pinchar el código de barras, el usuario podrá buscar esa información manualmente.

Para buscar un contenedor el usuario debe acceder a "Buscar" y en el botón flotante seleccionar "contenedor". Luego debe ingresar manualmente el EAN del contenedor.

Inmediatamente se le reenviará al modo contenedor que contiene la siguiente información:

- **ID** : **Identificador del** contenedor.
- **Id de ubicación** : **Identificador de ubicación** .
- **Nombre de la ubicación** : información del nombre asignado a la ubicación.
- **Lista de artículos** : información SKU y nombre de los artículos presentes en el contenedor y la cantidad respectiva.

Además de esta información, también existe la posibilidad de imprimir la etiqueta identificativa del envase. Para hacer esto, el usuario debe seleccionar el icono de la impresora en la esquina superior derecha de la pantalla.

![imagen alt <>](assets/container_search.gif)

#### Búsqueda de ubicación

El TMR permite pinchar el código de barras de ubicación y el usuario es dirigido inmediatamente al modo de información de ubicación. Si no es posible pinchar el código de barras, el usuario podrá buscar esa información manualmente.

Para buscar una ubicación, el usuario debe acceder a "Buscar" y en el botón flotante seleccionar "ubicación". Luego, debe ingresar manualmente la identificación de la ubicación.

Se le dirigirá inmediatamente al modo de ubicación que contiene la siguiente información:

- **ID** : **Identificador de** ubicación.
- **Nombre** : Información del nombre de la ubicación buscada.
- **Limpiar ubicación** : Botón que le permite eliminar todos los elementos y contenedores presentes en la ubicación.
- **Lista de artículos** : información SKU y nombre de los artículos presentes en la ubicación y la cantidad respectiva. Cada artículo también contiene la información del contenedor al que pertenece.

![imagen alt <>](assets/location_search.gif)