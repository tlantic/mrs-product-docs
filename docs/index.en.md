# Guia de Autonomização

### Welcome to the Tlantic Support Guide


The purpose of this guide is to inform the necessary steps for the correct use of Tlantic's SAAS solutions and services. The actions described here may be carried out by partners, delivery and support teams.

!!! abstract "Choose the solution below"



<table width="40%" border="0" cellspacing="10" cellpadding="4" bgcolor="YELLOW">
   <caption><b></b></caption>
   <tr align="center">
      <td align="center"><a href="/intro-tmr"> <img src="https://www.tlantic.com/assets/public/images/solutions/tlantic-solution-mobile-retail-icon.png" width="100""/></a> <td align="center"><a href="/emconstrucao"> <img src="https://www.tlantic.com/assets/public/images/solutions/tlantic-solution-workforce-management-icon.png" width="100""/></td></a> <td align="center"><a href="/emconstrucao"><img src="https://www.tlantic.com/assets/public/images/solutions/tlantic-solution-store-sales-service-icon.png" width="100""/></a></td align="center">  <td align="center"><a href="#"><img src="https://www.tlantic.com/assets/public/images/home/tlantic-digital-store-processes.png" width="100""/></td></a> 
   </tr>
   <tr align="center"  bgcolor="e9e9e9">
      <td>Tlantic Mobile Retail</td> <td>Tlantic Workforce Management</td> <td>Tlantic Store Sales Services</td><td>Tlantic SmartTask</td>
   </tr>
</table>




    
<br>
<br>
