# Guia de Autonomização

### Bem-vindo ao Guia de apoio da Tlantic


O objetivo deste guia é informar as etapas necessárias  para a correta utilização das soluções e serviços em SAAS da Tlantic. As ações aqui descritas poderão ser realizadadas pelos parceiros, delivery e equipes de sustentação. 

!!! abstract "Escolha a solução abaixo"



<table width="40%" border="0" cellspacing="10" cellpadding="4" bgcolor="YELLOW">
   <caption><b></b></caption>
   <tr align="center">
      <td align="center"><a href="/intro-tmr"> <img src="https://www.tlantic.com/assets/public/images/solutions/tlantic-solution-mobile-retail-icon.png" width="100""/></a> <td align="center"><a href="/emconstrucao"> <img src="https://www.tlantic.com/assets/public/images/solutions/tlantic-solution-workforce-management-icon.png" width="100""/></td></a> <td align="center"><a href="/emconstrucao"><img src="https://www.tlantic.com/assets/public/images/solutions/tlantic-solution-store-sales-service-icon.png" width="100""/></a></td align="center">  <td align="center"><a href="#"><img src="https://www.tlantic.com/assets/public/images/home/tlantic-digital-store-processes.png" width="100""/></td></a> 
   </tr>
   <tr align="center"  bgcolor="e9e9e9">
      <td>Tlantic Mobile Retail</td> <td>Tlantic Workforce Management</td> <td>Tlantic Store Sales Services</td><td>Tlantic SmartTask</td>
   </tr>
</table>




    
<br>
<br>
