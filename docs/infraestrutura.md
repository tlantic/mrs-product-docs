


| **REQUISITO INFRAESTRUTURA**                          | **RECOMENDAÇÃO MÍNIMA**                                      |
| ----------------------------------------------------- | ------------------------------------------------------------ |
| Largura de Banda: Loja <-> Nuvem                      | À partir de 600kbps                                          |
| Largura  de Banda: Loja <-> Serviços de Stock e Preço | À partir de 600kbps                                          |
| Abrangência na Loja e no Depósito                     | -         Pode ser parcial dado que o sistema tem a possibilidade de trabalhar  em modo offline.      -         Contudo é necessário ter alguns pontos com rede para sincronização da  informação.      -         Adicionalmente  para ter consulta de estoque on line tem que se garantir cobertura WI FI em  toda a área de venda em que se pretenda ter esta funcionalidade ativa. |
| Cobertura e compatibilidade Wireless                  | Coletores x impressoras, Alertas à roamings                  |




 

### Requisitos de Computador

 

 

 

| **FABRICANTE** | **TLANTIC – OBS.**                                      |
| -------------- | ------------------------------------------------------- |
| Indiferente    | Capaz de rodar o navegador Chrome  acima da versão 89.0 |
|                |                                                         |

 

 

 

### Requisitos de Insumos

| **FORNECEDOR** | **TLANTIC – OBS.**                                           |
| -------------- | ------------------------------------------------------------ |
| Indiferente    | O  cliente deve providenciar também etiquetas para impressão de preço |

 