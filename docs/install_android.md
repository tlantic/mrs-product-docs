# Guia de Instalação para Android


##### 1 | Acessar repositório do APP
Realizar o acesso ao seu painel de controle da Tlantic e realizar o download do instalador.
Este acesso e download ao painel de controle da Tlantic pode ser realizado diretamente do Coletor através do navegador  ou por computador para depois transferir o instalador ao coletor.

![image alt <>](assets/install_android/1-android (1).png)

##### 2 | Modelo do dispositivo 
Selecionar a pasta “Android”
    

![image alt <>](assets/install_android/1-android (2).png)


<br>

##### 3 | Selecionar a última versão 
Selecionar o arquivo APK com maior número de versão, e então será realizado o download
    

![image alt <>](assets/install_android/1-android (3).png)


<br>

##### 4 | Localize o download

Abra a pasta Downloads do dispositivo

![image alt <>](assets/install_android/1-android (4).png)    



<br>

##### 5 | Instalação da APP
Selecione o APK e confirme a instalação:
    

![image alt <>](assets/install_android/1-android (5).png) 

<br>

##### 6 | Progresso da instalação
Escolha a pasta a instalar e clique em OK

![image alt <>](assets/install_android/1-android (6).png) 

<br>

##### 7 | Instalação Finalizada
Após instalado, clique em “Abrir” para abrir o TMR

![image alt <>](assets/install_android/1-android (7).png) 

<br>

##### 8 | Validar APP instalada
Será apresentada a tela de login


![image alt <>](assets/install_android/1-android (8).png) 

