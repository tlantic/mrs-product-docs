# Guia de Instalação para Windows CE


**Atenção**: é necessário a instalação dos pré-requisitos antes de seguir com o passo-a passo abaixo. Instale os componentes de acordo com a sua versão do sistema.

##### Pré-requisitos de instalação
!!! info "Componentes"
    - **Wince 6 ou Superior**
    
        [NETCFv35.Messages.EN.cab](/assets/install_wince/pre-requirements/NETCFv35.Messages.EN.cab)

    - **Wince 5 ou Inferior**
    
        [NETCFv35.Messages.EN.cab](/assets/install_wince/pre-requirements/NETCFv35.Messages.EN.cab)

        [NETCFv2.wce4.ARMV4.cab](/assets/install_wince/pre-requirements/NETCFv2.wce4.ARMV4.cab)

        [NETCFv2.wce5.armv4i.cab](/assets/install_wince/pre-requirements/NETCFv2.wce5.armv4i.cab)

        [System_SR_ENU.CAB](/assets/install_wince/pre-requirements/System_SR_ENU.CAB)
        

##### Requisitos mínimos de Hardware



???+ example "Coletor:"
    * **Sistema Operativo:** Windows CE 4.2 ou superior.
    * **Memória ROM:** 60MB ou superior. 
    * **Leitor de Código de Barras:** Leitor 1D - LASER
    * **WI-FI:** 802.11 B/G/N.
    * **Tamanho do Ecrã Recomendado:** 3” ou superior.
    * **Resolução Mínima Recomendada:**  QVGA 320 x 240.
        
**nota**: o dispositivo deve estar homologado. consulte a [lista](/eqpto_homologado/) 
<br>

#### Passo a passo da instalação


    .
##### 1 | Acessar repositório do APP
Realizar o acesso ao seu painel de controle da Tlantic e realizar o download do instalador.
Este acesso e download ao painel de controle da Tlantic pode ser realizado diretamente do Coletor através do navegador *Internet Explorer* ou por computador para depois transferir o instalador ao coletor.
   

![image alt <>](assets/install_wince/1-wince.png)

    
<br>

##### 2 | Modelo do dispositivo 
Escolher o modelo do dispositivo e fazer download da última versão:
    

![image alt <>](assets/install_wince/2-wince.png)


<br>

##### 3 | Selecionar a última versão 
Selecionar a última versão
    

![image alt <>](assets/install_wince/3-wince.png)


<br>

##### 4 | Indicar local de instalação 
Escolher “Run this program from its current location” e clicar OK
    

![image alt <>](assets/install_wince/4-wince.png)

<br>

##### 5 | Download da APP
Será realizado download
    

![image alt <>](assets/install_wince/5-wince.png)

<br>

##### 6 | Instalação da APP
Escolha a pasta a instalar e clique em OK

![image alt <>](assets/install_wince/6-wince.png)

<br>

##### 7 | Progresso da instalação
Será realizada a instalação

![image alt <>](assets/install_wince/7-wince.png)

<br>

##### 8 | Validar APP instalada
Será criado ícone na área de trabalho

![image alt <>](assets/install_wince/8-wince.png)