
!!! info  "Áreas"

     [Manual de instalação ](../install_wince)<br>
     [Manual do utilizador ](../multiplataforma)<br>
     [Manual do administrador ](../bo_instore)<br>
     [Manual de Parametrização ](../configuracao_avancada)<br>
     [Integração ](http://mrs-docs.s3-eu-west-1.amazonaws.com/index.html)<br>
     [Equipamentos Homologados ](../eqpto_homologado)<br>
     [Release Notes ](../MP_change)

  
## Módulos que compõem o TMR
[comment]: # (### Ficha de Produto)

!!! abstract "Fluxos de Preço"
    - Auditoria de preço
    - Remarcação de preço
    - Impressão de Etiqueta

!!! abstract "Fluxos de Estoque"
    - Recebimento
    - Transferências
    - Devolução
    - Contagens
    - Inventários

!!! abstract "Fluxos de Reposição"
    - Auditoria de ruptura
    - Separação de mercadoria
    - Reposição de produtos
    - Reposição por vendas
    - Reposição urgente

!!! abstract "Fluxos de Qualidade"
    - Checklist de controle e verificação

!!! abstract "Fluxos de Sortimento"
    - Auditoria de sortimento

!!! abstract "Cockpit"
    - Dashboard operacional





