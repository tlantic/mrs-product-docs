# Modelo de Implementação 

 
#### Modelo SAAS 
  A nossa oferta da solução de mobilidade implica o alojamento da plataforma MRS em Cloud (modelo SaaS), monitorada pela Tlantic de modo a garantir sempre a melhor performance. Contudo, é possível o alojamento em Private Cloud ou na infraestrutura do cliente. 
 
  No modelo SaaS, a Tlantic responsabiliza-se por toda a estrutura necessária para a disponibilização do sistema (servidores, conectividade e segurança da informação), assim como pela sua gestão e manutenção. Assim, o cliente não tem necessidade de criar uma estrutura própria e de capacitar profissionais para manter o sistema em funcionamento. 
 
  Este modelo permite uma abordagem gradual de implementação (escalabilidade): o cliente pode ir ajustando em função da sua necessidade quer o número de lojas a suportar pelo sistema quer as funcionalidades que pretende aceder. 
 
  Adicionalmente, neste modelo o cliente tem acesso à evolução funcional dos módulos que está a arrendar assim como serviços de suporte de 3ª linha. 
 
  O cliente utiliza o software via Internet pagando um valor mensal pelo serviço oferecido. 
 
<br>

#### Metodologia de Adoção à Projetos 


![image alt <>](assets/metodologia/metodologia_projeto.jpg){align=left}

<br>

#### Metodologia de Adoção para Passagem à Sustentação 

![image alt <>](assets\metodologia\suporte.png){align=left}