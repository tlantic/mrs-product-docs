# Server Settings

Esta secção é dedicada à listagem de settings que influenciam o comportamento do Agente, Integração e Serviço de tarefas.

*[Agente]: agent-tasks
*[Integração]: integration-tasks
*[Serviço]: service-tasks
*[stock-default]: "default": integer
*[encontrados]: "include-resources-not-found": boolean

## Agent Tasks

Neste capítulo serão apresentadas as settings que definem o comportamento do agente de tarefas.

### **agent-tasks-task-creating-flow**

Type: ``object`` | Setting ao nível da **Organização**

Setting que define diversos parâmetros associados à criação de tarefas. 

???+ example "Valores possíveis:"
    * **soh**: ``object`` - Define o parâmetro de stock no processo de criação de tarefas:
        * default: ``integer`` - Define o valor default de stock para todos os tipos de tarefa que não tiverem parametrização de exceção.
        * {task-type}: ``object`` - Permite parametrizar exceção por tipo de tarefa. 
			* default: ``interger`` - Define o stock mínimo default para determinado tipo de tarefa.
			* include-resources-not-found: ``boolean``  - Define se inclui artigos em que não foi possível consultar stock. 
		* concurrent-requests: ``interger`` - Define o número de pedidos paralelos para pesquisa de stock.
		* include-resources-not-found: ``boolean`` - Define se inclui artigos em que não foi possível consultar stock. 
	* **check-soh**: ``array`` - Define os tipos de tarefa para os quais está ativa a validação de soh.
	* **contention**: ``object`` - Define os tipos de tarefa que estão sujeitos a contenção e os respetivos parâmetros. Para saber mais sobre contenção, consultar o manual relativo ao Módulo de Validades.
		* {task-type}: ``object`` - Define o tipo de tarefa associado à contenção.
			* days: ``interger`` - Define o número de dias para a criação da tarefa.
			* mode: ``string`` - Define o modo de criação.
	* **change-price**: ``array`` - Define os tipos de tarefa que utilizam a tabela de prices para recolher informação para criação. 
	* **scheduled-start**: ``object`` - Define a hora prevista de criação das tarefas via integração.
		* default: ``string`` - Define a hora prevista de criação default.
		* {task-type}: ``string`` - Define a hora prevista de criação para cada tipo de tarefa parametrizada. 
	* **keep-expirations**: ``array`` - Define os tipos de tarefa que tratam os resources dos tipo expirations. 
	* **validation-output**: ``array`` - Define o tipo de tarefa a ser gerado para o type withdraw.
	 

!!! note "Exemplo"
    ```json
    {
	"soh": {
		"default": 1,
		"SCANAUDIT": {
			"default": -1,
			"include-resources-not-found": true
		},
		"EXTERNALAUDIT": {
			"default": -1,
			"include-resources-not-found": true
		},
		"PRESENCECHECK": {
			"default": 0,
			"include-resources-not-found": true
		},
		"include-resources-not-found": true,
		"concurrent-requests": 10
	},
	"check-soh": [
		"PRESENCECHECK",
		"REPLENISHPREP",
		"REPLENISH"
	],
	"contention": {
		"VALIDITYCHECK": {
			"days": 7,
			"mode": "full"
		}
	},
	"change-price": [
		"RELABELLING",
		"CARDDISCOUNT",
		"PRICEUP",
		"PRICEDOWN",
		"PROMOTIONIN",
		"PROMOTIONOUT"
	],
	"scheduled-start": {
		"default": "01:00:00",
		"VALIDITYCHECK": "01:00:00"
	},
	"keep-expirations": [
		"VALIDITYCHECK",
		"EXPIRATIONCONTROL",
		"WITHDRAWAL"
	],
	"validation-output": [
		"EXPIRATIONCONTROL"
	]
    }
    ```

### **agent-tasks-task-closing-flow**

Type: ``object`` | Setting ao nível da **Organização**

Setting que define diversos parâmetros associados ao fecho de tarefas e geração consequente.

???+ example "Valores possíveis:"
	* **tasks-flow**: ``object`` - Define os fluxos parametrizados para determinado tipos de tarefa.
		* {task-type}: ``array`` - Define o tipo de tarefa parametrizado para determinado fluxo.
			* flow-type: ``string`` - Define o tipo de fluxo parametrizado.
			* output_task: ``string`` - Define a tarefa consequente (se aplicável).
			* params: ``object`` - Define os parâmetros que influenciam o comportamento do fluxo parametrizado (flow-type).
	* **notify-no-stock**: ``array`` - Define o fluxo que notifica o serviço externo quando existem artigos que foram excluidos por não haver stock. No exemplo, todos os artigos que foram excluidos da Separação vindos da Verificação de Presença por não haver stock são notificados para o serviço externo (webwook).
	* **use-extradata**: ``object`` - Define os campos que são lidos pelo agente dentro do objeto extra-data ao nível do resource.
		* validitycheck: ``array`` - Define em que tipos de tarefa o agente mapeia a informação de validitycheck dentro do objeto extra-data ao nível do resource.
	* **scheduled-start**: ``object`` - Define a hora prevista de início associada à tarefa.
		* default: ``string`` - Define a hora prevista de início default para os tipos de tarefa não parametrizados.
		* {task-type}: ``string`` - Define a hora prevista de início para o tipo de tarefa parametrizada. 
	* **inventory-divergences**: ``object`` - Define parametros na criação de tarefa de "Correção de divergência" nas tarefas do tipo Inventory.
		* name-prefix: ``string`` - Define o prefixo colocado no nome das tarefas de "correção de divergência".
	* **replenishprep-minimum-soh-required**: ``object`` - Define o valor de stock mínimo na geração das tarefas de Separação (replenishprep).
		* default: ``interger`` - Define o valor de stock mínimo default (para todos os tipos de tarefa não parametrizados).
		* {task-type}: ``interger`` - Define o valor de stock mínimo para cada tipo de tarefa parametrizado.
	* **filter-resources-concurrent-requests**: ``object`` - Define o número de pedidos em simultâneo que o agente realiza para realizar o filtro de artigos a incluir nas tarefas.
		* default: ``interger`` - Define o número default de pedidos em simultâneo que o agente realiza para realizar o filtro de artigos a incluir nas tarefas.
	* **create-stockcount-on-replenish-closing**: ``object`` - Define os parâmetros para a inclusão de artigos nas tarefas de Contagem de stock (stockcount) no fecho das tarefa de Reposição (replenish).
		* include-resources-not-found: ``boolean`` - Define se inclui nas tarefas de Contagem de stock artigos não encontrados nas tarefas de Reposição. 
		* include-resources-untreated: ``boolean`` - Define se inclui nas tarefas de Contagem de stock artigos não tratados nas tarefas de Reposição.  
		* include-resources-unexpected-quantity: ``boolean`` - 	Define se inclui nas tarefas de Contagem de stock artigos em que a quantidade tratada é diferente da quantidade esperada nas tarefas de Reposição.
	* **create-relabelling-on-relabelling-closing**: ``object`` - Define os parâmetros para a inclusão de artigos nas tarefas de Remarcação (relabelling) no fecho das tarefa de Remarcação (relabelling).
		* include-resources-not-found: ``boolean`` - Define se inclui nas tarefas de Remarcação artigos não encontrados nas tarefas de Remarcação. 
		* include-resources-untreated: ``boolean`` - Define se inclui nas tarefas de Remarcação artigos não tratados nas tarefas de Remarcação.  
		* include-resources-unexpected-quantity: ``boolean`` - 	Define se inclui nas tarefas de Remarcação artigos em que a quantidade tratada é diferente da quantidade esperada nas tarefas de Remarcação.
		* sequence-limit: ``interger`` - Define o número limite de tarefas que serão geradas na sequência. 
	* **create-expirationcontrol-on-expirationcontrol-closing**: ``object`` - Define os parâmetros para criação de Controlo de Validades no fecho de Controlo de Validades.
		* expiration-sequence-limit: ``interger`` - Define o número limite de tarefas geradas na sequência.
	
!!! example "Fluxos possíveis:"
	* **FILTER-BY-AVAILABLE-SOH**: Fluxo que define o filtro de artigos por indisponibilidade do serviço de consulta de stock.
	* **CREATE-ITEM-LIST**: Fluxo que define a geração de lista de artigos no fecho de tarefa parametrizado.
	* **CHECK-PROCESSED-CONTAINERS**: Fluxo que define a gestão de containers tratados no fecho de tarefa parametrizado. Os parâmetros configurados influenciam o comportamento da inserção dos containers tratados na tarefa consequente.
	* **EXTRACT-SNAPSHOT-EXPIRATIONS**: Fluxo que define o tratamento de expirations no fecho da tarefa parametrizada.
	* **CHECK-STOCK-DIVERGENCE** Fluxo que define a gestão de artigos com atributo "divergence". 


!!! note "Exemplo"
    ```json
    {
  		"tasks-flow": {
    		"STOCKOUTAUDIT": [
				{
					"flow_type": "FILTER-BY-AVAILABLE-SOH",
					"output_task": "STANDARDREPLENISHPREP"
				}
    	],
    		"ITEMLIST": [
				{
					"flow_type": "CREATE-ITEM-LIST"
				}
    	],
    		"RECEIVING": [
				{
					"flow_type": "CHECK-PROCESSED-CONTAINERS",
					"output_task": "DESPICKING",
					"params": {
						"include-items-with-parentid": true,
						"include-containers-with-parentid": false
					}
				},
				{
					"flow_type": "USE-EXTRADATA",
					"output_task": "VALIDITYCHECK"
				}
    	],
    		"EXPIRATIONPICKING": [
				{
					"flow_type": "EXTRACT-SNAPSHOT-EXPIRATIONS",
					"output_task": "VALIDITYCHECK"
				},
				{
					"flow_type": "CHECK-STOCK-DIVERGENCE",
					"output_task": "STOCKCOUNT"
				}
    	]
  	},
  		"notify-no-stock": [
    		"REPLENISHPREP-ON-PRESENCECHECK"
  		],
  		"use-extradata": {
    		"validitycheck": [
      			"RECEIVING"
    		]
  		},
  		"scheduled-start": {
    		"default": "01:00:00",
    		"VALIDITYCHECK": "01:00:00"
  		},
  		"inventory-divergences": {
    		"name-prefix": "Correção Divergência"
  		},
  		"replenishprep-minimum-soh-required": {
    		"default": 0,
    		"SCANAUDIT": 3,
    		"RELABELLING": 3,
    		"REPLENISHPREP": 0
  		},
  		"filter-resources-concurrent-requests": {
    		"default": 10
  		},
		"create-stockcount-on-replenish-closing": {
			"include-resources-not-found": false,
			"include-resources-untreated": false,
			"include-resources-unexpected-quantity": false
		},
		"create-relabelling-on-relabelling-closing": {
			"sequence-limit": 0,
			"include-resources-untreated": true,
			"include-resources-unexpected-quantity": true
		},
		"create-expirationcontrol-on-expirationcontrol-closing": {
			"expiration-sequence-limit": 30
		}
	}
    ```
### **agent-task-create-task-soh-unavailable**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Aplicação**

Setting que define o comportamento de inserção de artigos na geração da tarefa na falha de resposta do serviço de stock.

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-task-soh-unavailable": {"active": true}
    }
    ```

### **agent-task-create-priceaudit-on-relabelling**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Auditoria de Preço (priceaudit) no fecho da Remarcação (relabelling).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-priceaudit-on-relabelling": {"active": true}
    }
    ```

### **agent-task-create-replenishprep-on-relabelling**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Separação (replenishprep) no fecho da tarefa de Remarcação (relabelling).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-replenishprep-on-relabelling": {"active": true}
    }
    ```

### **agent-task-create-replenishprep-on-priorityrelabelling**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Separação (replenishprep) no fecho da tarefa de Remarcação Prioritária (priorityrelabelling).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-replenishprep-on-priorityrelabelling": {"active": true}
    }
    ```

### **agent-task-create-replenishprep-on-priceaudit**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Separação (replenishprep) no fecho da tarefa de Auditoria de Preço (priceaudit).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-replenishprep-on-priceaudit": {"active": true}
    }
    ```

### **agent-task-create-replenish-on-replenishprep**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Reposição (replenish) no fecho da tarefa de Separação (replenishprep).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-replenish-on-replenishprep": {"active": true}
    }
    ```

### **agent-task-create-replenishprep-on-externalaudit**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Separação (replenishprep) no fecho da tarefa de Auditoria Externa (externalaudit).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-replenishprep-on-externalaudit": {"active": true}
    }
    ```

### **agent-task-create-replenishprep-on-scanaudit**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Separação (replenishprep) no fecho da tarefa de Auditoria Varrimento (scanaudit).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-replenishprep-on-scanaudit": {"active": true}
    }
    ```

### **agent-task-create-replenishprep-on-stockoutaudit**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Separação (replenishprep) no fecho da tarefa de Auditoria de Ruturas (stockoutaudit).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-replenishprep-on-stockoutaudit": {"active": true}
    }
    ```

### **agent-task-create-stockcount-on-replenishprep**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Contagem de Stock (stockcount) no fecho da tarefa de Separação (replenishprep).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-stockcount-on-replenishprep": {"active": true}
    }
    ```

### **agent-task-create-stockcount-on-replenish**

!!! warning "Deprecated"
    Use [agent-tasks-task-closing-flow](#agent-tasks-task-closing-flow) instead

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Contagem de Stock (stockcount) no fecho da tarefa de Reposição (replenish).

### **agent-task-create-stockcount-on-externalaudit**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Contagem de Stock (stockcount) no fecho da tarefa de Auditoria Externa (externalaudit).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-stockcount-on-externalaudit": {"active": true}
    }
    ```

### **agent-task-create-stockcount-on-scanaudit**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Contagem de Stock (stockcount) no fecho da tarefa de Auditoria de Varrimento (scanaudit).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-stockcount-on-scanaudit": {"active": true}
    }
    ```


### **agent-task-create-presencecheck-on-externalaudit**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Verificação de Presença (presencheck) no fecho da tarefa de Auditoria Externa (externalaudit).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-presencecheck-on-externalaudit": {"active": true}
    }
    ```

### **agent-task-create-presencecheck-on-scanaudit**

Type: ``object`` (default: ``active: false``) | Setting ao nível da **Organização**

Setting que define se é gerada uma tarefa de Verificação de Presença (presencheck) no fecho da tarefa de Auditoria de Varrimento (scanaudit).

!!! note "Exemplo"
    ```json
    {
      "agent-task-create-presencecheck-on-scanaudit": {"active": true}
    }
    ```

### **agent-task-tasks-change-price**

!!! warning "Deprecated"
    Use [agent-tasks-task-closing-flow](#agent-tasks-task-closing-flow) instead

Type: ``object`` (default: ``{}``) | Setting ao nível da **Organização**

Setting que define que tipos de tarefa realizam integração na tabela de pricing.

### **agent-task-tasks-keep-expirations**

!!! warning "Deprecated"
    Use [agent-tasks-task-closing-flow](#agent-tasks-task-closing-flow) instead

Type: ``object`` (default: ``{}``) | Setting ao nível da **Organização**

Setting que define que tipos de tarefa tratam expirations. 

### **agent-task-tasks-validation-output**

!!! warning "Deprecated"
    Use [agent-tasks-task-closing-flow](#agent-tasks-task-closing-flow) instead

Type: ``object`` (default: ``{}``) | Setting ao nível da **Organização**

Setting que define que tipo de tarefa é criada na geração de validitycheck.


## **Integration Tasks**

Neste capítulo é apresentada a setting que definem o comportamento do módulo de Integração de tarefas.

### **integration-setting**

Type: ``object`` (default: ``{}``) | Setting ao nível da **Organização**

Setting que define o número máximo de artigos que a integração permite na criação de tarefas.

!!! note "Exemplo"
    ```json
    {
  		"max-items": {
    		"default": 0,
			"SCANAUDIT": 1000,
			"EXTERNALAUDIT": 1000,
			"PRESENCECHECK": 1000
		}
	}
    ```

## **Service Tasks**

Neste capítulo sãp apresentadas as settings que definem o comportamento do módulo de Serviço de tarefas.

### **parent-tasks-not-closing**

Type: ``object`` (default: ``{}``) | Setting ao nível da **Organização**

Setting que define os tipos de tarefa que não terminam automaticamente no fecho das tarefas filhas. No exmplo está parametrizada o tipo de tarefa "Inventory". Isto significa que a tarefa de inventário não é fechada automáticamente quando todas as zonas estiverem terminadas.

!!! note "Exemplo"
    ```json
    {
  		"tasks": [
    		"INVENTORY"
  		]
	}
    ```

### **tasks-ttl**

Type: ``object`` (default: ``{}``) | Setting ao nível da **Organização**

Setting que define, por tipo de tarefa, o número de dias marcado para ttl no couchbase.

???+ example "Valores possíveis:"
	* **default**: ``interger`` - Define o número de dias default para os tipos de tarefa não parametrizados.
	* **{task-type}**: ``object`` - Define o número de dias parametrizado para o tipo de tarefa. 
		* default: ``interger`` - Define o número de dias parametrizado default para o tipo de tarefa e para os estados não parametrizados.
		* {status}: ``interger`` - Define o número de dias parametrizado para o estado e tipo de tarefa. 

!!! note "Exemplo"
    ```json
    {
  		"default": 365,
  		"priceup": {
    		"default": 4,
    		"a":40 
  		},
		"inventory": {
			"default": 30,
			"a":40
		},
		"pricedown": {
			"default": 4,
			"a":40
		},
		"scanaudit": {
			"default": 15,
			"a":40
		},
		"promotionin": {
			"default": 4,
			"a":40
		},
		"carddiscount": {
			"default": 4,
			"a":40
		},
		"promotionout": {
			"default": 4,
			"a":40
		},
		"presencecheck": {
			"default": 15,
			"a":40
		},
		"replenishprep": {
			"default": 15,
			"a":40
		}
	}
    ```







<!--![image alt <>](assets/under_construction.png){:height="200px" width="200px"}

![image alt <>](assets/under_construction.png){:height="500px" width="500px"}

| |                                |
| :----------------------: | :-----------------------------------: |
| ![image alt <>](assets/under_construction.png){:height="300px" width="300px"}       | ![image alt <>](assets/under_construction.png){:height="300px" width="300px"}  |-->

