## **Release Notes Versão v5.12.14.1** 
#### Data de lançamento 18-12-2020

???+ abstract "**Correções / Melhorias**"

    - **Auditoria de Varrimento**: Pedido de Reposição - Erro NullReferenceException

## **Release Notes Versão v5.12.14.0** 
#### Data de lançamento 14-12-2020

???+ done "**Novas Funcionalidades**"

    - **Controlo de Validades**: Depreciação de artigo de preço/peso variável através do select do artigo

    - **Controlo de Validades**: Apresentação do SOH 

    - **Controlo de Validades**: Apresentação dos parâmetros

    - **Reposição**: tradução do quantidade a repor deverá ser "a abastecer"

???+ abstract "**Correções / Melhorias**"

    - **Receções**: Aprovar tarefa dá erro NullReferenceException

    - **Transferencias/ Devoluções**: App crasha e deixa de se conectar à rede depois de selecionar a data a fazer a transferência

    - **Controlo de Validades**: Botão Continuar está ativo com os campos obrigatórios por preencher

    - **Receção**: atributo expiration_control com valor negativo é descartado


