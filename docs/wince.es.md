<!--![image alt <>](assets/under_construction.png){:height="500px" width="500px"}-->

# WINCE
![imagen alt <>](assets/capa_wince.png)

## Historia del documento[¶](es/wince/#historico-do-documento)

| Datos      | Descripción                  | Autor        | Versión | Revisión         |
| :--------- | :--------------------------- | :----------- | :------ | :--------------- |
| 01/04/2021 | Creación de documentos.      | Filipe Silva | 1.0     |                  |
| 01/06/2021 | Versión final del documento. | Filipe Silva | 2.0     |                  |
| 22/01/2021 | Revisión documental final.   | Filipe Silva | 2.1     | Helena Gonçalves |

## **Introducción**[¶](es/wince/#introducao)

### TMR - Tienda[¶](es/wince/#tmr-instore)

Este documento describe las funcionalidades del sistema Tlantic Mobile Retail (TMR) - InStore. El principal objetivo del documento es proporcionar al usuario toda la información necesaria para que pueda utilizar el sistema de forma correcta y eficaz.

InStore es uno de los módulos que componen Tlantic Mobile Retail (TMR) orientado a las operaciones de la tienda, con un enfoque en la solución de problemas comunes de retail:

<details class="example" open="open" style="box-sizing: inherit; background-color: var(--md-admonition-bg-color); border-left: 0.2rem solid rgb(124, 77, 255); border-radius: 0.1rem; box-shadow: rgba(0, 0, 0, 0.05) 0px 0.2rem 0.5rem, rgba(0, 0, 0, 0.05) 0px 0.025rem 0.05rem; color: rgba(0, 0, 0, 0.87); font-size: 0.64rem; margin: 1.5625em 0px; overflow: visible; padding: 0px 0.6rem; break-inside: avoid; display: flow-root; border-top-color: rgb(124, 77, 255); border-right-color: rgb(124, 77, 255); border-bottom-color: rgb(124, 77, 255); font-family: Roboto, -apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><summary style="box-sizing: inherit; background-color: rgba(124, 77, 255, 0.1); border-left: 0.2rem solid rgb(124, 77, 255); font-weight: 700; margin: 0px -0.6rem 0px -0.8rem; padding: 0.4rem 1.8rem 0.4rem 2rem; position: relative; border-top-left-radius: 0.1rem; border-top-right-radius: 0.1rem; cursor: pointer; display: block; min-height: 1rem; border-top-color: rgb(124, 77, 255); border-right-color: rgb(124, 77, 255); border-bottom-color: rgb(124, 77, 255); -webkit-tap-highlight-color: transparent; outline: none;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Principales características:</font></font></summary><ul style="box-sizing: inherit; margin: 1em 0px 0.6rem 0.625em; list-style-type: disc; display: flow-root; padding: 0px;"><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Inventarios.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Lista de artículos, hoja de artículos, auditoría de precios,</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Registro y aprobación de rupturas.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Impresión de etiquetas.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Auditoría y Control de Validez (Depreciaciones y Retiros).</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Auditoría de escaneo y verificación de presencia.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Auditoría de Ruptura, Separación, Reemplazo.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0px; margin-left: 1.25em;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Recepciones, transferencias y devoluciones.</font></font></li></ul></details>

El módulo Instore busca simplificar y solucionar problemas de retail realizando tareas orientadas a un perfil de usuario. Cada tarea está asociada con una característica para resolver una necesidad particular de la aleta. TMR se compone de los módulos: **BackOffice** , **Cockpit** , **Aplicaciones móviles (WinCE y Multiplataforma)** , **Scheduler** y **TMR Server** .

### Arquitectura[¶](es/wince/#arquitetura)

TMR es un sistema que consta de:

- Aplicaciones de cliente (aplicación WinCE, aplicación de Android y aplicación de iOS): aplicación distribuida por los dispositivos móviles utilizados en la operación de la tienda.
- TMR Server: componente central que contiene lógica y datos y que interactúa con otros sistemas.
- Backoffice - Componente web que permite la definición de datos de referencia y configuración de parámetros.
- Cockpit: componente web para el análisis operativo de las tareas realizadas en TMR Instore.

![imagen alt <>](assets/Arquitetura.png)

### Aplicación móvil - Windows CE[¶](es/wince/#aplicacao-movel-windows-ce)

En los siguientes puntos se presentarán todas las funcionalidades presentes en la aplicación Windows CE y cómo se pueden ejecutar correctamente. A lo largo de los capítulos, también se presentarán imágenes ilustrativas para una mejor orientación del usuario en relación con el sistema.

### Requisitos mínimos[¶](es/wince/#requisitos-minimos)

Para una correcta instalación del TMR, el dispositivo debe cumplir con los siguientes requisitos:

<details class="example" open="open" style="box-sizing: inherit; background-color: var(--md-admonition-bg-color); border-left: 0.2rem solid rgb(124, 77, 255); border-radius: 0.1rem; box-shadow: rgba(0, 0, 0, 0.05) 0px 0.2rem 0.5rem, rgba(0, 0, 0, 0.05) 0px 0.025rem 0.05rem; color: rgba(0, 0, 0, 0.87); font-size: 0.64rem; margin: 1.5625em 0px; overflow: visible; padding: 0px 0.6rem; break-inside: avoid; display: flow-root; border-top-color: rgb(124, 77, 255); border-right-color: rgb(124, 77, 255); border-bottom-color: rgb(124, 77, 255); font-family: Roboto, -apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><summary style="box-sizing: inherit; background-color: rgba(124, 77, 255, 0.1); border-left: 0.2rem solid rgb(124, 77, 255); font-weight: 700; margin: 0px -0.6rem 0px -0.8rem; padding: 0.4rem 1.8rem 0.4rem 2rem; position: relative; border-top-left-radius: 0.1rem; border-top-right-radius: 0.1rem; cursor: pointer; display: block; min-height: 1rem; border-top-color: rgb(124, 77, 255); border-right-color: rgb(124, 77, 255); border-bottom-color: rgb(124, 77, 255); -webkit-tap-highlight-color: transparent; outline: none;">Requisitos mínimos:</summary><ul style="box-sizing: inherit; margin: 1em 0px 0.6rem 0.625em; list-style-type: disc; display: flow-root; padding: 0px;"><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Sistema operativo:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>Windows CE 4.2 o superior.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Memoria ROM:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>60 MB o más.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Lector de<span>&nbsp;</span></font><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">códigos de barras:</font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>Lector 1D - LASER</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">WI-FI:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>802.11 B / G / N.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Tamaño de pantalla recomendado:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>3 ”o más.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0px; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Resolución mínima recomendada:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>QVGA 320 x 240.</font></font></li></ul></details>

## **Acceso**[¶](es/wince/#login)

Para iniciar sesión, se le pide al usuario que ingrese su nombre de usuario y contraseña. Para verificar el texto ingresado en el campo Contraseña, el usuario puede seleccionar "Mostrar contraseña" para mostrar la información. Después de iniciar sesión, se le presenta al usuario una entrada para ingresar la identificación de la tienda deseada, y es obligatorio colocarla para continuar. Si el usuario está asociado con una sola tienda, el sistema guarda esta información automáticamente y pasa a la siguiente pantalla.

Luego de ingresar a la tienda, es necesario seleccionar la impresora que se utilizará durante el procesamiento de tareas. Sin embargo, esta selección no es obligatoria y la impresora se puede cambiar más tarde.

![imagen alt <>](assets/login_wince.gif)

## **Comienzo**[¶](es/wince/#inicio)

Después de iniciar sesión, se dirige al usuario a la pantalla de inicio de la aplicación, donde tiene acceso a las pestañas Tareas, Tareas locales y Menú.

![imagen alt <>](assets/menu_wince.png)

### Tareas[¶](es/wince/#tarefas)

La pestaña "Tareas" es la página de inicio que presenta el resumen de las tareas agregadas por tipo. De esta forma, los tipos de tareas son visibles con tareas disponibles para procesar o configuradas para aparecer en el resumen incluso cuando no hay tareas por realizar.

![imagen alt <>](assets/tarefas_wince.png)

### Tareas locales[¶](es/wince/#tarefas-locais)

La pestaña "Tareas locales" contiene la lista de tareas almacenadas localmente en el dispositivo y que el usuario está ejecutando en el dispositivo actual.

![imagen alt <>](assets/user_tasks_wince.png)

### Menú[¶](es/wince/#menu)

La pestaña "Menú" contiene acceso a los formularios de solicitud. Las opciones disponibles dependen de las parametrizaciones del cliente.

<details class="example" open="open" style="box-sizing: inherit; background-color: var(--md-admonition-bg-color); border-left: 0.2rem solid rgb(124, 77, 255); border-radius: 0.1rem; box-shadow: rgba(0, 0, 0, 0.05) 0px 0.2rem 0.5rem, rgba(0, 0, 0, 0.05) 0px 0.025rem 0.05rem; color: rgba(0, 0, 0, 0.87); font-size: 0.64rem; margin: 1.5625em 0px; overflow: visible; padding: 0px 0.6rem; break-inside: avoid; display: flow-root; border-top-color: rgb(124, 77, 255); border-right-color: rgb(124, 77, 255); border-bottom-color: rgb(124, 77, 255); font-family: Roboto, -apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><summary style="box-sizing: inherit; background-color: rgba(124, 77, 255, 0.1); border-left: 0.2rem solid rgb(124, 77, 255); font-weight: 700; margin: 0px -0.6rem 0px -0.8rem; padding: 0.4rem 1.8rem 0.4rem 2rem; position: relative; border-top-left-radius: 0.1rem; border-top-right-radius: 0.1rem; cursor: pointer; display: block; min-height: 1rem; border-top-color: rgb(124, 77, 255); border-right-color: rgb(124, 77, 255); border-bottom-color: rgb(124, 77, 255); -webkit-tap-highlight-color: transparent; outline: none;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Formas disponibles:</font></font></summary><ul style="box-sizing: inherit; margin: 1em 0px 0.6rem 0.625em; list-style-type: disc; display: flow-root; padding: 0px;"><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Ficha de artículo:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>Formulario que permite acceder al detalle de un artículo.<span>&nbsp;</span></font><font style="box-sizing: inherit; vertical-align: inherit;">La información proporcionada es configurable para el cliente.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Impresoras:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>Formulario para seleccionar impresora y tipo de etiqueta.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Impresión de etiquetas:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>formulario para acceder al tipo de trabajo de impresión de etiquetas ad hoc.<span>&nbsp;</span></font><font style="box-sizing: inherit; vertical-align: inherit;">Esta opción está disponible mediante<span>&nbsp;</span></font></font><a href="es/app_settings#show-labelprint-on-summary" style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; color: var(--md-typeset-a-color); text-decoration: none; word-break: break-word; transition: color 125ms ease 0s;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">parametrización</font></font></a><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Cambiar contraseña:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>Formulario que permite cambiar la contraseña asociada al usuario registrado.<span>&nbsp;</span></font><font style="box-sizing: inherit; vertical-align: inherit;">Esta opción está disponible mediante<span>&nbsp;</span></font></font><a href="es/app_settings#allow-change-password" style="box-sizing: inherit; -webkit-tap-highlight-color: transparent; color: var(--md-typeset-a-color); text-decoration: none; word-break: break-word; transition: color 125ms ease 0s;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">parametrización</font></font></a><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0.5em; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Cerrar sesión:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>formulario para cerrar sesión en la aplicación.</font></font></li><li style="box-sizing: inherit; margin-bottom: 0px; margin-left: 1.25em;"><strong style="box-sizing: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;">Salir:</font></font></strong><font style="box-sizing: inherit; vertical-align: inherit;"><font style="box-sizing: inherit; vertical-align: inherit;"><span>&nbsp;</span>Accede para cerrar la aplicación.</font></font></li></ul></details>

![imagen alt <>](assets/menu_wince_2.png)

#### Hoja de artículo[¶](es/wince/#ficha-de-artigo)

El formulario de formulario de artículo le permite consultar información asociada a un artículo específico. Para hacerlo, simplemente acceda al formulario en el menú y luego busque el artículo deseado cortando el EAN o insertando manualmente la información. Cabe destacar que la información presentada en el Formulario de Artículo es configurable por el cliente y que puede contener cualquier información que el cliente desee dar visibilidad en la operación.

![imagen alt <>](assets/ficha_de_artigo_wince.gif)

## **Tareas**[¶](es/wince/#tarefas_1)

### Lista de tareas[¶](es/wince/#lista-de-tarefas)

El menú Tareas muestra los tipos de tareas disponibles para procesar. Cuando se selecciona un tipo de tarea, aparece la lista de tareas disponibles para el tipo seleccionado. Para cada tarea, se muestran el nombre, su disponibilidad, la fecha de creación y el usuario que está trabajando en la tarea (si corresponde).

![imagen alt <>](assets/available_tasks_wince.png)

### Descarga de tareas[¶](es/wince/#download-da-tarefa)

Al descargar una tarea, se asigna al usuario que ha iniciado sesión y toda la información se almacena localmente en el dispositivo.

De esta forma, es posible procesar tareas sin conectividad a internet, ya que toda la información necesaria se almacena en el dispositivo (a excepción de la información que se obtiene en el momento del chipping, como es el caso del SOH del artículo o el precio).

Configurando el cliente, es posible definir si se permite descargar tareas que están en ejecución y que no están guardadas previamente en la memoria del dispositivo. Esta protección tiene como objetivo no permitir que un mismo usuario descargue la tarea al mismo tiempo en varios dispositivos, ya que esto podría resultar en la pérdida del trabajo realizado anteriormente.

![imagen alt <>](assets/download_tarefa_wince.gif)

### Procesamiento de tareas[¶](es/wince/#processamento-da-tarefa)

El tratamiento de las tareas guiadas sigue el mismo flujo base, independientemente del tipo de tarea.

Después de descargar una tarea, el usuario tiene una lista de artículos para guiarse durante el tratamiento de la tarea. Inicialmente, todos estos artículos aparecen en la pestaña PENDIENTE hasta que el usuario realiza algún cambio en el artículo.

Al cortar un código de barras o seleccionar manualmente un artículo, el sistema verifica si pertenece a la lista de artículos de la tarea. En caso de que no pertenezcas, hay dos opciones:

- Si la tarea permite la adición de elementos imprevistos, el usuario tiene la posibilidad de agregarla a la tarea y tratarla con el resto de elementos. (1)
- En caso contrario, se informa al usuario de que el artículo en cuestión no pertenece a la tarea y no puede procesarlo. (dos)

​    ![Foto](assets/adhoc_item_wince.png) ![Foto](assets/not_adhoc_item_wince.png)

### Creación de tareas ad hoc[¶](es/wince/#criacao-de-tarefas-adhoc)

El usuario puede crear tareas ad hoc de tres formas distintas: **Con lista** , **Sin lista** o **Por estructura jerárquica** . Para hacer esto, seleccione el botón "Crear tarea" en la esquina inferior derecha de la tarea.

#### Sin lista[¶](es/wince/#sem-lista)

El usuario debe presionar el botón "Crear tarea" en la esquina inferior derecha y elegir la opción **SIN LISTA** . Después de crear la tarea, el usuario es redirigido a la pantalla de tratamiento donde puede iniciar el tratamiento de los artículos.

![imagen alt <>](assets/criacao_tarefas_adhoc_wince.gif)

#### Con lista[¶](es/wince/#com-lista)

El usuario debe presionar el botón "Crear tarea" en la esquina inferior derecha y elegir la opción **CON LISTA** . Después de crear la tarea, el usuario es dirigido a la pantalla donde elegirá con qué lista quiere tratar y cuáles están disponibles. Después de la selección, el usuario puede comenzar a procesar artículos.

![imagen alt <>](assets/criacao_tarefas_adhoc_lista__wince.gif)

#### Por estructura jerárquica[¶](es/wince/#por-estrutura-hierarquica)

El usuario debe presionar el botón "Crear Tarea" en la esquina inferior derecha y elegir la opción **POR ESTRUCTURA JERÁRQUICA** . Luego de crear la tarea, el usuario es dirigido a la pantalla donde seleccionará el nivel de la estructura jerárquica que desee, siendo el primer nivel obligatorio y el siguiente opcional. Después de la selección, el usuario puede comenzar a procesar artículos.

![imagen alt <>](assets/criacao_tarefas_eh__wince.gif)

## **Configuración de la impresora**[¶](es/wince/#configuracao-de-impressora)

El usuario, mientras maneja una tarea, puede cambiar la impresora en cualquier momento sin tener que abandonar la tarea.

Al hacer clic en el icono "Configuración de impresión", aparece una nueva ventana con la opción de elegir la impresora, el número de etiquetas que desea imprimir y en algunos casos el tipo de etiqueta.

![imagen alt <>](assets/sele%C3%A7%C3%A3o_de_impressao_wince.gif)

## **Resumen**[¶](es/wince/#resumo)

El usuario puede acceder al resumen de la tarea en cualquier momento, usando el botón "Finalizar" en la esquina superior derecha de la pantalla de la tarea.

En este punto, al usuario se le presenta la opción de marcar todos los elementos no tratados como no encontrados si existe dicha configuración para el cliente.

Configurando el cliente, es posible definir si un tipo específico de tarea se puede aprobar con artículos pendientes de tratamiento o si es obligatorio procesarlos para la aprobación de la tarea.

![imagen alt <>](assets/resume_wince.png)

El resumen proporciona una descripción general de la tarea y su procesamiento. La aplicación tiene la capacidad de calcular individualmente los recursos de la tarea, tales como: artículos, cajas, cantidades y fechas de vencimiento. Los algoritmos de cálculo disponibles en los resúmenes son los siguientes:

- PLANIFICADO: recursos previstos en la tarea.
- PENDIENTE: recursos previstos en la tarea que no fueron tratados.
- PROCESADO: recursos procesados por el usuario:
  - EXTRAVIADO
  - PARCIALMENTE
  - EN EXCESO
  - EN TODO
- NO PLANIFICADO: Recursos agregados ad hoc en la tarea.

Esta información está agrupada por secciones y se puede definir según el tipo de tarea. La descripción del cálculo también se puede definir según el tipo de tarea, como por ejemplo: los artículos tratados se pueden marcar en el Markdown, recibir en Recepción y auditar en Auditoría. El detalle de cada resumen se puede consultar en el detalle de cada tipo de tarea.

Para obtener más detalles, consulte la sección Resumen de cada tipo de tarea.

En esta pantalla también es posible LIBERAR o APROBAR la tarea.

![imagen alt <>](assets/resume_wince_2.png)

## TAREAS - PRECIO[¶](es/wince/#tarefas-preco)

### **REDUCCIÓN**[¶](es/wince/#remarcacao)

La funcionalidad de Markdown te permite actualizar las etiquetas de la tienda, los artículos que sufren cambios de precio y / u otras características que se comunican al cliente a diario.

Al configurar, es posible segmentar los artículos para marcar en diferentes tipos de tareas de Markdown: aumento de precio, caída de precio, entrada de promoción, salida de promoción, descuento de tarjeta y cambio de reserva de emergencia.

#### Lista de articulos[¶](es/wince/#listagem-de-artigos)

Al ingresar al menú Volver a marcar, el usuario tendrá acceso a la lista de tareas disponibles que se pueden descargar al dispositivo. Después de la descarga, se muestra una lista de elementos a tratar con el número de etiquetas a imprimir. Al seleccionar manualmente el artículo, aparece el modal con la información del artículo picado, el precio anterior, el nuevo y la cantidad a imprimir.

Si el trabajo está configurado con el modo de impresión automática, la etiqueta del artículo se imprime, de forma predeterminada, después de cortarlo.

![imagen alt <>](assets/relabelling_item_list_wince.png)

La lista de artículos para las tareas de Markdown contiene la siguiente información:

- **Información del artículo:** foto (si corresponde), descripción, EAN y SKU.
- **Cantidad: En** la parte derecha de la lista está la información sobre la cantidad tratada y la cantidad esperada para reprogramar. Formato: Cantidad tratada / Cantidad esperada.

#### Filtrar[¶](es/wince/#filtro)

En la lista de artículos en las tareas de marcado, es posible filtrar los artículos presentados en la lista. A través de la configuración, el TMR presenta la opción en la esquina inferior izquierda que le permite filtrar los artículos presentados en la lista bajo las condiciones "Subidas de precio" y "Bajas de precio".

Dependiendo de la operación de la tienda, puede ser importante presentar los aumentos de precio y las disminuciones de precio por separado, ya que pueden tratarse en diferentes momentos de la operación.

![imagen alt <>](assets/relabelling_filter_wince.png)

#### Ordenando[¶](es/wince/#ordenacao)

En la lista de artículos de las tareas de marcado, es posible ordenar los artículos presentados en la lista. A través de la configuración, el TMR presenta la opción en la esquina inferior derecha que permite ordenar los artículos presentados en la lista utilizando algunas opciones que también son configurables. Además, al configurarlo, es posible establecer un valor predeterminado para el pedido, de modo que al descargar la tarea, los artículos se clasifiquen automáticamente por el valor establecido como predeterminado.

Opciones de clasificación:

- **Modificado (ascensos-descensos):** muestra primero los ascensos y luego los descensos.
- **Modificado (descensos-ascensos):** muestra primero los descensos y luego los ascensos.
- **Estructura jerárquica:** muestra los artículos ordenados por el identificador de estructura jerárquica, de menor a mayor.
- **Marca (AZ):** presenta artículos ordenados por marca de la A a la Z.
- **Marca (ZA):** presenta elementos ordenados de Z a A.

![imagen alt <>](assets/relabelling_order.png)

#### Artículo modal[¶](es/wince/#modal-de-artigo)

Al configurar el cliente, es posible definir si el modal del artículo se abre inmediatamente cuando se corta el artículo o si la etiqueta se imprime inmediatamente.

El modal del artículo contiene la siguiente información:

- **Información del artículo:** foto (si corresponde), descripción, EAN, SKU y SOH (si corresponde).
- **Dinámica de precios:** Información del precio actual y el nuevo precio a marcar.
- Cantidad esperada **:** información sobre el número de etiquetas a remarcar en el campo "Previsión".
- Cantidad rellamada **:** información sobre la cantidad de etiquetas rellamadas en el campo "Remarcadas".
- Entrada de **cantidad:** Entrada para definir el número de etiquetas reprogramadas.
- Información de **etiqueta:** información de tipo de etiqueta predeterminada para el artículo. (si es aplicable)
- **Botón No encontrado / Continuar: le** permite establecer el artículo como No encontrado. Si la cantidad en la entrada es mayor que cero, el botón No encontrado se reemplaza por el botón "Continuar".

![imagen alt <>](assets/relabelling_item_modal_win.png)

#### Resumen[¶](es/wince/#resumo_1)

La pantalla de resumen de la tarea Markdown contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en las tareas.

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados en los que la cantidad reprogramada es igual a la cantidad esperada.
  - **PARCIALMENTE:** Total de artículos procesados en los que la cantidad reprogramada es menor que la cantidad esperada.
  - **EN EXCESO:** Número total de artículos procesados en los que la cantidad reprogramada es mayor que la cantidad esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

![imagen alt <>](assets/relabelling_resume_win.png)

### **IMPRESIÓN DE ETIQUETAS**[¶](es/wince/#impressao-de-etiquetas)

La función de impresión de etiquetas permite al usuario imprimir etiquetas ad hoc. De esta manera, siempre que la operación de la tienda identifique artículos que no tienen etiqueta o que están dañados, pueden usar la función de Impresión de etiquetas para realizar la impresión.

Una vez configurada para el cliente, la función de Impresión de etiquetas puede estar disponible en el resumen de tareas o en el menú de la aplicación.

![imagen alt <>](assets/impressao_etiquetas_wince.gif)

#### Selección de impresora y etiqueta[¶](es/wince/#selecao-de-impressora-e-etiqueta)

En la lista de artículos hay un botón de selección de impresora en la esquina superior derecha de la aplicación. Al seleccionar el botón, la aplicación abre inmediatamente una pantalla con las impresoras y etiquetas disponibles para su selección.

![imagen alt <>](assets/labelprint_printer_select.gif)

#### Artículo modal[¶](es/wince/#modal-de-artigo_1)

Al configurar el cliente, es posible definir si el modal del artículo se abre inmediatamente cuando se corta el artículo o si la etiqueta se imprime inmediatamente.

El modal del artículo contiene la siguiente información:

- **Información del artículo:** foto (si corresponde), descripción, EAN, SKU y SOH (si corresponde).
- **Cantidad impresa:** información sobre la cantidad de etiquetas ya impresas para un artículo determinado.
- Entrada de **cantidad:** Entrada para definir la cantidad de etiquetas a imprimir.
- **Botón Imprimir:** Acción para imprimir el número de etiquetas definidas.

![imagen alt <>](assets/labelprint_modal_wince.png)

### **AUDITORÍA DE PRECIOS**[¶](es/wince/#auditoria-de-preco)

El propósito del tipo de tarea Auditoría de precios es permitirle auditar los precios presentes en la tienda. Mediante el pinchazo de etiquetas, TMR realiza la comparación de los valores de las etiquetas ( label_price ), los valores de ERP ( ERP_price ) y los valores de POS ( POS_price ).

De esta forma es posible auditar los precios presentes en la tienda y en caso de divergencia, el TMR imprime inmediatamente una nueva etiqueta con el precio actualizado.

#### Ubicación de la auditoría de precios[¶](es/wince/#localizacao-da-auditoria-de-preco)

En el proceso de [creación de tareas Adhoc](es/wince/#criacao-de-tarefas-adhoc) , el usuario puede definir una ubicación para la tarea. Esta ubicación está asociada a la tarea y permite definir en qué área de la tienda se realizó la auditoría.

Hay dos ubicaciones disponibles para elegir:

- Tienda
- Almacenamiento

La ubicación elegida por el usuario está asociada con la tarea y está disponible para consulta en el detalle de la tarea en el Cockpit. Con esta información, el gerente de operaciones puede comprender qué elementos se auditan en cada ubicación y la cantidad total de precios divergentes o convergentes en cada ubicación.

![imagen alt <>](assets/priceaudit_location_win.png)

#### Inserción manual de precios[¶](es/wince/#insercao-manual-do-preco)

Durante el procesamiento de una tarea, cuando se corta un EAN o se ingresa manualmente la información del artículo, el TMR identifica que el precio de la etiqueta no ha sido informado y le pide al usuario que ingrese manualmente ese valor.

![imagen alt <>](assets/priceaudit_manual_insert_win.png)

En el caso de que se pique una etiqueta TMR que indique el precio del artículo, no se le pedirá al usuario que ingrese el precio. En este caso, el TMR dirige al usuario de inmediato al modal del artículo donde realiza la comparación de precios.

#### Auditoría modal y de precios de artículos[¶](es/wince/#modal-de-artigo-e-auditoria-de-precos)

Después de obtener el precio de la etiqueta (mediante la lectura de la etiqueta o la introducción del manual), el artículo modal aparece inmediatamente. Al abrir el modal, TMR solicita precios de ERP y POS a través de una llamada a los servicios prestados por el cliente. Al comparar los valores, es posible que la TMR concluya si el precio de la etiqueta es Divergente o Convergente .

**Convergente**

En caso de que los valores sean iguales, TMR entiende que la auditoría es convergente. En este escenario, presenta el mensaje "Sin divergencia". El artículo se procesa automáticamente sin necesidad de realizar ninguna otra acción.

![imagen alt <>](assets/priceaudit_equal_price_win.png)

**Divergente**

En caso de que los valores sean diferentes, TMR entiende que la auditoría es diferente. En este escenario presenta el mensaje "Divergencia encontrada". Inmediatamente, se imprime una nueva etiqueta y el artículo se considera procesado. Si es necesario volver a imprimir la etiqueta, el usuario tiene el botón de impresión disponible en la esquina superior derecha del modal.

![imagen alt <>](assets/priceaudit_not_equal_price_win.png)

#### Auditoría de dos precios[¶](es/wince/#auditoria-de-dois-precos)

La tarea Auditoría de precios también le permite auditar dos precios simultáneamente. Esto solo se aplica a los clientes que ofrecen dos precios en la etiqueta y el servicio de precios que devuelve dos precios (ERP 1, ERP 2 POS 1 y POS 2).

La auditoría de dos precios mantiene el mismo flujo descrito anteriormente con la diferencia de auditar dos precios al mismo tiempo. En este sentido, un artículo es divergente si uno o los dos precios son diferentes y es convergente si los dos precios son iguales.

#### Resumen[¶](es/wince/#resumo_2)

La pantalla de resumen de la tarea Auditoría de precios contiene la siguiente información:

- **ESPERADOS:** Total de artículos previstos en las tareas (solo aplica para tareas orientadas).

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **DIVERGENCIAS:** Total de artículos procesados con divergencia.
  - **SIN ETIQUETA:** Total de artículos procesados con información manual sobre el precio de la etiqueta.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de ítems no procesados en la tarea (solo aplica para tareas orientadas).

![imagen alt <>](assets/priceaudit_resume_win.png)

## TAREAS - REEMPLAZO[¶](es/wince/#tarefas-reposicao)

### **AUDITORÍA DE ESCANEO**[¶](es/wince/#auditoria-de-varrimento)

El propósito del tipo de tarea Scan Audit es permitir al usuario verificar la presencia de un determinado grupo de artículos en la tienda.

Para ello, el usuario pasa por la tienda y recoge los artículos en el tablero de ventas, permitiendo que el TMR reciba la información de que el artículo está presente en la tienda.

Después de completar la tarea, esta información está disponible para su consulta y exportación en el Cockpit. Sobre esta información se realizarán cálculos adicionales que permitan inferir el porcentaje de pausa por tarea.

Con esta información, es posible que el gerente de la tienda conozca la cantidad de artículos rotos y la información de stock disponible por artículo.

#### Creación de la tarea de auditoría de análisis[¶](es/wince/#criacao-da-tarefa-de-auditoria-de-varrimento)

En cuanto a la creación de tareas de Scan Audit, existen las siguientes posibilidades:

- Adhoc **basado en** listas **:** Posibilidad de crear tareas directamente en el dispositivo móvil mediante la definición de una lista de artículos previamente existente. Esta lista de artículos se crea en TMR Backoffice. Para obtener más detalles, consulte el capítulo sobre la [creación de tareas basadas en la lista](es/wince/#com-lista) .
- **Adhoc basado en estructura jerárquica:** Posibilidad de crear tareas directamente en el dispositivo móvil definiendo el nivel deseado de la estructura jerárquica. Para obtener más detalles, consulte el capítulo sobre la [creación de tareas por estructura jerárquica](es/wince/#por-estrutura-hierárquica) .
- **Programado:** Posibilidad de crear tareas a través de TMR Scheduler. A través del Programador es posible definir un horario único o una recurrencia automática en la creación de tareas. Para cada cronograma creado, es posible definir una lista de artículos a auditar o un nivel específico de la estructura jerárquica.

#### Auditoría de análisis dirigida[¶](es/wince/#auditoria-de-varrimento-orientada)

Configurando el cliente, es posible definir que las tareas de Scan Audit estén orientadas. Esto significa que la lista de artículos pendientes está disponible para su visualización durante el manejo de la tarea. Esta lista de artículos ayuda al usuario a comprender qué artículos se espera que sean tratados en la tarea a través de la fotografía (si corresponde) y la descripción del artículo.

![imagen alt <>](assets/scanaudit_oriented_win.png)

#### Auditoría de escaneo no dirigida[¶](es/wince/#auditoria-de-varrimento-nao-orientada)

Configurando el cliente, es posible definir que las tareas de Scan Audit no estén orientadas. Esto significa que la lista de artículos pendientes no está disponible para su visualización mientras se realiza la tarea. Esto permite no influir en el usuario con una lista de artículos definidos, aunque la TMR valida en cada imagen si el artículo pertenece a la tarea. También configurando el cliente, es posible definir si el usuario puede agregar elementos no previstos en las tareas o si solo puede seleccionar elementos previamente definidos al crearlos.

![imagen alt <>](assets/scanaudit_not_oriented_win.png)

En ambos escenarios (orientados o no orientados) los artículos triturados se presentarán en la pestaña Procesados.

#### Orden de reemplazo urgente[¶](es/wince/#pedido-de-reposicao-urgente)

Durante el manejo de la tarea de auditoría de análisis, se solicita al usuario que solicite un reinicio urgente. Este facilitador está presente junto a la fotografía en forma de artículo. A través de este facilitador es posible que el usuario defina un pedido de reemplazo y la cantidad respectiva para un artículo dado.

Inmediatamente se generará una tarea de Separación para el artículo solicitado.

![imagen alt <>](assets/replenish_item_on_modal_win.gif)

#### Resumen[¶](es/wince/#resumo_3)

La pantalla de resumen de la tarea de auditoría de análisis contiene la siguiente información:

- **ESPERADOS:** Total de artículos previstos en la tarea (solo aplica para tareas orientadas).

- PROCESADOS:

   Total de artículos procesados en la tarea. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados previstos en la tarea en la que la cantidad tratada es igual a la cantidad esperada.
  - **PARCIALMENTE:** Número total de artículos procesados previstos en la tarea en la que la cantidad tratada es menor a la cantidad esperada (solo aplica para tareas que permiten agregar artículos inesperados).

- **PENDIENTE:** Total de ítems no procesados en la tarea (solo aplica para tareas orientadas).

![imagen alt <>](assets/scanaudit_resume_win.png)

### **AUDITORÍA EXTERNA**[¶](es/wince/#auditoria-externa)

El propósito del tipo de tarea Auditoría externa es permitir al usuario verificar la presencia de un determinado grupo de artículos en la tienda. Por esta razón, la Auditoría Externa es similar a la Auditoría de Escaneo pero con algunas especificidades:

#### Especificidades[¶](es/wince/#especificidades)

- Fue diseñado para ser utilizado por un equipo externo a los procesos de la tienda, es decir, permite un flujo de proceso diferente al del Scan Audit. Como en el anterior, los cálculos del porcentaje de rotura también se realizan en base a los datos recopilados.
- Permite al equipo externo a la tienda auditar a través de este tipo de tareas el trabajo operativo realizado en el Scan Audit. En el Cockpit es posible comparar los porcentajes de rotura calculados entre los dos tipos de tarea.
- Dado que la Auditoría Externa fue diseñada para ser operada por un equipo externo a la tienda, es posible definir (configurando al cliente) el número de zonas de conteo y la identificación de cada una. A diferencia de la auditoría de escaneo, que genera una sola tarea, la auditoría externa le permite configurar zonas de conteo que pueden ser realizadas simultáneamente por diferentes operadores.
- Cuando todas las zonas de tareas están cerradas, se lleva a cabo la consolidación de datos y los siguientes cálculos (por ejemplo, ruptura de porcentaje) se basarán en datos ya consolidados.

#### Contando zonas[¶](es/wince/#zonas-de-contagem)

Configurando el cliente, es posible definir zonas de conteo para tareas de Auditoría Externa.

De esta forma, al generar tareas (ad hoc o programadas), se crean zonas de conteo según la cantidad configurada y su respectiva identificación. Las zonas de recuento siempre están asociadas con la tarea principal de Auditoría externa.

Las zonas de recuento pueden estar orientadas (con los artículos predichos visibles) o no orientadas (lista de artículos predichos no visibles para el usuario) configurando el cliente.

![imagen alt <>](assets/externalaudit_zones_childs_win.gif)

#### Resumen[¶](es/wince/#resumo_4)

La pantalla de resumen de la tarea de Auditoría externa contiene la siguiente información:

- **ESPERADOS:** Total de artículos previstos en las tareas (solo aplica para tareas orientadas).

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados previstos en la tarea en la que la cantidad tratada es igual a la cantidad esperada.
  - **PARCIALMENTE:** Número total de artículos procesados previstos en la tarea en la que la cantidad tratada es menor a la cantidad esperada (solo aplica para tareas que permiten agregar artículos inesperados).

- **PENDIENTE:** Total de ítems no procesados en la tarea (solo aplica para tareas orientadas).

![imagen alt <>](assets/scanaudit_resume_win.png)

### **VERIFICACIÓN DE PRESENCIA**[¶](es/wince/#verificacao-de-presenca)

El propósito del tipo de tarea Verificación de asistencia es permitirle confirmar la presencia de elementos que no se encuentran en las tareas Auditoría de análisis y Auditoría externa. Por esta razón, las tareas de Verificación de asistencia no se crean ahdoc ni se programan, sino que se generan automáticamente cuando se cierran las tareas de Escaneo y Auditoría Externa.

#### Generación de tareas[¶](es/wince/#geracao-de-tarefas)

La generación de las tareas de Verificación de Presencia son configurables por el cliente, es decir, es posible definir por cliente si se generan cuando se cierran las tareas de Escaneo y Auditoría Externa.

Si la configuración permite la generación de las tareas de Verificación de presencia, esta generación sigue la siguiente regla:

- Los artículos **no tratados** y **no encontrados** en el escaneo y la Auditoría Externa y el stock mayor que cero se ingresarán automáticamente en la tarea de escaneo de Presencia.
- También existe la posibilidad de filtrar por estado del artículo. Es decir, es posible configurar el estado de los elementos que se incluirán en las tareas de Verificación de asistencia. Por ejemplo, puede definir si los elementos inactivos se incluyen en la tarea o solo los elementos activos y descontinuados.

#### Artículo modal[¶](es/wince/#modal-de-artigo_2)

Durante el procesamiento de las tareas de Presence Check, al cortar un artículo, el modal se abre inmediatamente. Este comportamiento lo puede configurar el cliente.

El modal del artículo contiene la siguiente información:

- **Información del artículo:** el encabezado del modal incluye la fotografía (si corresponde), SKU, EAN y la información de SOH (si corresponde).

- **PS y ventas:** información PS y ventas diarias (solo se aplica a los clientes que brindan esta información).

- Motivo:

   menú desplegable que ofrece dos motivos:

  - Artículo no lineal
  - Artículo en otro lugar

Estos motivos permiten identificar el lugar donde se encontró el artículo. Esta información está disponible para su consulta en el detalle del artículo en TMR Cockpit. Esta información permite que la operación de la tienda comprenda si los artículos están en las ubicaciones correctas o si son necesarios cambios en el planograma de la tienda.

Al configurar el cliente, es posible definir si los motivos están presentes en el artículo modal para la selección. Si están presentes, su llenado es obligatorio porque no es posible procesar el artículo sin definir un motivo.

![imagen alt <>](assets/presencecheck_modal_win.gif)

#### Resumen[¶](es/wince/#resumo_5)

La pantalla de resumen de la tarea Verificación de asistencia contiene la siguiente información:

- **ESPERADOS:** Total de artículos previstos en las tareas.

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **ESPERADOS:** Número total de artículos procesados previstos en la tarea.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

![imagen alt <>](assets/presencecheck_resume_win.png)

### **AUDITORÍA DE RUTUROS**[¶](es/wince/#auditoria-de-ruturas)

Una Auditoría de Rotura permite identificar en la tienda artículos que están parcialmente rotos (hay un artículo en la tienda pero en menor cantidad que el espacio disponible) o en rotura total (no hay ningún artículo en la tienda). También permite la confirmación de la cantidad a reponer de un artículo en el lineal.

Al crear la tarea Auditoría de rupturas, es posible definir una ubicación. Esta información se transferirá a la tarea de Separación consiguiente, lo que permitirá al usuario consultar más rápidamente la ubicación de los artículos para su reemplazo.

![imagen alt <>](assets/stockoutaudit_location_win.png)

Hay dos tipos de auditoría de rupturas:

- Guiado: el usuario tiene una lista de artículos para guiar el procesamiento de la tarea.
- Adhoc: sin una lista de artículos previamente definida. La creación de tareas ad hoc se puede consultar en el apartado [Creación](es/wince/#criacao-de-tarefas-adhoc) de tareas ad hoc .

#### Orientada[¶](es/wince/#orientada)

Al seleccionar Auditoría de Fallos en el resumen de tareas, el usuario tendrá acceso a la lista de tareas disponibles que se pueden descargar al dispositivo, permitiendo así su ejecución en modo offline. Tenga en cuenta que en el modo offline el usuario no tendrá acceso a determinada información como Stock de presentación, Ventas del día y Stock disponible, ya que se ordenan online durante el manejo de la tarea.

Una vez descargada la tarea, se presenta una lista de artículos a tratar, que pueden tener o no una cantidad esperada asociada. En este último caso, se espera que el usuario confirme que en realidad esta es la cantidad del artículo que debe ser reemplazado, pudiendo cambiar el valor en cualquier momento.

![imagen alt <>](assets/stockoutaudit_item_list_win.png)

#### Artículo modal[¶](es/wince/#modal-de-artigo_3)

Al cortar un código de barras o seleccionar el artículo de la lista, el modal del artículo aparece inmediatamente. En este modal hay dos posibles entradas:

- **Cantidad de estante: le** permite ingresar la cantidad de artículos presentes en el lineal.
- **Cantidad para restablecer: le** permite ingresar la cantidad deseada para restablecer. Esta cantidad se transferirá a la tarea de separación consiguiente como la cantidad a separar.

![imagen alt <>](assets/stockoutaudit_item_modal_win.png)

La información " PS " y " VENTAS " también está presente en el modal . Esta información consultada online permite calcular la cantidad a reponer de un determinado artículo utilizando la cantidad presente en la línea.

**Nota:** Estos valores solo están disponibles para los clientes que cuentan con un servicio que permite consultar esta información.

En el caso de la información de PS , el usuario solo tiene que ingresar la Cantidad Lineal y la Cantidad a Reemplazar se calcula automáticamente de la siguiente manera:

![imagen alt <>](assets/stockoutaudit_PS.png)

Aunque se calcula automáticamente, el usuario puede cambiar el valor de la entrada "Cantidad a reponer" en cualquier momento.

##### CAJAS PARA FACILITADORES[¶](es/wince/#facilitador-caixas)

El tipo de elemento del tipo de tarea Auditoría de ruptura también incluye un facilitador de unidad / efectivo. Esto permite al usuario colocar una cantidad a reponer informando la cantidad de cajas presentes y la cantidad de unidades por caja.

**CAJA**

Este facilitador le permite indicar el número de cajas y el número de unidades por caja dentro de la lista preexistente. El resultado de este cálculo se coloca en la entrada "Cantidad a reponer".

![imagen alt <>](assets/stockoutaudit_box_win.png)

**DINERO GRATIS**

Este facilitador es similar al anterior, con la particularidad de permitir al usuario informar libremente el número de unidades por caja. El resultado del cálculo tiene el mismo comportamiento que el mencionado anteriormente.

![imagen alt <>](assets/stockoutaudit_box_free_win.png)

Después de ingresar la cantidad para restablecer, el botón "Continuar" está disponible. Al seleccionar "Continuar" el modal se cierra inmediatamente y el artículo pasa a la pestaña de procesado con la cantidad indicada.

Además del botón "Continuar", también está disponible el botón "No he encontrado". Si el usuario selecciona "No he encontrado", el artículo se procesa como no encontrado y se descarta para la consiguiente tarea de separación.

![imagen alt <>](assets/stockoutaudit_item_modal_win.png)

#### Moda Clientes[¶](es/wince/#clientes-fashion)

Para adaptar la tarea Auditoría de rupturas a la especificidad del catálogo de moda, en el modal del artículo está presente una función para enumerar los artículos skus que pertenecen al mismo producto (solo para clientes de moda). El propósito de esta característica es permitir auditar la ruptura de skus que no están presentes en el lineal mordiendo un sku hermano (que pertenece al mismo producto).

Para ello, el usuario debe seleccionar el botón "Más artículos" presente en el modal y TMR proporciona una lista de skus asociados con el mismo producto. En esta lista el usuario puede poner una cantidad a reemplazar en cualquier artículo y se agregarán como ad hoc en las tareas.

![imagen alt <>](assets/stockoutaudit_more_items_modal_win.png)

#### Resumen[¶](es/wince/#resumo_6)

La pantalla de resumen de la tarea Auditoría de rupturas contiene la siguiente información:

- **ESPERADOS:** Total de artículos previstos en las tareas (solo aplica para tareas orientadas).

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados con cantidad tratada igual a la cantidad esperada.
  - **PARCIALMENTE:** Total de artículos procesados con una cantidad menor a la esperada.
  - **EN EXCESO:** Número total de artículos procesados con una cantidad superior a la esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de ítems no procesados en la tarea (solo aplica para tareas orientadas).

![imagen alt <>](assets/stockoutaudit_resume_win.png)

### **SEPARACIÓN**[¶](es/wince/#separacao)

El propósito del tipo de tarea Separación es permitir que el usuario separe los artículos que se reemplazarán en la tienda. Las tareas de separación siempre se generan por flujo de trabajo de tareas, es decir, a través de tareas de Auditoría de rupturas, solicitudes de reemplazo urgentes, Verificación de presencia, Auditoría de escaneo o Auditoría externa.

Para ver el flujo completo de tareas que pueden llevar a la Separación, consulte la pestaña [Flujo de trabajo de tareas](es/workflow/) .

#### Separación: clientes minoristas de alimentos[¶](es/wince/#separacao-clientes-de-retalho-alimentar)

Las tareas de separación siempre están orientadas, es decir, los elementos previstos en la tarea siempre son visibles para el usuario. De esta forma, el usuario puede comprender más rápidamente qué elementos se solicitan para la separación mediante la ayuda de la fotografía (si corresponde) y la descripción del artículo.

En este sentido, los artículos son visibles en la lista de artículos pendientes e incluso pueden contener información sobre la cantidad a reemplazar - "Cantidad esperada". Si la tarea que dio lugar a la separación no indica la cantidad esperada para un artículo dado, esta información no es visible para el operador.

![imagen alt <>](assets/replenishprep_item_list_win.png)

##### MODAL DE ARTÍCULO - ALIMENTACIÓN[¶](es/wince/#modal-de-artigo-alimentar)

Durante el procesamiento de la tarea, al cortar un producto, el modal del artículo se abre inmediatamente. Este modal contiene la siguiente información:

- **Información del artículo:** el encabezado del modal incluye la fotografía (si corresponde), SKU, EAN y la información de SOH (si corresponde).
- **Separate and Separate:** Contiene información sobre la cantidad a separar y ya separada del artículo en tratamiento.
- **Origen y Ubicación:** Contiene información integrada en la tarea y que permite definir el origen de la tarea y la ubicación de la tienda que se trató.
- Entrada de **cantidad:** Entrada para definir la cantidad que se separará de un artículo determinado.
- **Botón No encontrado** Le permite establecer el artículo como No encontrado.
- Botón **"Devolver / Retirar:** " Le permite definir que el artículo está en la ubicación esperada pero que no está en condiciones de separarse. (ejemplo: artículo que se encuentra en período de desistimiento y, por lo tanto, no puede ser reemplazado en una tienda a la venta). De esta forma el artículo se procesará con cantidad cero.
- **Cajas y Cajas Gratis:** Facilitador de insertar cantidad por "Cajas" y "Cajas Gratis".

![imagen alt <>](assets/replenishprep_item_modal_win_2.png)

#### Separación - Clientes de moda[¶](es/wince/#separacao-clientes-fashion)

Las tareas de separación se adaptan a las necesidades de operación de los clientes de moda a través de la configuración. Los clientes de moda disponen de un catálogo compuesto por productos y skus. En este catálogo los productos son el modelo y los skus son los diferentes colores y tamaños. Ejemplo: El modelo A es la camiseta del producto y los skus son las tallas L, M y XL. Asimismo, los diferentes colores de la camiseta también son skus del mismo producto.

En este sentido, las tareas de Separación tienen un comportamiento diferente para adaptarse a las necesidades específicas del catálogo de moda.

##### ARTÍCULO MODAL - MODA[¶](es/wince/#modal-de-artigo-fashion)

Las tareas de separación para los clientes de moda están orientadas al sku como las de los clientes de alimentación. Es decir, en la lista de artículos pendientes aparecen los skus (colores y tallas) que deben estar separados.

Durante el procesamiento de la tarea, al cortar un sku predicho, se abre inmediatamente el modo de elemento. Este modal presenta el sku picado y todos los skus que pertenecen al mismo producto. Esto permite al usuario separar colores y tamaños distintos a los previstos inicialmente en la tarea.

Para esto, el modal contiene una entrada de cantidad en cada sku y la información de la cantidad esperada. El usuario podrá así poner cantidad en cada sku que quiera separar. Estos skus imprevistos se agregarán como ad hoc en la tarea.

![imagen alt <>](assets/replenisprep_fashion_win.gif)

#### Resumen[¶](es/wince/#resumo_7)

La pantalla de resumen de la tarea de separación contiene la siguiente información:

- **ESPERADOS:** Total de artículos previstos en la tarea.

- PROCESADOS:

   Total de artículos procesados en la tarea. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados en los que la cantidad separada es igual a la cantidad esperada.
  - **PARCIALMENTE:** Total de artículos procesados en los que la cantidad separada es menor que la cantidad esperada.
  - **EN EXCESO:** Número total de artículos procesados en los que la cantidad separada es mayor que la cantidad esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.
  - **DEVOLUCIÓN / RETIRO:** Total de artículos procesados Devolución / Retiro.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

![imagen alt <>](assets/replenishprep_resume_win.png)

### **REEMPLAZO**[¶](es/wince/#reposicao)

El propósito del tipo de tarea Reemplazo es permitir al usuario confirmar la separación de los artículos que se vuelven a almacenar. Por este motivo, las tareas de Reemplazo están orientadas y se generan exclusivamente al final de una tarea de Separación.

Los elementos tratados en la Separación con una cantidad mayor que cero generan automáticamente, al final de la tarea, una tarea de Reemplazo.

Al igual que en Separación, las tareas de Reemplazo están orientadas a permitir que el usuario reconozca más rápidamente los artículos separados que deben colocarse en la tienda.

Al consultar el Cockpit, es posible ver si todos los artículos separados realmente se han reemplazado en la tienda. Esta métrica es importante para comprender la calidad del reemplazo de la tienda.

#### Artículo modal[¶](es/wince/#modal-de-artigo_4)

Durante el procesamiento de la tarea, al cortar un producto, el modal del artículo se abre inmediatamente. Este modal contiene la siguiente información:

- **Información del artículo:** el encabezado del modal incluye la fotografía (si corresponde), SKU, EAN y la información de SOH (si corresponde).
- **Restablecer, origen y ubicación:** contiene información sobre la cantidad a restablecer, el origen del pedido y la ubicación (si corresponde).
- Entrada de **cantidad:** Entrada para definir la cantidad que reemplazará un determinado artículo.
- Botón **"Devolver / Retirar:** " Le permite definir que el artículo está en la ubicación esperada pero que no está en condiciones de separarse. (ejemplo: artículo que se encuentra en período de desistimiento y, por lo tanto, no puede ser reemplazado en una tienda a la venta). De esta forma el artículo se procesará con cantidad cero. Si la cantidad en la entrada es mayor que 0, el botón "Devolver / Retirar" desaparece y aparece "Continuar".
- **Cajas y Cajas Gratis:** Facilitador de insertar cantidad por "Cajas" y "Cajas Gratis".

![imagen alt <>](assets/replenish_modal_win.png)

#### Resumen[¶](es/wince/#resumo_8)

La pantalla de resumen de la tarea de confirmación contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en las tareas.

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados en los que la cantidad separada es igual a la cantidad esperada.
  - **PARCIAL:** Total de artículos procesados en los que la cantidad separada es menor que la cantidad esperada.
  - **EN EXCESO:** Número total de artículos procesados en los que la cantidad separada es mayor que la cantidad esperada.
  - **SIN CANTIDAD:** Total de artículos procesados Devolución / retirada.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

![imagen alt <>](assets/replenish_resume_win.png)

## TAREAS - INVENTARIOS | RECONTAJE | RECUENTO DE STOCK[¶](es/wince/#tarefas-inventarios-recontagem-contagem-de-stock)

### **INVENTARIO**[¶](es/wince/#inventario)

**Las** tareas de **inventario** siempre están integradas por ERP y el propósito de esta tarea es permitirle confirmar el stock de un determinado grupo de artículos cortando gradualmente el stock total en la tienda y el almacén.

Las tareas de Inventario a las que se hace referencia en el punto anterior son tareas de agregación, es decir, son tareas que agregan un cierto número de tareas secundarias llamadas **Zonas de Conteo** . Estas zonas se definen cuando se integra el inventario y permiten definir un área geográfica específica de la tienda o almacén donde se contabilizarán los artículos. Esto significa, por ejemplo, que en la zona de recuento "Abarrotes", solo se tratarán los productos comestibles que pertenezcan al inventario. De la misma forma, cada zona de conteo puede ser manejada por diferentes usuarios permitiendo una ejecución más ágil del propio inventario.

Al integrar las tareas de Inventario, también es posible definir si cada zona de conteo tendrá **una o dos lecturas obligatorias** . Si la zona de recuento incluye una segunda, se generan dos tareas de lectura para esa misma zona de recuento y el nombre de la tarea incluirá la información a la que corresponde la lectura a una determinada tarea. El beneficio de dos lecturas obligatorias es una reducción significativa en el error de ejecución del inventario, ya que cada zona de conteo se tratará dos veces y todos los artículos se cortarán dos veces.

Las dos lecturas mencionadas anteriormente tienen la obligación de **no divergencia** , es decir, los artículos y cantidades procesadas en las dos lecturas deberán ser exactamente iguales. En caso de diferencia entre productos o cantidades, el TMR genera inmediatamente una tarea de **Corrección de Divergencia** asociada a la zona de recuento en tratamiento. Esta tarea está disponible inmediatamente al cierre de la segunda lectura y permite corregir las diferencias encontradas entre las dos lecturas anteriores.

#### Tarea de inventario y zonas de recuento[¶](es/wince/#tarefa-de-inventario-e-zonas-de-contagem)

Al entrar en el menú de Inventario, el usuario tendrá acceso a la lista de tareas disponibles, de las que puede ocuparse en el momento. Cada tarea de Inventario consta de un conjunto de zonas, lo que permite que el mismo inventario sea manejado por más de un usuario al mismo tiempo.

En la lista de tareas de Inventario hay un facilitador de búsqueda que permite al usuario buscar un área de conteo específica leyendo el código de barras. Este facilitador está disponible para los clientes que incluyan un código de barras que identifique la zona de conteo. El TMR le permite configurar una regla de código de barras que identifica el inventario y la zona de conteo respectiva. De esta forma, cuando el usuario lee este código de barras, la aplicación busca en la zona de conteo y su inventario y automáticamente descarga la tarea respectiva.

![imagen alt <>](assets/inventory_zones_win.gif)

#### Creación de Zonas Adhoc[¶](es/wince/#criacao-de-zonas-adhoc)

Durante el Inventario, y si no hay suficientes zonas de conteo, el usuario puede solicitar nuevas zonas de conteo directamente en la aplicación móvil a través del botón "Crear Zona". Estas zonas ordenadas ad hoc pueden contener una o dos lecturas obligatorias. Además, el usuario puede definir el nombre de las zonas ad hoc creadas en la aplicación. Si ya existe una zona creada con el mismo nombre, la aplicación indicará en la respuesta cuál es el nombre de la zona creada.

![imagen alt <>](assets/inventory_create_adhoc_2_win.png)

#### Artículo modal[¶](es/wince/#modal-de-artigo_5)

En caso de que el artículo triturado sea un artículo de **peso variable** , debe abrir el modal de información del artículo y completar el peso leído en la etiqueta en la entrada de peso del artículo. En caso de picar artículos más iguales, pero con diferente peso, el operador siempre debe indicar el peso del artículo leído en esta entrada. La cantidad del artículo debe agregarse y presentarse en el campo "Cant. Ac.".

![imagen alt <>](assets/inventory_modal_insert_price_win.png)

En caso de que el artículo triturado sea de **Precio Variable** , se debe solicitar al usuario que ingrese el peso del artículo y, así, se calcula el precio por kilo. En los siguientes apartados del mismo artículo, este cálculo se utilizará para obtener el peso del artículo leído. En el modal del artículo se presentará la información del peso acumulado del artículo, el precio por kilo calculado y el peso del artículo (calculado en base al precio por kilo). Este peso del artículo debería ser posible cambiar.

![imagen alt <>](assets/inventory_modal_insert_weight_win.png)

Si los artículos no son de peso o precio variable, el astillado se puede realizar de forma sucesiva (barrido) o incremental. Para que se haga de forma incremental es necesario acceder al modal y poner el valor total de los artículos existentes en el área.

Durante el tratamiento de la tarea, es posible cambiar entre los modos "Normal" e "Incremental". El usuario debe acceder al botón ubicado en la esquina inferior izquierda de la aplicación.

![imagen alt <>](assets/inventory_picking_mode_win.gif)

#### Corrección de divergencia[¶](es/wince/#correcao-de-divergencia)

Cuando haya una divergencia de artículos entre la 1ª y la 2ª lectura, se creará automáticamente una tarea para corregir la divergencia. Las tareas de Corrección de divergencias son tareas orientadas con todos los artículos divergentes en el área de conteo en cuestión. Al abrir el modo de artículo, la información de recuento de la 1ª y 2ª lectura y las entradas Agregar y Reemplazar están presentes para ingresar la cantidad final del artículo.

![imagen alt <>](assets/inventory_modal_diverngence_win.png)

#### Resumen[¶](es/wince/#resumo_9)

La pantalla de resumen de la tarea de inventario contiene la siguiente información en relación con los artículos:

- **PROCESADOS:** Total de artículos procesados en las tareas.

Para las unidades, la pantalla de resumen de la tarea de Inventario contiene la siguiente información:

- **PROCESADOS:** Total de unidades procesadas en las tareas.

![imagen alt <>](assets/inventory_resume_win.png)

### **RECUENTO**[¶](es/wince/#recontagem)

Las tareas de recuento son tareas generadas después de la crítica de inventario. Estas tareas se utilizan para corregir cualquier desajuste de existencias detectado en el momento de la revisión.

Los recuentos son tareas guiadas, en las que los artículos fueron seleccionados durante el Inventario Crítica para el recuento y consecuente ajuste de stock. Al cerrar las tareas de Recuento, la información anterior será reemplazada por la información actual.

#### Artículo modal[¶](es/wince/#modal-de-artigo_6)

El modal del artículo contiene la siguiente información:

- **Información del artículo:** foto (si aplica), descripción, EAN, SKU y SOH (si aplica y configurable).
- **Total:** Información de la cantidad procesada previamente.
- Entrada de **cantidad:** Entrada para definir el número de unidades o peso.
- **Botón No encontrado / Continuar: le** permite establecer el artículo como No encontrado. Si la cantidad en la entrada es mayor que cero, el botón "No he encontrado" se reemplaza por el botón "Continuar".

![imagen alt <>](assets/recounting_modal_win.png)

#### Resumen[¶](es/wince/#resumo_10)

La pantalla de resumen de la tarea Recuento contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en la tarea.

- PROCESADOS:

   Total de artículos procesados en la tarea. En productos procesados existe la siguiente métrica:

  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

Para las unidades, la pantalla de resumen de la tarea de recuento contiene la siguiente información:

- **PROCESADO:** Total de unidades procesadas en la tarea. En productos procesados existe la siguiente métrica:
- **NO ENCONTRADO:** Total de unidades procesadas como no encontradas.

### **RECUENTO DE STOCK**[¶](es/wince/#contagem-de-stock)

La función de recuento de existencias le permite contar artículos para el ajuste de existencias en diferentes áreas de la tienda. Para garantizar la agregación de tareas de diferentes zonas, mediante la configuración del cliente, la aplicación permite la creación de tareas con afiliación. Esto significa que cuando se crea la tarea, se genera una tarea de conteo "madre" con una tarea "hija" para cada zona definida.

Ejemplo:

- Contando Fresco:

   \- Tarea "madre".

  - **Almacén de Frescos:** - Tarea "hija".
  - **Tienda Frescos:** - tarea "hija"

No es posible descargar tareas 'principales', solo sirven como un agregador de tareas. De esta manera, las tareas 'secundarias' son tareas individuales y pueden ser realizadas por diferentes usuarios. El estado de la tarea 'principal' evoluciona a cerrada cuando se terminan todas las tareas 'secundarias'.

#### Procesamiento de tareas[¶](es/wince/#processamento-da-tarefa_1)

Las tareas de recuento de existencias pueden ser específicas o ad hoc. La tarea ad hoc no está orientada (no tiene artículos previstos) y permite al usuario procesar artículos libremente.

Las tareas específicas (con artículos visibles para el usuario) se pueden crear ad hoc en base a una [lista](es/wince/#com-lista) o estructura [jerárquica](es/wince/#por-estrutura-hierárquica) . Las tareas guiadas también se pueden generar automáticamente cuando se cierran otras tareas. Para obtener más detalles, consulte [Flujo de trabajo de tareas](es/workflow/) .

Al ingresar al menú Stock Count, el usuario tendrá acceso a la lista de tareas disponibles, que podrá descargar al dispositivo, permitiendo así que la tarea se ejecute fuera de línea.

Si la tarea seleccionada tiene afiliación, el usuario es redirigido a una pantalla con tareas "secundarias". Después de descargar la tarea, el usuario es redirigido a la lista de artículos disponibles (solo se aplica a las tareas guiadas).

![imagen alt <>](assets/stockcount_zones_win.gif)

#### Artículo modal[¶](es/wince/#modal-de-artigo_7)

Al cortar un código de barras o seleccionar manualmente un artículo de la lista, aparece el modo de información del artículo donde el usuario puede colocar las unidades contadas del artículo o ingresar el número de casillas.

El modal del artículo contiene la siguiente información:

- **Información del artículo:** foto (si aplica), descripción, EAN, SKU y SOH (si aplica y configurable).
- **Cantidad acumulada:** Información de la cantidad procesada previamente.
- **Ingresar Agregar:** Ingrese para agregar el número de unidades o peso.
- **Ingresar Reemplazar:** Ingrese para reemplazar el número de unidades o peso.
- **Botón No encontrado / Continuar: le** permite establecer el artículo como No encontrado. Si la cantidad en la entrada es mayor que cero, el botón "No he encontrado" se reemplaza por el botón "Continuar".

![imagen alt <>](assets/stockcount_modal_win.png)

#### Resumen[¶](es/wince/#resumo_11)

La pantalla de resumen de la tarea Recuento de existencias contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en las tareas. (solo se aplica a orientado a tareas)

- PROCESADOS:

   Total de artículos procesados en las tareas. En productos procesados existe la siguiente métrica:

  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de ítems no procesados en la tarea (solo aplica para tareas orientadas).

Para las unidades, la pantalla de resumen de la tarea Recuento de existencias contiene la siguiente información:

- **PROCESADOS:** Total de unidades procesadas en las tareas. En productos procesados existe la siguiente métrica:
- **UNIDADES DE CANTIDAD:** Total de unidades procesadas en la tarea.

![imagen alt <>](assets/stockcount_resume_win.png)

## TAREA - RECEPCIÓN[¶](es/wince/#tarefa-rececao)

### RECEPCIÓN[¶](es/wince/#rececao)

La función de Recepción le permite recibir mercadería proveniente de almacenes, proveedores u otras tiendas. Al finalizar las tareas, la información de los artículos o cajas recibidas se envía a ERP y consecuente ajuste de stock.

TMR permite volver a listar Recepción de dos formas configurando la tarea:

- **Artículo por artículo:** El usuario revisa todos los artículos presentes en una caja o soporte.
- **Caja o Soporte:** El usuario marca solo las cajas o soportes integrados en la tarea y todos los elementos presentes en esas cajas o soportes serán marcados automáticamente.

#### Procesamiento de tareas[¶](es/wince/#processamento-da-tarefa_2)

Al acceder a la Recepción presente en el resumen de tareas, el usuario tendrá acceso a la lista de tareas disponibles, que podrá descargar al dispositivo. Si el usuario desea recibir mercadería que pertenece a una tarea que no está disponible, la aplicación permite buscar tareas futuras, utilizando la Orden de Compra, Guía de Transporte o simplemente por la fecha de la tarea.

De esta forma solo se presentarán las tareas que coincidan con los criterios de búsqueda utilizados.

##### BÚSQUEDA DE TAREAS[¶](es/wince/#pesquisa-de-tarefas)

Para acceder a la búsqueda avanzada, el usuario debe presionar el símbolo de búsqueda en la aplicación. De esta forma, la aplicación móvil proporciona una pantalla con los insumos para la investigación. Configurando el cliente, es posible definir qué entradas se presentarán.

![imagen alt <>](assets/receiving_task_search_win.gif)

##### RECEPCIÓN DE CAJAS O SOPORTES[¶](es/wince/#rececao-de-caixas-ou-suportes)

Al descargar la tarea, el usuario tendrá una lista de las cajas o soportes proporcionados. El usuario puede ingresar el código de la caja mediante inserción manual o mediante bip. De esta forma, se recibirán automáticamente las cajas o soportes y todos los artículos contenidos en ellos.

Si desea consultar artículo por artículo, puede seleccionar la casilla esperada y, en consecuencia, procesar los artículos presentes. Para sumar el número de unidades correspondientes al artículo, el usuario abre el modal del artículo, teniendo aquí la posibilidad de incrementar la cantidad mediante el ingreso de unidades, cajas o cajas libres.

![imagen alt <>](assets/receiving_picking_item_win.gif)

##### CAJAS DE SEGURIDAD[¶](es/wince/#caixas-de-seguranca)

Dependiendo de las necesidades específicas de cada negocio, es posible definir atributos específicos para el cajero. TMR permite distinguir cajas cuyo contenido es de mayor valor y mantener reglas específicas asociadas a esa caja. Si el código de esa caja está asociado con una pre-definición de su propia seguridad, entonces el usuario recibirá la caja de manera diferente, garantizando la seguridad de su contenido.

El astillado sigue reglas diferentes: estas cajas se reciben con sellos de seguridad que deben registrarse en la aplicación. Al pinchar la caja se abrirá el modo de seguridad, donde el usuario deberá insertar correctamente el código de los sellos. Tienes 3 oportunidades para registrarte con éxito. En caso de avería, la caja se considerará dañada, obligando al usuario a cortar obligatoriamente los artículos.

Durante el proceso de Recepción de una caja de seguridad, si los precintos no están disponibles, el usuario puede informar al TMR a través de la opción: "Sin información de precintos de seguridad".

![imagen alt <>](assets/receiving_container_safety_win.gif)

##### CAJAS NO PREVISTAS EN LA TAREA[¶](es/wince/#caixas-nao-previstas-na-tarefa)

La parametrización en el TMR también le permite personalizar la flexibilidad de la tarea en la aceptación y procesamiento de artículos y cajas no previstas. Cuando el usuario pincha o ingresa manualmente un EAN no previsto en la tarea, el TMR pregunta si quiere recibirlo como un artículo o como una caja. Al seleccionar un cuadro, se inserta en la tarea como un ad hoc, lo que le permite incluso recibir artículos dentro de este cuadro o volumen ad hoc.

##### CAJAS SIN ETIQUETA[¶](es/wince/#caixas-sem-etiqueta)

De manera similar a la recepción de buzones ad hoc, también es posible agregar cuadros sin etiquetar a la tarea, configurando el cliente. Las cajas sin etiqueta son cajas que no tienen identificación o que no se pueden leer. Para poder recibir esta caja y todos los elementos insertados en ella, la aplicación móvil proporciona al usuario un facilitador de inserción de cajas.

Esta opción es configurable para el cliente y permite la creación de un cuadro no identificado dentro de una tarea. Tanto la caja como los elementos insertados en ella son naturalmente ad hoc. Después de insertar la caja, es necesario cortar los artículos y, por lo tanto, la aplicación dirige automáticamente a la pantalla de corte de artículos.

![imagen alt <>](assets/receiving_container_without_label_win.gif)

##### RECIBO DE ARTÍCULO EN OTRA CAJA[¶](es/wince/#rececao-de-artigo-noutra-caixa)

Durante el procesamiento de la tarea, si el usuario escoge un artículo que no pertenece a una determinada casilla, la aplicación busca a qué casillas pertenece el artículo y lo recibe en la casilla donde se encuentra el artículo. Si hay más de una casilla, la aplicación pregunta de qué casilla desea recibir el artículo.

![imagen alt <>](assets/receiving_item_in_other_container_win.gif)

##### RECIBIENDO EL ARTICULO[¶](es/wince/#rececao-ao-artigo)

Las tareas de recepción solo pueden estar orientadas al artículo y no contienen recuadros como vimos en los puntos anteriores. Esta configuración depende únicamente de cómo el cliente entiende su funcionamiento y la TMR admite cualquiera de estos comportamientos.

Si la tarea está orientada solo a artículos, la lista de ítems pendientes no aparecen recuadros ni volúmenes, solo los artículos a recibir. El usuario debe verificar el EAN o seleccionar el artículo de la lista para recibirlo.

![imagen alt <>](assets/receiving_items_win.gif)

##### ALERTAS: RECUADROS Y ARTÍCULOS[¶](es/wince/#alertas-caixas-e-artigos)

En una tarea de Recepción, se pueden enviar cajas y artículos con alertas. Las alertas son identificadores de la tipología de cada artículo que conforma las tareas de recepción. Por ejemplo, un artículo que pertenece al catálogo actual viene con esta alerta asociada.

Por este motivo, el objetivo es que la aplicación muestre las alertas asociadas a cada artículo. Además, también es objetivo presentar la misma alerta a nivel de caja, para que el usuario tenga información sobre qué tipos de artículos hay en cada caja.

Los artículos o casillas con alertas asociadas incluyen la indicación visual a través del símbolo de notificación azul presente en la línea o casilla del artículo.

Para acceder a la lista de alertas, el usuario debe mantener presionada la línea o casilla del producto y en esa lista se muestran todas las alertas asociadas.

![imagen alt <>](assets/receiving_alerts_win.gif)

#### Recepción de clientes de moda[¶](es/wince/#rececao-clientes-fashion)

Configurando el cliente, es posible definir qué tipo de artículo de listado presenta la aplicación. Para los clientes de moda, TMR proporciona una lista simplificada donde no presenta la información incremental de los artículos, sino solo el número total de artículos pendientes, contados e imprevistos.

Si el usuario hace clic en el botón "Pendiente" o "Contado", se le dirige a la lista incremental de artículos donde puede consultar con más detalle toda la información disponible de los artículos.

Si el usuario hace clic en el botón "No previsto", se le lleva inmediatamente a la pantalla de fragmentación de elementos.

![imagen alt <>](assets/rececao_view_fashion_wince.gif)

Además, también puede definir que el procesamiento de la tarea incluya la recepción del artículo. Para ello, seleccione el campo "Bipulación al artículo".

![imagen alt <>](assets/rececao_view_fashion_wince_2.gif)

##### FECHAS DE VENCIMIENTO[¶](es/wince/#datas-de-validade)

Durante el manejo de una tarea de Recepción, el TMR permite recibir la fecha de vencimiento y la información de cantidad asociada a un determinado artículo. Esta funcionalidad es configurable mediante el comportamiento definido en la integración de la tarea.

![imagen alt <>](assets/receiving_expiration_audit_win.gif)

Además de la recopilación de la fecha de vencimiento, también es posible, configurando la integración de tareas, controlar la fecha de vencimiento ingresada. Así, es posible definir los días mínimos para recibir la fecha de vencimiento.

Por ejemplo: si se define en la tarea que no se puede recibir un artículo con una fecha mínima de 5 días, se alerta al usuario o se le impide recibir una fecha menor a 5 días en el futuro en relación con la fecha actual de recepción de la tarea.

Configurando el cliente es posible definir si se evita la recepción de una fecha de caducidad menor a los días mínimos definidos:

![imagen alt <>](assets/receiving_control_not_allow_win.png)

O si la recepción está permitida solo con una alerta al usuario.

![imagen alt <>](assets/receiving_control_allow_win.png)

#### Resumen: cajas y volúmenes[¶](es/wince/#resumo-caixas-e-volumes)

La pantalla de resumen de las Cajas o Volúmenes en las tareas de Recepción contiene la siguiente información en relación a los artículos:

- **ESPERADOS:** Total de artículos previstos en las tareas.

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados en los que la cantidad recibida es igual a la cantidad esperada.
  - **PARCIAL:** Total de artículos procesados en los que la cantidad recibida es menor a la esperada.
  - **EN EXCESO:** Número total de artículos procesados en los que la cantidad recibida es superior a la esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

![imagen alt <>](assets/receiving_resume_container_win.png)

#### Resumen: tarea[¶](es/wince/#resumo-tarefa)

La pantalla de resumen de la tarea de recepción contiene la siguiente información sobre cajas o volúmenes:

- **ESPERADOS:** Total de cajas o volúmenes previstos en las tareas.
- **PROCESADOS:** Total de cajas o volúmenes procesados en las tareas.
- **EUREKA:** Total de cajas o volúmenes procesados con el atributo de cajas de seguridad. En las cajas de seguridad se encuentra la siguiente métrica:
- **PENDIENTE:** Total de elementos no procesados en la tarea.

La pantalla de resumen de la tarea de recepción contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en las tareas.

- PROCESADOS:

   Total de artículos procesados en las tareas. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Número total de artículos procesados en los que la cantidad recibida es igual a la cantidad esperada.
  - **PARCIALMENTE:** Total de artículos procesados en los que la cantidad recibida es menor a la esperada.
  - **EN EXCESO:** Número total de artículos procesados en los que la cantidad recibida es superior a la esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea.

También en la pantalla de resumen de la tarea de Recepción, se encuentra disponible información sobre los detalles de la tarea, como las guías de Factura y Transporte. Esta información debe estar integrada por la tarea.

![imagen alt <>](assets/receiving_resume_win.png)

#### Aprobación de la tarea[¶](es/wince/#aprovacao-de-tarefa)

Una vez configurada para el cliente, la funcionalidad de Recepciones permite al usuario al final de la tarea tener visibilidad de la información previamente definida (alertas) o entradas disponibles para agregar la información necesaria a la tarea.

##### ALERTAS[¶](es/wince/#alertas)

Configurando al cliente, es posible definir que al final de una tarea de recepción, se muestre al usuario un mensaje informativo. Este mensaje está previamente definido y también configurable para el cliente.

Además de que el mensaje sea configurable por el cliente, también es posible definir un mensaje para tareas convergentes (todos los artículos tratados) y un mensaje para tareas divergentes (con artículos pendientes).

![imagen alt <>](assets/receiving_approve_action_win.png)

##### ENTRADAS[¶](es/wince/#inputs)

La recepción de tareas, al configurar el cliente, permite al usuario al final de la tarea agregar la información necesaria para la aprobación de la tarea. Las entradas configurables para la aprobación de tareas son las siguientes:

- **Identificación del transportista: le** permite identificar el transportista asociado con la tarea.
- **Nombre del transportista: le** permite identificar el nombre del transportista

![imagen alt <>](assets/receiving_approve_action_input_win2.png)

## TAREAS - TRASLADO | DEVOLUCIÓN[¶](es/wince/#tarefas-transferencia-devolucao)

### **TRANSFERIR**[¶](es/wince/#transferencia)

La función Transferir permite al usuario procesar ciertos artículos para el movimiento de existencias entre tiendas. De esta forma es posible seleccionar una tienda de destino al crear o descargar la tarea y procesar los artículos a transferir.

Las tareas de transferencia se pueden crear ad hoc en la aplicación móvil sin que se proporcione una lista de elementos. Durante este proceso de creación, al usuario se le proporciona una entrada de búsqueda que permite buscar y seleccionar la tienda de destino deseada.

Asimismo, las tareas de transferencia se pueden generar mediante integración. Esta integración permite al cliente realizar la gestión de stock a medida que se genera una tarea para que la tienda de origen transfiera determinados artículos con una tienda de destino ya definida.

![imagen alt <>](assets/transfer_create_task_win.gif)

#### Procesamiento de artículos[¶](es/wince/#processamento-de-artigos)

##### CREACIÓN DE CAJAS / VOLÚMENES[¶](es/wince/#criacao-de-caixasvolumes)

Las tareas de Transferencia, previa configuración al cliente, permiten la creación de cajas de volumen durante el tratamiento de la tarea. Para ello, la aplicación proporciona un botón en la parte superior de la pantalla.

![imagen alt <>](assets/transfer_manage_containers_win.gif)

##### VALIDACIÓN DEL RANGO DE TIENDA OBJETIVO[¶](es/wince/#validacao-gama-loja-destino)

La función Transferir permite al cliente configurar si el artículo existe en el rango de la tienda de destino a través de la configuración. Si el artículo no existe en el rango de la tienda de destino, la aplicación devuelve un error que no permite agregar el artículo a la tarea.

![imagen alt <>](assets/trasnfer_adhoc_item_win.png)

#### Aprobación de la tarea[¶](es/wince/#aprovacao-de-tarefa_1)

Una vez configurada para el cliente, la funcionalidad Transferir permite que el usuario al final de la tarea tenga visibilidad de la información previamente definida o de las entradas disponibles para agregar la información necesaria a la tarea.

##### ALERTAS[¶](es/wince/#alertas_1)

Al configurar el cliente, es posible definir que al final de una tarea de Transferencia, se muestre un mensaje informativo al usuario. Este mensaje está previamente definido y también configurable para el cliente.

Además de que el mensaje sea configurable por el cliente, también es posible definir un mensaje para tareas convergentes (todos los artículos tratados) y un mensaje diferente para tareas divergentes (con artículos pendientes).

![imagen alt <>](assets/receiving_approve_action_win.png)

##### ENTRADAS[¶](es/wince/#inputs_1)

Las tareas de transferencia, una vez configuradas para el cliente, permiten al usuario en el momento de la aprobación agregar información adicional. Las entradas configurables para la aprobación de tareas son las siguientes:

- **Identificación del transportista: le** permite identificar el transportista asociado con la tarea.
- **Nombre del transportista: le** permite identificar el nombre del transportista.
- **Prioridad: le** permite identificar la prioridad asociada con la transferencia. Los valores posibles son "Normal" y "Urgente".
- **Fecha de entrega: le** permite definir una fecha de entrega esperada.

![imagen alt <>](assets/transfer_approve_action_input_win.gif)

#### Resumen[¶](es/wince/#resumo_12)

La pantalla de resumen de la tarea Transferir contiene la siguiente información sobre cajas / volúmenes:

- **ESPERADOS:** Total de cajas / volúmenes previstos en la tarea. (aplicable solo a tareas orientadas)
- **PROCESADO:** Total de cajas / volúmenes procesados en la tarea.
- **PENDIENTE:** Total de cajas / volúmenes no procesados en la tarea. (aplicable solo a tareas orientadas)

La pantalla de resumen de la tarea de transferencia contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en la tarea. (aplicable solo a tareas orientadas)

- PROCESADOS:

   Total de artículos procesados en la tarea. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados en los que la cantidad transferida es igual a la cantidad esperada.
  - **PARCIAL:** Total de artículos procesados en los que la cantidad transferida es menor que la cantidad esperada.
  - **EN EXCESO:** Total de artículos procesados en los que la cantidad transferida es mayor que la cantidad esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea. (aplicable solo a tareas orientadas)

Para las unidades, la pantalla de resumen de la tarea Transferir contiene la siguiente información:

- **ESPERADOS:** Total de unidades previstas en la tarea.
- **PROCESADO:** Total de unidades procesadas en la tarea. En los productos procesados existen las siguientes métricas:
- **EN EXCESO:** Total de unidades procesadas en las que la cantidad separada es mayor que la cantidad esperada.
- **NO ENCONTRADO:** Total de unidades procesadas como no encontradas.

![imagen alt <>](assets/trasnfer_resume_win.png)

### DEVOLUCIÓN[¶](es/wince/#devolucao)

La función Devolución permite al usuario procesar ciertos artículos para devolución de existencias al almacén o al proveedor. De esta forma es posible seleccionar un destino al crear o descargar la tarea y procesar los artículos a devolver. Los posibles destinos son Almacén y Proveedor.

Las tareas de devolución se pueden crear ad hoc en la aplicación móvil sin que se proporcione una lista de elementos. Durante este proceso de creación, se le ofrece al usuario la posibilidad de seleccionar el Almacén o Proveedor de destino y una entrada de búsqueda (aplicable solo al seleccionar Proveedor como destino).

En cuanto a los almacenes, solo se devuelven a la aplicación los almacenes que están asociados con la tienda que inició sesión. Esta asociación es configurable para el cliente.

Asimismo, las tareas de devolución se pueden generar mediante integración. Esta integración permite al cliente realizar la gestión de stock a medida que se genera una tarea para que la tienda de origen traslade determinados artículos a un Proveedor o Almacén de destino ya definido.

![imagen alt <>](assets/itemreturn_create_task_win.gif)

#### Procesamiento de artículos[¶](es/wince/#processamento-de-artigos_1)

##### CREACIÓN DE CAJAS / VOLÚMENES[¶](es/wince/#criacao-de-caixasvolumes_1)

Las tareas de devolución, una vez configuradas al cliente, permiten la creación de cajas de volumen durante el tratamiento de la tarea. Para ello, la aplicación proporciona un botón en la parte superior de la pantalla.

![imagen alt <>](assets/transfer_manage_containers_win.gif)

#### Aprobación de la tarea[¶](es/wince/#aprovacao-de-tarefa_2)

Una vez configurada para el cliente, la funcionalidad Devolución permite que el usuario al final de la tarea tenga visibilidad de la información previamente definida o de las entradas disponibles para agregar la información necesaria a la tarea.

##### ALERTAS[¶](es/wince/#alertas_2)

Al configurar el cliente, es posible definir que al cierre de una tarea de devolución, se muestre un mensaje informativo al usuario. Este mensaje está previamente definido y también configurable para el cliente.

Además de que el mensaje sea configurable por el cliente, también es posible definir un mensaje para tareas convergentes (todos los artículos tratados) y un mensaje diferente para tareas divergentes (con artículos pendientes).

![imagen alt <>](assets/receiving_approve_action_win.png)

##### ENTRADAS[¶](es/wince/#inputs_2)

Las tareas de Devolución, al configurar el cliente, permiten al usuario en el momento de la aprobación agregar información adicional. Las entradas configurables para la aprobación de tareas son las siguientes:

- **Identificación del transportista: le** permite identificar el transportista asociado con la tarea.
- **Nombre del transportista: le** permite identificar el nombre del transportista.
- **Prioridad: le** permite identificar la prioridad asociada con la transferencia. Los valores posibles son "Normal" y "Urgente".
- **Fecha de entrega: le** permite definir una fecha de entrega esperada.

![imagen alt <>](assets/transfer_approve_action_input_win.gif)

#### Resumen[¶](es/wince/#resumo_13)

La pantalla de resumen de la tarea de devolución contiene la siguiente información sobre cajas / volúmenes:

- **ESPERADOS:** Total de cajas / volúmenes previstos en la tarea. (aplicable solo a tareas orientadas)
- **PROCESADO:** Total de cajas / volúmenes procesados en la tarea.
- **PENDIENTE:** Total de cajas / volúmenes no procesados en la tarea. (aplicable solo a tareas orientadas)

La pantalla de resumen de la tarea de devolución contiene la siguiente información en relación con los elementos:

- **ESPERADOS:** Total de artículos previstos en la tarea. (aplicable solo a tareas orientadas)

- PROCESADOS:

   Total de artículos procesados en la tarea. En los productos procesados existen las siguientes métricas:

  - **EN TOTALIDAD:** Total de artículos procesados en los que la cantidad devuelta es igual a la cantidad esperada.
  - **PARCIAL:** Total de artículos procesados en los que la cantidad devuelta es menor que la cantidad esperada.
  - **EN EXCESO:** Total de artículos procesados en los que la cantidad devuelta es mayor que la cantidad esperada.
  - **NO ENCONTRADO:** Total de artículos procesados como no encontrados.

- **PENDIENTE:** Total de elementos no procesados en la tarea. (aplicable solo a tareas orientadas)

Para las unidades, la pantalla de resumen de la tarea Devolver contiene la siguiente información:

- **ESPERADOS:** Total de unidades previstas en la tarea.
- **PROCESADO:** Total de unidades procesadas en la tarea. En los productos procesados existen las siguientes métricas:
- **EN EXCESO:** Total de unidades procesadas en las que la cantidad devuelta es mayor que la cantidad esperada.
- **NO ENCONTRADO:** Total de unidades procesadas como no encontradas.

![imagen alt <>](assets/trasnfer_resume_win.png)

## TAREAS - AUDITORÍA Y CONTROL DE VALIDEZ[¶](es/wince/#tarefas-auditoria-e-controlo-de-validades)

### **AUDITORÍA DE validada**[¶](es/wince/#auditoria-de-validades)

La función de auditoría de validez le permite registrar las fechas de vencimiento de los artículos presentes en una tienda o almacén. Este registro permite al TMR almacenar información sobre fechas de vencimiento y cantidades respectivas. Esta información será utilizada por el TMR para generar las respectivas tareas de Control de Validez.

#### Procesamiento de tareas[¶](es/wince/#processamento-da-tarefa_3)

Las tareas de Auditoría de validez son en su mayoría ad hoc sin una lista esperada de artículos. El usuario es libre de procesar los artículos y las fechas respectivas de forma autónoma. Sin embargo, al configurar el cliente, las tareas también se pueden crear ad hoc en la aplicación móvil en [función de una lista](es/wince/#com-lista) de artículos o [estructura jerárquica](es/wince/#por-estrutura-hierárquica) .

En tareas ad hoc sin una lista definida de artículos, el usuario debe recoger el artículo o ingresar manualmente el EAN y la aplicación proporciona un modal con la siguiente estructura:

- **DÍAS DEPRECED:** Información sobre el número de días parametrizados para la generación de tareas de amortización (Validity Control).
- **DÍAS DE JUBILACIÓN:** Información sobre el número de días parametrizados para generar las tareas de baja (Control de Validez).
- **TABLA DE FECHAS:** Información de las fechas ya registradas en la tarea y las fechas futuras del artículo en tratamiento.
- **FECHAS DE ENTRADA:** Entrada para insertar la fecha de caducidad del artículo. Solo se permite una fecha igual o mayor que el día actual.
- **CANTIDAD DE ENTRADA:** Entrada para insertar la cantidad de artículos asociados a la fecha de vencimiento respectiva.

![imagen alt <>](assets/expirationpicking_task_win.gif)

#### Lista de fechas[¶](es/wince/#listagem-de-datas)

Una vez registradas las fechas para un determinado artículo, la aplicación móvil presenta la lista del mismo y las cantidades respectivas. En esta lista, el usuario también tiene la posibilidad de "Eliminar fechas", "Editar fechas", "Registrar nueva validez" y "Continuar" para el modo de listado de artículos.

Si la fecha de registro se encuentra dentro del período de depreciación o retiro, la aplicación pregunta al usuario si desea continuar con el registro mediante el siguiente mensaje:

"El artículo se encuentra en período de depreciación / retiro. ¿Quieres continuar con el registro?".

Si el usuario selecciona "SÍ", la aplicación continúa con el registro. Si selecciona "Cancelar", la aplicación vuelve a la lista de fechas sin insertar la nueva fecha registrada.

![imagen alt <>](assets/expirationpicking_delete_date_win.gif)

#### Resumen[¶](es/wince/#resumo_14)

La pantalla de resumen de la tarea Auditoría de validez contiene la siguiente información en relación con los artículos:

- **ARTÍCULOS:** Información sobre el número de artículos procesados en la tarea.

La pantalla de resumen de la tarea Auditoría de validez contiene la siguiente información en relación con las fechas de vencimiento:

- **FECHAS DE VIGENCIA:** Información sobre las fechas de caducidad totales registradas en la tarea.

La pantalla de resumen de la tarea Auditoría de validez contiene la siguiente información en relación con las unidades:

- **UNIDADES:** Información de las unidades procesadas en la tarea.

![imagen alt <>](assets/expirationpicking_resume_win.png)

### **CONTROL DE validada**[¶](es/wince/#controlo-de-validades)

La función Control de validez le permite eliminar y depreciar artículos en la misma tarea. De esta forma, el tratamiento de las fechas de caducidad se convierte en un proceso más rápido y eficiente ya que el usuario procesa los artículos y sus fechas de caducidad en un solo momento.

Las tareas de Validity Control están orientadas exclusivamente y con una lista de artículos visibles.

#### Depreciación[¶](es/wince/#depreciacao)

La depreciación (Markdown) le permite aplicar acciones promocionales para deshacerse de los artículos que se acercan a la fecha de vencimiento.

Después de seleccionar el elemento de la lista, la aplicación muestra una lista de fechas para abordar. Para comenzar a procesar una fecha en el período de depreciación, el usuario debe seleccionar la fecha de la lista con la referencia "depreciación".

![imagen alt <>](assets/expirationcontrol_markdown_win.gif)

Durante el proceso de Depreciación, la aplicación valida la cantidad de SOH disponible (si hay un servicio de stock) y la compara con la cantidad de depreciación ingresada por el usuario. Si la cantidad ingresada es mayor que SOH, la aplicación devuelve el siguiente mensaje al usuario:

![imagen alt <>](assets/expirationcontrol_markdown_over_stock_win.png)

#### Depreciación modal[¶](es/wince/#modal-depreciacao)

El modal Depreciación presenta la siguiente información e insumos:

- **INFORMACIÓN DEL ARTÍCULO:** Descripción del artículo, EAN y SKU.
- **FECHA DE VIGENCIA:** Información sobre la fecha de caducidad en tratamiento.
- **SOH:** información de stock disponible para el artículo (si aplica).
- **DEPRECIADO:** Información sobre el monto de artículos depreciados asociados a la fecha de vencimiento en tratamiento.
- **PVP ACTUAL:** Información del precio actual del artículo (previa consulta del servicio de precios online).
- **ENTRADA DESCUENTO / CANTIDAD FIJA:** Entrada que permite al usuario definir el precio de depreciación del artículo. Le permite ingresar el descuento por monto o porcentaje final. El precio final lo calcula la aplicación y se muestra en la parte derecha de la entrada. En este momento, también se valida si el precio final se encuentra dentro del porcentaje máximo de descuento. Si este no es el caso, la aplicación muestra un mensaje de error y no le permite continuar con el proceso de depreciación.
- **ETIQUETA:** Entrada que permite definir el tipo de etiqueta de depreciación a imprimir.
- **CANTIDAD:** Entrada que permite definir la cantidad de etiquetas de depreciación a imprimir.
- **BOTÓN FECHA NO ENCONTRADA / DEPRECIAR:** Si el número de etiquetas es igual a cero, aparece el botón "Fecha no encontrada" para que el usuario pueda definir que no se ha encontrado la fecha para el tratamiento. De esta forma será una fecha procesada con cantidad cero. Si el número de etiquetas es mayor que cero, aparece el botón "Depreciar". Al hacer clic en "Depreciar" generará la acción de imprimir la cantidad de etiquetas de depreciación definidas por el usuario.

![imagen alt <>](assets/expirationcontrol_markdown_modal_win_2.png)

#### Retiro[¶](es/wince/#retirada)

El retiro le permite recolectar del tablero de ventas los artículos que se acercan a la fecha de vencimiento. De esta forma es posible que el usuario comprenda los artículos y respectivas fechas que se encuentran al final de su vigencia y los retire de la tienda.

Los días a eliminar, así como los días a depreciar, son parámetros definidos por artículo o estructura jerárquica. En el proceso de baja, es posible que el usuario defina un motivo y un destino, que también son parámetros configurables por cliente.

![imagen alt <>](assets/expirationcontrol_withdraw_win.gif)

#### Modal de retirada[¶](es/wince/#modal-de-retirada)

El modal de Retiro presenta la siguiente información y entradas:

- **INFORMACIÓN DEL ARTÍCULO:** Foto (si aplica), descripción, EAN, SKU y SOH (si aplica y configurable).
- **FECHA DE VIGENCIA:** Información sobre la fecha de caducidad en tratamiento.
- **DEPRECIACIÓN y RETIRO:** Información sobre los parámetros de validez.
- **DEPRECIADO, VENTAS y RETIRO:** Información sobre artículos previamente depreciados, la cantidad de ventas para la fecha en trámite y la cantidad a remover de la respectiva fecha de vencimiento. Esta información se consulta en línea y si no se devuelve información, la aplicación muestra N / A.
- **CANTIDAD ACUMULADA:** Información sobre la cantidad de elementos eliminados previamente en la tarea.
- **AGREGAR y REEMPLAZAR:** Entradas que permiten definir el número de artículos que se eliminarán de la fecha en tratamiento.
- **MOTIVO:** Entrada que le permite definir el motivo del retiro. Si el artículo se ha depreciado anteriormente, el motivo "Validez" se completa de forma predeterminada. Los motivos que aparecen en la lista de selección son configurables por el cliente.
- **DESTINO:** Entrada que le permite definir el destino del artículo después de que sea retirado de la tienda. Los destinos que aparecen en la lista de selección son configurables por el cliente.
- **BOTÓN FECHA NO ENCONTRADA / RETIRO:** Si la cantidad extraída es igual a cero, aparece el botón "Fecha no encontrada" para que el usuario pueda definir que no se ha encontrado la fecha para el tratamiento. De esta forma será una fecha procesada con cantidad cero. Si el número de etiquetas es mayor que cero, aparece el botón "Eliminar".

![imagen alt <>](assets/expirationcontrol_withdraw_modal_win.png)

#### Fechas futuras[¶](es/wince/#datas-futuras)

En el artículo modal es posible consultar y gestionar las fechas futuras a través de la opción "Gestión de Fechas Futuras". En este modal es posible consultar las fechas futuras ya registradas para un determinado artículo y también borrarlas si el usuario se da cuenta durante el tratamiento de la tarea que las fechas ya no están físicamente en la tienda.

Hay tres formas de seleccionar fechas:

- A partir de la entrada de fecha existente, el usuario puede seleccionar todas las fechas registradas hasta la fecha definida en la entrada a la vez.
- Directamente de la lista de fechas.
- A través del facilitador "Seleccionar todas las fechas".

Después de seleccionar las fechas deseadas, el usuario debe seleccionar "Eliminar". Inmediatamente, las fechas se eliminarán de la TMR.

![imagen alt <>](assets/expirationcontrol_show_future_dates_win.gif)

#### Agregar fecha ad hoc[¶](es/wince/#adicionar-data-adhoc)

Al manejar una tarea de Control de Validez, es posible agregar una fecha ad hoc si el usuario identifica una fecha en la tienda que no está en la lista de fechas futuras.

En el modal de fechas para tratamiento y en el modal de listado de fechas futuras está presente la entrada de "Agregar fecha ad hoc". Al seleccionar esta opción, el usuario es dirigido a un nuevo modal que permite definir una nueva fecha a través del input disponible y la cantidad respectiva.

![imagen alt <>](assets/expirationcontrol_adhoc_date_win.gif)

En el momento en que el usuario ingresa una nueva fecha, la aplicación valida si ya existe. Si es así, el siguiente mensaje se devuelve al usuario:

"No se puede agregar. La fecha de vencimiento ya existe".

![imagen alt <>](assets/expirationcontrol_date_already_existes_win.png)

Asimismo, la aplicación es válida si la fecha ingresada ya se encuentra en período de depreciación o se retira. En caso afirmativo, abra inmediatamente el respectivo modal para manipular el artículo.

#### Resumen[¶](es/wince/#resumo_15)

La pantalla de resumen de la tarea Control de validez contiene la siguiente información en relación con los artículos:

- **ESPERADOS:** Total de artículos previstos en la tarea.
- **PROCESADOS:** Total de artículos procesados en la tarea.

Con respecto a las fechas de vencimiento, la pantalla de resumen de la tarea Control de validez contiene la siguiente información:

- **ESPERADOS:** Fechas totales previstas en la tarea.
- **PROCESADO:** Total de fechas procesadas en la tarea. En los productos procesados existen las siguientes métricas:
- **DEPRECIADO:** Fechas totales procesadas como depreciación.
- **NO ENCONTRADO:** Total de fechas procesadas como no encontradas.
- **RETIROS:** Fechas totales procesadas como retiros.

![imagen alt <>](assets/expirationcontrol_resume_win.png)

## TAREAS - LISTA DE VERIFICACIÓN | LISTA DE ARTÍCULOS[¶](es/wince/#tarefas-checklist-lista-de-artigos)

### **LISTA DE VERIFICACIÓN**[¶](es/wince/#checklist)

La función Lista de verificación le permite realizar tareas diarias en la tienda utilizando información creada previamente. Las listas de verificación de cada tienda se realizan previamente en el Backoffice TMR y luego están disponibles en la aplicación móvil para ser utilizadas en la creación y realización de la Lista de Verificación.

Además de la creación ad hoc en la aplicación móvil, es posible programar las tareas de la lista de verificación a través del Programador.

Las tareas de la lista de verificación se basan en preguntas y respuestas. En este sentido, las listas de verificación creadas en el Backoffice son preguntas que incluyen dos tipos de posibles respuestas y que se definen al crear la lista de verificación.

Cuando la aplicación móvil descarga la tarea, le presenta al usuario la respuesta elegida en el momento de su creación.

![imagen alt <>](assets/checklist_create_task_win.gif)

#### Procesamiento de tareas[¶](es/wince/#processamento-da-tarefa_4)

Una vez descargada la tarea, la aplicación móvil presenta la lista de verificación asociada con la tarea y las respuestas asociadas con las preguntas respectivas.

Las posibles respuestas son las siguientes:

- **Verificado / No verificado**
- **Sí No**

Las respuestas se asocian con las preguntas al crear la lista de verificación.

![imagen alt <>](assets/checklist_processing_win.gif)

#### Resumen[¶](es/wince/#resumo_16)

La pantalla de resumen de la tarea Lista de verificación contiene la siguiente información:

- **PROPORCIONADO:** Total de elementos previstos en la tarea.
- **PROCESADO:** Total de elementos procesados en la tarea.

![imagen alt <>](assets/checklist_resume_win.png)

### **LISTA DE ARTÍCULOS**[¶](es/wince/#lista-de-artigos)

La función Lista de artículos permite al usuario crear una lista en la aplicación móvil utilizando los artículos que se están procesando en la tarea. Los artículos procesados se agregan a la Lista de artículos y al final de la tarea, la lista se creará y estará disponible en el Backoffice TMR para ser utilizada en otros tipos de tareas. (tareas ad hoc o programadas basadas en la lista)

#### Procesamiento de tareas[¶](es/wince/#processamento-de-tarefa)

Las tareas se crean ad hoc sin una lista definida. Durante el tratamiento de la tarea, los artículos después de ser cortados se asocian inmediatamente con la tarea tal como se procesan. El modal de artículo solo contiene información de artículo ya que el procesamiento de esta tarea no implica información de cantidad o entradas adicionales.

![imagen alt <>](assets/itemlist_processing_win.gif)

#### Resumen[¶](es/wince/#resumo_17)

La pantalla de resumen de la tarea Lista de elementos contiene la siguiente información:

- **PROCESADO:** Total de elementos procesados en la tarea.

![imagen alt <>](assets/itemlist_resume_win.png)

### **ROMPER REGISTRO**[¶](es/wince/#registo-de-quebras)

La función Break Log permite al usuario procesar ciertos elementos como un descanso. Las tareas de Registro de Breaks pueden ser ad hoc sin una lista guiada donde el usuario es libre de procesar los artículos que se encuentran en estas condiciones.

Las tareas también se pueden guiar con una lista de artículos visibles donde se guía al usuario para identificar y procesar cómo romper ciertos artículos previstos en la tarea. Las tareas guiadas se pueden crear mediante TMR Scheduler o mediante la integración.

#### Artículo modal[¶](es/wince/#modal-de-artigo_8)

Durante el tratamiento de la tarea, el usuario pinchará el EAN de los artículos o lo insertará manualmente y la aplicación abre inmediatamente el modal del artículo con la siguiente información:

- **INFORMACIÓN DEL ARTÍCULO:** Foto (si aplica), descripción, EAN, SKU y SOH (si aplica y configurable).
- **UNIDADES:** Entrada que permite definir la cantidad de elementos a agregar o reemplazar y que serán procesados como ruptura.
- **MOTIVO:** Entrada que le permite definir el motivo de la rotura. Los motivos que aparecen en la lista de selección son configurables por el cliente.
- **DESTINO:** Entrada que le permite definir el destino del artículo después de que sea retirado de la tienda. Los destinos que aparecen en la lista de selección son configurables por el cliente.
- **BOTÓN NO PUEDO ENCONTRAR / CONTINUAR:** Si el monto a romper es igual a cero, aparece el botón "No encontré" para que el usuario pueda definir que no se encontró la fecha de tratamiento. De esta forma será una fecha procesada con cantidad cero. Si la cantidad a romper es mayor que cero, aparece el botón "Continuar".

![imagen alt <>](assets/damage_modal_win.png)

#### Resumen[¶](es/wince/#resumo_18)

La pantalla de resumen de tareas del Registro de resumen contiene la siguiente información en relación con los artículos:

- PROCESADOS:

   Total de artículos procesados en la tarea.

  - **INESPERADOS:** Total de elementos no previstos en la tarea.

![imagen alt <>](assets/damage_resume_win.png)