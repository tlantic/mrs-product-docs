<!--![image alt <>](assets/under_construction.png){:height="500px" width="500px"}-->

# WINCE

![image alt <>](assets/capa_wince.png) 

## Histórico do documento

| Data      | Descrição    | Autor | Versão | Revisão
| ------------- | ---------------------------------------------------------------------------------------- | ---------- | ---------- | -------- | 
| 04/01/2021         | Criação do documento. &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;|  Filipe Silva &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;  |   1.0 &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;   |     |
| 06/01/2021         | Versão final do documento. &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;|  Filipe Silva &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;  |   2.0 &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;   |     |
| 22/01/2021     | Revisão final do documento.       | Filipe Silva  |  2.1    |  Helena Gonçalves|


## **Introdução**

### TMR - Instore

Este documento descreve as funcionalidades do sistema Tlantic Mobile Retail (TMR) – InStore. O principal objectivo do documento é fornecer ao utilizador toda a informação necessária para que este possa utilizar o sistema de forma correta e eficaz.

InStore é um dos módulos que compõe o Tlantic Mobile Retail (TMR) voltado para as operações de loja, com foco na solução de problemas comuns do retalho:
???+ example "Principais funcionalidades:"
    * Inventários.
    * Lista de Artigos, Ficha de Artigos, Auditoria de Preço, 
    * Registo e Aprovação de Quebras.
    * Impressão de etiquetas.
    * Auditoria  e Controlo de Validades (Depreciação e Retirada).
    * Auditoria de Varrimento e Verificação de Presença.
    * Auditoria de Rutura, Separação, Reposição. 
    * Receções, Transferências e Devoluções.

O módulo Instore procura simplificar e solucionar problemas do retalho com a execução de tarefas orientadas a um perfil de utilizador.
Cada tarefa está associada a uma funcionalidade com o objetivo de resolver uma determinada necessidade do retalho.
O TMR é composto pelos módulos: **BackOffice**, **Cockpit**, **Aplicações móveis (WinCE e Multiplataforma)**, **Scheduler** e **TMR Server**.

### Arquitetura

O TMR é um sistema constituído por:

* Aplicações Cliente (App WinCE, App Android e App iOS) - Aplicação distribuida pelos dispositivos móveis usados na operação de loja.
* TMR Server - Componente central que contém a lógica e dados e que interage com outros sistemas.
* Backoffice - Componente web que permite definição de dados de referência e configuração de parâmetros.
* Cockpit - Componente web para análises operacionais das tarefas executadas no TMR Instore.


![image alt <>](assets/Arquitetura.png) 

### Aplicação móvel - Windows CE

Nos pontos seguintes serão apresentadas todas as funcionalidades presentes na aplicação Windows CE e de que forma poderão ser executadas corretamente. Ao longo dos capítulos serão também apresentadas imagens ilustrativas para uma melhor orientação do utilizador em relação ao sistema.

### Requisitos mínimos

Para uma correta instalação do TMR, o dispositivo deve respeitar os seguintes requisitos:

???+ example "Requisitos mínimos:"
    * **Sistema Operativo:** Windows CE 4.2 ou superior.
    * **Memória ROM:** 60MB ou superior. 
    * **Leitor de Código de Barras:** Leitor 1D - LASER
    * **WI-FI:** 802.11 B/G/N.
    * **Tamanho do Ecrã Recomendado:** 3” ou superior.
    * **Resolução Mínima Recomendada:**  QVGA 320 x 240.


## **Login**
Para iniciar sessão é pedido ao utilizador que introduza o seu username e password.  Para verificar o texto introduzido no campo Password, o utilizador pode selecionar "Mostrar Palavra-Passe" para mostrar a informação. 
Após login, é apresentada ao utilizador um input para introdução do id da loja pretendida, sendo obrigatória a sua colocação para continuar. Se o utilizador estiver associado apenas a uma loja, o sistema guarda esta informação automaticamente e avança para o ecrã seguinte.

Após introdução de loja é necessário selecionar a impressora que será utilizada durante o tratamento das tarefas. No entanto, esta seleção não é obrigatória e a impressora pode ser alterada posteriormente. 

![image alt <>](assets/login_wince.gif){:height="300px" width="300px"}

## **Início**

Após login, o utilizador é direcionado para o ecrã inicial da aplicação onde tem acesso aos separadores de Tarefas, Tarefas Locais e Menu.

![image alt <>](assets/menu_wince.png)

### Tarefas

O separador "Tarefas" é a página inicial que apresenta o sumário de tarefas agregadas por tipo.  Desta forma, estão vísiveis os tipos de tarefas com tarefas disponíveis para processamento ou configurados para surgir no sumário mesmo quando não existem tarefas por realizar.

![image alt <>](assets/tarefas_wince.png)

### Tarefas Locais

O separador "Tarefas Locais" contém a lista de tarefas armazenadas localmente no dispositivo e que estão em execução pelo utilizador no dispositivo atual.

![image alt <>](assets/user_tasks_wince.png)

<!--### Pesquisa

O separador "Pesquisa" é um facilitador que permite a pesquisa de artigo por nome, descrição, EAN ou SKU. Ao selecionar um artigo, é apresentada a respetiva ficha do artigo. O template da Ficha de Artigo (as informações apresentadas e a sua ordem de apresentação) são definidas pelo cliente. 

Para aceder a informação de um determinado artigo fora do contexto de tarefa, o utilizador poderá aceder ao facilitador pesquisa presente na parte inferior da aplicação móvel. 

De seguida deverá pesquisar o artigo pretendido mediante a picagem do EAN ou inserção manual da informação. De forma imediata é apresentada a ficha de artigo com toda a informação disponibilizada pelo cliente. De notar que a informação apresentada na Ficha de Artigo é parametrizável por cliente e que pode conter qualquer informação que o cliente pretenda dar visibilidade na operação. 

![image alt <>](assets/search_item.gif){:height="300px" width="300px"}-->

### Menu

O separador "Menu" contém acessos para formulários da aplicação. As opções disponíveis dependem de parametrizações ao cliente.

???+ example "Formulários disponíveis:"
    * **Ficha de artigo:** Formulário que permite aceder ao detalhe de um artigo. A informação disponibilizada é configurável ao cliente.
    * **Impressoras:** Formulário para seleção de impressora e tipo de etiqueta. 
    * **Impressão de Etiquetas:** Formulário para acesso ao tipo de tarefa de impressão de etiquetas adhoc. Esta opão está disponível mediante [parametrização](app_settings.md#show-labelprint-on-summary).
    * **Alterar Palavra-Passe:** Formulário que permite alterar a palavra-passe associada ao utilizador logado. Esta opção está disponível mediante [parametrização](app_settings.md#allow-change-password).
    * **Terminar sessão:** Formulário para finalizar sessão na aplicação. 
    * **Sair:** Acesso para fecho da aplicação. 

![image alt <>](assets/menu_wince_2.png)

#### Ficha de Artigo

O formulário Ficha de Artigo permite consultar informações associadas a um determinado artigo. Para isso basta aceder ao formulário presente no menu e de seguida deverá pesquisar o artigo pretendido mediante a picagem do EAN ou inserção manual da informação. De notar que a informação apresentada na Ficha de Artigo é parametrizável por cliente e que pode conter qualquer informação que o cliente pretenda dar visibilidade na operação. 

![image alt <>](assets/ficha_de_artigo_wince.gif){:height="300px" width="300px"}

<!--#### Modal de artigo

Durante o tratamento de uma tarefa é possível consultar a Ficha de Artigo de um determinado produto. Para isso o utilizador deve aceder ao botão "Informação" presente no canto superior direto da modal de artigo. De imediato a aplicação direciona para a Ficha de Artigo.

![image alt <>](assets/search_item_modal.gif){:height="300px" width="300px"}-->

## **Tarefas**

### Lista de Tarefas

No menu Tarefas são apresentados os tipos de tarefa disponíveis para processamento. Quando é selecionado um tipo de tarefa, surge a lista de tarefas disponíveis para o tipo selecionado. Para cada tarefa é apresentado o nome, a sua disponibilidade, a data de criação e o utilizador que está a trabalhar na tarefa (se aplicável).

![image alt <>](assets/available_tasks_wince.png)


### Download da Tarefa
Ao efetuar download  de uma tarefa, esta fica atribuída ao utilizador logado e toda a informação é armazenada localmente no dispositivo. 

Desta forma, é possível processar tarefas sem conectividade à internet, já que todas as informações necessárias ficam guardadas no dispositivo (à exceção de informação que é obtida no momento da picagem, como é o caso do SOH do artigo ou o preço). 

Mediante configuração ao cliente é possível definir se é permitido o download de tarefas em execução e que não estejam previamente guardadas na memória do dispositivo. Esta proteção tem como objetivo não permitir ao mesmo utilizador realizar em simultaneo download da tarefa em diversos dispositivos pois, desta forma, poderia implicar perda de trabalho previamente realizado. 

![image alt <>](assets/download_tarefa_wince.gif){:height="300px" width="300px"}

### Processamento da Tarefa
O tratamento de tarefas orientadas seguem todas o mesmo fluxo base, independentemente do tipo de tarefa. 

Após download de uma tarefa, o utilizador tem uma lista de artigos para se guiar durante o tratamento da mesma. Inicialmente, estes artigos surgem todos na tab PENDENTES até que o utilizador faça alguma alteração no artigo.

Ao picar um código de barras ou ao selecionar manualmente um artigo, o sistema verifica se pertence à lista de artigos da tarefa. No caso de não pertencer, existem duas opções:

* Se a tarefa permitir adição de artigos não previstos, o utilizador tem a possibilidade de adicioná-lo à tarefa e tratá-lo com os restantes artigos. (1)
* Se não permitir, o utilizador é informado que o artigo em questão não pertence à tarefa e não pode processar o mesmo. (2)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![foto](assets/adhoc_item_wince.png){:height="350px" width="350px"}  ![foto](assets/not_adhoc_item_wince.png){:height="335px" width="335px"}

<!--Se o artigo pertencer à tarefa, surge uma modal com detalhe do artigo, dividido em três secções:

![image alt <>](assets/item_modal.png)-->

### Criação de tarefas Adhoc

O utilizador pode criar tarefas adhoc de três formas distintas: **Com lista**, **Sem lista** ou **Por estrutura hierárquica**. Para isso deve selecionar o botão "Criar Tarefa" presente no canto inferior direito da tarefa. 

#### Sem Lista

O utilizador deve pressionar o botão "Criar Tarefa" presente no canto inferior direito e escolher a opção **SEM LISTA**. Após a criação da tarefa, o utilizador é reencaminhado para o ecrã de tratamento onde pode iniciar o tratamento de artigos. 

![image alt <>](assets/criacao_tarefas_adhoc_wince.gif){:height="300px" width="300px"}

#### Com Lista

O utilizador deve pressionar o botão "Criar Tarefa" presente no canto inferior direito e escolher a opção **COM LISTA**. Após a criação da tarefa, o utilizador é reencaminhado para o ecrã onde vai escolher qual a lista que quer tratar das que estão disponiveis. Após a seleção o utilizador pode começar o tratamento de artigos. 

![image alt <>](assets/criacao_tarefas_adhoc_lista__wince.gif){:height="300px" width="300px"}

#### Por Estrutura Hierárquica

O utilizador deve pressionar o botão "Criar Tarefa" presente no canto inferior direito e escolher a opção  **POR ESTRUTURA HIERÁRQUICA**. Após a criação da tarefa, o utilizador é reencaminhado para o ecrã onde vai selecionar o nível da estrutura hierárquica que pretende, sendo o primeiro nível de carácter obrigatório e os seguintes facultativos. Após a seleção, o utilizador pode começar o tratamento de artigos.

![image alt <>](assets/criacao_tarefas_eh__wince.gif){:height="300px" width="300px"}

## **Configuração de Impressora**

O utilizador, durante o tratamento de uma tarefa, pode alterar a impressora em qualquer altura sem ter que sair da tarefa. 

Ao clicar no icone de "Definições de impressão" surge uma nova janela com a opção de escolha de impressora, o número de etiquetas quer imprimir e em alguns casos o tipo de etiqueta. 


![image alt <>](assets/seleção_de_impressao_wince.gif){:height="300px" width="300px"}

## **Resumo**

O utilizador pode aceder ao resumo da tarefa em qualquer momento, através do botão "Concluir" presente no canto superior direito no ecrã da tarefa. 

Neste momento é apresentado ao utilizador a opção de marcar todos os artigos não tratados como não encontrados caso exista essa configuração ao cliente.

Mediante configuração ao cliente é possível definir se um determinado tipo de tarefa pode ser aprovada com artigos pendentes de tratamento ou se é obrigatório o processamento dos mesmos para aprovação da tarefa. 

![image alt <>](assets/resume_wince.png)

O resumo fornece uma visão geral da tarefa e do seu processamento. A aplicação tem a capacidade de calcular individualmente os resources da tarefa, tais como: artigos, caixas, quantidades e datas de validade. Os algoritmos de cálculo disponíveis no resumos são os seguintes:


* PREVISTOS: resources previstos na tarefa.
* PENDENTES: resources  previstos na tarefa que não foram tratados.
* PROCESSADOS: resources processados pelo utilizador:
    * NÃO ENCONTRADOS
    * PARCIALMENTE
    * EM EXCESSO
    * NA TOTALIDADE
* NÃO PLANEADOS: Resources adiconados adhoc na tarefa.


Esta informação é agrupada por seções e podem ser definidas ao tipo da tarefa. O descritivo do cálculo pode também ser definido consoante o tipo de tarefa, como por exemplo: os artigos tratados podem ser remarcados na Remarcação, recebidos na Receção e auditados numa Auditoria. O detalhe de cada resumo poderá ser consultado no detalhe de cada tipo de tarefa.

Para mais detalhes, consultar a seção Resumo em cada tipo de tarefa.

Neste ecrã também é possível LIBERTAR ou APROVAR a tarefa.

![image alt <>](assets/resume_wince_2.png)


## TAREFAS - PREÇO

### **REMARCAÇÃO**

A funcionalidade de Remarcação permite atualizar diariamente as etiquetas de loja, dos artigos que sofrem alterações de preço e/ou outras características que são comunicadas ao cliente. 

Mediante configuração é possível segmentar os artigos para remarcar em diversos tipos de tarefas de Remarcação: Subida de Preço, Descida de Preço, Entrada em Promoção, Saída de Promoção, Desconto em Cartão e Remarcação Emergencial.

#### Listagem de artigos

Ao entrar no menu Remarcação, o utilizador terá acesso à lista de tarefas disponíveis que pode descarregar para o dispositivo. Após o download, é apresentada uma lista de artigos a tratar com o número de etiquetas a imprimir. Ao selecionar manualmente o artigo, surge a modal com a informação do artigo picado, o preço antigo, preço novo e a quantidade a imprimir.

Se a tarefa for configurada com modo de impressão automática, é impressa a etiqueta, por default, do artigo após picagem do mesmo.  

![image alt <>](assets/relabelling_item_list_wince.png){:height="300px" width="300px"} 

A listagem de artigos das tarefas de Remarcação contém a seguinte informação:

* **Informação do artigo:** Fotografia (se aplicável), descrição, EAN e SKU.
* **Quantidade:** Da parte direita da listagem está a informação de quantidade tratada e quantidade expectável para remarcação. Formato: Quantidade tratada/Quantidade expectável. 

#### Filtro

Na listagem de artigos nas tarefas de Remarcação é possível filtrar os artigos apresentados na lista. Mediante configuração, o TMR apresenta a opção no canto inferior esquerdo que permite filtrar os artigos apresentados na lista mediante as condições "Subidas de Preço" e "Descidas de Preço".

Dependendo da operação de loja, pode ser importante apresentar as subidas de preço e as descidas de preço em separado pois podem ser tratadas em momentos da operação diferentes.

![image alt <>](assets/relabelling_filter_wince.png){:height="300px" width="300px"}

#### Ordenação

Na listagem de artigos nas tarefas de Remarcação é possível ordenar os artigos apresentados na lista. Mediante configuração, o TMR apresenta a opção no canto inferior direito que permite ordenar os artigos apresentados na lista mediante algumas opções também elas configuráveis. Também mediante configuração é possível definir um valor default para ordenação de forma que no download da tarefa os artigos são automaticamente ordenados pelo valor configurado como default. 

Opções de ordenação:

* **Alterados (subidas-descidas):** Apresenta primeiro as subidas e depois as descidas.
* **Alterados (descidas-subidas):** Apresenta primeiro as descidas e depois as subidas.
* **Estrutura Hierárquica:** Apresenta os artigos ordenados pelo identificador da estrutura hierárquica - do menor para o maior.
* **Marca (A-Z):** Apresenta os artigos ordenados por marca de A a Z.
* **Marca (Z-A):** Apresenta os artigos ordenados por marca de Z a A.

![image alt <>](assets/relabelling_order.png){:height="300px" width="300px"}

#### Modal de artigo

Mediante configuração ao cliente é possível definir se a modal de artigo abre de forma imediata na picagem de artigo ou se na picagem imprime de imediato a etiqueta. 

A modal de artigo contém a seguinte informação:

* **Informação do artigo:** Fotografia (se aplicável), descrição, EAN, SKU e SOH (se aplicável).
* **Dinâmica de preço:** Informação do preço atual e o novo preço a ser remarcado.
* **Quantidade expectável:** Informação de quantidade de etiquetas a remarcar no campo "Previstos".
* **Quantidade remarcada:** Informação da quantidade de etiquetas remarcadas no campo "Remarcados".
* **Input de Quantidade:** Input para definir a quantidade de etiquetas remarcadas.
* **Informação da etiqueta:** Informação do tipo de etiqueta default para o artigo. (se aplicável)
* **Botão Não Encontrei/Continuar:** Permite definir o artigo como Não encontrado. Se a quantidade no input for superior a zero, o botão Não Encontrei é subtituido pelo botão "Continuar".

![image alt <>](assets/relabelling_item_modal_win.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Remarcação contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos nas tarefas.
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados em que a quantidade remarcada é igual à quantidade expectável.
    * **PARCIALMENTE:** Total de artigos processados em que a quantidade remarcada é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade remarcada é superior à quantidade expectável.
    * **NÃO ENCONTRADO:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa.

![image alt <>](assets/relabelling_resume_win.png){:height="300px" width="300px"}

### **IMPRESSÃO DE ETIQUETAS**

A funcionalidade de Impressão de Etiquetas permite ao utilizador realizar impressão de etiquetas adhoc. Desta forma, sempre que a operação de loja identifica artigos que não tenham etiqueta ou que esta esteja danificada, podem utilizar a funcionalidade de Impressão de Etiquetas para realizar a impressão. 

Mediante configuração ao cliente, a funcionalidade de Impressão de Etiquetas pode estar disponível no sumário de tarefas ou no menu da aplicação. 

![image alt <>](assets/impressao_etiquetas_wince.gif){:height="300px" width="300px"}

#### Seleção de impressora e etiqueta

Na listagem de artigos está presente o botão de seleção de impressora presente no canto superior direito da aplicação. Ao selecionar o botão, a aplicação abre de imediato um ecrã com as impressoras e etiquetas disponíveis para seleção. 

![image alt <>](assets/labelprint_printer_select.gif){:height="300px" width="300px"}

#### Modal de artigo

Mediante configuração ao cliente é possível definir se a modal de artigo abre de forma imediata na picagem de artigo ou se na picagem imprime de imediato a etiqueta. 

A modal de artigo contém a seguinte informação:

* **Informação do artigo:** Fotografia (se aplicável), descrição, EAN, SKU e SOH (se aplicável).
* **Quantidade impressa:** Informação da quantidade de etiquetas já impressas para determinado artigo.
* **Input de Quantidade:** Input para definir a quantidade de etiquetas a imprimir.
* **Botão Imprimir:** Ação de imprimir a quantidade de etiquetas definidas.

![image alt <>](assets/labelprint_modal_wince.png){:height="300px" width="300px"}

### **AUDITORIA DE PREÇO**

O objectivo do tipo de tarefa Auditoria de Preço é permitir auditar os preços presentes em loja. Através da picagem da etiqueta, o TMR realiza a comparação dos valores da etiqueta (label_price), os valores do ERP (ERP_price) e os valores do POS (POS_price).

*[label_price]: Preço impresso na etiqueta.
*[ERP_price]: Serviço do ERP que informa o preço do artigo em vigor.
*[POS_price]: Serviço do POS/PDV que informa o preço do artigo que será utilizado no momento do registo ao cliente.

Desta forma é possível auditar os preços presentes em loja e no caso de existir alguma divergência, o TMR imprime de imediato uma nova etiqueta com o preço atualizado.

#### Localização da Auditoria de Preço

No processo de [Criação de tarefas Adhoc](#criacao-de-tarefas-adhoc) o utilizador pode definir uma localização para a tarefa. Esta localização fica associada à tarefa e possibilita definir em que zona da loja foi realizada a auditoria. 

Existem duas localizações disponíveis para escolha:

* Loja
* Armazem

A localização escolhida pelo utilizador fica associada à tarefa e fica disponível para consulta no detalhe da tarefa no Cockpit. Com esta informação é possível ao gestor de operação perceber quais os artigos auditados em cada localização e a quantidade total de preços divergentes ou convergentes em cada localização

![image alt <>](assets/priceaudit_location_win.png){:height="300px" width="300px"}

#### Inserção manual do preço

Durante o tratamento de uma tarefa, quando é picado um EAN ou introduzido manualmente a informação de artigo, o TMR identifica que não foi informado o preço da etiqueta e pede a inserção manual desse valor ao utilizador.

![image alt <>](assets/priceaudit_manual_insert_win.png){:height="300px" width="300px"}

No caso de ser picada uma etiqueta do TMR que informe o preço do artigo, não é solicitado ao utilizador que informe o preço. Neste caso o TMR direciona o utilizador de forma imediata para a modal do artigo onde realiza a comparação dos preços.

#### Modal de artigo e auditoria de preços

Após obtenção do preço da etiqueta (por leitura de etiqueta ou introdução manual) surge de forma imediata a modal de artigo. Na abertura da modal, o TMR faz o pedido de preço ERP e POS através de uma chamada aos serviços disponibilizados pelo cliente. Realizando a comparaçãos dos valores é possível ao TMR concluir se o preço de etiqueta é Divergente ou Convergente.

*[Divergente]: Preço da etiqueta é diferente de preço ERP ou preço POS
*[Convergente]: Preço da etiqueta é igual a preço ERP e preço POS 

**Convergente**

No caso dos valores serem iguais, o TMR entende que a auditoria é convergente. Neste cenário apresenta a mensagem de "Sem Divergência". O artigo é dado como processado de forma automática sem necessidade de nenhuma ação adicional.

![image alt <>](assets/priceaudit_equal_price_win.png){:height="300px" width="300px"}

**Divergente**

No caso dos valores serem diferentes, o TMR entende que a auditoria é divergente. Neste cenário apresenta a mensagem de "Divergência Encontrada". De forma imediata é gerada a impressão de uma nova etiqueta e o artigo é considerado processado. Se existir a necessidade de reimpressão da etiqueta, o utilizador tem disponível o botão de impressão no canto superior direito da modal.

![image alt <>](assets/priceaudit_not_equal_price_win.png){:height="300px" width="300px"}

#### Auditoria de dois preços

A tarefa de Auditoria de Preço permite também auditar em simultâneo dois preços. Isto apenas de aplica a clientes que disponibilizem dois preços na etiqueta e que o serviço de preço que retorne dois preços (ERP 1, ERP 2 POS 1 e POS 2).

A auditoria de dois preços mantem o mesmo fluxo descrito anteriormente apenas com a diferença de auditar em simultâneo dois preços. Neste sentido um artigo é divergente se um ou os dois preços forem diferentes e é convergente se os dois preços forem iguais.

<!--![image alt <>](assets/priceaudit_two_prices_win.png){:height="300px" width="300px"}-->

#### Resumo

O ecrã de resumo da tarefa de Auditoria de preço contém a seguinte informação:

* **PREVISTOS:** Total de artigos previstos nas tarefas (apenas se aplica a tarefas orientadas).
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **DIVERGÊNCIAS:** Total de artigos processados com divergência.
    * **SEM ETIQUETA:** Total de artigos processados com informação manual do preço da etiqueta.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa (apenas se aplica a tarefas orientadas).

![image alt <>](assets/priceaudit_resume_win.png){:height="300px" width="300px"}

## TAREFAS - REPOSIÇÃO

### **AUDITORIA DE VARRIMENTO**

O objectivo do tipo de tarefa Auditoria de Varrimento é permitir ao utilizador verificar a presença de um determinado grupo de artigos em loja. 

Para esse efeito, o utilizador percorre a loja e vai picando os artigos presentes na placa de vendas permitindo ao TMR receber a informação de que o artigo está presente em loja.

Após o término da tarefa, esta informação fica disponível para consulta e exportação no Cockpit. Sobre esta informação serão realizados cálculos adicionais que permitem inferir a percentagem de rutura por tarefa. 

Com esta informação é possível ao gestor de loja saber a quantidade de artigos em rutura e a informação de stock disponível por artigo. 

#### Criação da tarefa de Auditoria de Varrimento

Em relação à criação de tarefas de Auditoria de Varrimento existem as seguintes possibilidades:

* **Adhoc com base em lista:** Possiblidade de criação de tarefas diretamente no dispositivo móvel definindo uma lista de artigos previamente existente. Essa lista de artigos é criada no Backoffice do TMR. Para mais detalhes consultar o capítulo de [criação de tarefas com base em lista](#com-lista).

* **Adhoc com base em estrutura hierárquica:**  Possiblidade de criação de tarefas diretamente no dispositivo móvel definindo o nível pretendido da estrutura hierárquica.  Para mais detalhes consultar o capítulo de [criação de tarefas por estrutura hierárquica](#por-estrutura-hierárquica).

* **Calendarizadas:** Possibilidade de criação de tarefas via TMR Scheduler. Através do Scheduler é possível definir um agendamento único ou uma recorrência automática na criação de tarefas. Por cada agendamento criado é possível definir uma lista de artigos a auditar ou um nível específico da estrutura hierárquica.

#### Auditoria de Varrimento orientada

Mediante configuração ao cliente é possível definir que as tarefas de Auditoria de Varrimento são orientadas. Isto significa que a listagem de artigos pendentes está disponível para visualização durante o tratamento da tarefa. Esta listagem de artigos auxilia o utilizador a perceber quais os artigos previstos para tratamento na tarefa através da fotografia (se aplicável) e descrição do artigo. 

![image alt <>](assets/scanaudit_oriented_win.png){:height="300px" width="300px"}

#### Auditoria de Varrimento não orientada

Mediante configuração ao cliente é possível definir que as tarefas de Auditoria de Varrimento não são orientadas. Isto significa que a listagem de artigos pendentes não está disponível para visualização durante o tratamento da tarefa. Isto permite não influenciar o utilizador com uma listagem de artigos definidas apesar de o TMR validar em cada picagem se o artigo pertence à tarefa. Também mediante configuração ao cliente é possível definir se o utilizador pode adicionar artigos não previstos nas tarefas ou se apenas pode picar artigos previamente definidos na criação da mesma. 

![image alt <>](assets/scanaudit_not_oriented_win.png){:height="300px" width="300px"}

Em ambos os cenários (orientada ou não orientada) os artigos picados serão apresentados na aba de Processados.

#### Pedido de reposição urgente

Durante o tratamento da tarefa de Auditoria de Varrimento está disponível ao utilizador o pedido de Reposição urgente. Este facilitador está presente junto à fotografia na modal de artigo. Através deste facilitador é possível ao utilizador definir para determinado artigo um pedido de reposição e a respetiva quantidade.

De forma imediata será gerada uma tarefa de Separação para o artigo pedido.

![image alt <>](assets/replenish_item_on_modal_win.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Auditoria de Varrimento contém a seguinte informação:

* **PREVISTOS:** Total de artigos previstos na tarefa (apenas se aplica a tarefas orientadas).
* **PROCESSADOS:** Total de artigos processados na tarefa. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados previstos na tarefa em que a quantidade tratada é igual à quantidade esperada.
    * **PARCIALMENTE:** Total de artigos processados previstos na tarefa em que a quantidade tratada é inferior à quantidade esperada (apenas se aplica nas tarefas que permite adicionar artigos não previstos).
* **PENDENTES:** Total de artigos não processados na tarefa (apenas se aplica a tarefas orientadas).

![image alt <>](assets/scanaudit_resume_win.png){:height="300px" width="300px"}

### **AUDITORIA EXTERNA**

O objectivo do tipo de tarefa Auditoria Externa é permitir ao utilizador verificar a presença de um determinado grupo de artigos em loja. Por esse motivo, a Auditoria Externa é semelhante a Auditoria de Varrimentos mas com algumas especifícidades:

#### Especificidades

* Foi desenhada para ser utilizada por uma equipa externa aos processos de loja, ou seja, permite um fluxo de processo distinto ao da Auditoria de Varrimento. Tal como na anterior, os cálculos da percentagem de rutura também são realizados com base nos dados recolhidos. 

* Permite que equipa externa à loja audite através deste tipo de tarefa o trabalho da operação realizado na Auditoria de Varrimento. No Cockpit é possível comparar as percentagens de rutura cálculadas entre os dois tipos de tarefa.

* Dado que a Auditoria Externa foi desenhada para ser operada por uma equipa externa à loja, é possível definir (mediante configuração ao cliente) a quantidade de zonas de contagem e a identificação de cada uma. Ao contrário da Auditoria de Varrimento que gera apenas uma tarefa, a Auditoria Externa permite configurar zonas de contagem que poderão ser executadas em simultâneo por operadores diferentes.

* No fecho de todas as zonas da tarefa é realizada a consolidação dos dados e os cálculos seguintes (ex. percentagem rutura) terão como base os dados já consolidados.

#### Zonas de contagem

Mediante configuração ao cliente é possível definir zonas de contagem para as tarefas de Auditoria Externa. 

Desta forma, na geração das tarefas (adhoc ou calendarizadas) são criadas zonas de contagem mediante quantidade configurada e a sua respetiva identificação. As zonas de contagem ficam sempre associadas à tarefa mãe de Auditoria Externa.

As zonas de contagem podem ser orientadas (com artigos previstos visíveis) ou não orientadas (lista de artigos previstos não visíveis ao utilizador) mediante configuração ao cliente. 

![image alt <>](assets/externalaudit_zones_childs_win.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Auditoria Externa contém a seguinte informação:

* **PREVISTOS:** Total de artigos previstos nas tarefas (apenas se aplica a tarefas orientadas).
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados previstos na tarefa em que a quantidade tratada é igual à quantidade esperada.
    * **PARCIALMENTE:** Total de artigos processados previstos na tarefa em que a quantidade tratada é inferior à quantidade esperada (apenas se aplica nas tarefas que permite adicionar artigos não previstos).
* **PENDENTES:** Total de artigos não processados na tarefa (apenas se aplica a tarefas orientadas).

![image alt <>](assets/scanaudit_resume_win.png){:height="300px" width="300px"}

### **VERIFICAÇÃO DE PRESENÇA**

O objectivo do tipo de tarefa Verificação de Presença é permitir confirmar a presença dos artigos não encontrados nas tarefa de Auditoria de Varrimento e Auditoria Externa. Por este motivo, as tarefas de Verificação de Presença não são criadas ahdoc nem calendarizadas mas geradas automaticamente no fecho das tarefas de Auditoria de Varrimento e Externa.

#### Geração de tarefas

A geração das tarefas de Verificação de Presença são configuráveis ao cliente, ou seja, é possível definir por cliente se estas são geradas no fecho das tarefas de Auditoria de Varrimento e Externa.

Se a configuração permitir a geração das tarefas de Verificação de Presença, esta geração segue a seguinte regra:

* Os artigos **não tratados** e **não encontrados** no Varrimento e Auditoria Externa e com stock maior que zero serão automaticamente inseridos na tarefa de Verificação de Presença.

* Existe também a possibilidade de filtrar por estado do artigo. Ou seja, é possível configurar o estado dos artigos que serão incluídos nas tarefas de Verificação de Presença. Por exemplo, é possível definir se os artigos Inativos serão incluídos na tarefa ou apenas artigos Ativos e Descontinuados.

#### Modal de artigo

Durante o processamento de tarefas de Verificação de Presença, ao picar um artigo a modal abre de forma imediata. Este comportamento é configurável por cliente. 

A modal de artigo contém a seguinte informação:

* **Informação do artigo:** No cabeçalho da modal está presenta a fotografia (se aplicável), a informação de SKU, EAN e SOH (se aplicável).
* **PS e Vendas:** Informação de PS e Vendas do dia (apenas aplicável a clientes que disponibilizem esta informação).
* **Motivo:** Dropdown que disponibiliza dois motivos:
    * Artigo no linear
    * Artigo noutro local

Estes motivos permitem identificar o local em que foi encontrado o artigo. Esta informação fica disponível para consulta no detalhe de artigo no TMR Cockpit. Esta informação permite à operação de loja perceber se os artigos estão nos locais corretos ou se é necessário alterações ao planograma de loja. 

Mediante configuração ao cliente é possível definir se os motivos estão presentes na modal de artigo para seleção. Se estiverem presentes o seu preenchimento é obrigatório pois não é possível processar o artigo sem definir um motivo.

![image alt <>](assets/presencecheck_modal_win.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Verificação de Presença contém a seguinte informação:

* **PREVISTOS:** Total de artigos previstos nas tarefas.
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **PREVISTOS:** Total de artigos processados previstos na tarefa.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa.

![image alt <>](assets/presencecheck_resume_win.png){:height="300px" width="300px"}

### **AUDITORIA DE RUTURAS**

Uma Auditoria de Rutura permite identificar em loja artigos que estejam em rutura parcial (existe o artigo na loja mas em quantidade menor que o espaço disponível) ou em rutura total (não existe o artigo em loja). Possibilita também a confirmação da quantidade a repor de um artigo no linear.

No momento de criação da tarefa de Auditoria de Ruturas é possível definir uma localização. Esta informação irá transitar para a tarefa de Separação consequente, permitindo ao utilizador consultar de forma mais ágil a localização dos artigos para reposição.

![image alt <>](assets/stockoutaudit_location_win.png){:height="300px" width="300px"}

Existem duas modalidades para Auditoria de Ruptura: 

* Orientada – O utilizador possui uma lista de artigos para orientar o processamento da tarefa.
* Adhoc - Sem lista de artigos previamente definida. A criação de tarefas adhoc pode ser consultada  na secção [Criação de tarefas Adhoc](#criacao-de-tarefas-adhoc).

#### Orientada

Ao selecionar Auditoria de Rutura no sumário de tarefas, o utilizador terá acesso à lista de tarefas disponíveis e que pode descarregar para o dispositivo, permitindo assim a sua execução em modo offline. Note-se que em modo offline o utilizador não terá acesso a certas informações como Presentation Stock, Vendas do dia e Stock on Hand, visto que são pedidos online durante o tratamento da tarefa.

Após o download da tarefa, é apresentada uma lista de artigos a tratar, que podem ou não ter uma quantidade expectável associada. Neste último caso, é esperado que o utilizador confirme que de facto é essa a quantidade do artigo que deve ser reposta, podendo alterar o valor em qualquer altura.

![image alt <>](assets/stockoutaudit_item_list_win.png){:height="300px" width="300px"}

#### Modal de artigo

Ao picar um código de barras ou selecionando o artigo da listagem, surge de forma imediata a modal do artigo. Nesta modal existem dois inputs possíveis: 

  * **Quantidade Prateleira:** Permite introduzir a quantidade de artigos presentes no linear.
  * **Quantidade a Repor:** Permite introduzir a quantidade desejada para reposição. Esta quantidade irá transitar para a consequente tarefa de Separação como quantidade a separar.  

![image alt <>](assets/stockoutaudit_item_modal_win.png){:height="300px" width="300px"}

Na modal está presente também a informação de "PS" e "VENDAS". Esta informação consultada online permite calcular a quantidade a repor de um determinado artigo mediante a quantidade presente no línear. 

**Nota:** Estes valores apenas estão disponíveis para clientes que detenham um serviço que possibilita a consulta dessa informação.

*[PS]: Presentation Stock - Valor que define a quantidade ideal de stock na prateleira
*[VENDAS]: Serviço que devolve a informação de vendas do dia para determinado produto. 

No caso de existir informação de PS, o utilizador tem apenas que introduzir a Quantidade Linear e a Quantidade a Repor é calculada automaticamente da seguinte forma:

![image alt <>](assets/stockoutaudit_PS.png){:height="500px" width="500px"}

Mesmo sendo calculada de forma automática, o utilizador pode alterar o valor do input "Quantidade a repor" em qualquer altura. 

##### Facilitador Caixas

A modal de artigo do tipo de tarefa Auditoria de Rutura inclui também um facilitador de Unidades/Caixa. Isto permite ao utilizador colocar uma quantidade a repôr informando a quantidade de caixas presentes e a quantidades de unidades por caixa.

**CAIXA**

Este facilitador permite indicar a quantidade de caixas e a quantidade de unidades por caixa dentro da lista pré-existente. O resultado deste cálculo é colocado no input "Quantidade a repor"

![image alt <>](assets/stockoutaudit_box_win.png){:height="300px" width="300px"}

**CAIXA LIVRES**

Este facilitador é semelhante ao anterior com a particularidade de permitir ao utilizador informar livremente a quantidade de unidades por caixa. O resultado do cálculo tem o mesmo comportamento referido anteriormente.

![image alt <>](assets/stockoutaudit_box_free_win.png){:height="300px" width="300px"}

Depois de introduzida a quantidade a repor, o botão "Continuar" fica disponível. Ao selecionar "Continuar" a modal é fechada de imediato e o artigo transita para a aba dos processados com a quantidade indicada.

A par do botão "Continuar" está também disponível o botão "Não Encontrei". Se o utilizador selecionar "Não Encontrei" o artigo é processado como não encontrado e será descartado para a tarefa de separação consequente. 

![image alt <>](assets/stockoutaudit_item_modal_win.png){:height="300px" width="300px"}

#### Clientes Fashion

No sentido de adaptar a tarefa de Auditoria de Rutura à especificidade do catálogo fashion, está presente na modal de artigo (apenas para clientes fashion) uma funcionalidade de listagem de artigos skus que pertencem ao mesmo produto. O objetivo desta funcionalidade é permitir auditar a rutura de skus que não estão presentes no linear mediante a picagem de um sku irmão (que pertence ao mesmo produto).

Para isto, o utilizador deverá selecionar o botão "Mais Artigos" presente modal  e o TMR disponibiliza uma listagem de skus associados ao mesmo produto. Nesta listagem o utilizador poderá colocar quantidade a repôr em qualquer artigo e eles serão adiconados como adhoc nas tarefas. 

![image alt <>](assets/stockoutaudit_more_items_modal_win.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Auditoria de Ruturas contém a seguinte informação:

* **PREVISTOS:** Total de artigos previstos nas tarefas (apenas se aplica a tarefas orientadas).
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados com quantidade tratada igual à quantidade prevista.
    * **PARCIALMENTE:** Total de artigos processados com quantidade tratada inferior à quantidade prevista.
    * **EM EXCESSO:** Total de artigos processados com quantidade tratada superior à quantidade prevista.
    * **NÃO ENCONTRADO:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa (apenas se aplica a tarefas orientadas).

![image alt <>](assets/stockoutaudit_resume_win.png){:height="300px" width="300px"}

### **SEPARAÇÃO**

O objectivo do tipo de tarefa Separação é permitir ao utilizador separar os artigos para serem repostos em loja. As tarefas de Separação são sempre geradas por workflow de tarefas, nomeadamente via tarefas de Auditoria de Ruturas, pedidos de reposição urgente, Verificação de Presença, Auditoria de Varrimento ou Auditoria Externa.

Para ver o fluxo completo de tarefas que podem originar Separação consultar o separador [Workflow de tarefas](workflow.md).

#### Separação - Clientes de retalho Alimentar

As tarefas de Separação são sempre orientadas, ou seja, os artigos previstos na tarefa estão sempre visíveis ao utilizador. Desta forma o utilizador consegue perceber mais rapidamente quais os artigos pedidos para separação através do auxílio da fotografia (se aplicável) e da descrição do artigo.

Neste sentido, os artigos estão visíveis na listagem de pendentes e podem conter inclusive informação da quantidade a repôr - "Quantidade expectável". Se a tarefa que deu origem à separação não indicar quantidade esperada para determinado artigo, esta informação não está visível ao operador.

![image alt <>](assets/replenishprep_item_list_win.png){:height="300px" width="300px"}

##### Modal de artigo - Alimentar

Durante o processamento da tarefa, ao picar um produto é aberta de imediato a modal de artigo. Esta modal contém a seguinte informação:

* **Informação do artigo:** No cabeçalho da modal está presenta a fotografia (se aplicável), a informação de SKU, EAN e SOH (se aplicável).
* **Separar e Separada:** Contém informação da quantidade a separar e já separada do artigo em tratamento.
* **Origem e Local:** Contém informação integrada na tarefa e que permite definir a origem da tarefa e o local da loja que foi tratada. 
* **Input de Quantidade:** Input para definir a quantidade que vai separar de determinado artigo.
* **Botão Não Encontrei** Permite definir o artigo como Não encontrado. 
* **Botão "Devolução/Retirada:**" Permite definir que o artigo se encontra no local previsto mas que não se encontra em condições de ser separado. (exemplo: Artigo que se encontra em período de retirada e que por isso não pode ser reposto em loja para venda). Desta forma o artigo será processado com quantidade zero.
* **Caixas e Caixas Livres:** Facilitador de inserção de quantidade por "Caixas" e "Caixas Livres".

![image alt <>](assets/replenishprep_item_modal_win_2.png){:height="300px" width="300px"}

#### Separação - Clientes Fashion

As tarefas de Separação adaptam-se às necessidades da operação dos clientes fashion mediante configuração. Os clientes fashion têm um catálogo composto por produtos e skus. Neste catálogo os produtos são o modelo e os skus são as diversas cores e tamanhos. Exemplo: A camisola do modelo A é o produto e os skus são o tamanho L, M e XL. Da mesma forma, as diversas cores da camisola são também skus do mesmo produto.

Neste sentido, as tarefas de Separação têm um comportamento distinto para se adaptar às necessidades específicas do catálogo fashion.

##### Modal de artigo - Fashion

As tarefas de Separação dos clientes fashion são orientadas ao sku como as dos clientes alimentar. Ou seja, na listagem de artigos pendentes aparecem os skus (cores e tamanhos) que devem ser separados.

Durante o processamento da tarefa, ao picar um sku previsto abre de imediato a modal de artigo. Esta modal apresenta o sku picado e todos os skus que pertencem ao mesmo produto. Isto permite ao utilizador separar outras cores e tamanhos diferentes das inicialmente previstas na tarefa.

Para isso a modal contém um input de quantidade em cada sku e a informação da quantidade expectável. O utilizador poderá assim colocar quantidade em cada sku que pretende separar. Estes skus não previstos serão adicionados como adhoc na tarefa. 

![image alt <>](assets/replenisprep_fashion_win.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Separação contém a seguinte informação:

* **PREVISTOS:** Total de artigos previstos na tarefa.
* **PROCESSADOS:** Total de artigos processados na tarefa. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados em que a quantidade separada é igual à quantidade expectável.
    * **PARCIALMENTE:** Total de artigos processados em que a quantidade separada é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade separada é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
    * **DEVOLUÇÃO/RETIRADA:** Total de artigos processados Devolução/retirada.
* **PENDENTES:** Total de artigos não processados na tarefa.

![image alt <>](assets/replenishprep_resume_win.png){:height="300px" width="300px"}

### **REPOSIÇÃO**

O objectivo do tipo de tarefa Reposição é permitir ao utilizador confirmar a separação dos artigos repostos em loja. Por este motivos as tarefas de Reposição são orientadas e exclusivamente geradas no fecho de uma tarefa de Separação.

Os artigos tratados na Separação com quantidade maior que zero geram automaticamente, no fecho da tarefa, uma tarefa de Reposição. 

Tal como na Separação, as tarefas de Reposição são orientadas para permitir ao utilizador reconhecer mais rapidamente os artigos separados e que devem ser colocados em loja. 

Através da consulta no Cockpit é possível perceber se todos os artigos separados foram efetivamente repostos em loja. Este métrica é importante para perceber a qualidade da reposição de loja.

#### Modal de artigo

Durante o processamento da tarefa, ao picar um produto é aberta de imediato a modal de artigo. Esta modal contém a seguinte informação:

* **Informação do artigo:** No cabeçalho da modal está presenta a fotografia (se aplicável), a informação de SKU, EAN e SOH (se aplicável).
* **Repor, Origem e Localização:** Contém informação da quantidade a repor, Origem do pedido e Localização (se aplicável).
* **Input de Quantidade:** Input para definir a quantidade que vai repôr de determinado artigo.
* **Botão "Devolução/Retirada:**" Permite definir que o artigo se encontra no local previsto mas que não se encontra em condições de ser separado. (exemplo: Artigo que se encontra em período de retirada e que por isso não pode ser reposto em loja para venda). Desta forma o artigo será processado com quantidade zero. Se a quantidade no input for maior que 0, o botão "Devolução/Retirada" desaparece e surge o "Continuar".
* **Caixas e Caixas Livres:** Facilitador de inserção de quantidade por "Caixas" e "Caixas Livres".

![image alt <>](assets/replenish_modal_win.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Confirmação contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos nas tarefas.
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados em que a quantidade separada é igual à quantidade expectável.
    * **PARCIAL:** Total de artigos processados em que a quantidade separada é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade separada é superior à quantidade expectável.
    * **SEM QUANTIDADE:** Total de artigos processados Devolução/retirada.
* **PENDENTES:** Total de artigos não processados na tarefa.

![image alt <>](assets/replenish_resume_win.png){:height="300px" width="300px"}

## TAREFAS - INVENTÁRIOS | RECONTAGEM | CONTAGEM DE STOCK

### **INVENTÁRIO**

As tarefas de **Inventário** são sempre integradas pelo ERP e o objetivo desta tarefas é o de permitir confirmar o stock de um determinado grupo de artigos mediante a picagem incremental do stock total existente em loja e armazém.

As tarefas de Inventário referidas no ponto anterior são tarefas agregadoras, ou seja, são tarefas que agregam em si uma determinada quantidade de tarefas filhas denominadas por **Zonas de Contagem**. Estas zonas são definidas no momento da integração do inventário e permitem definir uma determinada área geográfica da loja ou armazém onde os artigos serão contados. Isto significa, por exemplo, que na zona de contagem "Mercearia" apenas serão tratados artigos de mercearia que pertençam ao inventário. Da mesma forma, cada zona de contagem pode ser tratada por utilizadores diferentes permitindo uma execução mais ágil do próprio inventário. 

No momento da integração das tarefas de Inventário é ainda possível definir se cada zona de contagem terá **uma ou duas leituras obrigatórias**. Se a zona de contagem incluir segunda, leitura são geradas duas tarefas para essa mesma zona de contagem e no nome da tarefa será incluída a informação a que leitura corresponde determinada tarefa. O benefício de duas leituras obrigatórias consiste na redução significativa do erro de execução do inventário pois cada zona de contagem será tratada duas vezes e todos os artigos serão picados duas vezes.

As duas leituras referidas anteriromente têm a obrigatoriedade de **não divergência**, ou seja, os artigos e quantidades processados nas duas leituras terão de ser exatamente iguais. No caso de haver diferença entre produtos ou quantidades, o TMR gera de imediato uma tarefa de **Correção de Divergência** associada à zona de contagem em tratamento. Esta tarefa é disponibilizada de imediato no fecho da segunda leitura e permite corrigir as diferenças encontradas entre as duas leituras anteriores.

#### Tarefa de Inventário e Zonas de Contagem

Ao entrar no menú Inventário, o utilizador terá acesso à lista de tarefas disponíveis, que pode tratar no momento. Cada tarefa de Inventário é constituida por um conjunto de zonas, o que permite que o mesmo inventário possa ser tratado por mais do que um utilizador em simultâneo.

Na listagem de tarefas de Inventário existe um facilitador de pesquisa que permite ao utilizador pesquisar determinada zona de contagem mediante leitura do código de barras. Este facilitador está disponível para os clientes que incluem código de barras identificador da zona de contagem. O TMR perimite configurar uma regra de código de barras que identifica o inventário e a respetiva zona de contagem. Desta forma, quano o utilizador faz a leitura deste código de barras, a aplicação pesquisa a zona de contagem e o respetivo inventário e realiza de forma automática o download da respetiva tarefa.

![image alt <>](assets/inventory_zones_win.gif){:height="300px" width="300px"}

#### Criação de Zonas Adhoc

No decorrer do Inventário, e no caso de não existirem zonas de contagem suficientes, o utilizador poderá pedir novas zonas de contagem diretamente na aplicação móvel através do botão "Criar Zona". Essas zonas pedidas adhoc podem conter uma ou duas leituras obrigatórias.  Além disso, o utilizador pode definir o nome das zonas adhoc criadas na aplicação. Se já existir uma zona criada com o mesmo nome, a aplicação indicará na resposta qual o nome da zona que foi criada.

![image alt <>](assets/inventory_create_adhoc_2_win.png){:height="300px" width="300px"}

#### Modal de artigo

No caso do artigo picado for um artigo de **Peso Variável**, deve abrir a modal de informação do artigo e preencher o peso lido da etiqueta no input de peso do artigo. No caso de picar mais artigos iguais, mas com peso diferente, o operador deve indicar sempre o peso do artigo lido neste input. A quantidade do artigo deve ser somada e apresentado no campo "Qtd. Ac.". 


![image alt <>](assets/inventory_modal_insert_price_win.png){:height="300px" width="300px"}

No caso do artigo picado ser de **Preço Variável**, deve pedir ao utilizador que insira o peso do artigo e, desta forma, calculado o preço por quilo. Nas picagens seguintes do mesmo artigo, este cálculo será usado para obter o peso do artigo lido. Na modal do artigo será apresentada a informação do peso acumulado do artigo, o preço por quilo calculado e o peso do artigo (calculado com base no preço por quilo). Este peso do artigo deve ser possível alterar. 

![image alt <>](assets/inventory_modal_insert_weight_win.png){:height="300px" width="300px"}

No caso de os artigos não serem de peso ou preço variável, a picagem pode ser feita de forma sucessiva (varrimento) ou incremental. Para que seja feita de forma incremental, é preciso aceder a modal e colocar o valor total de artigos existentes na zona.

Durante o tratamento da tarefa é possível alternar entre modo "Normal" e "Incremental". O utilizador deve aceder ao botão posicionado no canto inferior esquerdo da aplicação. 

![image alt <>](assets/inventory_picking_mode_win.gif){:height="300px" width="300px"}

#### Correção de divergência

Quando existe Divergência de artigos entre a 1ª e 2ª leitura, será criada automaticamente uma tarefa para correção de Divergência. As tarefas de Correção de Divergência, são tarefas orientadas com todos os artigos divergentes na zona de contagem em questão. Ao abrir a modal de artigo está presente a informação da contagem da 1º e 2º leitura e o inputs Adicionar e Substituir para introduzir a quantidade final do artigo. 

![image alt <>](assets/inventory_modal_diverngence_win.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo das tarefas de Inventário contém a seguinte informação em relação aos artigos:

* **PROCESSADOS:** Total de artigos processados nas tarefas. 

Em relação às unidades, o ecrã de resumo da tarefa de Inventário contém a seguinte informação:

* **PROCESSADOS:** Total de unidades processadas nas tarefas. 

![image alt <>](assets/inventory_resume_win.png){:height="300px" width="300px"}

### **RECONTAGEM**

As tarefas de Recontagem são tarefas geradas após a Crítica do Inventário. Estas tarefas servem para corrigir algum desajuste de stock detetado no momento da Crítica.

As recontagens são tarefas orientadas, em que os artigos foram selecionados durante a Crítica de Inventário para contagem e consequente ajuste de stock. No fecho das tarefas de Recontagem, a informação anterior será substituida pela informação atual.

#### Modal de artigo

A modal de artigo contém a seguinte informação:

* **Informação do artigo:** Fotografia (se aplicável), descrição, EAN, SKU e SOH (se aplicável e configurável).
* **Total:** Informação da quantidade previamente processada.
* **Input de Quantidade:** Input para definir a quantidade de unidades ou peso.
* **Botão Não Encontrei/Continuar:** Permite definir o artigo como Não encontrado. Se a quantidade no input for superior a zero, o botão "Não Encontrei" é subtituido pelo botão "Continuar".

![image alt <>](assets/recounting_modal_win.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Recontagem contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos na tarefa.
* **PROCESSADOS:** Total de artigos processados na tarefa. Nos processados existe a seguinte métrica:
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa.

Em relação às unidades, o ecrã de resumo da tarefa de Recontagem contém a seguinte informação:

* **PROCESSADOS:** Total de unidades processadas na tarefa. Nos processados existe a seguinte métrica:
  * **NÃO ENCONTRADOS:** Total de unidades processadas como não encontradas.


### **CONTAGEM DE STOCK**

A funcionalidade de Contagem de Stock permite contar artigos para acerto de stock em diferentes zonas da loja. De forma a garantir a agregação de tarefas das diferentes zonas, mediante configuração ao cliente,  a aplicação permite a criação de tarefas com afiliação. Isto significa que no momento da criação da tarefa, é gerada uma tarefa de contagem "mãe" com uma tarefa "filha" por cada zona definida.

Exemplo:

* **Contagem de Frescos:** – tarefa "mãe".
    * **Frescos Armazém:** - Tarefa "filha".
    * **Frescos Loja:** – tarefa "filha"

Não é possível fazer o download das tarefas 'mães', servem apenas como agregador de tarefas. Desta forma, as tarefas 'filhas' são tarefas individuais e podem ser executadas por utilizadores diferentes. O estado da tarefa 'mãe' evolui para fechado quando todas as tarefas 'filhas' são terminadas.

#### Processamento da tarefa

As tarefas de Contagem de Stock podem ser Orientadas ou Adhoc. A tarefa adhoc não é orientada (não tem artigos previstos) e permite ao utilizador processar artigos livremente.

As tarefas orientadas (com artigos visíveis ao utilizador) podem ser criadas adhoc com base em [lista](#com-lista) ou estrutura [hierárquica](#por-estrutura-hierárquica). As tarefas orientadas também podem ser geradas de forma automática no fecho de outras tarefas. Para mais detalhes consultar [Workflow de tarefas](workflow.md).

Ao entrar no menu de Contagem de Stock, o utilizador terá acesso à lista de tarefas disponíveis, que pode descarregar para o dispositivo, permitindo assim a execução da tarefa em modo offline. 

Se a tarefa selecionada possuir afiliação, o utilizador é reencaminhado para um ecrã com as tarefas "filhas". Após o download da tarefa, o utilizador é reencaminhado para a listagem de artigos disponíveis (apenas se aplica a tarefas orientadas).

![image alt <>](assets/stockcount_zones_win.gif){:height="300px" width="300px"}

#### Modal de artigo

Ao picar um código de barras ou ao selecionar manualmente um artigo da lista, surge a modal de informação de artigo onde o utilizador pode colocar as unidades contadas do artigo ou inserir o número de caixas.

A modal de artigo contém a seguinte informação:

* **Informação do artigo:** Fotografia (se aplicável), descrição, EAN, SKU e SOH (se aplicável e configurável).
* **Quantidade Acumulada:** Informação da quantidade previamente processada.
* **Input Adicionar:** Input para adicionar a quantidade de unidades ou peso.
* **Input Substituir:** Input para subtituir a quantidade de unidades ou peso.
* **Botão Não Encontrei/Continuar:** Permite definir o artigo como Não encontrado. Se a quantidade no input for superior a zero, o botão "Não Encontrei" é subtituido pelo botão "Continuar".

![image alt <>](assets/stockcount_modal_win.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Contagem de Stock contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos nas tarefas. (apenas se aplica a tarefas orientadas)
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existe a seguinte métrica:
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa (apenas se aplica a tarefas orientadas).

Em relação às unidades, o ecrã de resumo da tarefa de Contagem de Stock contém a seguinte informação:

* **PROCESSADOS:** Total de unidades processadas nas tarefas. Nos processados existe a seguinte métrica:
  * **QUANTIDADE UNIDADES:** Total de unidades processadasna tarefa.

![image alt <>](assets/stockcount_resume_win.png){:height="300px" width="300px"}

## TAREFA - RECEÇÃO

### RECEÇÃO

A funcionalidade de Receção permite rececionar mercadoria originária de entrepostos, fornecedores ou outras lojas. No fecho das tarefas, a informação de artigos ou caixas rececionadas é enviada para ERP e consequente ajuste de stock.

O TMR permite que as Receção sejam relaizadas de duas formas mediante configuração da tarefa:

* **Artigo a artigo:** O utilizador confere todos os artigos presentes em caixa ou suporte.
* **Caixa ou Suporte:** O utilizador confere apenas as caixas ou suportes integrados na tarefa e todos os artigos presentes nessas caixas ou suportes serão automaticamente conferidos.

#### Processamento da tarefa

Ao aceder a Receção presente no sumário de tarefas, o utilizador terá acesso à lista de tarefas disponíveis, que pode descarregar para o dispositivo. Caso o utilizador pretenda rececionar mercadoria que pertença a uma tarefa que não se encontra disponível, a app permite pesquisar tarefas futuras, mediante a informação de Ordem de  Compra, Guia de transporte ou simplesmente pela data da tarefa. 

Desta forma serão apresentadas apenas as tarefas que correspondam aos critérios de pesquisa utilizados.

##### Pesquisa de tarefas

Para aceder à pesquisa avançada, o utilizador deverá pressionar o símbolo de pesquisa existente na aplicação. Desta forma a aplicação móvel disponibiliza um ecrã com os inputs para pesquisa. Mediante configuração ao cliente é possível definir quais os inputs que serão apresentados. 


![image alt <>](assets/receiving_task_search_win.gif){:height="300px" width="300px"}

##### Receção de caixas ou suportes

Ao descarregar a tarefa, o utilizador terá uma lista das caixas ou suportes previstos. O utilizador pode picar o código da caixa por inserção manual ou por bipagem. Deste modo, as caixas ou suportes serão rececionados de forma automática e todos os artigos neles contidos.  

Caso queira conferir artigo a artigo, poderá selecionar a caixa esperada e, consequentemente, processar os artigos presentes. Para adicionar o número de unidades correspondestes ao artigo, o utilizador abre a modal do mesmo, tendo aqui a possibilidade de incrementar a quantidade pelo input de unidades, caixas ou caixas livres. 

![image alt <>](assets/receiving_picking_item_win.gif){:height="300px" width="300px"}

##### Caixas de segurança

Mediante as necessidades específicas de cada negócio, é possível definir atributos específicos à caixa. O TMR permite distinguir caixas cujo conteúdo é de maior valor e manter regras específicas associadas a essa caixa. Se ao código daquela caixa estiver associada alguma pre-definição de segurança própria, então o utilizador irá rececionar a caixa de forma distinta, assegurando a segurança do seu conteúdo.

A picagem obedece a regras distintas - estas caixas são rececionadas com selos de segurança que têm de ser registados na aplicação. Ao picar a caixa, irá abrir a modal de segurança, onde o utilizador deverá inserir o código dos selos corretamente. Tem 3 oportunidades para registar com sucesso. No caso de falhar, a caixa será dada como danificada, obrigando o utilizador à picagem obrigatória dos artigos.

Durante o processo de Receção de uma caixa de segurança, se os selos não estiverem disponíveis, o utilizador poderá informar o TMR através da opção: "Sem informação de selos de segurança". 

![image alt <>](assets/receiving_container_safety_win.gif){:height="300px" width="300px"}

##### Caixas não previstas na tarefa

A parametrização no TMR permite também customizar a flexibilidade da tarefa na aceitação e processamento de artigos e caixas não previstas. Quando o utilizador pica ou introduz manualmente um EAN não previsto na tarefa, o TMR questiona se deseja rececionar como artigo ou como caixa. Ao selecionar caixa esta é inserida na tarefa como adhoc, permitindo inclusive rececionar artigos dentro desta caixa ou volume adhoc. 

<!--![image alt <>](assets/receiving_container_adhoc.gif){:height="300px" width="300px"}-->

##### Caixas sem etiqueta

De forma semelhante à receção de caixas adhoc, também é possível adiconar caixas sem etiqueta à tarefa, mediante configuração ao cliente. Caixas sem etiqueta são caixas que não têm identificação ou que a mesma não está legível. Para que seja possível rececionar essa caixa e todos os artigos nela inseridos, a aplicação móvel dispobiliza ao utilizador um facilitador de inserção de caixa.

Esta opção é parametrizavel ao cliente e permite a criação de uma caixa sem identificação dentro de uma tarefa. Tanto a caixa como os artigos nela inseridos são naturalmente adhoc. Depois de inserida a caixa é necessário a picagem dos artigos e por isso a aplicação direciona para o ecrã de picagem de artigos de forma automática. 

![image alt <>](assets/receiving_container_without_label_win.gif){:height="300px" width="300px"}

##### Receção de artigo noutra caixa

Durante o tratamento da tarefa se o utilizador picar um artigo que não pertence a determinada caixa, a aplicação pesquisa a que caixas o artigo pertence e receciona o mesmo na caixa onde o artigo se encontrar. Se existir mais do que uma caixa, a aplicação pergunta em qual das caixs pretende rececionar o artigo.

![image alt <>](assets/receiving_item_in_other_container_win.gif){:height="300px" width="300px"}


##### Receção ao artigo

As tarefas de Receção podem ser apenas orientadas ao artigo e não conter caixas como vimos nos pontos anteriores. Esta configuração depende unicamente da forma como o cliente entende a sua operação e o TMR comporta qualquer um deste comportamentos.

Se a tarefa for orientada apenas aos artigos, na listagem de pendentes não aparecem caixas nem volumes, apenas os artigos a rececionar. O utilizador deve picar o EAN ou selecionar o artigo da listagem para rececionar. 

![image alt <>](assets/receiving_items_win.gif){:height="300px" width="300px"}

##### Alertas - Caixas e Artigos

Numa tarefa de Receção, as caixas e os artigos podem ser enviados com alertas. Os alertas são identificadores da tipologia de cada item que compõe as tarefas de receção. Por exemplo, um artigo que pertence ao catálogo atual vem com esse alerta associado. 

Por este motivo, o objetivo é que aplicação apresente o(s) alerta(s) associados a cada artigo. Além disso, é objetivo também que apresente o mesmo alerta ao nível da caixa, para que o utilizador tenha informação de que tipos de artigos estão em cada caixa.

Os artigos ou caixas com alertas associados comportam a indicação visual através do simbolo de notificações azul presente na linha de artigo ou caixa.

Para aceder à listagem de alertas, o utilizador deve manter pressionado a linha do produto ou caixa e nessa listagem são apresentados todos os alertas associados. 

![image alt <>](assets/receiving_alerts_win.gif){:height="300px" width="300px"}


#### Receção clientes Fashion

Mediante configuração ao cliente é possível definir que tipo de listagem de artigos a aplicação apresenta. Para os clientes Fashion o TMR disponibiliza uma listagem simplificada onde não apresenta a informação incremental dos artigos mas apenas o total de artigos Pendentes, Contados e Não previstos.

Se o utilizador clicar no botão "Pendentes" ou "Contados" é direcionado para a listagem incremental dos artigos onde poderá consultar com mais detalhe toda a informação disponível dos artigos.

Se o utilizador clicar no botão "Não previstas" é direcionado de forma imediata para o ecrã de picagem de artigos. 

![image alt <>](assets/rececao_view_fashion_wince.gif){:height="300px" width="300px"}

Além disso, poderá também definir que o processamento da tarefa inclui receção ao artigo. Para isso deverá selecionar o campo "Bipagem ao Artigo".

![image alt <>](assets/rececao_view_fashion_wince_2.gif){:height="300px" width="300px"}

##### Datas de Validade

Durante o tratamento de uma tarefas de Receção, o TMR permite rececionar a informação de data de validade e quantidade associada a determinado artigo. Esta funcionalidade é parametrizável mediante o comportamento definido na integração da tarefa.

![image alt <>](assets/receiving_expiration_audit_win.gif){:height="300px" width="300px"}

Além da recolha da data de validade é possível também, por configuração na integração de tarefa, o controlo da data de validade inserida. Assim, é possível definir os dias mínimos para receção de data de validade. 

Por exemplo: se estiver definido na tarefa que um artigo não pode ser rececionado com uma data mínima de 5 dias, o utilizador é alertado ou impedido de rececionar uma data com menos de 5 dias no futuro em relação à data atual de receção de tarefa.

Mediante configuração ao cliente é possível definir se é impeditiva a receção de uma data de validade menor que os dias mínimos definidos:

![image alt <>](assets/receiving_control_not_allow_win.png){:height="300px" width="300px"}

Ou se é permitida a receção apenas com um alerta ao utilizador.

![image alt <>](assets/receiving_control_allow_win.png){:height="300px" width="300px"}

#### Resumo - Caixas e Volumes

O ecrã de resumo das Caixas ou Volumes nas tarefas de Receção contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos nas tarefas.
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados em que a quantidade rececionada é igual à quantidade expectável.
    * **PARCIAL:** Total de artigos processados em que a quantidade rececionada é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade rececionada é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa.

<!--Em relação às unidades, o ecrã de resumo das tarefas de Receção contém a seguinte informação:

* **PREVISTOS:** Total de unidades previstas nas tarefas.
* **PROCESSADOS:** Total de unidades processadas nas tarefas. Nos processados existem as seguintes métricas:
    * **EM EXCESSO:** Total de unidades processadas em que a quantidade rececionada é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de unidades processadas como não encontrados.-->

![image alt <>](assets/receiving_resume_container_win.png){:height="300px" width="300px"}

#### Resumo - Tarefa

O ecrã de resumo da tarefa de Receção contém a seguinte informação em relação às caixas ou volumes:

* **PREVISTOS:** Total de caixas ou volumes previstos nas tarefas.
* **PROCESSADOS:** Total de caixas ou volumes processados nas tarefas. 
* **EUREKA:** Total de caixas ou volumes processados com o atributo caixas de seguraça. Nas caixas de segurança existe a seguinte métrica:
* **PENDENTES:** Total de artigos não processados na tarefa.

O ecrã de resumo da tarefa de Receção contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos nas tarefas.
* **PROCESSADOS:** Total de artigos processados nas tarefas. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de  processados em que a quantidade rececionada é igual à quantidade expectável.
    * **PARCIALMENTE:** Total de artigos processados em que a quantidade rececionada é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade rececionada é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa.


Também no ecrã de resumo da tarefa de Receção está disponível informação sobre detalhes da tarefa como Nota Fiscal e Guias de transporte. Esta informação deve ser integrada pela tarefa.   

![image alt <>](assets/receiving_resume_win.png){:height="300px" width="300px"}

#### Aprovação de tarefa

Mediante configuração ao cliente, a funcionalidade de Receções permite ao utilizador no fecho da tarefa ter visibilidade de informação anteriormente definida (alertas) ou inputs disponíveis para adicionar informação necessária à tarefa. 

##### Alertas

Mediante configuração ao cliente é possível definir que no fecho de uma tarefa de receção é apresentada uma mensagem informativa ao utilizador. Esta mensagem é definida anteriormente e também ela configurável ao cliente. 

Além da mensagem ser parametrizável por cliente é possível também definir uma mensagem para as tarefas convergentes (todos os artigos tratados) e uma mensagem para tarefas divergentes (com artigos pendentes).

![image alt <>](assets/receiving_approve_action_win.png){:height="300px" width="300px"}

##### Inputs

As tarefas de Receções, mediante configuração ao cliente, permitem ao utilizador no fecho da tarefa adicionar informação necessária à aprovação da tarefa. Os inputs configuráveis na aprovação de tarefa são os seguinte:

* **Identificação Transportador:** Permite identificar o transportador associado à tarefa.
* **Nome Transportador:** Permite identificar o nome do transportador

![image alt <>](assets/receiving_approve_action_input_win2.png){:height="300px" width="300px"}

## TAREFAS - TRANSFERÊNCIA | DEVOLUÇÃO

### **TRANSFERÊNCIA**

A funcionalidade de Transferência permite ao utilizador processar determinados artigos para movimento de stock entre lojas. Desta forma é possível selecionar uma loja de destino na criação ou download da tarefa e processar artigos para serem transferidos. 

As tarefas de Transferência podem ser criadas adhoc na aplicação móvel sem lista de artigos previstos. Durante este processo de criação é dispobilizado ao utilizador um input de pesquisa que permite pesquisar e selecionar a loja de destino pretendida.

Da mesma forma, as tarefas de Transferência podem ser geradas via integração. Esta integração permite ao cliente realizar gestão de stocks na medida em que é gerada uma tarefa para a loja de origem transferir determinados artigos com uma loja de destino já definida.

![image alt <>](assets/transfer_create_task_win.gif){:height="300px" width="300px"}

#### Processamento de artigos

##### Criação de caixas/volumes

As tarefas de Transferência, mediante configuração ao cliente, permitem criar caixas volumes durante o tratamento da tarefa. Para isso a aplicação disponibiliza um botão na parte superior do ecrã.

![image alt <>](assets/transfer_manage_containers_win.gif){:height="300px" width="300px"}

##### Validação gama loja destino

A funcionalidade de Transferência permite mediante configuração ao cliente validar se o artigo existe na gama da loja de destino. Se o artigo não existir na gama da loja de destino a aplicação retorna um erro não permitindo adicionar o artigo à tarefa. 

![image alt <>](assets/trasnfer_adhoc_item_win.png){:height="300px" width="300px"}


#### Aprovação de tarefa

Mediante configuração ao cliente, a funcionalidade de Transferência permite ao utilizador no fecho da tarefa ter visibilidade de uma informação anteriormente definida ou inputs disponíveis para adicionar informação necessária à tarefa. 

##### Alertas

Mediante configuração ao cliente é possível definir que no fecho de uma tarefa de Transferência é apresentada uma mensagem informativa ao utilizador. Esta mensagem é definida anteriormente e também ela configurável ao cliente. 

Além da mensagem ser parametrizável por cliente é possível também definir uma mensagem para as tarefas convergentes (todos os artigos tratados) e uma diferente mensagem para tarefas divergentes (com artigos pendentes).

![image alt <>](assets/receiving_approve_action_win.png){:height="300px" width="300px"}

##### Inputs

As tarefas de Transferência, mediante configuração ao cliente, permitem ao utilizador no momento da aprovação adicionar informação complementar. Os inputs configuráveis na aprovação de tarefa são os seguinte:

* **Identificação Transportador:** Permite identificar o transportador associado à tarefa.
* **Nome Transportador:** Permite identificar o nome do transportador.
* **Prioridade:** Permite identificar a prioridade associada à transferência. Os valores possíveis são "Normal" e "Urgente".
* **Data de entrega:** Permite definir uma data expectável de entrega. 

![image alt <>](assets/transfer_approve_action_input_win.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Transferência contém a seguinte informação em relação às caixas/volumes:

* **PREVISTOS:** Total de caixas/volumes previstas na tarefa. (aplicável apenas a tarefas orientadas)
* **PROCESSADOS:** Total de caixas/volumes processadas na tarefa.
* **PENDENTES:** Total de caixas/volumes não processados na tarefa. (aplicável apenas a tarefas orientadas)

O ecrã de resumo da tarefa de Transferência contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos na tarefa. (aplicável apenas a tarefas orientadas)
* **PROCESSADOS:** Total de artigos processados na tarefa. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados em que a quantidade transferida é igual à quantidade expectável.
    * **PARCIAL:** Total de artigos processados em que a quantidade transferida é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade transferida é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa. (aplicável apenas a tarefas orientadas)

Em relação às unidades, o ecrã de resumo da tarefa de Transferência contém a seguinte informação:

* **PREVISTOS:** Total de unidades previstas na tarefa.
* **PROCESSADOS:** Total de unidades processadas na tarefa. Nos processados existem as seguintes métricas:
  * **EM EXCESSO:** Total de unidades processadas em que a quantidade separada é superior à quantidade expectável.
  * **NÃO ENCONTRADOS:** Total de unidades processadas como não encontradas.

![image alt <>](assets/trasnfer_resume_win.png){:height="300px" width="300px"}


### DEVOLUÇÃO

A funcionalidade de Devolução permite ao utilizador processar determinados artigos para retorno de stock para entreposto ou fornecedor. Desta forma é possível selecionar um destino na criação ou download da tarefa e processar artigos para serem devolvidos. Os destinos possíveis são Entreposto e Fornecedor.

As tarefas de Devolução podem ser criadas adhoc na aplicação móvel sem lista de artigos previstos. Durante este processo de criação é disponibilizado ao utilizador a possibilidade de seleção do destino Entreposto ou Fornecedor e um input de pesquisa (aplicável apenas quando selecionado Fornecedor como destino).

Em relação aos Entrepostos apenas são retornados para a aplicação os entrepostos que estão associados à loja logada. Esta associação é parametrizável ao cliente.

Da mesma forma, as tarefas de Devolução podem ser geradas via integração. Esta integração permite ao cliente realizar gestão de stocks na medida em que é gerada uma tarefa para a loja de origem transferir determinados artigos com destino Fornecedor ou Entreposto já definido.

![image alt <>](assets/itemreturn_create_task_win.gif){:height="300px" width="300px"}

#### Processamento de artigos

##### Criação de caixas/volumes

As tarefas de Devolução, mediante configuração ao cliente, permitem criar caixas volumes durante o tratamento da tarefa. Para isso a aplicação disponibiliza um botão na parte superior do ecrã.

![image alt <>](assets/transfer_manage_containers_win.gif){:height="300px" width="300px"}


#### Aprovação de tarefa

Mediante configuração ao cliente, a funcionalidade de Devolução permite ao utilizador no fecho da tarefa ter visibilidade de uma informação anteriormente definida ou inputs disponíveis para adicionar informação necessária à tarefa. 

##### Alertas

Mediante configuração ao cliente é possível definir que no fecho de uma tarefa de Devolução é apresentada uma mensagem informativa ao utilizador. Esta mensagem é definida anteriormente e também ela configurável ao cliente. 

Além da mensagem ser parametrizável por cliente é possível também definir uma mensagem para as tarefas convergentes (todos os artigos tratados) e uma diferente mensagem para tarefas divergentes (com artigos pendentes).

![image alt <>](assets/receiving_approve_action_win.png){:height="300px" width="300px"}

##### Inputs

As tarefas de Devolução, mediante configuração ao cliente, permitem ao utilizador no momento da aprovação adicionar informação complementar. Os inputs configuráveis na aprovação de tarefa são os seguinte:

* **Identificação Transportador:** Permite identificar o transportador associado à tarefa.
* **Nome Transportador:** Permite identificar o nome do transportador.
* **Prioridade:** Permite identificar a prioridade associada à transferência. Os valores possíveis são "Normal" e "Urgente".
* **Data de entrega:** Permite definir uma data expectável de entrega. 

![image alt <>](assets/transfer_approve_action_input_win.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Devolução contém a seguinte informação em relação às caixas/volumes:

* **PREVISTOS:** Total de caixas/volumes prevista na tarefa. (aplicável apenas a tarefas orientadas)
* **PROCESSADOS:** Total de caixas/volumes processadas na tarefa.
* **PENDENTES:** Total de caixas/volumes não processados na tarefa. (aplicável apenas a tarefas orientadas)

O ecrã de resumo da tarefa de Devolução contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos na tarefa. (aplicável apenas a tarefas orientadas)
* **PROCESSADOS:** Total de artigos processados na tarefa. Nos processados existem as seguintes métricas:
    * **NA TOTALIDADE:** Total de artigos processados em que a quantidade devolvida é igual à quantidade expectável.
    * **PARCIAL:** Total de artigos processados em que a quantidade devolvida é inferior à quantidade expectável.
    * **EM EXCESSO:** Total de artigos processados em que a quantidade devolvida é superior à quantidade expectável.
    * **NÃO ENCONTRADOS:** Total de artigos processados como não encontrados.
* **PENDENTES:** Total de artigos não processados na tarefa. (aplicável apenas a tarefas orientadas)

Em relação às unidades, o ecrã de resumo da tarefa de Devolução contém a seguinte informação:

* **PREVISTOS:** Total de unidades previstas na tarefa.
* **PROCESSADOS:** Total de unidades processadas na tarefa. Nos processados existem as seguintes métricas:
  * **EM EXCESSO:** Total de unidades processadas em que a quantidade devolvida é superior à quantidade expectável.
  * **NÃO ENCONTRADOS:** Total de unidades processadas como não encontradas.

![image alt <>](assets/trasnfer_resume_win.png){:height="300px" width="300px"}

## TAREFAS - AUDITORIA E CONTROLO DE VALIDADES

### **AUDITORIA DE VALIDADES**

A funcionalidade de Auditoria de Validades permite registar datas de validades dos artigos presentes em loja ou armazém. Este registo permite que o TMR armazene informação das datas de validades e respetivas quantidades. Esta informação servirá para o TMR gerar as respetivas tarefas de Controlo de Validades. 

#### Processamento da tarefa

As tarefas de Auditoria de Validade são maioritariamente adhoc sem lista de artigos prevista. O utilizador é livre de processar os artigos e respetivas datas autonomamente. No entanto, mediante configuração ao cliente, as tarefas podem ser também criadas adhoc na aplicação móvel com [base em lista](#com-lista) de artigos ou [estrutura hierárquica](#por-estrutura-hierárquica). 

Nas tarefas adhoc sem lista de artigos definida, o utilizador deve picar o artigo ou introduzir manualmente o EAN e a aplicação disponibiliza uma modal com a seguinte estrutura:

* **DIAS DEPREC.:** Informação da quantidade de dias parametrizada para geração das tarefas de  depreciação (Controlo de Validades).
* **DIAS RETIR.:** Informação da quantidade de dias parametrizada para geração das tarefas de retirada (Controlo de Validades).
* **TABELA DATAS:** Informação das datas já resgistadas na tarefa e as datas futuras para o artigo em tratamento.
* **INPUT DATAS:** Input para inserção da data de validade do artigo. Apenas é permitida a inserção de data igual ou superior ao dia atual.
* **INPUT QUANTIDADE:** Input para inserção de quantidade de artigos associados à respetiva data de validade.

![image alt <>](assets/expirationpicking_task_win.gif){:height="300px" width="300px"}


#### Listagem de datas

Depois de registadas as datas para determinado artigo, a aplicação móvel apresenta a listagem das mesmas e as respetivas quantidades. Nesta listagem o utilizador tem também a possibilidade de "Eliminar datas", "Editar datas", "Registar Nova Validade" e "Continuar" para a modal de listagem de artigos.

Se a data registada estiver dentro do período de depreciação ou retirada, a aplicação questiona o utilizador se deseja avançar com o registo mediante a seguinte mensagem:

"O artigo está em período de depreciação/retirada. Deseja continuar com o registo?". 

Se o utilizador selecionar "SIM" a aplicação avança com o registo. Se selecionar "Cancelar" a aplicação retrocede para a listagem de datas sem inserir a nova data registada.


![image alt <>](assets/expirationpicking_delete_date_win.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Auditoria de Validades contém a seguinte informação em relação aos artigos:

* **ARTIGOS:** Informação da quantidade de artigos processados na tarefa.

O ecrã de resumo da tarefa de Auditoria de Validades contém a seguinte informação em relação às datas de validade:

* **DATAS DE VALIDADE:** Informação do total de datas de validade registadas na tarefa.

O ecrã de resumo da tarefa de Auditoria de Validades contém a seguinte informação em relação às unidades:

* **UNIDADES:** Informação de unidades processadas na tarefa.

![image alt <>](assets/expirationpicking_resume_win.png){:height="300px" width="300px"}

### **CONTROLO DE VALIDADES**

A funcionalidade de Controlo de Validades permite retirar e depreciar artigos na mesma tarefa. Desta forma, o tratamento de datas de validade torna-se um processo mais rápido e eficiente uma vez que o utilizador processa os artigos e as suas datas de validades num único momento.

As tarefas de Controlo de Validades são exclusivamente orientadas e com listagem de artigos visível.

#### Depreciação

A Depreciação (Markdown) permite aplicar ações promocionais para escoamento de artigos que estejam com aproximação da data de validade.

Após selecionar o artigo da lista, a aplicação apresenta uma lista de datas a tratar. Para iniciar o tratamento de uma data em período de Depreciação, o utilizador deve selecionar a data da listagem com a referência "Depreciar".

![image alt <>](assets/expirationcontrol_markdown_win.gif){:height="300px" width="300px"}

Durante o processo de Depreciação, a aplicação valida a quantidade de SOH disponível (se existir serviço de stock) e compara com a quantidade para depreciação inserida pelo utilizador. Se a quantidade inserida for maior que SOH, a aplicação retorna a seguinte mensagem para o utlizador:

![image alt <>](assets/expirationcontrol_markdown_over_stock_win.png){:height="300px" width="300px"}

#### Modal Depreciação

A modal de Depreciação apresenta as seguintes informações e inputs:

* **INFORMAÇÃO DO ARTIGO:** Descrição do artigo, EAN e SKU. 
* **DATA DE VALIDADE:** Informação da data de validade em tratamento.
* **SOH:** Informação do stock disponível para o artigo (se aplicável).
* **DEPRECIADOS:** Informação da quantidade de artigos depreciados associados à data de validade em tratamento.
* **PVP ATUAL:** Informação do preço atual do artigo (mediante consulta online de serviço de preço).
* **INPUT DESCONTO/VALOR FIXO:** Input que permite ao utilizador definir o preço de depreciação do artigo. Permite inserir o desconto por valor final ou por percentagem. O preço final é cálculado pela aplicação e apresentado na parte direita do input. Neste momento é validado também se o preço final está dentro da percentagem máxima de desconto. Se isso não se verificar, a aplicação apresenta uma mensagem de erro e não permite avançar com o processo de depreciação.
* **ETIQUETA:** Input que permite definir o tipo de etiqueta de depreciação a ser impressa.
* **QUANTIDADE:** Input que permite definir a quantidade de etiquetas de depreciação a serem impressas. 
* **BOTÃO DATA NÃO ENCONTRADA/DEPRECIAR:** Se a quantidade de etiquetas for igual a zero, surge o botão de "Data Não Encontrada" para o utilizador poder definir que a data para tratamento não foi encontrada. Desta forma será uma data processada com quantidade zero. Se a quantidade de etiquetas for maior que zero, surge o botão "Depreciar". Ao clicar em "Depreciar" será gerada a ação de imprimir a quantidade de etiquetas de depreciação definidas pelo utilizador.

![image alt <>](assets/expirationcontrol_markdown_modal_win_2.png){:height="300px" width="300px"}

#### Retirada

A Retirada permite recolher da placa de vendas os artigos que estejam com aproximação da data de validade. Desta forma é possível ao utlizador perceber os artigos e respetivas datas que se encontram em fim de validade e retirar os mesmos da loja. 

Os dias a retirar, tal como os dias a depreciar, são parâmetros definidos por artigo ou estrutura hierárquica. No processo de retirada é possível ao utilizador definir um motivo e um destino que são também parâmetros configuráveis por cliente.

![image alt <>](assets/expirationcontrol_withdraw_win.gif){:height="300px" width="300px"}

#### Modal de Retirada

A modal de Retirada apresenta as seguintes informações e inputs:

* **INFORMAÇÃO DO ARTIGO:** Fotografia (se aplicável), descrição, EAN, SKU e SOH (se aplicável e configurável). 
* **DATA DE VALIDADE:** Informação da data de validade em tratamento.
* **DEPRECIAÇÃO e RETIRADA:** Informação dos parâmetros de validade.
* **DEPRECIADOS, VENDAS e A RETIRAR:** Informação de artigos previamente depreciados, da quantidade de vendas para a data em tratamento e a quantidade a retirar da respetiva data de validade. Estas informações são consultadas online e se não retornar informação a aplicação apresenta N/A.
* **QUANTIDADE ACUMULADA:** Informação da quantidade de artigos previamente retirados na tarefa.
* **ADICIONAR e SUBSTITUIR:** Inputs que permitem definir a quantidade de artigos que serão retirados da data em tratamento.
* **MOTIVO:** Input que permite definir o motivo para retirada. No caso de o artigo ter sido depreciado anteriormente, o motivo "Validades" aparece preenchido por default. Os Motivos que surgem na listagem para seleção são configuráveis ao cliente.
* **DESTINO:** Input que permite definir o destino do artigo após ser retirado de loja. Os Destinos que surgem na listagem para seleção são configuráveis ao cliente.
* **BOTÃO DATA NÃO ENCONTRADA/RETIRAR:** Se a quantidade retirada for igual a zero, surge o botão de "Data Não Encontrada" para o utilizador poder definir que a data para tratamento não foi encontrada. Desta forma será uma data processada com quantidade zero. Se a quantidade de etiquetas for maior que zero, surge o botão "Retirar".

![image alt <>](assets/expirationcontrol_withdraw_modal_win.png){:height="300px" width="300px"}


#### Datas futuras

Na modal de artigo é possível consultar e gerir as datas futuras através da opção "Gestão de Datas Futuras". Nesta modal é possível consultar as datas futuras já registadas para determinado artigo e também eliminar as mesmas caso o utilizador perceba durante o tratamento da tarefa que as datas já não se encontram fisicamente em loja. 

Existem três formas de seleção de datas:

* A partir do input de data existente é possível ao utilizador selecionar de uma só vez todas as datas registadas até à data definida no input.
* Diretamente a partir da listagem de datas.
* Através do facilitador "Selecionar todas as datas". 

Depois de selecionadas as datas pretendidas, o utilizador deverá selecionar "Remover". De forma imediata as datas serão removidas do TMR.

![image alt <>](assets/expirationcontrol_show_future_dates_win.gif){:height="300px" width="300px"}


#### Adicionar data adhoc

Durante o tratamento de uma tarefa de Controlo de Validades é possível adicionar uma data adhoc caso o utilizador identifique uma data em loja que não está na listagem de datas futuras.

Na modal de datas para tratamento e na modal de listagem de datas futuras está presente o input de "Adicionar data adhoc". Ao selecionar esta opção, o utilizador é encaminhado para uma nova modal que permite definir uma nova data através do input disponível e a respetiva quantidade. 

![image alt <>](assets/expirationcontrol_adhoc_date_win.gif){:height="300px" width="300px"}

No momento em que o utilizador insere uma nova data a aplicação valida se a mesma já existe. Se sim retorna a seguinte mensagem ao utilizador: 

"Não é possível adicionar. Data de validade já existente".

![image alt <>](assets/expirationcontrol_date_already_existes_win.png){:height="300px" width="300px"}

Da mesma forma, a aplicação valida se a data inserida já se encontra em período de depreciação ou retira. Se sim abre de imediato a modal respetiva para tratamento do artigo. 

<!--![image alt <>](assets/expirationcontrol_adhoc_date_withdral.gif){:height="300px" width="300px"}-->

#### Resumo

O ecrã de resumo da tarefa de Controlo de Validades contém a seguinte informação em relação aos artigos:

* **PREVISTOS:** Total de artigos previstos na tarefa.
* **PROCESSADOS:** Total de artigos processados na tarefa.

Em relação às datas de validade, o ecrã de resumo da tarefa de Controlo de Validades contém a seguinte informação:

* **PREVISTOS:** Total de datas previstas na tarefa.
* **PROCESSADOS:** Total de datas processadas na tarefa. Nos processados existem as seguintes métricas:
  * **DEPRECIADOS:** Total de datas processadas como depreciação.
  * **NÃO ENCONTRADOS:** Total de datas processadas como não encontrados.
  * **RETIRADOS:** Total de datas processadas como retirada.

![image alt <>](assets/expirationcontrol_resume_win.png){:height="300px" width="300px"}

## TAREFAS - CHECKLIST | LISTA DE ARTIGOS

### **CHECKLIST**

A funcionalidade de Checklist permite realizar tarefas diárias em loja mediante informações criadas previamente. As checklist de cada loja são realizadas previamente no Backoffice TMR e posteriormente ficam disponíveis na aplicação móvel para serem utilizados na criação e realização da Checklist.

Além da criação adhoc na aplicação móvel é possível calendarizar via Scheduler as tarefas de Checklist.

As tarefas de Checklist têm por base perguntas e respostas. Neste sentido, as checklist criadas no Backoffice são perguntas que incluem dois tipos de resposta possível e que são definidas na altura da criação da checklist.

Quando a aplicação móvel realiza o download da tarefa apresenta ao utilizador a resposta escolhida na altura da sua criação.

![image alt <>](assets/checklist_create_task_win.gif){:height="300px" width="300px"}

#### Processamento da tarefa

Depois de realizado o download da tarefa, a aplicação móvel apresenta a checklist associada à tarefa e as respostas associadas às respetivas perguntas. 

As respostas possíveis são as seguintes:

* **Verificado/Não Verificado**
* **Sim/Não**

As respostas são associadas às perguntas na hora de criação da checklist.

![image alt <>](assets/checklist_processing_win.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Checklist contém a seguinte informação:

* **PREVISTO:** Total de items previstos na tarefa.
* **PROCESSADOS:** Total de item processados na tarefa. 

![image alt <>](assets/checklist_resume_win.png){:height="300px" width="300px"}

### **LISTA DE ARTIGOS**

A funcionalidade Lista de Artigos permite ao utilizador criar na aplicação móvel uma listagem mediante os artigos que vai processando na tarefa. Os artigos processados vão sendo adicionados à Lista de Artigos e no fecho da tarefa a listagem será criada e disponibilizada no Backoffice TMR para ser utilizada em outros tipos de tarefa. (tarefas adhoc ou calendarizadas com base em lista)

#### Processamento de tarefa

As tarefas são criadas adhoc sem lista definida. Durante o tratamento da tarefa os artigos depois de picados são associados de forma imediata à tarefa como processados. A modal de artigo apenas contém informação do artigo pois o processamento desta tarefa não implica informação de quantidade ou inputs adicionais. 

![image alt <>](assets/itemlist_processing_win.gif){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Lista de Artigos contém a seguinte informação:


* **PROCESSADOS:** Total de item processados na tarefa. 

![image alt <>](assets/itemlist_resume_win.png){:height="300px" width="300px"}

### **REGISTO DE QUEBRAS**

A funcionalidade de Registo de Quebras permite ao utilizador processar determinados artigos como quebra. As tarefas de Registo de Quebras poderão ser adhoc sem lista orientada onde o utilizador é livre de processar os artigos que estiverem nestas condições. 

As tarefas poderão ser também orientadas com listagem de artigos visíveis onde o utilizador é orientado a identificar e processar como quebra determinados artigos previstos na tarefa. As tarefas orientadas podem ser criadas via TMR Scheduler ou integração.

#### Modal de artigo

Durante o tratamento da tarefa, o utilizador vai picando o EAN dos artigos ou inserindo manualmente o mesmo e a aplicação de forma imediata abre a modal do artigo com seguinte informação:  

* **INFORMAÇÃO DO ARTIGO:** Fotografia (se aplicável), descrição, EAN, SKU e SOH (se aplicável e configurável). 
* **UNIDADES:** Input que permite definir a quantidade de artigos a adicionar ou subtituir e que serão processados como quebra.
* **MOTIVO:** Input que permite definir o motivo para quebra.  Os Motivos que surgem na listagem para seleção são configuráveis ao cliente.
* **DESTINO:** Input que permite definir o destino do artigo após ser retirado de loja. Os Destinos que surgem na listagem para seleção são configuráveis ao cliente.
* **BOTÃO NÃO ENCONTREI/CONTINUAR:** Se a quantidade para quebra for igual a zero, surge o botão de "Não Encontrei" para o utilizador poder definir que a data para tratamento não foi encontrada. Desta forma será uma data processada com quantidade zero. Se a quantidade para quebra for maior que zero, surge o botão "Continuar".

![image alt <>](assets/damage_modal_win.png){:height="300px" width="300px"}

#### Resumo

O ecrã de resumo da tarefa de Registo de Quebra contém a seguinte informação em relação aos artigos:

* **PROCESSADOS:** Total de artigos processados na tarefa.
    * **NÃO PREVISTOS:** Total de artigos não previstos na tarefa.


![image alt <>](assets/damage_resume_win.png){:height="300px" width="300px"}
