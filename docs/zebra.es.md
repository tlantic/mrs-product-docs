Aplicable a las siguientes impresoras

Este procedimiento se puede aplicar a impresoras * QLn220 * QLn320

**Software requerido**

Para configurar su impresora en una red inalámbrica es necesario descargar un software de configuración **Zebra Setup Utilities** en [www.zebra.com/setup](https://www.zebra.com/br/pt/support-downloads/printer-software/printer-setup-utilities.html)


**Requisitos**

Impresora

Portátil o computadora

Cable USB

Software de utilidades de configuración de Zebra instalado

**1.** Conecte la impresora a la computadora portátil o computadora en el puerto USB, usando un cable USB

![img](/assets/install_printer/zebra/Picture1.png)

**2.** Conecte el equipo a la corriente y presione el botón de Encendido / Apagado ubicado en el frente.

**3.** Asegúrese de que el producto se haya instalado correctamente en **el panel de control** >> **Impresoras**

**4.** Haga doble clic en el icono de la Utilidad de configuración de Zebra.

![img](/assets/install_printer/zebra/Picture2.png)

**5.** Cuando abra Zebra Setup Utilities, se le presentará el equipo correctamente instalado.

**Haga clic** para seleccionarlo y luego haga clic en **Configurar la conectividad de la impresora** ![img](/assets/install_printer/zebra/Picture4.png)

**6.** Seleccione **"Inalámbrico" y luego haga clic en** el **botón siguiente**

![img](/assets/install_printer/zebra/Picture5.png)

**7.** Seleccione **"DHCP"** si no conoce la asignación de IP **y haga clic** en el **botón siguiente**

![img](/assets/install_printer/zebra/Picture6.png)

**8.** Seleccione la **configuración de seguridad** y **radio y** haga clic en **Siguiente**

![img](/assets/install_printer/zebra/Picture7.png)

**9.** Seleccione el país donde está funcionando su equipo

![img](/assets/install_printer/zebra/Picture8.png)

**10.** En **ESSID** ingrese el nombre de su red inalámbrica y en **Modo de seguridad** seleccione la seguridad de red habilitada en su enrutador inalámbrico y haga clic en

**OBS:** En la mayoría de los casos se utiliza

**WPA-PSK / WPA2-PSK**

![img](/assets/install_printer/zebra/Picture9.png)

**11.** Introduzca la contraseña de red en **Contraseña de seguridad** y haga clic en **Siguiente.**

**12.** Aparecerá una pantalla con un resumen de la configuración. Haga clic en **Siguiente**

![img](/assets/install_printer/zebra/Picture10.png)

**13.** En la siguiente pantalla, seleccione **Impresora** y **haga clic en Finalizar.**

Su máquina se reiniciará y mostrará la IP asignada a la impresora en la pantalla.

![img](/assets/install_printer/zebra/Picture11.png)