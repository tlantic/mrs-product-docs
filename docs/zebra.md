

!!! info "Aplicável as seguintes impressoras"

    Este procedimento pode ser aplicado as impressoras 
    * QLn220 
    * QLn320

**Software Necessário**

 

Para configurar a sua impressora em uma rede sem Fio é necessário que realize o download do software de configuração **Zebra Setup Utilities** em [www.zebra.com/setup](https://www.zebra.com/br/pt/support-downloads/printer-software/printer-setup-utilities.html)

 
<br>
**Requisitos Necessários**

 

Impressora

Notebook ou Computador

Cabo USB

Software Zebra Setup Utilities Instalado

 

 

**1.**  Conecte a impressora ao Notebook ou Computador na Porta USB, utilizando um Cabo USB

![img](\assets\install_printer\zebra\Picture1.png)

                                        
**2.**   Ligue o equipamento a energia e pressione o botão Power On/OFF situado a frente. 

 
**3.**   Certifique-se de que o produto foi devidamente instalado no **painel de controle** >> **Impressoras**


**4.**   Dê um duplo clique no ícone Zebra Setup Utility    

 

![img](\assets\install_printer\zebra\Picture2.png)

 


 

**5.**   Ao abrir o Zebra Setup Utilities será apresentado o seu equipamento devidamente instalado.


**Clique** para selecioná-lo e em seguida clique em **Configure Printer Connectivity**
![img](\assets\install_printer\zebra\Picture4.png)
 
 



**6.**   Selecione **“Wireless” e clique** no **botão next**

 ![img](\assets\install_printer\zebra\Picture5.png)

 


**7.**   Selecione **“DHCP”** caso não saiba as atribuições de IP **e clique** no **botão next** 

  ![img](\assets\install_printer\zebra\Picture6.png)

 

**8.**   Selecione as **configurações de Segurança** e **Rádio e** clique **em Next**

 ![img](\assets\install_printer\zebra\Picture7.png)





**9.**   Selecione o País onde seu equipamento está operando

 ![img](\assets\install_printer\zebra\Picture8.png)

 

**10.** Em **ESSID** insira o nome da sua rede sem fio e em **Security Mode** selecione a segurança de rede habilitada em seu roteador sem fio e clique em 

 
**OBS:** Na Maioria dos casos, utiliza-se 

**WPA-PSK / WPA2-PSK**

![img](\assets\install_printer\zebra\Picture9.png)

**11.** Insira a senha da rede em **Security Password** e clique em **Next**

**12.** Surgirá uma tela com o resumo de configurações, Clique em **Next**

 ![img](\assets\install_printer\zebra\Picture10.png)


**13.** Na próxima tela, selecione **Printer** e **Clique em Finish.**

Seu equipamento reiniciará e exibirá na tela o IP atribuído a impressora. 

 ![img](\assets\install_printer\zebra\Picture11.png)

 

 

 

 

 

 

 

 